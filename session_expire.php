<?php
date_default_timezone_set("Asia/Hong_Kong");
$time = date("h:i a");
$date = date('F d, Y', strtotime(date("Y-m-d")));
$datetime = $date." ".$time;
$expireAfter = 30;
include("../../dbcon.php");
if(isset($_SESSION['last_action'])){
    $secondsInactive = time() - $_SESSION['last_action'];
    $expireAfterSeconds = $expireAfter * 60 * 12;
    if($secondsInactive >= $expireAfterSeconds){
        try{
            $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $auditdata = [
            ':activity' => "Logged Out",
            ':username' => $_SESSION['login_user'],
            ':datetime' => $datetime
            ];
            $audit = "INSERT INTO audit_trail (activity, username, date) VALUES (:activity, :username, :datetime)";
            $sthaudit = $dbh->prepare($audit);
            $sthaudit->execute($auditdata);
            session_unset();
            if(session_destroy()) {
                header("Location: login.php");
            }
            $dbh = null;
        }
        catch(PDOException $e){
            error_log('PDOException - ' . $e->getMessage(), 0);
            http_response_code(500);
            die('Error establishing connection with database');
        }
    }
}
$_SESSION['last_action'] = time();
?>