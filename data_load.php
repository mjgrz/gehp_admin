<div class="card-header border-1">
  <h3 class="card-title"><b>Number of Application per Region</b></h3>
  <div class="card-tools">
    
  </div>
</div>
<div class="card-body" id="card-body" style="overflow:auto; height:430px;">

<?php
$countappsperreg = "SELECT COUNT(*) as TotalProjects, region.Region 
FROM applications 
LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE 
LEFT JOIN region ON project_info.region = region.psgc_reg 
GROUP BY region.Region";

include("dbcon.php");
$resultappsperreg = mysqli_query($conn,$countappsperreg);
while($rowappsperreg = mysqli_fetch_assoc($resultappsperreg)){
  
?>
  <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
    <p class="text-info text-xl">
      <b><?php echo $rowappsperreg["TotalProjects"]; ?></b>
    </p>
    <p class="d-flex flex-column text-right">
      <span class="font-weight-bold">
      </span>
      <span class="text-muted"><?php echo $rowappsperreg["Region"]; ?></span>
    </p>
  </div>
<?php
}
?>
</div>