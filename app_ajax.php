<?php
include('../../dbcon.php');
$category = $_POST["getOption"];
if($category == 'project'){
  try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $datefromcurr = date('Y-m-d H:i:s', strtotime('-1 months'));
    $datetocurr = date('Y-m-d H:i:s');
    $datesearch=[
      ':datefromcurr' => $datefromcurr,
      ':datetocurr' => $datetocurr
    ];
    $countappsperproj ="SELECT SUM(TotalProjects) as TotalProjects, SUM(Current) as Current, PJ_NAME
                        FROM (SELECT applications.PJ_CODE, project_info.PJ_NAME, COUNT(*) as TotalProjects, 
                        SUM(CASE WHEN applications.app_date BETWEEN :datefromcurr AND :datetocurr THEN 1 ELSE 0 END) as Current
                        FROM applications
                        LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
                        WHERE project_info.archived != '1'
                        GROUP BY applications.PJ_CODE
                        UNION ALL
                        SELECT ofw_applications.PJ_CODE, project_info.PJ_NAME, COUNT(*) as TotalProjects, 
                        SUM(CASE WHEN ofw_applications.app_date BETWEEN :datefromcurr AND :datetocurr THEN 1 ELSE 0 END) as Current
                        FROM ofw_applications
                        LEFT JOIN project_info ON ofw_applications.PJ_CODE = project_info.PJ_CODE
                        WHERE project_info.archived != '1'
                        GROUP BY ofw_applications.PJ_CODE) Bas
                        GROUP BY PJ_NAME";
    $sthappsperproj = $dbh->prepare($countappsperproj);
    $sthappsperproj->execute($datesearch);
    $sthappsperproj->setFetchMode(PDO::FETCH_ASSOC);
    $cc = 0;
    while ($rowappsperproj = $sthappsperproj->fetch(PDO::FETCH_ASSOC)) { 
    $cc++;
    ?>
      <div class="d-flex justify-content-between align-items-center border-bottom mb-3 pt-3" id="showList">
        <p><?php echo $rowappsperproj["PJ_NAME"]; ?></p>
        <p class="d-flex flex-column text-right">
          <span class="font-weight-bold h4"><?php echo $rowappsperproj["TotalProjects"]; ?></span>
        </p>
      </div>
    <?php
    }
    $dbh = null;
  }
  catch(PDOException $e){
      error_log('PDOException - ' . $e->getMessage(), 0);
      http_response_code(500);
      die('Error establishing connection with database');
  }
}
if($category == 'region'){
  try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $countappsperreg = "SELECT SUM(TotalProjects) as TotalProjects1, Region FROM (SELECT COUNT(*) as TotalProjects, region.Region 
                        FROM applications 
                        LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE 
                        LEFT JOIN region ON project_info.region = region.psgc_reg 
                        GROUP BY region.Region
                        UNION ALL
                        SELECT COUNT(*) as TotalProjects, region.Region 
                        FROM ofw_applications 
                        LEFT JOIN project_info ON ofw_applications.PJ_CODE = project_info.PJ_CODE 
                        LEFT JOIN region ON project_info.region = region.psgc_reg 
                        GROUP BY region.Region) A
                        GROUP BY Region";
    $sthappsperreg = $dbh->prepare($countappsperreg);
    $sthappsperreg->execute();
    $sthappsperreg->setFetchMode(PDO::FETCH_ASSOC);
    while ($rowappsperreg = $sthappsperreg->fetch(PDO::FETCH_ASSOC)) { 
  ?>
    <div class="d-flex justify-content-between align-items-center border-bottom mb-3 pt-3" id="showList">
      <p><?php echo $rowappsperreg["Region"]; ?></p>
      <p class="d-flex flex-column text-right">
        <span class="font-weight-bold h4"><?php echo $rowappsperreg["TotalProjects1"]; ?></span>
      </p>
    </div>
  <?php
    }
    $dbh = null;
  }
  catch(PDOException $e){
      error_log('PDOException - ' . $e->getMessage(), 0);
      http_response_code(500);
      die('Error establishing connection with database');
  }

}
?>
<style>
  #showList {
    -webkit-animation-duration: 0.7s;
    animation-duration: 0.7s;
  }
  
  @-webkit-keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  
  @keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  
  #showList {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
  }
</style>