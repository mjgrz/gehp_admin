<!DOCTYPE html>
<html lang="en">
<head>
<?php
include('login_session.php');
?>

<link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
<link rel="stylesheet" href="plugins/toastr/toastr.min.css">
<link rel="stylesheet" href="dist/css/adminlte.min.css">

<script src="plugins/jquery/jquery.min.js"></script>
<script src="plugins/toastr/toastr.min.js"></script>

<?php
if (isset($_SESSION["entry"]) && $_SESSION["entry"] !='') {
  $msg = $_SESSION['entry'];
  echo "<span><script type='text/javascript'>toastr.success('$msg')</script></span>";
  unset($_SESSION['entry']);
}
else {

}
?>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>NHA | Government Employee's Housing Program</title>
  <link rel="shortcut icon" type="image/png" href="dist/img/nha_logo_circle.png">

  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins&display=swap">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="font-weight: bold;">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item">
        <a href="index.php" class="nav-link">Home</a>
      </li>
    </ul>

    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a href="logout_session.php" class="nav-link">Logout</a>
      </li>
    </ul>
    
  </nav>

  <aside class="main-sidebar sidebar-dark-primary elevation-4" style="background: #002d32; position: fixed;">
      <a href="index.php" class="brand-link">
          <img src="dist/img/nha_logo_circle.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
          <span class="brand-text font-weight-light"><b>NHA | GEHP</b></span>
      </a>
    
    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <?php
          $target_dir = "pages/forms/uploads/users/";
          $target_file = $target_dir . basename("$login_image");
          if(file_exists($target_file)){
            echo "<img src='pages/forms/uploads/users/$login_image' class='img-circle elevation-3' alt='User Image' style='height: 33px;'>";
          } 
          else {
            echo "<img src='pages/forms/uploads/users/user.png' width='150px'>";
          }
          ?>
        </div>
        <div class="info">
          <a href="pages/forms/users_profile.php" class="d-block"><?php echo $_SESSION['login_user'] ?></a>
        </div>
      </div>

      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
          <li class="nav-item">
            <a href="index.php" class="nav-link active" style="background: #67a2b2;">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-briefcase"></i>
              <p>
                Applicants
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <input type="text" class="form-control" id="sessionempty" name="sessionempty" value="" placeholder="Search" hidden>
              <li class="nav-item">
                <a href="pages/forms/registration.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Government Employees</p>
                </a>
              </li>
              <li class="nav-item" <?php echo $deploy;?>>
                <a href="pages/forms/registration_Uni.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Uniformed Personnel</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/forms/registration_OFW.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>OFW</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item" <?php echo $project_hide ?>>
            <a href="pages/forms/project_view.php" onclick="sessionempty()" class="nav-link">
              <i class="nav-icon fa fa-building"></i>
              <p>
              Projects
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $user_hide ?>>
            <a href="pages/forms/users.php" onclick="sessionempty()" class="nav-link">
              <i class="nav-icon fa fa-users"></i>
              <p>
              Users
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $element_hide ?>>
            <a href="pages/forms/report_gen.php" class="nav-link">
              <i class="nav-icon fas fa-scroll"></i>
              <p>
                Reports
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $pages_hide ?>>
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-clone"></i>
              <p>
                Pages
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              
              <li class="nav-item">
                <a href="pages/forms/home.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Home</p>
                </a>
              </li>
              <li class="nav-item" hidden>
                <a href="pages/forms/howToApply.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>How To Apply</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/forms/about.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>About</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="pages/forms/banner.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Banner</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="pages/forms/bulletin.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Bulletin</p>
                </a>
              </li>
            </ul>
          </li>
              
          <li class="nav-item" <?php echo $inv_hide;?>>
            <a href="pages/forms/inventory.php" class="nav-link">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                Inventory
              </p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>

  <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div>
        </div>
      </div>
    </div>

    <?php
      //GOVERNMENT COUNTING
      include("../../dbcon.php");
      try{
        $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        if($login_office == 'Regional'){
          $pendingselect = "SELECT app_status FROM applications
          LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
          LEFT JOIN employment_status ON applications.employment_id = employment_status.employment_id 
          WHERE app_status = 'Pending' AND project_info.region = :reglogin_officeID AND project_info.archived != '1' AND employment_status.uni_rank = ''";
          
          //PENDING COUNTER
          $sthpend = $dbh->prepare($pendingselect);
          $sthpend->bindParam(':reglogin_officeID', $reglogin_officeID);
          $sthpend->execute();
          $pendingcount=$sthpend->rowCount();

          $qualifiedselect = "SELECT app_status FROM applications
          LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
          LEFT JOIN employment_status ON applications.employment_id = employment_status.employment_id 
          WHERE app_status = 'Qualified' AND project_info.region = :reglogin_officeID AND project_info.archived != '1' AND employment_status.uni_rank = ''";
          
          //QUALIFIED COUNTER
          $sthquad = $dbh->prepare($qualifiedselect);
          $sthquad->bindParam(':reglogin_officeID', $reglogin_officeID);
          $sthquad->execute();
          $qualifiedcount=$sthquad->rowCount();

          $disqualifiedselect = "SELECT app_status FROM applications
          LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
          LEFT JOIN employment_status ON applications.employment_id = employment_status.employment_id 
          WHERE app_status = 'Disqualified' AND project_info.region = :reglogin_officeID AND project_info.archived != '1' AND employment_status.uni_rank = ''";
          
          //DISQUALIFIED COUNTER
          $sthdisq = $dbh->prepare($disqualifiedselect);
          $sthdisq->bindParam(':reglogin_officeID', $reglogin_officeID);
          $sthdisq->execute();
          $disqualifiedcount=$sthdisq->rowCount();

          $awardedselect = "SELECT app_status FROM applications
          LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
          LEFT JOIN employment_status ON applications.employment_id = employment_status.employment_id 
          WHERE app_status = 'New Applicant' AND project_info.region = :reglogin_officeID AND project_info.archived != '1' AND employment_status.uni_rank = ''";
          
          //AWARDED COUNTER
          $sthaward = $dbh->prepare($awardedselect);
          $sthaward->bindParam(':reglogin_officeID', $reglogin_officeID);
          $sthaward->execute();
          $awardedcount=$sthaward->rowCount();
          
          $totalGovCount = $awardedcount + $disqualifiedcount + $qualifiedcount + $pendingcount;
          $dbh = null;
        }

        if($login_office != 'Regional') {
          $pendingselect = "SELECT app_status FROM applications
          LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
          LEFT JOIN employment_status ON applications.employment_id = employment_status.employment_id 
          WHERE app_status = 'Pending' AND project_info.archived != '1' AND employment_status.uni_rank = ''";
          //PENDING COUNTER
          $sthpend = $dbh->prepare($pendingselect);
          $sthpend->execute();
          $pendingcount=$sthpend->rowCount();

          $qualifiedselect = "SELECT app_status FROM applications
          LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
          LEFT JOIN employment_status ON applications.employment_id = employment_status.employment_id 
          WHERE app_status = 'Qualified' AND project_info.archived != '1' AND employment_status.uni_rank = ''";
          //QUALIFIED COUNTER
          $sthquad = $dbh->prepare($qualifiedselect);
          $sthquad->execute();
          $qualifiedcount=$sthquad->rowCount();

          $disqualifiedselect = "SELECT app_status FROM applications
          LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
          LEFT JOIN employment_status ON applications.employment_id = employment_status.employment_id 
          WHERE app_status = 'Disqualified' AND project_info.archived != '1' AND employment_status.uni_rank = ''";
          //DISQUALIFIED COUNTER
          $sthdisq = $dbh->prepare($disqualifiedselect);
          $sthdisq->execute();
          $disqualifiedcount=$sthdisq->rowCount();

          $awardedselect = "SELECT app_status FROM applications
          LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
          LEFT JOIN employment_status ON applications.employment_id = employment_status.employment_id 
          WHERE app_status = 'New Applicant' AND project_info.archived != '1' AND employment_status.uni_rank = ''";
          //AWARDED COUNTER
          $sthaward = $dbh->prepare($awardedselect);
          $sthaward->execute();
          $awardedcount=$sthaward->rowCount();
          
          $totalGovCount = $awardedcount + $disqualifiedcount + $qualifiedcount + $pendingcount;
          $dbh = null;
        }
      }
      catch(PDOException $e){
          error_log('PDOException - ' . $e->getMessage(), 0);
          http_response_code(500);
          die('Error establishing connection with database');
      }

      try{
        $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        if($login_office == 'Regional'){
          $pendingselect = "SELECT app_status FROM applications
          LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
          LEFT JOIN employment_status ON applications.employment_id = employment_status.employment_id 
          WHERE app_status = 'Pending' AND project_info.region = :reglogin_officeID AND project_info.archived != '1' AND employment_status.uni_rank != ''";
          
          //PENDING COUNTER
          $sthpend = $dbh->prepare($pendingselect);
          $sthpend->bindParam(':reglogin_officeID', $reglogin_officeID);
          $sthpend->execute();
          $unipendingcount=$sthpend->rowCount();

          $qualifiedselect = "SELECT app_status FROM applications
          LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
          LEFT JOIN employment_status ON applications.employment_id = employment_status.employment_id 
          WHERE app_status = 'Qualified' AND project_info.region = :reglogin_officeID AND project_info.archived != '1' AND employment_status.uni_rank != ''";
          
          //QUALIFIED COUNTER
          $sthquad = $dbh->prepare($qualifiedselect);
          $sthquad->bindParam(':reglogin_officeID', $reglogin_officeID);
          $sthquad->execute();
          $uniqualifiedcount=$sthquad->rowCount();

          $disqualifiedselect = "SELECT app_status FROM applications
          LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
          LEFT JOIN employment_status ON applications.employment_id = employment_status.employment_id 
          WHERE app_status = 'Disqualified' AND project_info.region = :reglogin_officeID AND project_info.archived != '1' AND employment_status.uni_rank != ''";
          
          //DISQUALIFIED COUNTER
          $sthdisq = $dbh->prepare($disqualifiedselect);
          $sthdisq->bindParam(':reglogin_officeID', $reglogin_officeID);
          $sthdisq->execute();
          $unidisqualifiedcount=$sthdisq->rowCount();

          $awardedselect = "SELECT app_status FROM applications
          LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
          LEFT JOIN employment_status ON applications.employment_id = employment_status.employment_id 
          WHERE app_status = 'New Applicant' AND project_info.region = :reglogin_officeID AND project_info.archived != '1' AND employment_status.uni_rank != ''";
          
          //AWARDED COUNTER
          $sthaward = $dbh->prepare($awardedselect);
          $sthaward->bindParam(':reglogin_officeID', $reglogin_officeID);
          $sthaward->execute();
          $uniawardedcount=$sthaward->rowCount();
          
          $totaluniCount = $uniawardedcount + $unidisqualifiedcount + $uniqualifiedcount + $unipendingcount;
          $dbh = null;
        }

        //Uniformed Personnel
        if($login_office != 'Regional') {
          $pendingselect = "SELECT app_status FROM applications
          LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
          LEFT JOIN employment_status ON applications.employment_id = employment_status.employment_id 
          WHERE app_status = 'Pending' AND project_info.archived != '1' AND employment_status.uni_rank != ''";
          //PENDING COUNTER
          $sthpend = $dbh->prepare($pendingselect);
          $sthpend->execute();
          $unipendingcount=$sthpend->rowCount();

          $qualifiedselect = "SELECT app_status FROM applications
          LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
          LEFT JOIN employment_status ON applications.employment_id = employment_status.employment_id 
          WHERE app_status = 'Qualified' AND project_info.archived != '1' AND employment_status.uni_rank != ''";
          //QUALIFIED COUNTER
          $sthquad = $dbh->prepare($qualifiedselect);
          $sthquad->execute();
          $uniqualifiedcount=$sthquad->rowCount();

          $disqualifiedselect = "SELECT app_status FROM applications
          LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
          LEFT JOIN employment_status ON applications.employment_id = employment_status.employment_id 
          WHERE app_status = 'Disqualified' AND project_info.archived != '1' AND employment_status.uni_rank != ''";
          //DISQUALIFIED COUNTER
          $sthdisq = $dbh->prepare($disqualifiedselect);
          $sthdisq->execute();
          $unidisqualifiedcount=$sthdisq->rowCount();

          $awardedselect = "SELECT app_status FROM applications
          LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
          LEFT JOIN employment_status ON applications.employment_id = employment_status.employment_id 
          WHERE app_status = 'New Applicant' AND project_info.archived != '1' AND employment_status.uni_rank != ''";
          //AWARDED COUNTER
          $sthaward = $dbh->prepare($awardedselect);
          $sthaward->execute();
          $uniawardedcount=$sthaward->rowCount();
          
          $totaluniCount = $uniawardedcount + $unidisqualifiedcount + $uniqualifiedcount + $unipendingcount;
          $dbh = null;
        }
      }
      catch(PDOException $e){
          error_log('PDOException - ' . $e->getMessage(), 0);
          http_response_code(500);
          die('Error establishing connection with database');
      }

      //OFW COUNTING
      try{
        $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        if($login_office == 'Regional'){
          $ofw_pendingselect = "SELECT app_status FROM ofw_applications
          LEFT JOIN project_info ON ofw_applications.PJ_CODE = project_info.PJ_CODE
          WHERE app_status = 'Pending' AND project_info.region = :reglogin_officeID AND project_info.archived != '1'";
          
          //PENDING COUNTER
          $sthofwpend = $dbh->prepare($ofw_pendingselect);
          $sthofwpend->bindParam(':reglogin_officeID', $reglogin_officeID);
          $sthofwpend->execute();
          $ofw_pendingcount=$sthofwpend->rowCount();
  
          $ofw_qualifiedselect = "SELECT app_status FROM ofw_applications
          LEFT JOIN project_info ON ofw_applications.PJ_CODE = project_info.PJ_CODE
          WHERE app_status = 'Qualified' AND project_info.region = :reglogin_officeID AND project_info.archived != '1'";
          
          //QUALIFIED COUNTER
          $sthofwquad = $dbh->prepare($ofw_qualifiedselect);
          $sthofwquad->bindParam(':reglogin_officeID', $reglogin_officeID);
          $sthofwquad->execute();
          $ofw_qualifiedcount=$sthofwquad->rowCount();
  
          $ofw_disqualifiedselect = "SELECT app_status FROM ofw_applications
          LEFT JOIN project_info ON ofw_applications.PJ_CODE = project_info.PJ_CODE
          WHERE app_status = 'Disqualified' AND project_info.region = :reglogin_officeID AND project_info.archived != '1'";
          
          //DISQUALIFIED COUNTER
          $sthofwdisq = $dbh->prepare($ofw_disqualifiedselect);
          $sthofwdisq->bindParam(':reglogin_officeID', $reglogin_officeID);
          $sthofwdisq->execute();
          $ofw_disqualifiedcount=$sthofwdisq->rowCount();
  
          $ofw_awardedselect = "SELECT app_status FROM ofw_applications
          LEFT JOIN project_info ON ofw_applications.PJ_CODE = project_info.PJ_CODE
          WHERE app_status = 'New Applicant' AND project_info.region = :reglogin_officeID AND project_info.archived != '1'";

          //AWARDED COUNTER
          $sthofwaward = $dbh->prepare($ofw_awardedselect);
          $sthofwaward->bindParam(':reglogin_officeID', $reglogin_officeID);
          $sthofwaward->execute();
          $ofw_awardedcount=$sthofwaward->rowCount();
          
          $totalOFWCount = $ofw_awardedcount + $ofw_disqualifiedcount + $ofw_qualifiedcount + $ofw_pendingcount;
          $dbh = null;
        }
  
        if($login_office != 'Regional') {
          $ofw_pendingselect = "SELECT app_status FROM ofw_applications
          LEFT JOIN project_info ON ofw_applications.PJ_CODE = project_info.PJ_CODE
          WHERE app_status = 'Pending' AND project_info.archived != '1'";
          //PENDING COUNTER
          $sthofwpend = $dbh->prepare($ofw_pendingselect);
          $sthofwpend->execute();
          $ofw_pendingcount=$sthofwpend->rowCount();

          $ofw_qualifiedselect = "SELECT app_status FROM ofw_applications
          LEFT JOIN project_info ON ofw_applications.PJ_CODE = project_info.PJ_CODE
          WHERE app_status = 'Qualified' AND project_info.archived != '1'";
          //QUALIFIED COUNTER
          $sthofwquad = $dbh->prepare($ofw_qualifiedselect);
          $sthofwquad->execute();
          $ofw_qualifiedcount=$sthofwquad->rowCount();

          $ofw_disqualifiedselect = "SELECT app_status FROM ofw_applications
          LEFT JOIN project_info ON ofw_applications.PJ_CODE = project_info.PJ_CODE
          WHERE app_status = 'Disqualified' AND project_info.archived != '1'";
          //DISQUALIFIED COUNTER
          $sthofwdisq = $dbh->prepare($ofw_disqualifiedselect);
          $sthofwdisq->execute();
          $ofw_disqualifiedcount=$sthofwdisq->rowCount();

          $ofw_awardedselect = "SELECT app_status FROM ofw_applications
          LEFT JOIN project_info ON ofw_applications.PJ_CODE = project_info.PJ_CODE
          WHERE app_status = 'New Applicant' AND project_info.archived != '1'";
          //AWARDED COUNTER
          $sthofwaward = $dbh->prepare($ofw_awardedselect);
          $sthofwaward->execute();
          $ofw_awardedcount=$sthofwaward->rowCount();
          
          $totalOFWCount = $ofw_awardedcount + $ofw_disqualifiedcount + $ofw_qualifiedcount + $ofw_pendingcount;
          $dbh = null;
        }
      }
      catch(PDOException $e){
          error_log('PDOException - ' . $e->getMessage(), 0);
          http_response_code(500);
          die('Error establishing connection with database');
      }
    ?>


    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-8">

            <div class="card border-card">
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-5" style="margin: auto;">
                    <div class="card elevation-3 p-3 card-bg-total" style="background: #67a2b2;">
                      <div class="center text-center text-white">
                        <h1 class="font-weight-bold"><?php echo $totalGovCount; ?></h1>
                        <hr class="w-50">
                        <h5 class="font-weight-normal">Government Employees <br> Applicant</h5>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-7 pl-sm-5">
                    <h5 class="pl-sm-1 pb-2 pt-2 font-weight-bold text-lg text-a">Application Status</h5>
                    <ul class="nav nav-pills flex-column">
                      <li class="nav-item">
                        <a class="nav-link pl-0 pr-0" onclick="sessionempty()" href="pages/forms/registrationCAT.php?cat=new_app">
                          <i class="fa-solid fa-user-plus fa-lg text-info pr-2"></i>
                          <span class="h6 text-stat">&nbsp;New Applications</span>
                          <span class="font-weight-bold h4 float-right text-dark">
                          <?php echo $awardedcount; ?></span>
                        </a>
                      </li>

                      <li class="nav-item">
                        <a class="nav-link pl-0 pr-0" onclick="sessionempty()" href="pages/forms/registrationCAT.php?cat=qua_app">
                          <i class="fa-solid fa-check fa-lg text-olive pr-3"></i>
                          <span class="h6 text-stat">&nbsp;Qualified</span>
                          <span class="font-weight-bold h4 float-right text-dark">
                          <?php echo $qualifiedcount; ?></span>
                        </a>
                      </li>

                      <li class="nav-item">
                        <a class="nav-link pl-0 pr-0" onclick="sessionempty()" href="pages/forms/registrationCAT.php?cat=pend_app">
                          <i class="fa-solid fa-spinner fa-lg text-warning pr-3"></i>
                          <span class="h6 text-stat"> Pending</span>
                          <span class="font-weight-bold h4 float-right text-dark">
                          <?php echo $pendingcount; ?></span>
                        </a>
                      </li>

                      <li class="nav-item">
                        <a class="nav-link pl-0 pr-0" onclick="sessionempty()" href="pages/forms/registrationCAT.php?cat=dis_app">
                          <i class="fa-solid fa-user-xmark fa-lg text-danger pr-2"></i>
                          <span class="h6 text-stat">&nbsp;Disqualified</span>
                          <span class="font-weight-bold h4 float-right text-dark">
                          <?php echo $disqualifiedcount; ?></span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>

            <div class="card border-card">
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-5" style="margin: auto;">
                    <div class="card elevation-3 p-3 card-bg-total" style="background: #67a2b2;">
                      <div class="center text-center text-white">
                        <h1 class="font-weight-bold"><?php echo $totaluniCount; ?></h1>
                        <hr class="w-50">
                        <h5 class="font-weight-normal">Military Uniformed Personnel <br> Applicant</h5>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-7 pl-sm-5">
                    <h5 class="pl-sm-1 pb-2 pt-2 font-weight-bold text-lg text-a">Application Status</h5>
                    <ul class="nav nav-pills flex-column">
                      <li class="nav-item">
                        <a class="nav-link pl-0 pr-0" onclick="sessionempty()" href="pages/forms/registrationCAT_uni.php?cat=new_app">
                          <i class="fa-solid fa-user-plus fa-lg text-info pr-2"></i>
                          <span class="h6 text-stat">&nbsp;New Applications</span>
                          <span class="font-weight-bold h4 float-right text-dark">
                          <?php echo $uniawardedcount; ?></span>
                        </a>
                      </li>

                      <li class="nav-item">
                        <a class="nav-link pl-0 pr-0" onclick="sessionempty()" href="pages/forms/registrationCAT_uni.php?cat=qua_app">
                          <i class="fa-solid fa-check fa-lg text-olive pr-3"></i>
                          <span class="h6 text-stat">&nbsp;Qualified</span>
                          <span class="font-weight-bold h4 float-right text-dark">
                          <?php echo $uniqualifiedcount; ?></span>
                        </a>
                      </li>

                      <li class="nav-item">
                        <a class="nav-link pl-0 pr-0" onclick="sessionempty()" href="pages/forms/registrationCAT_uni.php?cat=pend_app">
                          <i class="fa-solid fa-spinner fa-lg text-warning pr-3"></i>
                          <span class="h6 text-stat"> Pending</span>
                          <span class="font-weight-bold h4 float-right text-dark">
                          <?php echo $unipendingcount; ?></span>
                        </a>
                      </li>

                      <li class="nav-item">
                        <a class="nav-link pl-0 pr-0" onclick="sessionempty()" href="pages/forms/registrationCAT_uni.php?cat=dis_app">
                          <i class="fa-solid fa-user-xmark fa-lg text-danger pr-2"></i>
                          <span class="h6 text-stat">&nbsp;Disqualified</span>
                          <span class="font-weight-bold h4 float-right text-dark">
                          <?php echo $unidisqualifiedcount; ?></span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>

            <div class="card border-card">
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-5" style="margin: auto;">
                    <div class="card elevation-3 p-3 card-bg-total" style="background: #67a2b2;">
                      <div class="center text-center text-white">
                        <h1 class="font-weight-bold"><?php echo $totalOFWCount; ?></h1>
                        <hr class="w-50">
                        <h5 class="font-weight-normal">Overseas Filipino Worker <br> Applicant</h5>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-7 pl-sm-5">
                    <h5 class="pl-sm-1 pb-2 pt-2 font-weight-bold text-lg text-a">Application Status</h5>
                    <ul class="nav nav-pills flex-column">
                      <li class="nav-item">
                        <a class="nav-link pl-0 pr-0" onclick="sessionempty()" href="pages/forms/registration_OFWCAT.php?catofw=new_app">
                          <i class="fa-solid fa-user-plus fa-lg text-info pr-2"></i>
                          <span class="h6 text-stat">&nbsp;New Applications</span>
                          <span class="font-weight-bold h4 float-right text-dark">
                          <?php echo $ofw_awardedcount?></span>
                        </a>
                      </li>

                      <li class="nav-item">
                        <a class="nav-link pl-0 pr-0" onclick="sessionempty()" href="pages/forms/registration_OFWCAT.php?catofw=qua_app">
                          <i class="fa-solid fa-check fa-lg text-olive pr-3"></i>
                          <span class="h6 text-stat">&nbsp;Qualified</span>
                          <span class="font-weight-bold h4 float-right text-dark">
                          <?php echo $ofw_qualifiedcount?></span>
                        </a>
                      </li>

                      <li class="nav-item">
                        <a class="nav-link pl-0 pr-0" onclick="sessionempty()" href="pages/forms/registration_OFWCAT.php?catofw=pend_app">
                          <i class="fa-solid fa-spinner fa-lg text-warning pr-3"></i>
                          <span class="h6 text-stat"> Pending</span>
                          <span class="font-weight-bold h4 float-right text-dark">
                          <?php echo $ofw_pendingcount?></span>
                        </a>
                      </li>

                      <li class="nav-item">
                        <a class="nav-link pl-0 pr-0" onclick="sessionempty()" href="pages/forms/registration_OFWCAT.php?catofw=dis_app">
                          <i class="fa-solid fa-user-xmark fa-lg text-danger pr-2"></i>
                          <span class="h6 text-stat">&nbsp;Disqualified</span>
                          <span class="font-weight-bold h4 float-right text-dark">
                          <?php echo $ofw_disqualifiedcount?></span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-sm-4">
            <div class="card border-card card-app-per-h">
              <div class="card-body">
                <div class="card elevation-2 card-app-per">
                  <div class="text-lg card-body p-3 font-weight-bold text-a" style="background: #67a2b2; border-radius: 10px">
                    <span id="titleAppCat" style="color: white;">Application Per Project</span>
                    <span class="float-right">
                      <div class="btn-group">
                        <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">
                          <i class="fas fa-ellipsis-vertical"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right dropdown-default border-mid" role="menu">
                          <input type="text" value="project" id="appProSelectorTxt" hidden>
                          <input type="text" value="region" id="appRegSelectorTxt" hidden>
                          <button onclick="appProSelector()" id="appProSelector" value="project" class="dropdown-item">Application Per Project</button>
                          <button onclick="appRegSelector()" id="appRegSelector" value="region" class="dropdown-item">Application Per Region</button>
                        </div>
                      </div>
                    </span>
                  </div>
                </div>
                <script>
                    document.getElementById("appProSelector").style.display = 'none';
                    function appProSelector() {
                      $.ajax({
                        type:'POST',
                        url:'app_ajax.php',
                        data:{getOption:$("#appProSelectorTxt").val()},
                        success:function(response){
                          document.getElementById("cardContentApp").innerHTML=response;
                          document.getElementById("appRegSelector").style.display = 'block';
                          document.getElementById("appProSelector").style.display = 'none';
                          document.getElementById("titleAppCat").innerHTML = 'Application Per Project';
                        }
                      })
                    };
                    function appRegSelector() {
                      $.ajax({
                        type:'POST',
                        url:'app_ajax.php',
                        data:{getOption:$("#appRegSelectorTxt").val()},
                        success:function(response){
                          document.getElementById("cardContentApp").innerHTML=response;
                          document.getElementById("appRegSelector").style.display = 'none';
                          document.getElementById("appProSelector").style.display = 'block';
                          document.getElementById("titleAppCat").innerHTML = 'Application Per Region';
                        }
                      })
                    };
                </script>

                <div class="p-2 card-scroll-a" id="cardContentApp">
                  <?php
                    try{
                      $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                      $datefromcurr = date('Y-m-d H:i:s', strtotime('-1 months'));
                      $datetocurr = date('Y-m-d H:i:s');
                      $datesearch=[
                        ':datefromcurr' => $datefromcurr,
                        ':datetocurr' => $datetocurr
                      ];
                      $countappsperproj ="SELECT SUM(TotalProjects) as TotalProjects, SUM(Current) as Current, PJ_NAME
                                          FROM (SELECT applications.PJ_CODE, project_info.PJ_NAME, COUNT(*) as TotalProjects, 
                                          SUM(CASE WHEN applications.app_date BETWEEN :datefromcurr AND :datetocurr THEN 1 ELSE 0 END) as Current
                                          FROM applications
                                          LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
                                          WHERE project_info.archived != '1'
                                          GROUP BY applications.PJ_CODE
                                          UNION ALL
                                          SELECT ofw_applications.PJ_CODE, project_info.PJ_NAME, COUNT(*) as TotalProjects, 
                                          SUM(CASE WHEN ofw_applications.app_date BETWEEN :datefromcurr AND :datetocurr THEN 1 ELSE 0 END) as Current
                                          FROM ofw_applications
                                          LEFT JOIN project_info ON ofw_applications.PJ_CODE = project_info.PJ_CODE
                                          WHERE project_info.archived != '1'
                                          GROUP BY ofw_applications.PJ_CODE) Bas
                                          GROUP BY PJ_NAME";
                      $sthappsperproj = $dbh->prepare($countappsperproj);
                      $sthappsperproj->execute($datesearch);
                      $sthappsperproj->setFetchMode(PDO::FETCH_ASSOC);
                      $cc = 0;
                      while ($rowappsperproj = $sthappsperproj->fetch(PDO::FETCH_ASSOC)) { 
                      $cc++;
                      ?>
                        <div class="d-flex justify-content-between align-items-center border-bottom mb-3 pt-3">
                          <p><?php echo $rowappsperproj["PJ_NAME"]; ?></p>
                          <p class="d-flex flex-column text-right">
                            <span class="font-weight-bold h4"><?php echo $rowappsperproj["TotalProjects"]; ?></span>
                          </p>
                        </div>
                      <?php
                      }
                      $dbh = null;
                    }
                    catch(PDOException $e){
                        error_log('PDOException - ' . $e->getMessage(), 0);
                        http_response_code(500);
                        die('Error establishing connection with database');
                    }
                  ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <footer class="main-footer">
    <?php include('pages/forms/footer.php'); ?>
  </footer>

  <aside class="control-sidebar control-sidebar-dark">
  </aside>
</div>

<script src="plugins/jquery/jquery.min.js"></script>
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="plugins/summernote/summernote-bs4.min.js"></script>
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<script src="dist/js/adminlte.js"></script>
<script src="dist/js/demo.js"></script>
<script src="dist/js/pages/dashboard3.js"></script>
</body>
</html>

<script language="javascript">
           var message="This function is not allowed here.";
           function clickIE4(){
                 if (event.button==2){
                     alert(message);
                     return false;
                 }
           }
           function clickNS4(e){
                if (document.layers||document.getElementById&&!document.all){
                        if (e.which==2||e.which==3){
                                  alert(message);
                                  return false;
                        }
                }
           }
           if (document.layers){
                 document.captureEvents(Event.MOUSEDOWN);
                 document.onmousedown=clickNS4;
           }
           else if (document.all&&!document.getElementById){
                 document.onmousedown=clickIE4;
           }
           document.oncontextmenu=new Function("alert(message);return false;")
</script>
<script>
  function sessionempty() {
    $("#loaderIcon").show();
        jQuery.ajax({
        url: "session_clear.php",
        data: {'sessionempty': $("#sessionempty").val()},
        type: "POST",
        success:function(data){
            $("#").html(data);
            $("#loaderIcon").hide();
        },
        error:function (){
        }
    });
  }
</script>

<style>
  .card, .small-box, .btn {
    -webkit-animation-duration: 0.7s;
    animation-duration: 0.7s;
  }
  
  @-webkit-keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  
  @keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  
  .card, .small-box {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
  }

  .card, .small-box {
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  }

  .text-a {
    color: #002d32;
  }
  .card-bg-total {
    background-color: #518a89;
    border-radius: 10px;
  }
  .card-scroll-a {
    height: 445px;
    overflow: scroll;
    overflow-x: hidden;
  }
  .card-app-per-h {
    height: 556px;
  }
  .card-app-per {
    background-color: #d7c5a6;
    border-radius: 10px;
  }
  .border-card {
    border-radius: 10px;
  }
  .text-stat {
    color: black;
  }
  .text-stat:hover {
    color: #518a89;
  }
  .border-mid {
    border-radius: 10px;
  }
  .dropdown-toggle {
    color:white;
  }
  .dropdown-toggle:hover {
    color:#696969;
  }
</style>