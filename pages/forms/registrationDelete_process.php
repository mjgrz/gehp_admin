<?php
session_start();
date_default_timezone_set("Asia/Hong_Kong");
$time = date("h:i a");
$date = date('F d, Y', strtotime(date("Y-m-d")));
$datetime = $date." ".$time;
  
include("../../../../dbcon.php");
$URL = $_POST['URL'];
if(isset($_POST['deleteGovSubBtn'])){
  if(isset($_POST['check'])) {
    $checkbox = $_POST['check'];
    for($i=0;$i<count($checkbox);$i++){
      $del_id = $checkbox[$i]; 
      if ( is_numeric($del_id) == true){
        try{
          $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
          $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $sqldata = [
            ':del_id' => $del_id
          ];
          $appGov = "DELETE FROM applications WHERE applicant_id=:del_id";
          $sthappGov = $dbh->prepare($appGov);
  
          $info = "DELETE FROM applicant_info WHERE applicant_id=:del_id";
          $sthinfo = $dbh->prepare($info);
  
          $spouse = "DELETE FROM applicant_spouse WHERE spouse_id=:del_id";
          $sthspouse = $dbh->prepare($spouse);
  
          $employment = "DELETE FROM employment_status WHERE employment_id=:del_id";
          $sthemployment = $dbh->prepare($employment);
  
          $dependent = "DELETE FROM applicant_family WHERE dependent_id=:del_id";
          $sthdependent = $dbh->prepare($dependent);
  
          $requirements = "DELETE FROM requirements WHERE req_id=:del_id";
          $sthrequirements = $dbh->prepare($requirements);
  
            if($sthappGov->execute($sqldata) &&
            $sthinfo->execute($sqldata) &&
            $sthspouse->execute($sqldata) &&
            $sthemployment->execute($sqldata) &&
            $sthdependent->execute($sqldata) &&
            $sthrequirements->execute($sqldata)){
                $auditdata = [
                  ':activity' => "GOV Application(s) Deleted $del_id",
                  ':username' => $_SESSION['login_user'],
                  ':datetime' => $datetime
                ];
                $audit = "INSERT INTO audit_trail_applications (activity, username, date) VALUES (:activity, :username, :datetime)";
                $sthaudit = $dbh->prepare($audit);
                $sthaudit->execute($auditdata);
  
                $_SESSION["status"] = "Record(s) have been deleted successfully.";
                header('Location: '. $URL .'');
                $dbh = null;
            }
            else{
                $_SESSION["error"] = "Sorry, record(s) were not deleted.";
                header('Location: '. $URL .'');
                $dbh = null;
            }
        }
        catch(PDOException $e){
            error_log('PDOException - ' . $e->getMessage(), 0);
            http_response_code(500);
            die('Error establishing connection with database');
        }
      }
      else{
        http_response_code(400);
        die('Error processing bad or malformed request');
      }
    }
  }
  if(!isset($_POST['check'])) {
    $_SESSION["info"] = "Sorry, no record(s) to be deleted.";
    header('Location: '. $URL .'');
  }
}
else{
  header('Location: '. $URL .'');
}
?>