<?php
  session_start();
  include("../../../../dbcon.php");
  $userLname = $_POST["userLname"];
  $userFname = $_POST["userFname"];
  $userMname = $_POST["userMname"];
  $userSuffix = $_POST["userSuffix"];
  $location = $_POST["office"];
  $office_id = $_POST["office_id"];
  $userlevel = $_POST["userlevel"];
  $status = $_POST["status"];
  $email = $_POST["email"];
  $usernamex = $_POST["username"];
  $id = $_POST["id"];   
  date_default_timezone_set("Asia/Hong_Kong");
  $time = date("h:i a");
  $date = date('F d, Y', strtotime(date("Y-m-d")));
  $datetime = $date." ".$time;
  if(is_numeric($id)){
    try{
      $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $userarray = [
        ':email' => $email,
        ':username' => $usernamex,
        ':userLname' => $userLname,
        ':userFname' => $userFname,
        ':userMname' => $userMname,
        ':userSuffix' => $userSuffix,
        ':location' => $location,
        ':office_id' => $office_id,
        ':userlevel' => $userlevel,
        ':status' => $status,
        ':id' => $id
      ];
      $sqluser = "UPDATE users SET email = :email, username = :username, lastname = :userLname, firstname = :userFname, 
                  middlename = :userMname, suffix = :userSuffix, office= :location, office_id = :office_id, userlevel = :userlevel , status = :status
                  WHERE user_id = :id";
      $sthsqluser = $dbh->prepare($sqluser);

      if ($sthsqluser->execute($userarray)) {
          $auditdata = [
            ':activity' => "Updated the information of $usernamex / id = $id",
            ':username' => $_SESSION['login_user'],
            ':datetime' => $datetime
          ];
          $audit = "INSERT INTO audit_trail (activity, username, date) VALUES (:activity, :username, :datetime)";
          $sthaudit = $dbh->prepare($audit);
          $sthaudit->execute($auditdata);

          $_SESSION["status"] = "Your data have been saved successfully.";
          header('Location: users.php');
          $dbh = null;
      }
      else {
          $_SESSION["error"] = "Sorry, your data were not saved.";
          header('Location: users.php');
          $dbh = null;
      }
    }
    catch(PDOException $e){
        error_log('PDOException - ' . $e->getMessage(), 0);
        http_response_code(500);
        die('Error establishing connection with database');
    }
  }
  else{
    http_response_code(400);
    die('Error processing bad or malformed request');
  }

?>