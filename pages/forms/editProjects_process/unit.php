<?php
$project_name_final = str_replace(" ", "-", "$project_name");
$target_dir = "uploads/projects/units/";
$target_filefloor1 = $target_dir . basename("$project_name_final-floor1.png");
$target_filefloor2 = $target_dir . basename("$project_name_final-floor2.png");
$uploadOkfloor1 = 1;
$uploadOkfloor2 = 1;
$floor1 = "$project_name_final-floor1.png";
$floor2 = "$project_name_final-floor2.png";
$imageFileTypefloor1 = strtolower(pathinfo($target_filefloor1,PATHINFO_EXTENSION));
$imageFileTypefloor2 = strtolower(pathinfo($target_filefloor2,PATHINFO_EXTENSION));

  if(isset($_POST["submit"])) {
    
    $check1 = getimagesize($_FILES["floor_imgUp1"]["tmp_name"]);
    if($check1 !== false) {
      echo "File is an image - " . $check1["mime"] . ".";
      $uploadOkfloor1 = 1;
    } else {
      echo "File is not an image.";
      $uploadOkfloor1 = 0;
    }
  }

  if ($_FILES["floor_imgUp1"]["size"] <= 50000000) {
    if (file_exists($target_filefloor1)) {
      unlink($target_filefloor1);
    }
  }

  if ($_FILES["floor_imgUp1"]["size"] > 50000000) {
    echo "Sorry, your file is too large.";
    $uploadOkfloor1 = 0;
  }

  if($imageFileTypefloor1 != "jpg" && $imageFileTypefloor1 != "png" && $imageFileTypefloor1 != "jpeg"
  && $imageFileTypefloor1 != "gif" && $imageFileTypefloor1 != "") {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOkfloor1 = 0;
  }

  if ($uploadOkfloor1 == 0) {
    echo "Sorry, your file was not uploaded.";
  } else {

    if (move_uploaded_file($_FILES["floor_imgUp1"]["tmp_name"], $target_filefloor1)) {
    } 
    else 
    {
    }
  }
  //<--------- END ---------->
  if(isset($_POST["submit"])) {
    
    $check2 = getimagesize($_FILES["floor_imgUp2"]["tmp_name"]);
    if($check2 !== false) {
      echo "File is an image - " . $check2["mime"] . ".";
      $uploadOkfloor2 = 1;
    } else {
      echo "File is not an image.";
      $uploadOkfloor2 = 0;
    }
  }

  if ($_FILES["floor_imgUp2"]["size"] <= 50000000) {
    if (file_exists($target_filefloor2)) {
      unlink($target_filefloor2);
    }
  }

  if ($_FILES["floor_imgUp2"]["size"] > 50000000) {
    echo "Sorry, your file is too large.";
    $uploadOkfloor2 = 0;
  }

  if($imageFileTypefloor2 != "jpg" && $imageFileTypefloor2 != "png" && $imageFileTypefloor2 != "jpeg"
  && $imageFileTypefloor2 != "gif" && $imageFileTypefloor2 != "") {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOkfloor2 = 0;
  }

  if ($uploadOkfloor2 == 0) {
    echo "Sorry, your file was not uploaded.";
  } else {

    if (move_uploaded_file($_FILES["floor_imgUp2"]["tmp_name"], $target_filefloor2)) {
    } 
    else 
    {
    }
  }
?>