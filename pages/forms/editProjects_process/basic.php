<?php
$project_name_final = str_replace(" ", "-", "$project_name");
$target_dir_main = "uploads/projects/main_image/";
$target_file_main = $target_dir_main . basename("$project_name_final.png");
$uploadOk = 1;
$main_image = "$project_name_final.png";
$imageFileType_main = strtolower(pathinfo($target_file_main,PATHINFO_EXTENSION));

  if(isset($_POST["submit"])) {
    
    $check = getimagesize($_FILES["main_imageUpload"]["tmp_name"]);
    if($check !== false) {
      echo "File is an image - " . $check["mime"] . ".";
      $uploadOk = 1;
    } else {
      echo "File is not an image.";
      $uploadOk = 0;
    }
  }

  if ($_FILES["main_imageUpload"]["size"] <= 50000000) {
    if (file_exists($target_file_main)) {
      unlink($target_file_main);
      $uploadOk1 = 1;
    }
  }

  if ($_FILES["main_imageUpload"]["size"] > 50000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
  }

  if($imageFileType_main != "jpg" && $imageFileType_main != "png" && $imageFileType_main != "jpeg"
  && $imageFileType_main != "gif" && $imageFileType_main != "") {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
  }

  if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
  } else {

    if (move_uploaded_file($_FILES["main_imageUpload"]["tmp_name"], $target_file_main)) {
    } 
    else 
    {
    }
  }
?>