<?php
$project_name_final = str_replace(" ", "-", "$project_name");
$target_dir = "uploads/projects/gallery/";
$target_fileimg1 = $target_dir . basename("$project_name_final-img1.png");
$target_fileimg2 = $target_dir . basename("$project_name_final-img2.png");
$target_fileimg3 = $target_dir . basename("$project_name_final-img3.png");
$target_fileimg4 = $target_dir . basename("$project_name_final-img4.png");
$target_fileimg5 = $target_dir . basename("$project_name_final-img5.png");
$target_fileimg6 = $target_dir . basename("$project_name_final-img6.png");
$uploadOkimg1 = 1;
$uploadOkimg2 = 1;
$uploadOkimg3 = 1;
$uploadOkimg4 = 1;
$uploadOkimg5 = 1;
$uploadOkimg6 = 1;
$unit_img1 = "$project_name_final-img1.png";
$unit_img2 = "$project_name_final-img2.png";
$unit_img3 = "$project_name_final-img3.png";
$unit_img4 = "$project_name_final-img4.png";
$unit_img5 = "$project_name_final-img5.png";
$unit_img6 = "$project_name_final-img6.png";
$imageFileTypeimg1 = strtolower(pathinfo($target_fileimg1,PATHINFO_EXTENSION));
$imageFileTypeimg2 = strtolower(pathinfo($target_fileimg2,PATHINFO_EXTENSION));
$imageFileTypeimg3 = strtolower(pathinfo($target_fileimg3,PATHINFO_EXTENSION));
$imageFileTypeimg4 = strtolower(pathinfo($target_fileimg4,PATHINFO_EXTENSION));
$imageFileTypeimg5 = strtolower(pathinfo($target_fileimg5,PATHINFO_EXTENSION));
$imageFileTypeimg6 = strtolower(pathinfo($target_fileimg6,PATHINFO_EXTENSION));

  if(isset($_POST["submit"])) {
    
    $checkimg1 = getimagesize($_FILES["unit_imgUp1"]["tmp_name"]);
    if($checkimg1 !== false) {
      echo "File is an image - " . $checkimg1["mime"] . ".";
      $uploadOkimg1 = 1;
    } else {
      echo "File is not an image.";
      $uploadOkimg1 = 0;
    }
  }

  if ($_FILES["unit_imgUp1"]["size"] <= 50000000) {
    if (file_exists($target_fileimg1)) {
      unlink($target_fileimg1);
    }
  }

  if ($_FILES["unit_imgUp1"]["size"] > 50000000) {
    echo "Sorry, your file is too large.";
    $uploadOkimg1 = 0;
  }

  if($imageFileTypeimg1 != "jpg" && $imageFileTypeimg1 != "png" && $imageFileTypeimg1 != "jpeg"
  && $imageFileTypeimg1 != "gif" && $imageFileTypeimg1 != "") {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOkimg1 = 0;
  }

  if ($uploadOkimg1 == 0) {
    echo "Sorry, your file was not uploaded.";
  } else {

    if (move_uploaded_file($_FILES["unit_imgUp1"]["tmp_name"], $target_fileimg1)) {
    } 
    else 
    {
    }
  }
  //<--------- END ---------->
  if(isset($_POST["submit"])) {
    
    $checkimg2 = getimagesize($_FILES["unit_imgUp2"]["tmp_name"]);
    if($checkimg2 !== false) {
      echo "File is an image - " . $checkimg2["mime"] . ".";
      $uploadOkimg2 = 1;
    } else {
      echo "File is not an image.";
      $uploadOkimg2 = 0;
    }
  }

  if ($_FILES["unit_imgUp2"]["size"] <= 50000000) {
    if (file_exists($target_fileimg2)) {
      unlink($target_fileimg2);
    }
  }

  if ($_FILES["unit_imgUp2"]["size"] > 50000000) {
    echo "Sorry, your file is too large.";
    $uploadOkimg2 = 0;
  }

  if($imageFileTypeimg2 != "jpg" && $imageFileTypeimg2 != "png" && $imageFileTypeimg2 != "jpeg"
  && $imageFileTypeimg2 != "gif" && $imageFileTypeimg2 != "") {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOkimg2 = 0;
  }

  if ($uploadOkimg2 == 0) {
    echo "Sorry, your file was not uploaded.";
  } else {

    if (move_uploaded_file($_FILES["unit_imgUp2"]["tmp_name"], $target_fileimg2)) {
    } 
    else 
    {
    }
  }
  //<--------- END ---------->
  if(isset($_POST["submit"])) {
    
    $checkimg3 = getimagesize($_FILES["unit_imgUp3"]["tmp_name"]);
    if($checkimg3 !== false) {
      echo "File is an image - " . $checkimg3["mime"] . ".";
      $uploadOkimg3 = 1;
    } else {
      echo "File is not an image.";
      $uploadOkimg3 = 0;
    }
  }

  if ($_FILES["unit_imgUp3"]["size"] <= 50000000) {
    if (file_exists($target_fileimg3)) {
      unlink($target_fileimg3);
    }
  }

  if ($_FILES["unit_imgUp3"]["size"] > 50000000) {
    echo "Sorry, your file is too large.";
    $uploadOkimg3 = 0;
  }

  if($imageFileTypeimg3 != "jpg" && $imageFileTypeimg3 != "png" && $imageFileTypeimg3 != "jpeg"
  && $imageFileTypeimg3 != "gif" && $imageFileTypeimg3 != "") {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOkimg3 = 0;
  }

  if ($uploadOkimg3 == 0) {
    echo "Sorry, your file was not uploaded.";
  } else {

    if (move_uploaded_file($_FILES["unit_imgUp3"]["tmp_name"], $target_fileimg3)) {
    } 
    else 
    {
    }
  }
  //<--------- END ---------->
  if(isset($_POST["submit"])) {
    
    $checkimg4 = getimagesize($_FILES["unit_imgUp4"]["tmp_name"]);
    if($checkimg4 !== false) {
      echo "File is an image - " . $checkimg4["mime"] . ".";
      $uploadOkimg4 = 1;
    } else {
      echo "File is not an image.";
      $uploadOkimg4 = 0;
    }
  }

  if ($_FILES["unit_imgUp4"]["size"] <= 50000000) {
    if (file_exists($target_fileimg4)) {
      unlink($target_fileimg4);
    }
  }

  if ($_FILES["unit_imgUp4"]["size"] > 50000000) {
    echo "Sorry, your file is too large.";
    $uploadOkimg4 = 0;
  }

  if($imageFileTypeimg4 != "jpg" && $imageFileTypeimg4 != "png" && $imageFileTypeimg4 != "jpeg"
  && $imageFileTypeimg4 != "gif" && $imageFileTypeimg4 != "") {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOkimg4 = 0;
  }

  if ($uploadOkimg4 == 0) {
    echo "Sorry, your file was not uploaded.";
  } else {

    if (move_uploaded_file($_FILES["unit_imgUp4"]["tmp_name"], $target_fileimg4)) {
    } 
    else 
    {
    }
  }
  //<--------- END ---------->
  if(isset($_POST["submit"])) {
    
    $checkimg5 = getimagesize($_FILES["unit_imgUp5"]["tmp_name"]);
    if($checkimg5 !== false) {
      echo "File is an image - " . $checkimg5["mime"] . ".";
      $uploadOkimg5 = 1;
    } else {
      echo "File is not an image.";
      $uploadOkimg5 = 0;
    }
  }

  if ($_FILES["unit_imgUp5"]["size"] <= 50000000) {
    if (file_exists($target_fileimg5)) {
      unlink($target_fileimg5);
    }
  }

  if ($_FILES["unit_imgUp5"]["size"] > 50000000) {
    echo "Sorry, your file is too large.";
    $uploadOkimg5 = 0;
  }

  if($imageFileTypeimg5 != "jpg" && $imageFileTypeimg5 != "png" && $imageFileTypeimg5 != "jpeg"
  && $imageFileTypeimg5 != "gif" && $imageFileTypeimg5 != "") {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOkimg5 = 0;
  }

  if ($uploadOkimg5 == 0) {
    echo "Sorry, your file was not uploaded.";
  } else {

    if (move_uploaded_file($_FILES["unit_imgUp5"]["tmp_name"], $target_fileimg5)) {
    } 
    else 
    {
    }
  }
  //<--------- END ---------->
  if(isset($_POST["submit"])) {
    
    $checkimg6 = getimagesize($_FILES["unit_imgUp6"]["tmp_name"]);
    if($checkimg6 !== false) {
      echo "File is an image - " . $checkimg6["mime"] . ".";
      $uploadOkimg6 = 1;
    } else {
      echo "File is not an image.";
      $uploadOkimg6 = 0;
    }
  }

  if ($_FILES["unit_imgUp6"]["size"] <= 50000000) {
    if (file_exists($target_fileimg6)) {
      unlink($target_fileimg6);
    }
  }

  if ($_FILES["unit_imgUp6"]["size"] > 50000000) {
    echo "Sorry, your file is too large.";
    $uploadOkimg6 = 0;
  }

  if($imageFileTypeimg6 != "jpg" && $imageFileTypeimg6 != "png" && $imageFileTypeimg6 != "jpeg"
  && $imageFileTypeimg6 != "gif" && $imageFileTypeimg6 != "") {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOkimg6 = 0;
  }

  if ($uploadOkimg6 == 0) {
    echo "Sorry, your file was not uploaded.";
  } else {

    if (move_uploaded_file($_FILES["unit_imgUp6"]["tmp_name"], $target_fileimg6)) {
    } 
    else 
    {
    }
  }
?>
