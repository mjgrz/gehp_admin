<div class="modal fade" id="modal<?php echo $hcRow['hc_id'] ?>">
  <div class="modal-dialog modal-xxl" style="border-radius: 5px;">
    <div class="modal-content" id="modalBody">
      <div class="modal-header" style="background: #67a2b2; color: white;">
        <h4 class="modal-title">Replace Image</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: white;">&times;</span>
        </button>
      </div>
      <!--Submit Form for editing carousel slides-->
      <form method="POST" action="homeCarousel_update.php" enctype="multipart/form-data">
        <div class="modal-body" style="overflow: visible;">
          <div class="row">
            <div class="col-sm-12">
              <input type="text" name="hc_id" value="<?php echo $hcRow['hc_id'] ?>" hidden>
              <div class="form-group" id="uploadDiv2slide">
              <?php
                include("../../../../dbcon.php");
                $id = $hcRow['hc_id'];
                if ( is_numeric($id) == true){
                  try{
                    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    $bgSelect = "SELECT hc_image FROM homecarousel WHERE hc_id = :id";
                    $sthbgSelect = $dbh->prepare($bgSelect);
                    $sthbgSelect->bindParam(':id', $id);
                    $sthbgSelect->execute();
                    $sthbgSelect->setFetchMode(PDO::FETCH_ASSOC);

                    if($sthbgSelect->rowCount() > 0){
                      while ($bgRow = $sthbgSelect->fetch(PDO::FETCH_ASSOC)) {
                        $BGs = $bgRow["hc_image"];
                      } 
                      $dbh = null;
                    }
                    else{ }
                  }
                  catch(PDOException $e){
                    error_log('PDOException - ' . $e->getMessage(), 0);
                    http_response_code(500);
                    die('Error establishing connection with database');
                  }
                } 
                else{
                http_response_code(400);
                die('Error processing bad or malformed request');
                }
                ?>
                <input type="file" id="slide_image2" name="slide_image2" title="Click in this area to upload an image." onchange="preview2<?php echo $id ?>()">
                <p style="background-image:url('uploads/homeCarousel/<?php echo $BGs ?>');
                          text-align: center;
                          height: 100%;
                          display: flex;
                          justify-content: center;
                          align-items: center;
                          padding: 5px;
                          background-position: center;
                          background-repeat: no-repeat;
                          background-size: 100% auto;" id="textimage2<?php echo $id ?>">
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-right">
          <div class="project-actions text-right">
            <button style="width:auto; font-size:large;" type="submit" class="btn btn-default color-cyan"><i class="fa fa-paper-plane"></i>&nbsp; Submit</button>
          </div>
        </div>
      </form>
      <!--End of Submit Form for editing carousel slides-->
    </div>  
  </div>
</div>
<style>
  @media (min-width: 496px) {
    .modal-xxl {
      max-width: 60%;
    }
  }
  @media (min-width: 600px) {
    .modal-xxl {
      max-width: 60%;
    }
  }
  #uploadDiv2slide {
    width: 100%;
    height: 528px;
    border: 2px dashed #67a2b2;
    border-radius: 5px;  
  }
  #slide_image2 {
    position: absolute;
    margin: 0;
    padding: 0;
    width: 98%;
    height: 100%;
    outline: none;
    opacity: 0;
  }
  #slide_image2:hover {
    cursor: pointer;
  }
  #textimage2, #textimageUpdate2 {
    text-align: center;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 5px;
  }
  #textimage2 {
    background-position: center;
    background-repeat: no-repeat;
    background-size: 100% auto;
  }
  .content {
    -webkit-animation-duration: 0.7s;
    animation-duration: 0.7s;
  }
  @-webkit-keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  @keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  .content {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
  }
  input:hover, select:hover, textarea:hover {
    border: 1px solid #67a2b2;
  }
</style>
<script>
  function preview2<?php echo $id ?>() {
  var urlimage = URL.createObjectURL(event.target.files[0]);
  document.getElementById("textimage2<?php echo $id ?>").style.backgroundImage = "url("+urlimage+")";
  document.getElementById("textimage2<?php echo $id ?>").style.backgroundSize = "100% auto";
  document.getElementById("textimage2<?php echo $id ?>").style.backgroundRepeat = "no-repeat";
  document.getElementById("textimage2<?php echo $id ?>").style.backgroundPosition = "center";
  document.getElementById("textimage2<?php echo $id ?>").innerHTML = "";
  }
</script>