<div class="card card-primary" style="box-shadow: none; border: 1px solid #67a2b2;"><form method="POST" action="" enctype="multipart/form-data">
  <div class="card-body">
    <div class="row">
      <div class="col-sm-6">
        <div class="col-md-12" style="margin-top: 32px;">
          <div class="form-group" id="main_imageDiv">
            <input type="file" id="main_imageUpload" name="main_imageUpload" title="Click in this area to upload an image." onchange="previewMain_img()">
            <p id="main_imageText">
            <?php 
            $target_dir = "uploads/projects/main_image/";
            $target_file = $target_dir . basename("$main_image");
            if(!file_exists($target_file)){
              echo "Click in this area to upload an image.";
            } 
            ?>
            </p>
          </div>
        </div>
      </div>

      <div class="col-sm-6">
        <div class="form-group">
          <label>Status</label>
          <select class="form-control" name="status" id="status" style="padding-left:5px;" required>
            <option value="">- Select Status -</option>
            <option <?php if($status == 'Completed'){ echo 'selected';} else {} ?> value="Completed">Completed Project</option>
            <option <?php if($status == 'Ongoing'){ echo 'selected';} else {} ?> value="Ongoing">Ongoing Project</option>
            <option <?php if($status == 'Upcoming'){ echo 'selected';} else {} ?> value="Upcoming">Upcoming Project</option>
          </select>
        </div>
        
        <div class="form-group">
          <label>Region</label>
          <select class="form-control" name="region" id="region" style="padding-left:5px;" required>
            <option value="">- Select Region -</option>
            <?php
            include('../../../../../dbcon.php'); 
            try{
              $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
              $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
              $regionselect = "SELECT psgc_reg, Region FROM region ORDER by Region";
              $sthregionselect = $dbh->prepare($regionselect);
              $sthregionselect->execute();
              $sthregionselect->setFetchMode(PDO::FETCH_ASSOC); 

              if($sthregionselect->rowCount() > 0){
                while ($regionrow = $sthregionselect->fetch(PDO::FETCH_ASSOC)) {
                
                  if($regionrow['psgc_reg'] == $region){
                    $setregion = 'selected';
                  }
                  else
                  {
                    $setregion = '';
                  }
            ?>
                  <option <?php echo $setregion ?> value="<?php echo $regionrow['psgc_reg'] ?>"><?php echo strtoupper($regionrow['Region']) ?></option>
            <?php 
                } 
                $dbh = null;
              }
              else 
              { 
                $dbh = null;
              }
            }
            catch(PDOException $e){
              error_log('PDOException - ' . $e->getMessage(), 0);
              http_response_code(500);
              die('Error establishing connection with database');
            }
            ?>
          </select>
        </div>
        <div class="form-group">
          <label>District</label>
          <select class="form-control" name="district" id="district" style="padding-left:5px;" required>
            <option value=''>- Select District Office -</option>
            <?php
            include('../../../../../dbcon.php'); 
            if (is_numeric($region) == true){
              try{
                $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $districtselect = "SELECT ro_id FROM table_ro_ WHERE region_id = :region";
                $sthdistrictselect = $dbh->prepare($districtselect);
                $sthdistrictselect->bindParam(':region', $region);
                $sthdistrictselect->execute();
                $sthdistrictselect->setFetchMode(PDO::FETCH_ASSOC); 

                if($sthdistrictselect->rowCount() > 0){
                  while ($districtrow = $sthdistrictselect->fetch(PDO::FETCH_ASSOC)) {
                    $dis_id = $districtrow['ro_id'];
                    if (is_numeric($dis_id) == true){
                      try{
                        $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        $dis_query = "SELECT id, DO FROM table_do WHERE ro_id = :dis_id"; 
                        $sthdis_query = $dbh->prepare($dis_query);
                        $sthdis_query->bindParam(':dis_id', $dis_id);
                        $sthdis_query->execute();
                        $sthdis_query->setFetchMode(PDO::FETCH_ASSOC); 
                        
                        while ($dis_row = $sthdis_query->fetch(PDO::FETCH_ASSOC)) {
                          
                          if($dis_row['id'] == $district){
                            $setdistrict = 'selected';
                          }
                          else
                          {
                            $setdistrict = '';
                          }
            ?>
                    <option <?php echo $setdistrict ?> value="<?php echo $dis_row['id'] ?>"><?php echo strtoupper($dis_row['DO']) ?></option>
            <?php 
                        }
                        $dbh = null;
                      }
                      catch(PDOException $e){
                        error_log('PDOException - ' . $e->getMessage(), 0);
                        http_response_code(500);
                        die('Error establishing connection with database');
                      }
                    } 
                    else{
                    http_response_code(400);
                    die('Error processing bad or malformed request');
                    }
                  } 
                }
                else 
                { 
                  $dbh = null;
                }
              }
              catch(PDOException $e){
                error_log('PDOException - ' . $e->getMessage(), 0);
                http_response_code(500);
                die('Error establishing connection with database');
              }
            } 
            else{
              http_response_code(400);
              die('Error processing bad or malformed request');
            }
            ?>
          </select>
        </div>
        
        <div class="form-group">
          <label for="projectName">Project Code</label>&nbsp&nbsp&nbsp<span id="PJcode-availability-status"></span>
          <input type="text" class="form-control" id="projectCode" name="projectCode" pattern="[0-9]+" value="<?php echo $code?>" onchange="checkPJcodeAvailability()"
          oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" required/>
        </div>
        <?php include("projects/projectcode_script.php"); ?>
        <div class="form-group">
          <label for="projectName">Name of Project</label>
          <input type="text" class="form-control" id="projectName" name="projectName" value="<?php echo $name ?>"
          oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" required>
        </div>
        <div class="form-group">
          <label for="overview">Overview</label>
          <textarea class="form-control" id="overview" name="overview" style="resize: none; padding: 6px;" rows="4"
          oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\.d*)\./g, '$1');" value="<?php echo $overview?>" required><?php echo $overview?></textarea >
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(function () {
  bsCustomFileInput.init();
});
</script>

<style>
  #main_imageDiv {
    width: 100%;
    height: 540px;
    border: 2px dashed #67a2b2;
    border-radius: 5px;
  }

  #main_imageUpload {
    position: absolute;
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    outline: none;
    opacity: 0;
  }

  #main_imageUpload:hover {
    cursor: pointer;
  }

  #main_imageText {
    text-align: center;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 5px;
  }

  #main_imageText {
    background-image: url('uploads/projects/main_image/<?php echo $main_image ?>');
    background-position: center;
    background-repeat: no-repeat;
    background-size: auto 100%;
  }


  .content {
    -webkit-animation-duration: 0.7s;
    animation-duration: 0.7s;
  }
  
  @-webkit-keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  
  @keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  
  .content {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
  }
</style>

<script>
  $(function () {
    $("#hc").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "buttons": [""]
    }).buttons().container().appendTo('#hc_wrapper .col-md-6:eq(0)');
  });
  
  function previewMain_img() {
    var urlimage = URL.createObjectURL(event.target.files[0]);
    document.getElementById("main_imageText").style.backgroundImage = "url("+urlimage+")";// specify the image path here
    document.getElementById("main_imageText").style.backgroundSize = "auto 100%";
    document.getElementById("main_imageText").style.backgroundRepeat = "no-repeat";
    document.getElementById("main_imageText").style.backgroundPosition = "center";
    document.getElementById("main_imageText").innerHTML = "";
  }
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script>
$(document).ready(function(){
    $('#region').on('change', function(){
        var countryID = $(this).val();
        if(countryID){
            $.ajax({
                type:'POST',
                url:'projects_edit/ajaxData_edit.php',
                data:'region_id='+countryID,
                success:function(html){
                    $('#district').html(html);
                }
            }); 
        }else{
        }
    });
});
</script>