<div class="card card-primary" style="box-shadow: none; border: 1px solid #67a2b2;">
  <div class="card-body">
    <div class="row">

      <div class="col-sm-6">
        <div class="col-md-12" style="margin-top: 32px;">
          <div class="form-group" id="map_imgDiv">
            <input type="file" id="map_imgUp1" name="map_imgUp1" title="Click in this area to upload an image." onchange="map_imgPreview1()" >
            <p id="map_imgText1">
            <?php 
            $target_dir = "uploads/projects/maps/";
            $target_file = $target_dir . basename("$locmap");
            if(!file_exists($target_file)){
              echo "Click in this area to upload an image.";
            } 
            ?>
            </p>
          </div>
        </div>
      </div>
      
      <div class="col-sm-6">
        <div class="col-md-12" style="margin-top: 32px;">
          <div class="form-group" id="map_imgDiv">
            <input type="file" id="map_imgUp2" name="map_imgUp2" title="Click in this area to upload an image." onchange="map_imgPreview2()" >
            <p id="map_imgText2">
            <?php 
            $target_dir = "uploads/projects/maps/";
            $target_file = $target_dir . basename("$vicmap");
            if(!file_exists($target_file)){
              echo "Click in this area to upload an image.";
            } 
            ?>
            </p>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
<script>
$(function () {
  bsCustomFileInput.init();
});
</script>

<style>
  #map_imgDiv {
    width: 100%;
    height: 467px;
    border: 2px dashed #67a2b2;
    border-radius: 5px;
  }

  #map_imgUp1, #map_imgUp2 {
    position: absolute;
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    outline: none;
    opacity: 0;
  }

  #map_imgUp1:hover, #map_imgUp2:hover {
    cursor: pointer;
  }

  #map_imgText1, #map_imgText2 {
    text-align: center;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 5px;
  }

  #map_imgText1 {
    background-image: url('uploads/projects/maps/<?php echo $locmap ?>');
    background-position: center;
    background-repeat: no-repeat;
    background-size: auto 100%;
  }
  
  #map_imgText2 {
    background-image: url('uploads/projects/maps/<?php echo $vicmap ?>');
    background-position: center;
    background-repeat: no-repeat;
    background-size: auto 100%;
  }

  .content {
    -webkit-animation-duration: 0.7s;
    animation-duration: 0.7s;
  }
  
  @-webkit-keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  
  @keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  
  .content {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
  }
</style>

<script>
  $(function () {
    $("#hc").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "buttons": [""]
    }).buttons().container().appendTo('#hc_wrapper .col-md-6:eq(0)');
  });

  function map_imgPreview1() {
    var urlimage = URL.createObjectURL(event.target.files[0]);
    document.getElementById("map_imgText1").style.backgroundImage = "url("+urlimage+")";// specify the image path here
    document.getElementById("map_imgText1").style.backgroundSize = "auto 100%";
    document.getElementById("map_imgText1").style.backgroundRepeat = "no-repeat";
    document.getElementById("map_imgText1").style.backgroundPosition = "center";
    document.getElementById("map_imgText1").innerHTML = "";
  }

  function map_imgPreview2() {
    var urlimage = URL.createObjectURL(event.target.files[0]);
    document.getElementById("map_imgText2").style.backgroundImage = "url("+urlimage+")";// specify the image path here
    document.getElementById("map_imgText2").style.backgroundSize = "auto 100%";
    document.getElementById("map_imgText2").style.backgroundRepeat = "no-repeat";
    document.getElementById("map_imgText2").style.backgroundPosition = "center";
    document.getElementById("map_imgText2").innerHTML = "";
  }
</script>