<?php
session_start();
date_default_timezone_set("Asia/Hong_Kong");
$time = date("h:i a");
$date = date('F d, Y', strtotime(date("Y-m-d")));
$datetime = $date." ".$time;

include('../../../../dbcon.php');
    
  $about_title = $_POST["about_title"];
  $about_desc = $_POST["about_desc"];
  $uploadVid = $_POST["uploadVid"];
  $aboutid = 0;
  if (is_numeric($aboutid) == true){
    try{
      $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $sqlupdatedata = [
        ':about_title' => $about_title,
        ':about_desc' => $about_desc,
        ':filename' => $uploadVid,
        ':aboutid' => $aboutid
      ];
        $sqlupdate = "UPDATE about SET about_title=:about_title, about_desc=:about_desc, about_video=:filename WHERE about_id=:aboutid";
        $sthsqlupdate = $dbh->prepare($sqlupdate);
        if ($sthsqlupdate->execute()) {
          $auditdata = [
            ':activity' => 'Edited the About_Page',
            ':username' => $_SESSION['login_user'],
            ':datetime' => $datetime
          ];
          $audit = "INSERT INTO audit_trail (activity, username, date) VALUES (:activity, :username, :datetime)";
          $sthaudit = $dbh->prepare($audit);
          $sthaudit->execute($auditdata);

          $_SESSION["status"] = "Your data have been saved successfully.";
          header('Location: ../forms/about.php');
          $dbh = null;
        }
        else {
          $_SESSION["error"] = "Your data were not saved.";
          header('Location: ../forms/about.php');
          $dbh = null;
        }
    }
    catch(PDOException $e){
      error_log('PDOException - ' . $e->getMessage(), 0);
      http_response_code(500);
      die('Error establishing connection with database');
    }
  } 
  else{
    http_response_code(400);
    die('Error processing bad or malformed request');
  }
?>

