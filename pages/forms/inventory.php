<!DOCTYPE html>
<html lang="en">
<head>
<?php

use Google\Service\CloudSearch\PushItem;

include('login_session.php');
// include("../../../../dbcon.php"); 
if($login_officeID != "h60000000" || $login_officeID != "h50000000"){
  if($login_officeID == "h60000000"){
    
  }
  else if($login_officeID == "h50000000"){
    
  }
  else{
    header("location:../../index.php");
  }
}
error_reporting(E_ERROR | E_PARSE);

?>

<link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
<link rel="stylesheet" href="../../plugins/toastr/toastr.min.css">
<link rel="stylesheet" href="../../dist/css/adminlte.min.css">

<script src="../../plugins/jquery/jquery.min.js"></script>
<script src="../../plugins/toastr/toastr.min.js"></script>

<?php
if (isset($_SESSION["status"]) && $_SESSION["status"] !='') {
  $msg = $_SESSION["status"];
  echo "<span><script type='text/javascript'>toastr.success('$msg')</script></span>";
  
  unset($_SESSION["status"]);
}
if (isset($_SESSION["error"]) && $_SESSION["error"] !='') {
  $msg = $_SESSION["error"];
  echo "<span><script type='text/javascript'>toastr.error('$msg')</script></span>";
  
  unset($_SESSION["error"]);
}
if (isset($_SESSION["info"]) && $_SESSION["info"] !='') {
  $msg = $_SESSION["info"];
  echo "<span><script type='text/javascript'>toastr.info('$msg')</script></span>";
  
  unset($_SESSION["info"]);
}
else {

}
?>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>NHA | Government Employee's Housing Program</title>
  <link rel="shortcut icon" type="image/png" href="../../dist/img/nha_logo_circle.png">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
</head>
<body class="hold-transition sidebar-mini">
  
<script language="javascript">
           var message="This function is not allowed here.";
           function clickIE4(){
                 if (event.button==2){
                     alert(message);
                     return false;
                 }
           }
           function clickNS4(e){
                if (document.layers||document.getElementById&&!document.all){
                        if (e.which==2||e.which==3){
                                  alert(message);
                                  return false;
                        }
                }
           }
           if (document.layers){
                 document.captureEvents(Event.MOUSEDOWN);
                 document.onmousedown=clickNS4;
           }
           else if (document.all&&!document.getElementById){
                 document.onmousedown=clickIE4;
           }
           document.oncontextmenu=new Function("alert(message);return false;")
</script>

<div class="wrapper">
  <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="font-weight: bold;">
    <?php include("header.php"); ?>
  </nav>

  <aside class="main-sidebar sidebar-dark-primary elevation-4" style="background: #002d32; position: fixed;">
    <a href="../../index.php" class="brand-link">
      <img src="../../dist/img/nha_logo_circle.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"><b>NHA | GEHP</b></span>
    </a>

    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <?php
          $target_dir = "uploads/users/";
          $target_file = $target_dir . basename("$login_image");
          if(file_exists($target_file)){
            echo "<img src='uploads/users/$login_image' class='img-circle elevation-3' alt='User Image' style='height: 33px;'>";
          } 
          else {
            echo "<img src='uploads/users/user.png' width='150px'>";
          }
          ?>
        </div>
        <div class="info">
          <a href="users_profile.php" class="d-block"><?php echo $_SESSION['login_user'] ?></a>
        </div>
      </div>

      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
          <li class="nav-item">
            <a href="../../index.php" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-briefcase"></i>
              <p>
                Applicants
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <input type="text" class="form-control" id="sessionempty" name="sessionempty" value="" placeholder="Search" hidden>
              <li class="nav-item">
                <a href="../forms/registration.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Government Employees</p>
                </a>
              </li>
              <li class="nav-item" <?php echo $deploy;?>>
                <a href="../forms/registration_Uni.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Uniformed Personnel</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/registration_OFW.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>OFW</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item" <?php echo $project_hide ?>>
            <a href="../forms/project_view.php" onclick="sessionempty()" class="nav-link">
              <i class="nav-icon fas fa-building"></i>
              <p>
                Projects
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $user_hide ?>>
            <a href="../forms/users.php" onclick="sessionempty()" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Users
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $element_hide ?>>
            <a href="../forms/report_gen.php" class="nav-link">
              <i class="nav-icon fas fa-scroll"></i>
              <p>
                Reports
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $pages_hide ?>>
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-clone"></i>
              <p>
                Pages
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              
              <li class="nav-item">
                <a href="../forms/home.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Home</p>
                </a>
              </li>
              <li class="nav-item" hidden>
                <a href="../forms/howToApply.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>How To Apply</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/about.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>About</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/banner.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Banner</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="../forms/bulletin.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Bulletin</p>
                </a>
              </li>
            </ul>
          </li>
          
          <li class="nav-item" <?php echo $inv_hide;?>>
            <a href="../forms/inventory.php" class="nav-link active" style="background: #67a2b2;">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                Inventory
              </p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>
  <script>
    function sessionempty() {
      $("#loaderIcon").show();
          jQuery.ajax({
          url: "session_clear.php",
          data: {'sessionempty': $("#sessionempty").val()},
          type: "POST",
          success:function(data){
              $("#").html(data);
              $("#loaderIcon").hide();
          },
          error:function (){
          }
      });
    }
  </script>

  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Inventory</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <button class="btn btn-default color-green-dark add_item"  style="width:auto; font-size:large;"
              data-toggle='modal' data-keyboard='false' href='#modalviewItem'>
                <i class=" fas fa-plus"></i>&nbsp; Add Map Inventory
              </button>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <?php include("inv_item_modal.php"); ?>
    <script>
      $('.add_item').click(function(){
          $.ajax({url:"inv_item_modal_result.php",cache:false,success:function(result){
              $(".item_modal").html(result);
          }});
      });
    </script>
    <?php include('custom_btn.php'); ?>

    <?php
      $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $query = "SELECT * FROM project_info 
                LEFT JOIN (SELECT COUNT(*) as InvCounter, project_id FROM inventory GROUP BY project_id) a
                ON project_info.project_id = a.project_id
                WHERE availability = 'Available' ORDER BY PJ_NAME";
      $sth = $dbh->prepare($query);
      $sth->execute();
      $sth->setFetchMode(PDO::FETCH_ASSOC); 
    ?>
    
    <section class="content">
      <div class="container-fluid">
        <div class="col-12">
            <div class="col-12">
              <div class="row" style="justify-content:center;">
                <?php while ($pjrow = $sth->fetch(PDO::FETCH_ASSOC)) { if($pjrow['InvCounter'] != NULL){ ?>
                    <a class="col-sm-2 m-3" href="inventory_info.php?ID=<?php echo $pjrow['project_id']; ?>&FLOOR=1"
                    style="color:black; border-radius:10px; 
                    padding:0px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                      <div class="col-12 p-0">
                        <?php 
                        $main_image = $pjrow['main_image'];
                        $target_dir = "uploads/projects/main_image/";
                        $target_file = $target_dir . basename("$main_image");
                        if(file_exists($target_file)){
                        ?>
                        <img src="uploads/projects/main_image/<?php echo $main_image ?>" 
                        style="width:100%; height:100%; border-radius: 10px 10px 0px 0px;"> 
                        <?php
                        } 
                        else
                        {
                          echo "No image available.";
                        }
                        ?>
                      </div>
                      <div class="col-12" style="padding:7px 10px;"><?php echo $pjrow['PJ_NAME'] ?></div>
                    </a>
                  <?php } else { ?>
                    <span class="col-sm-2 m-3 invContainer"
                    style="color:black; border-radius:10px; 
                    padding:0px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                      <div class="col-12 p-0 invItem">
                        <?php 
                        $main_image = $pjrow['main_image'];
                        $target_dir = "uploads/projects/main_image/";
                        $target_file = $target_dir . basename("$main_image");
                        if(file_exists($target_file)){
                        ?>
                        <img src="uploads/projects/main_image/<?php echo $main_image ?>" 
                        style="width:100%; height:100%; border-radius: 10px 10px 0px 0px;"> 
                        <?php
                        } 
                        else
                        {
                          echo "No image available.";
                        }
                        ?>
                      </div>
                      <div class="col-12" style="padding:7px 10px;"><?php echo $pjrow['PJ_NAME'] ?></div>
                      <div class="invMiddle">
                        <div class="invText">No Available Map Inventory</div>
                      </div>
                    </span>
                  <?php } ?>
                <?php } ?>
              </div>
            </div>
        </div>
      </div>
    </section>
  </div>
  <footer class="main-footer">
    <?php include('footer.php'); ?>
    <?php include('custom_listing.php'); ?>
  </footer>

  <aside class="control-sidebar control-sidebar-dark">
  </aside>
</div>
</body>
</html>

<style>
  .content {
    -webkit-animation-duration: 0.7s;
    animation-duration: 0.7s;
  }
  
  @-webkit-keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  
  @keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  
  .content {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
  }

  td {
    background: #f5f5f5;
  }
  th {
    cursor: pointer;
  }
  
  .btn-info-dark {
  color: #fff;
  background-color: #014047;
  border-color: #014047;
  box-shadow: none;
  }

  .btn-info-dark:hover {
    color: #fff;
    background-color: #003035;
    border-color: #003035;
  }

  .btn-info-dark:focus, .btn-info-dark.focus {
    color: #fff;
    background-color: #014047;
    border-color: #002c31;
    box-shadow: 0 0 0 0 rgba(58, 176, 195, 0.5);
  }

  .btn-info-dark.disabled, .btn-info-dark:disabled {
    color: #fff;
    background-color: #17a2b8;
    border-color: #17a2b8;
  }

  .btn-info-dark:not(:disabled):not(.disabled):active, .btn-info-dark:not(:disabled):not(.disabled).active,
  .show > .btn-info-dark.dropdown-toggle {
    color: #fff;
    background-color: #002c31;
    border-color: #002c31;
  }

  .btn-info-dark:not(:disabled):not(.disabled):active:focus, .btn-info-dark:not(:disabled):not(.disabled).active:focus,
  .show > .btn-info-dark.dropdown-toggle:focus {
    box-shadow: 0 0 0 0 rgba(58, 176, 195, 0.5);
  }
</style>

<script src="../../plugins/jquery/jquery.min.js"></script>
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script src="../../dist/js/adminlte.min.js"></script>
<script src="../../dist/js/demo.js"></script>
<script src="../../plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>

<script>
  $(function () {
    $("#projectTable").DataTable({
      "paging": false,
      "lengthChange": true,
      "searching": false,
      "ordering": false,
      "info": false,
      "autoWidth": false,
      "responsive": false,
      //"buttons": ["excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#projectTable_wrapper .col-md-6:eq(0)');
  });
</script>
