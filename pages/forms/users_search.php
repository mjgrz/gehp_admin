<?php
include('login_session.php');
$_SESSION["userSearch"] = $_POST["checkers"];
error_reporting(E_ERROR | E_PARSE);
?>
<span>
<div style="overflow:auto; height: 650px; padding: 7px;">
<table id="userTable" class="table table-bordered table-striped">
  <thead>
  <tr>
    <th <?php echo $delete_hide; ?> style="white-space: nowrap; " class="project-actions text-center"><input id="chk_all" name="chk_all" type="checkbox"></th>
    <th>Image</th>
    <th>Email Address</th>
    <th>Username</th>
    <th>Status</th>
    <th>Full Name</th>
    <th>Region Office</th>
    <th>District Office</th>
    <th>User Level</th>
    <th>Action</th>
  </tr>
  </thead>
  <tbody>
  
  <?php
    try{
      $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      //Regional User
      if($login_office == 'Regional'){
        $selectdata=[
          ':userSearch' => $_SESSION["userSearch"],
          ':login_office' => $login_office,
          ':login_officeID' => $login_officeID,
          ':status' => 'Active'
        ];
        $select = "SELECT * FROM users
                      LEFT JOIN table_ro_ ON users.office_id = table_ro_.ro_id
                      WHERE lastname LIKE CONCAT('%', :userSearch, '%') AND office = :login_office AND office_id = :login_officeID AND status = :status
                      OR firstname LIKE CONCAT('%', :userSearch, '%') AND office = :login_office AND office_id = :login_officeID AND status = :status
                      ORDER BY userlevel";
      }
      //Head Office Any department except COSDD
      if($login_office == 'Head Office' && $login_officeID != 'h50000000' OR $login_officeID == 'h10000000'){
        $selectdata=[
          ':userSearch' => $_SESSION["userSearch"],
          ':login_office' => $login_office,
          ':login_officeID' => $login_officeID,
          ':login_officeIDx' => "h50000000",
          ':status' => 'Active'
        ];
        $select = "SELECT * FROM users
                      WHERE lastname LIKE CONCAT('%', :userSearch, '%') AND office = :login_office AND office_id = :login_officeID AND office_id != :login_officeIDx AND status = :status
                      OR firstname LIKE CONCAT('%', :userSearch, '%') AND office = :login_office AND office_id = :login_officeID AND office_id != :login_officeIDx AND status = :status
                      ORDER BY userlevel";
      }
      //Head Office COSDD
      if($login_office == 'Head Office' && $login_officeID == 'h50000000'){
        $selectdata=[
          ':userSearch' => $_SESSION["userSearch"],
        ];
        $select = "SELECT * FROM users WHERE lastname LIKE CONCAT('%', :userSearch, '%') OR firstname LIKE CONCAT('%', :userSearch, '%') ORDER BY userlevel";
      }
      //Head Office EMD
      if($login_office == 'Head Office' && $login_officeID == 'h10000000'){
        $selectdata=[
          ':userSearch' => $_SESSION["userSearch"],
          ':login_officeIDx1' => "h60000000",
          ':login_officeIDx2' => "h50000000",
          ':login_officeIDx3' => "h40000000",
          ':login_officeIDx4' => "h30000000",
          ':login_officeIDx5' => "h20000000",
          ':status' => 'Active'
        ];
        $select = "(SELECT * FROM users WHERE lastname LIKE CONCAT('%', :userSearch, '%') AND office_id != :login_officeIDx1
                   AND office_id != :login_officeIDx2 AND office_id != :login_officeIDx3 AND office_id != :login_officeIDx4 AND office_id != :login_officeIDx5
                   AND status = :status
                   OR firstname LIKE CONCAT('%', :userSearch, '%') AND office_id != :login_officeIDx1
                   AND office_id != :login_officeIDx2 AND office_id != :login_officeIDx3 AND office_id != :login_officeIDx4 AND office_id != :login_officeIDx5
                   AND status = :status) ORDER BY office, userlevel";
      }
      $sthselect = $dbh->prepare($select);
      $sthselect->execute($selectdata);
      $count = $sthselect->rowCount();
      $setcount = $count;
      
      if (isset($_GET['pageno'])) {
        $pageno = $_GET['pageno'];
        //$i = $_GET['pageno'];
        if($_GET['pageno'] == 1){
          $i = 2;
        }
      } 
      else {
        $pageno = 1;
        $i = 2;
      }

      $no_of_records_per_page = 5;
      $offset = ($pageno-1) * $no_of_records_per_page;

      $total_pages = ceil($count / $no_of_records_per_page);

      $cnt=1;
      
      $sql2 = "$select LIMIT $offset, $no_of_records_per_page";

      $sthsql2 = $dbh->prepare($sql2);
      $sthsql2->execute($selectdata);
      $sthsql2->setFetchMode(PDO::FETCH_ASSOC); 
      $app_count = $sthsql2->rowCount();
      $setcount = $app_count;

        $i=0;
        while ($userrow = $sthsql2->fetch(PDO::FETCH_ASSOC)) {
          $image = $userrow['user_image'];
          ?>
          <tr>
          <td <?php echo $delete_hide; ?> class="project-actions text-center" style="vertical-align: middle;"><input name="check[]" type="checkbox" class='chkbox' value="<?php echo $userrow['user_id']; ?>"/></td>
          <td class="project-actions text-center" style="vertical-align: middle;"> 
            <?php
            $target_dir = "uploads/users/";
            $target_file = $target_dir . basename("$image");
            if(file_exists($target_file)){
              echo "<img src='uploads/users/$image' width='150px'>";
            } 
            else {
              echo "<img src='uploads/users/user.png' width='150px'>";
            }
            ?>
          </td>
          <td style="vertical-align: middle;"> <?php echo $userrow['email'] ?> </td>
          <td style="vertical-align: middle;"> <?php echo $userrow['username'] ?> </td>
          <td style="vertical-align: middle;">
            <span <?php if($userrow['status']=='Active'){echo 'style="background:green; padding: 3px 6px; color: white; border-radius: 5px;"';} 
            else {echo 'style="background:red; padding: 3px 6px; color: white; border-radius: 5px;"';}?>>
              <?php echo $userrow['status'] ?>
            </span>
          </td>
          <td style="vertical-align: middle;"> <?php echo $userrow['lastname'] ?><?php if($userrow['suffix'] == 'N/A' || $userrow['suffix'] == ''){echo '';} else{echo ' '.$userrow['suffix'];}?>, <?php echo $userrow['firstname'] ?> <?php echo $userrow['middlename'] ?> </td>
          <td style="vertical-align: middle;"> 
            <?php 
            echo $userrow['office'];
            
            ?> 
          </td>
          <td style="vertical-align: middle;"> 
            <?php 
            if($userrow['office'] == 'Head Office'){
              $officeselect = "SELECT description FROM department WHERE office_id = :office_id";
              $sthofficeselect = $dbh->prepare($officeselect);
              $sthofficeselect->bindParam(':office_id', $userrow['office_id']);
              $sthofficeselect->execute();
              $sthofficeselect->setFetchMode(PDO::FETCH_ASSOC); 
              while ($officerow = $sthofficeselect->fetch(PDO::FETCH_ASSOC)) {
                echo $officerow['description'];
              }
            }
            if($userrow['office'] == 'Regional'){
              $officeselect = "SELECT RO FROM table_ro_ WHERE ro_id = :office_id";
              $sthofficeselect = $dbh->prepare($officeselect);
              $sthofficeselect->bindParam(':office_id', $userrow['office_id']);
              $sthofficeselect->execute();
              $sthofficeselect->setFetchMode(PDO::FETCH_ASSOC); 
              while ($officerow = $sthofficeselect->fetch(PDO::FETCH_ASSOC)) {
                echo $officerow['RO'];
              }
            }
            ?> 
          </td>
          <td style="vertical-align: middle;"> <?php echo $userrow['userlevel'] ?> </td>
          <td class="project-actions text-center" style="vertical-align: middle;">
            <a id="tooltip" href="users_edit.php?id=<?php echo $userrow['user_id'] ?>&office=<?php echo $userrow['office'] ?>" style="width:auto; font-size:large;" class="btn btn-default btn-sm color-cyan-dark">
              <i class="fas fa-edit">
              </i>
              <span class="tooltiptext">Edit</span>
            </a>
          </td>
          </tr>
          <?php 
          $i++; 
        }
        $dbh = null; 
    }
    catch(PDOException $e){
        error_log('PDOException - ' . $e->getMessage(), 0);
        http_response_code(500);
        die('Error establishing connection with database');
    }
  ?>
  </tbody>
</table>
</div>
<?php include("paging.php"); ?>
<?php include("delete_script.php");?>
</span>


<script>
  $(function () {
    $("#userTable").DataTable({
      "destroy": true,
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": false,
      "responsive": false,
    }).buttons().container().appendTo('#userTable_wrapper .col-md-6:eq(0)');
  });
</script>