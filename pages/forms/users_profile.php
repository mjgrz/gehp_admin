<!DOCTYPE html>
<html lang="en">
<head>
<?php
include('login_session.php');
error_reporting(E_ERROR | E_PARSE);
?>
<link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
<link rel="stylesheet" href="../../plugins/toastr/toastr.min.css">
<link rel="stylesheet" href="../../dist/css/adminlte.min.css">

<script src="../../plugins/jquery/jquery.min.js"></script>
<script src="../../plugins/toastr/toastr.min.js"></script>

<?php
if (isset($_SESSION["status"]) && $_SESSION["status"] !='') {
  $msg = $_SESSION["status"];
  echo "<span><script type='text/javascript'>toastr.success('$msg')</script></span>";
  
  unset($_SESSION["status"]);
}
if (isset($_SESSION["error"]) && $_SESSION["error"] !='') {
  $msg = $_SESSION["error"];
  echo "<span><script type='text/javascript'>toastr.error('$msg')</script></span>";
  
  unset($_SESSION["error"]);
}
?>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>NHA | Government Employee's Housing Program</title>
  <link rel="shortcut icon" type="image/png" href="../../dist/img/nha_logo_circle.png">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="font-weight: bold;">
    <?php include("header.php"); ?>
  </nav>

  <aside class="main-sidebar sidebar-dark-primary elevation-4" style="background: #002d32; position: fixed;">
    <a href="../../index.php" class="brand-link">
      <img src="../../dist/img/nha_logo_circle.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"><b>NHA | GEHP</b></span>
    </a>

    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <?php
          $target_dir = "uploads/users/";
          $target_file = $target_dir . basename("$login_image");
          if(file_exists($target_file)){
            echo "<img src='uploads/users/$login_image' class='img-circle elevation-3' alt='User Image' style='height: 33px;'>";
          } 
          else {
            echo "<img src='uploads/users/user.png' width='150px'>";
          }
          ?>
        </div>
        <div class="info">
          <a href="users_profile.php" class="d-block"><?php echo $_SESSION['login_user'] ?></a>
        </div>
      </div>

      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
          <li class="nav-item">
            <a href="../../index.php" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-briefcase"></i>
              <p>
                Applicants
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <input type="text" class="form-control" id="sessionempty" name="sessionempty" value="" placeholder="Search" hidden>
              <li class="nav-item">
                <a href="../forms/registration.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Government Employees</p>
                </a>
              </li>
              <li class="nav-item" <?php echo $deploy;?>>
                <a href="../forms/registration_Uni.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Uniformed Personnel</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/registration_OFW.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>OFW</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item" <?php echo $project_hide ?>>
            <a href="../forms/project_view.php" onclick="sessionempty()" class="nav-link">
              <i class="nav-icon fas fa-building"></i>
              <p>
                Projects
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $user_hide ?>>
            <a href="../forms/users.php" onclick="sessionempty()" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Users
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $element_hide ?>>
            <a href="../forms/report_gen.php" class="nav-link">
              <i class="nav-icon fas fa-scroll"></i>
              <p>
                Reports
              </p>
            </a>
          </li>

          
          <li class="nav-item" <?php echo $pages_hide ?>>
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-clone"></i>
              <p>
                Pages
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              
              <li class="nav-item">
                <a href="../forms/home.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Home</p>
                </a>
              </li>
              <li class="nav-item" hidden>
                <a href="../forms/howToApply.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>How To Apply</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/about.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>About</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/banner.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Banner</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="../forms/bulletin.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Bulletin</p>
                </a>
              </li>
            </ul>
          </li>
          
          <li class="nav-item" <?php echo $inv_hide;?>>
            <a href="../forms/inventory.php" class="nav-link">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                Inventory
              </p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>
  <script>
    function sessionempty() {
      $("#loaderIcon").show();
          jQuery.ajax({
          url: "session_clear.php",
          data: {'sessionempty': $("#sessionempty").val()},
          type: "POST",
          success:function(data){
              $("#").html(data);
              $("#loaderIcon").hide();
          },
          error:function (){
          }
      });
    }
  </script>

  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Profile</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div>
        </div>
      </div>
    </section>

    <?php
    include("../../../../dbcon.php");
    try{
      $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $profileselect = "SELECT * FROM users WHERE user_id = :login_id";
      $sthprofile = $dbh->prepare($profileselect);
      $sthprofile->bindParam(':login_id', $login_id);
      $sthprofile->execute();
      $sthprofile->setFetchMode(PDO::FETCH_ASSOC); 
      $profilecount=$sthprofile->rowCount();
      if($profilecount > 0){
        while ($profilerow = $sthprofile->fetch(PDO::FETCH_ASSOC)) {
          $user_id = $profilerow['user_id'];
          $username = $profilerow['username'];
          $email = $profilerow['email'];
          $lastname = $profilerow['lastname'];
          $firstname = $profilerow['firstname'];
          $middlename = $profilerow['middlename'];
          $suffix = $profilerow['suffix'];
          $userlevel = $profilerow['userlevel'];
          $image = $profilerow['user_image'];
          $office = $profilerow['office'];
          $office_id = $profilerow['office_id'];
        } 
        $dbh = null;
      }
      else{

      }
    }
    catch(PDOException $e){
        error_log('PDOException - ' . $e->getMessage(), 0);
        http_response_code(500);
        die('Error establishing connection with database');
    }
    ?>

    <section class="content" id="form">
      <div class="container-fluid">
        <div class="col-md-12">
          <div class="card card-primary" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
            <div class="card-header" style="background: #67a2b2;">
              <h3 class="card-title">
              </h3>
            </div>
            <div class="card-body">
              <div class="col-sm-12">
                <div class="row">
                  <div class="col-sm-4" style="padding: 5px; max-height: 500px; min-height: 350px;">
                    <?php
                    $target_dir = "uploads/users/";
                    $target_file = $target_dir . basename("$login_image");
                    if(file_exists($target_file)){
                      $profilepicmain = $login_image;
                    } 
                    else {
                      $profilepicmain = 'user.png';
                    }
                    ?>
                    <p id="profilepicmain"></p>
                    <style>
                      #profilepicmain{
                        background-image: url(uploads/users/<?php echo $profilepicmain ?>);
                        background-position: center;
                        background-repeat: no-repeat;
                        background-size:contain;
                        border: 2px solid #67a2b2;
                        border-radius: 5px;
                        text-align: center;
                        height: 100%;
                        width: 100%;
                        display: flex;
                        justify-content: center;
                        align-items: center;
                        
                      }
                    </style>
                  </div>
                  <div class="col-sm-8">
                    <div class="col-sm-12">
                      <span style="font-size: 40px;"><b><?php echo $username ?></b></span>
                      <p style="font-size: 20px;"><?php echo $userlevel ?></p>
                    </div>
                    <hr>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="col-sm-12">
                          <label for="name" style="font-size: 20px;">Name:</label>
                          <div>
                            <p id="name" style="font-size: 20px;"><?php echo $lastname ?><?php if($suffix == 'N/A' || $suffix == ''){echo '';} else{echo ' '.$suffix;}?>, <?php echo $firstname ?> <?php echo $middlename ?></p>
                          </div>
                          <hr>
                          <label for="email" style="font-size: 20px;">Email Address:</label>
                          <div>
                            <p id="email" style="font-size: 20px;"><?php echo $email ?></p>
                          </div>
                          <hr>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="col-sm-12">
                          <label for="ro_id" style="font-size: 20px;">Location:</label>
                          <div>
                            <p id="ro_id" style="font-size: 20px;">
                            <?php
                              echo $office;
                            ?>
                            </p>
                          </div>
                          <hr>
                          <label for="do_id" style="font-size: 20px;">
                            <?php
                              if($office == "Regional"){
                                echo "Region Office:";
                              }
                              if($office == "Head Office"){
                                echo "Department:";
                              }
                            ?>
                          </label>
                          <div>
                            <p id="do_id" style="font-size: 20px;">
                            <?php
                            include("../../../../dbcon.php");
                              if($office == "Regional"){
                                try{
                                  $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                                  $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                  $office_idselect = "SELECT RO FROM table_ro_ WHERE ro_id = :office_id";
                                  $sthofficeid = $dbh->prepare($office_idselect);
                                  $sthofficeid->bindParam(':office_id', $office_id);
                                  $sthofficeid->execute();
                                  $sthofficeid->setFetchMode(PDO::FETCH_ASSOC); 
                                  $officecount=$sthofficeid->rowCount();
                                  if($officecount > 0){
                                    while ($office_idrow = $sthofficeid->fetch(PDO::FETCH_ASSOC)) {
                                      echo $office_idrow['RO'];
                                    } 
                                    $dbh = null;
                                  }
                                  else{}
                                }
                                catch(PDOException $e){
                                    error_log('PDOException - ' . $e->getMessage(), 0);
                                    http_response_code(500);
                                    die('Error establishing connection with database');
                                }
                              }
                              if($office == "Head Office"){
                                try{
                                  $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                                  $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                  $depselect = "SELECT description FROM department WHERE office_id = :office_id";
                                  $sthdepid = $dbh->prepare($depselect);
                                  $sthdepid->bindParam(':office_id', $office_id);
                                  $sthdepid->execute();
                                  $sthdepid->setFetchMode(PDO::FETCH_ASSOC); 
                                  $depcount=$sthdepid->rowCount();
                                  if($depcount > 0){
                                    while ($deprow = $sthdepid->fetch(PDO::FETCH_ASSOC)) {
                                      echo $deprow['description'];
                                    } 
                                    $dbh = null;
                                  }
                                  else{}
                                }
                                catch(PDOException $e){
                                    error_log('PDOException - ' . $e->getMessage(), 0);
                                    http_response_code(500);
                                    die('Error establishing connection with database');
                                }
                              }
                            ?>
                            </p>
                          </div>
                          <hr>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-12" style=" padding-top: 10px;">
                      <div class="project-actions text-right">
                        <a id="change1" href="" type="button" data-toggle="modal" data-target="#profile"
                        class="btn btn-default color-cyan" style="font-size:large; width:auto;"><i class="fa fa-image"></i>&nbsp; Change Profile Picture</a>

                        <a id="change2" href="" type="button" data-toggle="modal" data-target="#password"
                        class="btn btn-default color-cyan" style="font-size:large; width:auto;"><i class="fa fa-lock"></i>&nbsp; Change Password</a>
                      </div>
                    </div>
                    <?php include("users_profile_modal.php"); ?>
                    <?php include("users_password_modal.php"); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <footer class="main-footer">
    <?php include('footer.php'); include('custom_btn.php');?>
  </footer>

  <aside class="control-sidebar control-sidebar-dark">
  </aside>
</div>

<script src="../../plugins/jquery/jquery.min.js"></script>
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script src="../../dist/js/adminlte.min.js"></script>
<script src="../../dist/js/demo.js"></script>

<script src="../../plugins/sweetalert2/sweetalert2.min.js"></script>
<script src="../../plugins/toastr/toastr.min.js"></script>

</body>
</html>

<style>
  #form {
    -webkit-animation-duration: 0.7s;
    animation-duration: 0.7s;
  }
  @-webkit-keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  @keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  #form {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
  }
  #change1, #change2 {
    margin-top: 5px;
  }
  #imgprof {
  background-size: cover;
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-position: center; 
  }
</style>
<script language="javascript">
           var message="This function is not allowed here.";
           function clickIE4(){
                 if (event.button==2){
                     alert(message);
                     return false;
                 }
           }
           function clickNS4(e){
                if (document.layers||document.getElementById&&!document.all){
                        if (e.which==2||e.which==3){
                                  alert(message);
                                  return false;
                        }
                }
           }
           if (document.layers){
                 document.captureEvents(Event.MOUSEDOWN);
                 document.onmousedown=clickNS4;
           }
           else if (document.all&&!document.getElementById){
                 document.onmousedown=clickIE4;
           }
           document.oncontextmenu=new Function("alert(message);return false;")
</script>