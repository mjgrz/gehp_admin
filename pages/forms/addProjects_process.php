<?php
  include("../../../../dbcon.php");
  date_default_timezone_set("Asia/Hong_Kong");
  $time = date("h:i a");
  $date = date('F d, Y', strtotime(date("Y-m-d")));
  $datetime = $date." ".$time;
  $region = $_POST["region"];
  if (is_numeric($region) == true){
    try{
      $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $PROJ_CODE = $_POST["projectCode"];
      $project_id = $_POST["project_id"];
      $project_name = $_POST["projectName"];
      $overview = $_POST["overview"];
      $district = $_POST["district"];
      $status = $_POST["status"];
      $estab1 = $_POST["estab1"];
      $estab2 = $_POST["estab2"];
      $estab3 = $_POST["estab3"];
      $estab4 = $_POST["estab4"];
      $estab5 = $_POST["estab5"];
      $estab6 = $_POST["estab6"];
      $location = $_POST["location"];
      $bedroom = $_POST["bedroom"];
      $bathroom = $_POST["bathroom"];
      $lotarea = $_POST["lotarea"];
      $floorarea = $_POST["floorarea"];
      $specs = $_POST["specs"];
      $islandselect = "SELECT island FROM region WHERE psgc_reg = :region";
      $sthislandselect = $dbh->prepare($islandselect);
      $sthislandselect->bindParam(':region', $region);
      $sthislandselect->execute();
      $sthislandselect->setFetchMode(PDO::FETCH_ASSOC); 
      if($sthislandselect->rowCount() > 0){
        while ($islandrow = $sthislandselect->fetch(PDO::FETCH_ASSOC)) {
          $island = $islandrow['island'];
        }
      }
      include('addProjects_process/basic.php'); 
      include('addProjects_process/unit.php'); 
      include('addProjects_process/maps.php'); 
      include('addProjects_process/gallery.php'); 
      include('addProjects_process/video.php'); 
      if ($uploadOk == 1 && $uploadOkfloor2 == 1 && $uploadOkfloor1 == 1 && $uploadOkmap2 == 1 && $uploadOkmap1 == 1 &&
          $uploadOkimg1 == 1 && $uploadOkimg2 == 1 && $uploadOkimg3 == 1 && $uploadOkimg4 == 1 && $uploadOkimg5 == 1 && $uploadOkimg6 == 1 &&
          $uploadOkvideo == 1) 
      {
        $sqlothersdata = [
          ':project_id' => $project_id,
          ':map1' => $map1,
          ':map2' => $map2,
          ':unit_img1' => $unit_img1,
          ':unit_img2' => $unit_img2,
          ':unit_img3' => $unit_img3,
          ':unit_img4' => $unit_img4,
          ':unit_img5' => $unit_img5,
          ':unit_img6' => $unit_img6,
          ':video' => $video
        ];
        $sqlothers = "INSERT INTO other_files (project_id, location_map, vicinity_map, unit_img1, unit_img2, unit_img3, unit_img4, unit_img5, unit_img6, video) 
        VALUES (:project_id, :map1, :map2, :unit_img1, :unit_img2, :unit_img3, :unit_img4, :unit_img5, :unit_img6, :video)";
        $sthsqlothers = $dbh->prepare($sqlothers);
        
        $sqlnearbydata = [
          ':project_id' => $project_id,
          ':estab1' => $estab1,
          ':estab2' => $estab2,
          ':estab3' => $estab3,
          ':estab4' => $estab4,
          ':estab5' => $estab5,
          ':estab6' => $estab6
        ];
        $sqlnearby = "INSERT INTO nearby_estab (project_id, estab1, estab2, estab3, estab4, estab5, estab6) 
        VALUES (:project_id, :estab1, :estab2, :estab3, :estab4, :estab5, :estab6)";
        $sthsqlnearby = $dbh->prepare($sqlnearby);
        
        $sqlunitsdata = [
          ':project_id' => $project_id,
          ':location' => $location,
          ':bedroom' => $bedroom,
          ':bathroom' => $bathroom,
          ':lotarea' => $lotarea,
          ':floorarea' => $floorarea,
          ':specs' => $specs,
          ':floor1' => $floor1,
          ':floor2' => $floor2
        ];
        $sqlunits = "INSERT INTO units_floor (project_id, location, bedroom, bathroom, lot_area, floor_area, specification, floor_img1, floor_img2) 
        VALUES (:project_id, :location, :bedroom, :bathroom, :lotarea, :floorarea, :specs, :floor1, :floor2)";
        $sthsqlunits = $dbh->prepare($sqlunits);
        
        $sqlprojectinfodata = [
          ':PROJ_CODE' => $PROJ_CODE,
          ':project_id' => $project_id,
          ':project_name' => $project_name,
          ':overview' => $overview,
          ':island' => $island,
          ':region' => $region,
          ':district' => $district,
          ':status' => $status,
          ':availability' => 'Available',
          ':main_image' => $main_image
        ];
        $sqlprojectinfo = "INSERT INTO project_info (PJ_CODE, project_id, PJ_NAME, overview, island, region, district, project_status, availability, main_image) 
        VALUES (:PROJ_CODE, :project_id, :project_name, :overview, :island, :region, :district, :status, :availability, :main_image)";
        $sthsqlprojectinfo = $dbh->prepare($sqlprojectinfo);
        if ($sthsqlothers->execute($sqlothersdata) && $sthsqlnearby->execute($sqlnearbydata) && $sthsqlunits->execute($sqlunitsdata) && $sthsqlprojectinfo->execute($sqlprojectinfodata)) {
          
          $auditdata = [
            ':activity' => "Added a Project (Project = $PROJ_CODE/$project_name)",
            ':username' => $_SESSION['login_user'],
            ':datetime' => $datetime
          ];
          $audit = "INSERT INTO audit_trail (activity, username, date) VALUES (:activity, :username, :datetime)";
          $sthaudit = $dbh->prepare($audit);
          $sthaudit->execute($auditdata);
          $_SESSION["status"] = "Your data have been saved successfully.";
          header('Location: ../forms/project_view.php');
          $dbh = null;
        }
        else {
          $_SESSION["error"] = "Sorry, your data were not saved.";
          header('Location: ../forms/project_view.php');
          $dbh = null;
        }
      }
      else {
          $_SESSION["error"] = "Sorry, there is a problem with the images you tried to upload.";
          header('Location: ../forms/project_view.php');
          $dbh = null;
      }
    }
    catch(PDOException $e){
      error_log('PDOException - ' . $e->getMessage(), 0);
      http_response_code(500);
      die('Error establishing connection with database');
    }
  } 
  else{
  http_response_code(400);
  die('Error processing bad or malformed request');
  }
?>