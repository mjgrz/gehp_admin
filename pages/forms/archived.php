<form method="POST" action="archivedprocess.php">
  <label class="toggle2">
    <input class="toggle-checkbox2" type="checkbox" name="toggleAvail2" id="toggleAvail2" onclick="SubArchived(event)" value="1"
      <?php if($projectrow['archived'] == '1'){echo 'checked';}else{} ?>>
    <div class="toggle-switch2"></div>
    <span class="toggle-label2" hidden>Available</span>
  </label>
  
  <input type="text" name="toggleID2" id="toggleID2" value="<?php echo $projectrow['project_id']; ?>" hidden>
  <button type="submit" class="SubAvailBtn2" name="SubAvailBtn2" id="SubAvailBtn2" hidden>submit</button>
</form>

<script>
  function SubArchived(event) {
  event.currentTarget.closest('form').submit()
  }
</script>

<style>

.toggle2 {
  cursor: pointer;
  display: inline-block;
}

.toggle-switch2 {
  display: inline-block;
  background: #ccc;
  border-radius: 16px;
  width: 58px;
  height: 32px;
  position: relative;
  vertical-align: middle;
  transition: background 0.25s;
}
.toggle-switch2:before, .toggle-switch2:after {
  content: "";
}
.toggle-switch2:before {
  display: block;
  background: linear-gradient(to bottom, #fff 0%, #eee 100%);
  border-radius: 50%;
  box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.25);
  width: 24px;
  height: 24px;
  position: absolute;
  top: 4px;
  left: 4px;
  transition: left 0.25s;
}
.toggle2:hover .toggle-switch2:before {
  background: linear-gradient(to bottom, #fff 0%, #fff 100%);
  box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.5);
}
.toggle-checkbox2:checked + .toggle-switch2 {
  background: red;
}
.toggle-checkbox2:checked + .toggle-switch2:before {
  left: 30px;
}

.toggle-checkbox2 {
  position: absolute;
  visibility: hidden;
}

</style>