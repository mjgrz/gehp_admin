<?php
session_start();
$req_id = $_GET['req_id'];
include("../../../../dbcon.php");
if ( is_numeric($req_id) == true){
  try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $req_select_modal = "SELECT * FROM requirements WHERE req_id = :req_id";
    $sthreq_select_modal = $dbh->prepare($req_select_modal);
    $sthreq_select_modal->bindParam(':req_id', $req_id);
    $sthreq_select_modal->execute();
    $sthreq_select_modal->setFetchMode(PDO::FETCH_ASSOC); 
    
    while ($req_modal = $sthreq_select_modal->fetch(PDO::FETCH_ASSOC)) {
      $app_form = $req_modal["app_form"];
      $compensation = $req_modal["compensation"];
      $bir = $req_modal["bir"];
      $active_service = $req_modal["active_service"];
      $birth_cert = $req_modal["birth_cert"];
      $marriage = $req_modal["marriage"];
      $separation = $req_modal["separation"];
      $gov_id = $req_modal["gov_id"];
      $income_tax = $req_modal["income_tax"];
      $pagibig_loan = $req_modal["pagibig_loan"];
      $pay_slip = $req_modal["pay_slip"];
      $sig_id = $req_modal["sig_id"];
      $borrower_confo = $req_modal["borrower_confo"];
      $amortization = $req_modal["amortization"];
      $disinterested = $req_modal["disinterested"];
    }
    $dbh = null;
  }
  catch(PDOException $e){
      error_log('PDOException - ' . $e->getMessage(), 0);
      http_response_code(500);
      die('Error establishing connection with database');
  }
}
if ( !is_numeric($req_id) == true){
  http_response_code(400);
  die('Error processing bad or malformed request');
}
?>
<div class="modal-header" style="background: #67a2b2; color: white;">
  <h4 class="modal-title" style="color: white;">Submitted Documents</h4>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true" style="color: white;">&times;</span>
  </button>
</div>
<!--List of requirements for GOV applicants-->
<form method="POST" action="reqGOV_process.php">
  <div class="modal-body">
    <div class="col-sm-12">
      <div class="row">
      <div>
      <input class="form-control" type="text" name="req_id"
      oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $req_id?>" hidden>
      </div>
        <div class="col-sm-6">
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="app_form" value="1" <?php if( $app_form == 1){echo "checked";} else{} ?>>
            <label class="form-check-label">Application Form</label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="compensation" value="1" <?php if( $compensation == 1){echo "checked";} else{} ?>>
            <label class="form-check-label">Certificate of Compensation</label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="bir" value="1" <?php if( $bir == 1){echo "checked";} else{} ?>>
            <label class="form-check-label">BIR Certified Income Tax Return</label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="active_service" value="1" <?php if( $active_service == 1){echo "checked";} else{} ?>>
            <label class="form-check-label">Certificate of Active in Service</label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="birth_cert" value="1" <?php if( $birth_cert == 1){echo "checked";} else{} ?>>
            <label class="form-check-label">Birth Certificate (Single)</label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="marriage" value="1" <?php if( $marriage == 1){echo "checked";} else{} ?>>
            <label class="form-check-label">Marriage Certificate</label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="separation" value="1" <?php if( $separation == 1){echo "checked";} else{} ?>>
            <label class="form-check-label">Affidavit of Separation-in-Fact</label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="gov_id" value="1" <?php if( $gov_id == 1){echo "checked";} else{} ?>>
            <label class="form-check-label">Two (2) Government-Issued ID</label>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="income_tax" value="1" <?php if( $income_tax == 1){echo "checked";} else{} ?>>
            <label class="form-check-label">Latest Income Tax Return (ITR)</label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="pagibig_loan" value="1" <?php if( $pagibig_loan == 1){echo "checked";} else{} ?>>
            <label class="form-check-label">PAG-IBIG Housing Loan Application</label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="pay_slip" value="1" <?php if( $pay_slip == 1){echo "checked";} else{} ?>>
            <label class="form-check-label">One (1) Month Pay Slip</label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="sig_id" value="1" <?php if( $sig_id == 1){echo "checked";} else{} ?>>
            <label class="form-check-label">One (1) Valid ID with Signature</label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="borrower_confo" value="1" <?php if( $borrower_confo == 1){echo "checked";} else{} ?>>
            <label class="form-check-label">Notarized Borrower's Conformity</label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="amortization" value="1" <?php if( $amortization == 1){echo "checked";} else{} ?>>
            <label class="form-check-label">Authority to Deduct Loan Amortization</label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="disinterested" value="1" <?php if( $disinterested == 1){echo "checked";} else{} ?>>
            <label class="form-check-label">Notarized Affidavit of Two Disinterested Person</label>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer justify-content-right" style="background:#f6f6f6;">
    <button type="submit" onclick="SubReqGov()" name="submitreq" class="btn btn-default color-cyan-dark" style="font-size:large; width:auto;"><i class="fa fa-paper-plane"></i>&nbsp; Submit</button>
  </div>
</form>

<script>
  function SubReqGov() {
var name = document.getElementById("disinterested").value;
// Returns successful data submission message when the entered information is stored in database.
var dataString = 'name1=' + name;

$.ajax({
type: "POST",
url: "reqGOV_process.php",
data: dataString,
cache: false,
success: function(html) {
}
});
}
</script>