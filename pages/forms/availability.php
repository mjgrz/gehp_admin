<form method="POST" action="availabilityprocess.php">
  <label class="toggle"
      <?php 
        if($projectrow['archived'] == '1'){echo 'style="pointer-events: none; opacity: 0.5;"';}
        if($projectrow['archived'] != '1'){echo 'style="pointer-events: unset; opacity: unset;"';}
      ?>>
    <input class="toggle-checkbox" type="checkbox" name="toggleAvail" id="toggleAvail" onclick="SubAvailability(event)" value="Available"
      <?php if($projectrow['availability'] == 'Available'){echo 'checked';}if($projectrow['availability'] != 'Available'){} ?>>
    <div class="toggle-switch">
    </div>
    <span class="toggle-label" hidden>Available</span>
  </label>
  
  <input type="text" name="toggleID" id="toggleID" value="<?php echo $projectrow['project_id']; ?>" hidden>
  <button type="submit" class="SubAvailBtn" name="SubAvailBtn" id="SubAvailBtn" hidden>submit</button>
</form>

<script>
  function SubAvailability(event) {
    event.currentTarget.closest('form').submit()
  }
</script>

<style>

.toggle {
  cursor: pointer;
  display: inline-block;
}

.toggle-switch {
  display: inline-block;
  background: #ccc;
  border-radius: 16px;
  width: 58px;
  height: 32px;
  position: relative;
  vertical-align: middle;
  transition: background 0.25s;
}
.toggle-switch:before, .toggle-switch:after {
  content: "";
}
.toggle-switch:before {
  display: block;
  background: linear-gradient(to bottom, #fff 0%, #eee 100%);
  border-radius: 50%;
  box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.25);
  width: 24px;
  height: 24px;
  position: absolute;
  top: 4px;
  left: 4px;
  transition: left 0.25s;
}
.toggle:hover .toggle-switch:before {
  background: linear-gradient(to bottom, #fff 0%, #fff 100%);
  box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.5);
}
.toggle-checkbox:checked + .toggle-switch {
  background: #56c080;
}
.toggle-checkbox:checked + .toggle-switch:before {
  left: 30px;
}

.toggle-checkbox {
  position: absolute;
  visibility: hidden;
}

.toggle-label {
  margin-left: 5px;
  position: relative;
  top: 2px;
}
</style>