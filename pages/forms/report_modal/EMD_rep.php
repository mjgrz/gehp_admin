<div class="modal fade" id="EMD_rep">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 5px;">
      <div class="modal-header" style="background: #67a2b2;">
        <h4 class="modal-title" style="color: white;">EMD Report</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: white;">&times;</span>
        </button>
      </div>
      <form method="POST" action="reports/EMD_report.php" target="_blank">
        <div class="modal-body">
          <div class="form-group col-md-12">
            <label for="fromdate">From:</label>
            <?php
            try{
              $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
              $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
              $selectfromdate = "SELECT (SELECT app_date FROM applications ORDER BY app_date LIMIT 1) as g_one,
                                (SELECT app_date FROM ofw_applications ORDER BY app_date LIMIT 1) as g_two";
              $sthselectfromdate = $dbh->prepare($selectfromdate);
              $sthselectfromdate->execute();
              $sthselectfromdate->setFetchMode(PDO::FETCH_ASSOC); 
              
              if($sthselectfromdate->rowCount() > 0){
                while ($rowfromdate = $sthselectfromdate->fetch(PDO::FETCH_ASSOC)) {
                  $g_one = $rowfromdate["g_one"];
                  $g_two = $rowfromdate["g_two"];
                  if($g_one < $g_two){
                    $fromdate = $g_one;
                  }
                  if($g_one > $g_two){
                    $fromdate = $g_two;
                  }
                  if($g_one == $g_two){
                    $fromdate = $g_one;
                  }
                }
                $dbh = null;
              }
            }
            catch(PDOException $e){
              error_log('PDOException - ' . $e->getMessage(), 0);
              http_response_code(500);
              die('Error establishing connection with database');
            }
            ?> 
            <?php
            try{
              $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
              $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
              $selecttodate = "SELECT (SELECT app_date FROM applications ORDER BY app_date DESC LIMIT 1) as ofw_one,
                              (SELECT app_date FROM ofw_applications ORDER BY app_date DESC LIMIT 1) as ofw_two";
              $sthselecttodate = $dbh->prepare($selecttodate);
              $sthselecttodate->execute();
              $sthselecttodate->setFetchMode(PDO::FETCH_ASSOC); 
              
              if($sthselecttodate->rowCount() > 0){
                while ($rowtodate = $sthselecttodate->fetch(PDO::FETCH_ASSOC)) {
                  $ofw_one = $rowtodate["ofw_one"];
                  $ofw_two = $rowtodate["ofw_two"];
                  if($ofw_one < $ofw_two){
                    $todate = $ofw_one;
                  }
                  if($ofw_one > $ofw_two){
                    $todate = $ofw_two;
                  }
                  if($ofw_one == $ofw_two){
                    $todate = $ofw_one;
                  }
                }
                $dbh = null;
              }
            }
            catch(PDOException $e){
              error_log('PDOException - ' . $e->getMessage(), 0);
              http_response_code(500);
              die('Error establishing connection with database');
            }
            ?> 
            <input required class="form-control" type="date" id="fromdate" name="fromdate" min="<?php echo date('Y-m-d', strtotime($fromdate)); ?>" max="<?php echo date('Y-m-d', strtotime($todate)); ?>">
          </div>
          <div class="form-group col-md-12">
            <label for="todate">To:</label>
            <input required class="form-control" type="date" id="todate" name="todate" min="<?php echo date('Y-m-d', strtotime($fromdate)); ?>" max="<?php echo date('Y-m-d', strtotime($todate)); ?>">
          </div>
        </div>
        <div class="modal-footer justify-content-right">
          <span>
            <button style="width:auto; font-size:large;" type="submit" name="emd_PDF" id="tooltip" class="btn btn-default color-red"><i class="fas fa-file-pdf"></i>&nbsp; PDF</button>
            <button style="width:auto; font-size:large;" type="submit" name="emd_Exc" id="tooltip" class="btn btn-default color-green"><i class="fas fa-file-excel"></i>&nbsp; Excel</button>
          </span>
        </div>
      </form>
    </div>
  </div>
</div>
<style>
  input:hover, select:hover, textarea:hover {
    border: 1px solid #67a2b2;
  }
</style>