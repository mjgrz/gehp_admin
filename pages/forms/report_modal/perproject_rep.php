

<div class="modal fade" id="perproject_rep">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 5px;">
      <div class="modal-header" style="background: #67a2b2;">
        <h4 class="modal-title" style="color: white;">Report of Applications - By Project</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: white;">&times;</span>
        </button>
      </div>
      <form method="POST" action="reports/perproject_rep.php" target="_blank">
        <div class="modal-body">
          <!-- /.card-header -->
          <div class="form-group col-md-12">
            <label for="project">Name of Project</label>
            <select class="form-control" name="project" id="project" required>
              <option value="">- Select Location -</option>
              <?php
              if($login_office == 'Regional'){
                $select = "SELECT * FROM table_ro_
                           LEFT JOIN users ON table_ro_.ro_id = users.office_id 
                           LEFT JOIN project_info ON table_ro_.region_id = project_info.region
                           WHERE table_ro_.ro_id = '$login_officeID'
                           ORDER BY PJ_NAME";
              }
              if($login_office == 'Head Office'){
                $select = "SELECT * FROM project_info
                           ORDER BY PJ_NAME";
              }
                $result = mysqli_query($conn,$select);
                  if(mysqli_num_rows($result) > 0){
                  $i=0;
                  while($row = mysqli_fetch_assoc($result)){
              ?>    
              
                  <option <?php if($row["PJ_NAME"] == ''){echo 'hidden';}?> value="<?php echo $row["PJ_NAME"]; ?>"><?php echo $row["PJ_NAME"]; ?></option> 

              <?php      
                  }
                }
              
              ?>
            </select>
          </div>
          <div class="form-group col-md-12">
            <label for="sort">Application Status:</label>
            <select class="form-control" name="status" id="status" required>
              <option value="">- Select Application Status -</option>
              <option value="New Applicant">New Application</option>
              <option value="Pending">Pending Application</option>
              <option value="Qualified">Qualified Application</option>
              <option value="Disqualified">Disqualified Application</option>
            </select>
          </div>
          <div class="form-group col-md-12">
            <label for="sort">Sort By:</label>
            <select class="form-control" name="sort" id="sort" required>
              <option value="">- Select Column Name -</option>
              <option value="app_date">Date of Application</option>
              <option value="LNAME">Name of Applicant</option>
            </select>
          </div>
          <div class="form-group col-md-12">
            <label for="fromdate">From:</label>
            <input class="form-control" type="date" id="fromdate" name="fromdate" max="<?php echo date('Y-m-d'); ?>">
          </div>
          <div class="form-group col-md-12">
            <label for="todate">To:</label>
            <input class="form-control" type="date" id="todate" name="todate" max="<?php echo date('Y-m-d'); ?>">
          </div>
          <!-- /.card-body -->
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-info"><i class="fas fa-external-link-alt"></i>&nbspProceed</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
