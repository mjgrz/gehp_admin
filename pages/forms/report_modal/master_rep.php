<div class="modal fade" id="master_rep">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 5px;">
      <div class="modal-header" style="background: #67a2b2;">
        <h4 class="modal-title" style="color: white;">Government Employee - Applications (PAGIBIG Masterlist)</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: white;">&times;</span>
        </button>
      </div>
      <form method="POST" action="reports/master_rep.php" target="_blank">
        <div class="modal-body">
          <div class="form-group col-md-12">
            <label for="project">Name of Project</label>
            <select class="form-control" name="project" id="project" required>
              <option value="">- Select Location -</option>
              <?php
              try{
                if($login_office == 'Regional'){
                  $select = "SELECT PJ_NAME FROM project_info WHERE region = '$reglogin_officeID' ORDER BY PJ_NAME";
                }
                if($login_office != 'Regional'){
                  $select = "SELECT PJ_NAME FROM project_info ORDER BY PJ_NAME";
                }
                $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $sthselect = $dbh->prepare($select);
                $sthselect->execute();
                $sthselect->setFetchMode(PDO::FETCH_ASSOC); 

                  if($sthselect->rowCount() > 0){
                  $i=0;
                  while ($row = $sthselect->fetch(PDO::FETCH_ASSOC)) {
              ?>    
              
                  <option value="<?php echo $row["PJ_NAME"]; ?>"><?php echo $row["PJ_NAME"]; ?></option> 

              <?php      
                  }
                  $dbh = null;
                }
              }
              catch(PDOException $e){
                error_log('PDOException - ' . $e->getMessage(), 0);
                http_response_code(500);
                die('Error establishing connection with database');
              }
              ?>
            </select>
          </div>
          <div class="form-group col-md-12">
            <label for="sort">Sort By:</label>
            <select class="form-control" name="sort" id="sort" required>
              <option value="">- Select Column Name -</option>
              <option value="LNAME">Last Name</option>
              <option value="FNAME">First Name</option>
              <option value="MNAME">Middle Name</option>
              <option value="motherSurname">Maiden Name</option>
              <option value="BDATE">Date of Birth</option>
            </select>
          </div>
          <div class="form-group col-md-12">
            <label for="fromdate">From:</label>
            <?php
            try{
              $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
              $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
              $selectfromdate = "SELECT app_date FROM applications ORDER BY app_date LIMIT 1";
              $sthselectfromdate = $dbh->prepare($selectfromdate);
              $sthselectfromdate->execute();
              $sthselectfromdate->setFetchMode(PDO::FETCH_ASSOC); 
              
              if($sthselectfromdate->rowCount() > 0){
                while ($rowfromdate = $sthselectfromdate->fetch(PDO::FETCH_ASSOC)) {
                  $fromdate = $rowfromdate["app_date"];
                }
                $dbh = null;
              }
            }
            catch(PDOException $e){
              error_log('PDOException - ' . $e->getMessage(), 0);
              http_response_code(500);
              die('Error establishing connection with database');
            }
            ?> 
            <?php
            try{
              $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
              $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
              $selecttodate = "SELECT app_date FROM applications ORDER BY app_date DESC LIMIT 1";
              $sthselecttodate = $dbh->prepare($selecttodate);
              $sthselecttodate->execute();
              $sthselecttodate->setFetchMode(PDO::FETCH_ASSOC); 
              
              if($sthselecttodate->rowCount() > 0){
                while ($rowtodate = $sthselecttodate->fetch(PDO::FETCH_ASSOC)) {
                    $todate = $rowtodate["app_date"];
                }
                $dbh = null;
              }
            }
            catch(PDOException $e){
              error_log('PDOException - ' . $e->getMessage(), 0);
              http_response_code(500);
              die('Error establishing connection with database');
            }
            ?> 
            <input class="form-control" type="date" id="fromdate" name="fromdate" min="<?php echo date('Y-m-d', strtotime($fromdate)); ?>" max="<?php echo date('Y-m-d', strtotime($todate)); ?>">
          </div>
          <div class="form-group col-md-12">
            <label for="todate">To:</label>
            <input class="form-control" type="date" id="todate" name="todate" min="<?php echo date('Y-m-d', strtotime($fromdate)); ?>" max="<?php echo date('Y-m-d', strtotime($todate)); ?>">
          </div>
        </div>
        <div class="modal-footer justify-content-right">
          <button style="width:auto; font-size:large;" type="submit" class="btn btn-default color-cyan"><i class="fas fa-external-link-alt"></i>&nbsp; Proceed</button>
        </div>
      </form>
    </div>
  </div>
</div>
<style>
  input:hover, select:hover, textarea:hover {
    border: 1px solid #67a2b2;
  }
</style>