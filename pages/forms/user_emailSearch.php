<?php
include('login_session.php');
$usermailsearchmatch = $_POST["user_email"];


if($_SESSION["validity_usernameadd"] == 0){
  echo "<script>document.getElementById('submitform').disabled = true;</script>";
  echo "<script>document.getElementById('password').disabled = true;</script>";
  echo "<script>document.getElementById('confirm_password').disabled = true;</script>";
  $validity_mail = 1;
  $_SESSION["validity_mailadd"] = $validity_mail;

  if(!empty($_POST["user_email"])) {
    try{
      $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $umailmatchquery = "SELECT username, email FROM users WHERE email=:usermailsearchmatch";
      $sthumailmatchquery = $dbh->prepare($umailmatchquery);
      $sthumailmatchquery->bindParam(':usermailsearchmatch', $usermailsearchmatch);
      
      if($sthumailmatchquery->execute()) {
        if($sthumailmatchquery->rowCount() > 0) {
          echo "<span style='color:red'> (Email already exists.)</span>";
          echo "<script>document.getElementById('email').style.border = '2px solid red';</script>";
          $dbh = null;
          $validity_mail = 0;
          $_SESSION["validity_mailadd"] = $validity_mail;
        }
        else {
          echo "<span style='color:green'> (Email available.)</span>";
          echo "<script>document.getElementById('email').style.border = '';</script>";
          echo "<script>
                if(document.getElementById('password').style.border == '2px solid red'){
                }
                else {
                }
                </script>";
          $dbh = null;
          $validity_mail = 1;
          $_SESSION["validity_mailadd"] = $validity_mail;
        }
      }
      else {
        echo "<span style='color:red'> (Invalid input.)</span>";
        $dbh = null;
      }
    }
    catch(PDOException $e){
        error_log('PDOException - ' . $e->getMessage(), 0);
        http_response_code(500);
        die('Error establishing connection with database');
    }
  }
  else{
    echo "<script>document.getElementById('email').style.border = '';</script>";
    echo "<script>document.getElementById('submitform').disabled = false;</script>";
  }
}
else{
  if(!empty($_POST["user_email"])) {
    try{
      $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $umailmatchquery = "SELECT username, email FROM users WHERE email=:usermailsearchmatch";
      $sthumailmatchquery = $dbh->prepare($umailmatchquery);
      $sthumailmatchquery->bindParam(':usermailsearchmatch', $usermailsearchmatch);
      
      if($sthumailmatchquery->execute()) {
        if($sthumailmatchquery->rowCount() > 0) {
          echo "<span style='color:red'> (Email already exists.)</span>";
          echo "<script>document.getElementById('email').style.border = '2px solid red';</script>";
          echo "<script>document.getElementById('password').disabled = true;</script>";
          echo "<script>document.getElementById('confirm_password').disabled = true;</script>";
          echo "<script>document.getElementById('submitform').disabled = true;</script>";
          $dbh = null;
          $validity_mail = 0;
          $_SESSION["validity_mailadd"] = $validity_mail;
        }
        else {
          echo "<span style='color:green'> (Email available.)</span>";
          echo "<script>document.getElementById('email').style.border = '';</script>";
          echo "<script>document.getElementById('submitform').disabled = false;</script>";
          echo "<script>document.getElementById('password').disabled = false;</script>";
          echo "<script>document.getElementById('confirm_password').disabled = false;</script>";
          echo "<script>
                if(document.getElementById('password').style.border == '2px solid red'){
                  document.getElementById('submitform').disabled = true;
                }
                else {
                }
                </script>";
          $dbh = null;
          $validity_mail = 1;
          $_SESSION["validity_mailadd"] = $validity_mail;
        }
      }
      else {
        echo "<span style='color:red'> (Invalid input.)</span>";
        $dbh = null;
      }
    }
    catch(PDOException $e){
        error_log('PDOException - ' . $e->getMessage(), 0);
        http_response_code(500);
        die('Error establishing connection with database');
    }
  }
  else{
    echo "<script>document.getElementById('email').style.border = '';</script>";
    echo "<script>document.getElementById('submitform').disabled = false;</script>";
    echo "<span style='color:red'>*Active email address required</span>";
  }
}
?>