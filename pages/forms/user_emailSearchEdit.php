<?php
include("login_session.php");
$umailEditsearchmatch = $_POST["user_email"];

if($_SESSION["validity_username"] == 0){
  echo "<script>document.getElementById('submitform').disabled = true;</script>";
  echo "<script>document.getElementById('edit_password').disabled = true;</script>";
  echo "<script>document.getElementById('edit_confirm_password').disabled = true;</script>";
  $validity_mail = 1;
  $_SESSION["validity_mail"] = $validity_mail;

  if(!empty($_POST["user_email"])) {
    try{
      $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      if($_POST['userid'] == "$login_id"){
        $umailEditmatchquery = "SELECT username, email FROM users WHERE email = :umailEditsearchmatch";
        $sthumailEditmatchquery = $dbh->prepare($umailEditmatchquery);
        $sthumailEditmatchquery->bindParam(':umailEditsearchmatch', $umailEditsearchmatch);

        if($sthumailEditmatchquery->execute()) {
          if($sthumailEditmatchquery->rowCount()>0) {
            if($_POST["user_email"] != $email_sender){
              echo "<span style='color:red'> (Email already exists.)</span>";
              echo "<script>document.getElementById('email').style.border = '2px solid red';</script>";
              $dbh = null;
              $validity_mail = 0;
              $_SESSION["validity_mail"] = $validity_mail;
            }
            if($_POST["user_email"] == $email_sender){
              echo "<span style='color:green'> (Email currently in use.)</span>";
              echo "<script>document.getElementById('email').style.border = '';</script>";
              echo "<script>
                    if(document.getElementById('email').style.border == '2px solid red'){
                    }
                    else {
                    }
                    </script>";
              $dbh = null;
              $validity_mail = 1;
              $_SESSION["validity_mailadd"] = $validity_mail;
            }
          }
          if($sthumailEditmatchquery->rowCount() == 0) {
            echo "<span style='color:green'> (Email available.)</span>";
            echo "<script>document.getElementById('email').style.border = '';</script>";
            echo "<script>
                  if(document.getElementById('email').style.border == '2px solid red'){
                  }
                  else {
                  }
                  </script>";
            $dbh = null;
          }
        }
        else 
        {
          echo "<span style='color:red'> (Invalid input.)</span>";
          $dbh = null;
          $validity_mail = 0;
          $_SESSION["validity_mail"] = $validity_mail;
        }
      }
      else 
      {
        $result2query = "SELECT user_id, email FROM users WHERE email=:umailEditsearchmatch";
        $sthresult2query = $dbh->prepare($result2query);
        $sthresult2query->bindParam(':umailEditsearchmatch', $umailEditsearchmatch);
        $sthresult2query->execute();
        $sthresult2query->setFetchMode(PDO::FETCH_ASSOC); 

        $result1query = "SELECT username, email FROM users WHERE email=:umailEditsearchmatch";
        $sthresult1query = $dbh->prepare($result1query);
        $sthresult1query->bindParam(':umailEditsearchmatch', $umailEditsearchmatch);
        $sthresult1query->execute();
        
        if($sthresult1query->execute()) {
          if($sthresult1query->rowCount() > 0) {
            while ($selectrow = $sthresult2query->fetch(PDO::FETCH_ASSOC)) {
              $user_id = $selectrow['user_id'];
            }
            if($user_id == $_POST['userid']) {
              echo "<span style='color:green'> (Email currently in use.)</span>";
              echo "<script>document.getElementById('email').style.border = '';</script>";
              echo "<script>
                    if(document.getElementById('email').style.border == '2px solid red'){
                    }
                    else {
                    }
                    </script>";
              $dbh = null;
              $validity_mail = 1;
              $_SESSION["validity_mailadd"] = $validity_mail;
            }
            else 
            {
              echo "<span style='color:red'> (Email already exists.)</span>";
              echo "<script>document.getElementById('email').style.border = '2px solid red';</script>";
              echo "<script>
                    if(document.getElementById('email').style.border == '2px solid red'){
                    }
                    else {
                    }
                    </script>";
              $dbh = null;
              $validity_mail = 0;
              $_SESSION["validity_mail"] = $validity_mail;
            }
          }
          else {
            echo "<span style='color:green'> (Email available.)</span>";
            echo "<script>document.getElementById('email').style.border = '';</script>";
            echo "<script>
                  if(document.getElementById('email').style.border == '2px solid red'){
                  }
                  else {
                  }
                  </script>";
            $dbh = null;
          }
        }
        else 
        {
          echo "<span style='color:red'> (Invalid input.)</span>";
          $dbh = null;
          $validity_mail = 0;
          $_SESSION["validity_mail"] = $validity_mail;
        }
      }
    }
    catch(PDOException $e){
        error_log('PDOException - ' . $e->getMessage(), 0);
        http_response_code(500);
        die('Error establishing connection with database');
    }
  }
  else{
    echo "<script>document.getElementById('email').style.border = '';</script>";
  }
}
else{
  $validity_mail = 1;
  $_SESSION["validity_mail"] = $validity_mail;
  if(!empty($_POST["user_email"])) {
    try{
      $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      if($_POST['userid'] == "$login_id"){
        $umailEditmatchquery = "SELECT username, email FROM users WHERE email = :umailEditsearchmatch";
        $sthumailEditmatchquery = $dbh->prepare($umailEditmatchquery);
        $sthumailEditmatchquery->bindParam(':umailEditsearchmatch', $umailEditsearchmatch);

        if($sthumailEditmatchquery->execute()) {
          if($sthumailEditmatchquery->rowCount()>0) {
            if($_POST["user_email"] != $email_sender){
              echo "<span style='color:red'> (Email already exists.)</span>";
              echo "<script>document.getElementById('email').style.border = '2px solid red';</script>";
              echo "<script>document.getElementById('submitform').disabled = true;</script>";
              $dbh = null;
              $validity_mail = 0;
              $_SESSION["validity_mail"] = $validity_mail;
            }
            if($_POST["user_email"] == $email_sender){
              echo "<span style='color:green'> (Email currently in use.)</span>";
              echo "<script>document.getElementById('email').style.border = '';</script>";
              echo "<script>document.getElementById('submitform').disabled = false;</script>";
              echo "<script>
                    if(document.getElementById('email').style.border == '2px solid red'){
                      document.getElementById('submitform').disabled = true;
                    }
                    else {
                    }
                    </script>";
              $dbh = null;
              $validity_mail = 1;
              $_SESSION["validity_mailadd"] = $validity_mail;
            }
          }
          if($sthumailEditmatchquery->rowCount() == 0) {
            echo "<span style='color:green'> (Email available.)</span>";
            echo "<script>document.getElementById('email').style.border = '';</script>";
            echo "<script>document.getElementById('submitform').disabled = false;</script>";
            echo "<script>
                  if(document.getElementById('email').style.border == '2px solid red'){
                    document.getElementById('submitform').disabled = true;
                  }
                  else {
                  }
                  </script>";
            $dbh = null;
          }
        }
        else 
        {
          echo "<span style='color:red'> (Invalid input.)</span>";
          $dbh = null;
          $validity_mail = 0;
          $_SESSION["validity_mail"] = $validity_mail;
        }
      }
      else 
      {
        $result2query = "SELECT user_id, email FROM users WHERE email=:umailEditsearchmatch";
        $sthresult2query = $dbh->prepare($result2query);
        $sthresult2query->bindParam(':umailEditsearchmatch', $umailEditsearchmatch);
        $sthresult2query->execute();
        $sthresult2query->setFetchMode(PDO::FETCH_ASSOC); 

        $result1query = "SELECT username, email FROM users WHERE email=:umailEditsearchmatch";
        $sthresult1query = $dbh->prepare($result1query);
        $sthresult1query->bindParam(':umailEditsearchmatch', $umailEditsearchmatch);
        $sthresult1query->execute();
        
        if($sthresult1query->execute()) {
          if($sthresult1query->rowCount() > 0) {
            while ($selectrow = $sthresult2query->fetch(PDO::FETCH_ASSOC)) {
              $user_id = $selectrow['user_id'];
            }
            if($user_id == $_POST['userid']) {
              echo "<span style='color:green'> (Email currently in use.)</span>";
              echo "<script>document.getElementById('email').style.border = '';</script>";
              echo "<script>document.getElementById('submitform').disabled = false;</script>";
              echo "<script>
                    if(document.getElementById('email').style.border == '2px solid red'){
                      document.getElementById('submitform').disabled = true;
                    }
                    else {
                    }
                    </script>";
              $dbh = null;
              $validity_mail = 1;
              $_SESSION["validity_mailadd"] = $validity_mail;
            }
            else 
            {
              echo "<span style='color:red'> (Email already exists.)</span>";
              echo "<script>document.getElementById('email').style.border = '2px solid red';</script>";
              echo "<script>document.getElementById('submitform').disabled = true;</script>";
              echo "<script>
                    if(document.getElementById('email').style.border == '2px solid red'){
                      document.getElementById('submitform').disabled = true;
                    }
                    else {
                    }
                    </script>";
              $dbh = null;
              $validity_mail = 0;
              $_SESSION["validity_mail"] = $validity_mail;
            }
          }
          else {
            echo "<span style='color:green'> (Email available.)</span>";
            echo "<script>document.getElementById('email').style.border = '';</script>";
            echo "<script>document.getElementById('submitform').disabled = false;</script>";
            echo "<script>
                  if(document.getElementById('email').style.border == '2px solid red'){
                    document.getElementById('submitform').disabled = true;
                  }
                  else {
                  }
                  </script>";
            $dbh = null;
          }
        }
        else 
        {
          echo "<span style='color:red'> (Invalid input.)</span>";
          $dbh = null;
          $validity_mail = 0;
          $_SESSION["validity_mail"] = $validity_mail;
        }
      }
    }
    catch(PDOException $e){
        error_log('PDOException - ' . $e->getMessage(), 0);
        http_response_code(500);
        die('Error establishing connection with database');
    }
  }
  else{
    echo "<script>document.getElementById('email').style.border = '';</script>";
    echo "<script>document.getElementById('submitform').disabled = false;</script>";
  }
}
?>