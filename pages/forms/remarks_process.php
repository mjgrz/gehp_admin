<?php
include("../../../../dbcon.php");
session_start();
date_default_timezone_set("Asia/Hong_Kong");
$time = date("h:i a");
$date = date('F d, Y', strtotime(date("Y-m-d")));
$datetime = $date." ".$time;

$APP_ID = $_POST["APP_ID"];
$usernam = $_POST["username"];
$status = $_POST["status"];
$remarks = $_POST["remarks"];
if(is_numeric($APP_ID)){
  try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sqldata = [
      ':username' => $usernam,
      ':status' => $status,
      ':remarks' => $remarks,
      ':APP_ID' => $APP_ID
    ];
    $sql = "UPDATE applications SET username = :username, app_status = :status, remarks = :remarks WHERE APP_ID = :APP_ID";
    $sthsql = $dbh->prepare($sql);
    
    if($sthsql->execute($sqldata)){
      $auditdata = [
        ':activity' => "$status GOV Application",
        ':username' => $_SESSION['login_user'],
        ':datetime' => $datetime
      ];
      $audit = "INSERT INTO audit_trail_applications (activity, username, date) VALUES (:activity, :username, :datetime)";
      $sthaudit = $dbh->prepare($audit);
      $sthaudit->execute($auditdata);

      $_SESSION["status"] = "Your data have been saved successfully.";
      header('Location: registration.php');
      $dbh = null;
    } 
    else {
      $_SESSION["error"] = "Your data were not saved.";
      header('Location: registration.php');
      $dbh = null;
    }
  }
  catch(PDOException $e){
    error_log('PDOException - ' . $e->getMessage(), 0);
    http_response_code(500);
    die('Error establishing connection with database');
  }
}
else{
  http_response_code(400);
  die('Error processing bad or malformed request');
}
?>