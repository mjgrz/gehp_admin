<?php
  session_start();
  if(isset($_POST["toggleAvail"])){
    $toggleAvail = $_POST["toggleAvail"];
  }
  if(!isset($_POST["toggleAvail"])){
    $toggleAvail = 'Unavailable';
  }
  $toggleID = $_POST["toggleID"];
  include("../../../../dbcon.php");
  try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $availTog_data = [
      ':toggleAvail' => $toggleAvail,
      ':toggleID' => $toggleID
    ];
    $availTog_sql = "UPDATE project_info SET availability = :toggleAvail WHERE project_id = :toggleID";
    $sthavailTog_sql = $dbh->prepare($availTog_sql);
    if($sthavailTog_sql->execute($availTog_data)){
      $_SESSION["status"] = "Your data have been saved successfully.";
      header('Location: ../forms/project_view.php');
      $dbh = null;
    }
    else{
      $_SESSION["error"] = "Sorry, your data were not saved.";
      header('Location: ../forms/project_view.php');
      $dbh = null;
    }
  }
  catch(PDOException $e){
    error_log('PDOException - ' . $e->getMessage(), 0);
    http_response_code(500);
    die('Error establishing connection with database');
  }
?>