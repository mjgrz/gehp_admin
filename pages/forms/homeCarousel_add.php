<?php
session_start();
date_default_timezone_set("Asia/Hong_Kong");
$time = date("h:i a");
$date = date('F d, Y', strtotime(date("Y-m-d")));
$datetime = $date." ".$time;
$target_dir = "uploads/homeCarousel/";
$target_file = $target_dir . basename("slideimage.png");
$uploadOk = 1;
$filename = "slideimage.png";
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
include('../../../../dbcon.php');
if(isset($_POST["submit"])) {
  $check = getimagesize($_FILES["slide_image"]["tmp_name"]);
  if($check !== false) {
    $_SESSION["info"] = "File is an image - " . $check["mime"] . ".";
    $uploadOk = 1;
  } else {
    $_SESSION["error"] = "File is not an image.";
    $uploadOk = 0;
  }
}

if ($_FILES["slide_image"]["size"] <= 5000000) {
  if (file_exists($target_file)) {
    unlink($target_file);
    $uploadOk = 1;
  }
}

if ($_FILES["slide_image"]["size"] > 5000000) {
  $_SESSION["error"] = "Sorry, your file is too large.";
  $uploadOk = 0;
}

if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
  $_SESSION["error"] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
  $uploadOk = 0;
}

if ($uploadOk == 0) {
  header('Location: ../forms/home.php');
} 
else 
{
  $id = 1;
  if (move_uploaded_file($_FILES["slide_image"]["tmp_name"], $target_file)) {
    if ( is_numeric($app_id) == true){
      try{
        $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sqlslidedata = [
          ':filename' => $filename,
          ':id' => $id
        ];
        $sqlslide = "UPDATE hc_background SET hc_background = :filename WHERE id = :id";
        $sthsqlslide = $dbh->prepare($sqlslide);
        if ($sthsqlslide->execute($sqlslidedata)) {
          $auditdata = [
            ':activity' => "Edited a Slide on Carousel Banner (With Image)",
            ':username' => $_SESSION['login_user'],
            ':datetime' => $datetime
          ];
          $audit = "INSERT INTO audit_trail (activity, username, date) VALUES
          (:activity, :username, :datetime)";
          $sthaudit = $dbh->prepare($audit);
          $sthaudit->execute($auditdata);
          $_SESSION["status"] = "Your data have been saved successfully.";
          header('Location: ../forms/home.php');
          $dbh = null;
        }
        else {
          $_SESSION["error"] = "Sorry, your data were not saved.";
          header('Location: ../forms/home.php');
          $dbh = null;
        }
      }
      catch(PDOException $e){
        error_log('PDOException - ' . $e->getMessage(), 0);
        http_response_code(500);
        die('Error establishing connection with database');
      }
    } 
    else{
    http_response_code(400);
    die('Error processing bad or malformed request');
    }
  } else {
    if ( is_numeric($app_id) == true){
      try{
        $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sqlslidedata = [
          ':filename' => $filename,
          ':id' => $id
        ];
        $sqlslide = "UPDATE hc_background SET hc_background = :filename WHERE id = :id";
        $sthsqlslide = $dbh->prepare($sqlslide);

        if ($sthsqlslide->execute($sqlslidedata)) {
          $auditdata = [
            ':activity' => "Edited a Slide on Carousel Banner (Without Image)",
            ':username' => $_SESSION['login_user'],
            ':datetime' => $datetime
          ];
          $audit = "INSERT INTO audit_trail (activity, username, date) VALUES (:activity, :username, :datetime)";
          $sthaudit = $dbh->prepare($audit);
          $sthaudit->execute($auditdata);
          $_SESSION["status"] = "Your data have been saved successfully.";
          header('Location: ../forms/home.php');
          $dbh = null;
        }
        else {
          $_SESSION["error"] = "Sorry, your data were not saved.";
          header('Location: ../forms/home.php');
          $dbh = null;
        }
      }
      catch(PDOException $e){
        error_log('PDOException - ' . $e->getMessage(), 0);
        http_response_code(500);
        die('Error establishing connection with database');
      }
    } 
    else{
    http_response_code(400);
    die('Error processing bad or malformed request');
    }
  }
}
?>