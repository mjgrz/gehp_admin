<?php
$target_dir = "uploads/featuredProject/";
$target_file4 = $target_dir . basename("featuredImage4.png");
$uploadOk4 = 1;
$filename4 = "featuredImage4.png";
$imageFileType4 = strtolower(pathinfo($target_file4,PATHINFO_EXTENSION));

  if(isset($_POST["submit"])) {
    
    $check4 = getimagesize($_FILES["featuredImage4"]["tmp_name"]);
    if($check4 !== false) {
      echo "File is an image - " . $check4["mime"] . ".";
      $uploadOk4 = 1;
    } else {
      echo "File is not an image.";
      $uploadOk4 = 0;
    }
  }

  if ($_FILES["featuredImage4"]["size"] > 50000000) {
    echo "Sorry, your file is too large.";
    $uploadOk4 = 0;
  }

  if($imageFileType4 != "jpg" && $imageFileType4 != "png" && $imageFileType4 != "jpeg"
  && $imageFileType4 != "gif" && $imageFileType4 != "") {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk4 = 0;
  }

  if ($uploadOk4 == 0) {
    echo "Sorry, your file was not uploaded.";
  } else {

    if (move_uploaded_file($_FILES["featuredImage4"]["tmp_name"], $target_file4)) {
    } 
    else 
    {
    }
  }
?>