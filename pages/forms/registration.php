<!DOCTYPE html>
<html lang="en">
<head>
<?php
include('login_session.php');
error_reporting(E_ERROR | E_PARSE);

?>

<link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
<link rel="stylesheet" href="../../plugins/toastr/toastr.min.css">
<link rel="stylesheet" href="../../dist/css/adminlte.min.css">

<script src="../../plugins/jquery/jquery.min.js"></script>
<script src="../../plugins/toastr/toastr.min.js"></script>

<?php
if (isset($_SESSION["status"]) && $_SESSION["status"] !='') {
  $msg = $_SESSION["status"];
  echo "<span><script type='text/javascript'>toastr.success('$msg')</script></span>";
  
  unset($_SESSION["status"]);
}
if (isset($_SESSION["error"]) && $_SESSION["error"] !='') {
  $msg = $_SESSION["error"];
  echo "<span><script type='text/javascript'>toastr.error('$msg')</script></span>";
  
  unset($_SESSION["error"]);
}
if (isset($_SESSION["info"]) && $_SESSION["info"] !='') {
  $msg = $_SESSION["info"];
  echo "<span><script type='text/javascript'>toastr.info('$msg')</script></span>";
  
  unset($_SESSION["info"]);
}
else {

}
?>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>NHA | Government Employee's Housing Program</title>
  <link rel="shortcut icon" type="image/png" href="../../dist/img/nha_logo_circle.png">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
</head>
<body class="hold-transition sidebar-mini">
  
<script language="javascript">
           var message="This function is not allowed here.";
           function clickIE4(){
                 if (event.button==2){
                     alert(message);
                     return false;
                 }
           }
           function clickNS4(e){
                if (document.layers||document.getElementById&&!document.all){
                        if (e.which==2||e.which==3){
                                  alert(message);
                                  return false;
                        }
                }
           }
           if (document.layers){
                 document.captureEvents(Event.MOUSEDOWN);
                 document.onmousedown=clickNS4;
           }
           else if (document.all&&!document.getElementById){
                 document.onmousedown=clickIE4;
           }
           document.oncontextmenu=new Function("alert(message);return false;")
</script>

<div class="wrapper">
  <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="font-weight: bold;">
    <?php include("header.php"); ?>
  </nav>

  <aside class="main-sidebar sidebar-dark-primary elevation-4" style="background: #002d32; position: fixed;">
    <a href="../../index.php" class="brand-link">
      <img src="../../dist/img/nha_logo_circle.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"><b>NHA | GEHP</b></span>
    </a>

    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <?php
          $target_dir = "uploads/users/";
          $target_file = $target_dir . basename("$login_image");
          if(file_exists($target_file)){
            echo "<img src='uploads/users/$login_image' class='img-circle elevation-3' alt='User Image' style='height: 33px;'>";
          } 
          else {
            echo "<img src='uploads/users/user.png' width='150px'>";
          }
          ?>
        </div>
        <div class="info">
          <a href="users_profile.php" class="d-block"><?php echo $_SESSION['login_user'] ?></a>
        </div>
      </div>

      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
          <li class="nav-item">
            <a href="../../index.php" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>

          <li class="nav-item menu-open">
            <a href="#" class="nav-link active" style="background: #67a2b2;">
              <i class="nav-icon fas fa-briefcase"></i>
              <p>
                Applicants
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <input type="text" class="form-control" id="sessionempty" name="sessionempty" value="" placeholder="Search" hidden>
              <li class="nav-item">
                <a href="../forms/registration.php" onclick="sessionempty()" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Government Employees</p>
                </a>
              </li>
              <li class="nav-item" <?php echo $deploy;?>>
                <a href="../forms/registration_Uni.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Uniformed Personnel</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/registration_OFW.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>OFW</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item" <?php echo $project_hide ?>>
            <a href="../forms/project_view.php" onclick="sessionempty()" class="nav-link">
              <i class="nav-icon fas fa-building"></i>
              <p>
                Projects
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $user_hide ?>>
            <a href="../forms/users.php" onclick="sessionempty()" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Users
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $element_hide ?>>
            <a href="../forms/report_gen.php" class="nav-link">
              <i class="nav-icon fas fa-scroll"></i>
              <p>
                Reports
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $pages_hide ?>>
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-clone"></i>
              <p>
                Pages
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              
              <li class="nav-item">
                <a href="../forms/home.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Home</p>
                </a>
              </li>
              <li class="nav-item" hidden>
                <a href="../forms/howToApply.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>How To Apply</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/about.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>About</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/banner.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Banner</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="../forms/bulletin.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Bulletin</p>
                </a>
              </li>
            </ul>
          </li>
          
          <li class="nav-item" <?php echo $inv_hide;?>>
            <a href="../forms/inventory.php" class="nav-link">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                Inventory
              </p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>
  <script>
    function sessionempty() {
      $("#loaderIcon").show();
          jQuery.ajax({
          url: "session_clear.php",
          data: {'sessionempty': $("#sessionempty").val()},
          type: "POST",
          success:function(data){
              $("#").html(data);
              $("#loaderIcon").hide();
          },
          error:function (){
          }
      });
    }
  </script>

  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Government Employees<span style="color: gray;"><?php //echo $CAT ?></span></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div>
        </div>
      </div>
    </section>

    <form method="POST" action="registrationDelete_process.php" id="deleteForm"><input type="text" name="URL" value="<?php echo $_SERVER['REQUEST_URI']; ?>" hidden>
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div style="justify-content:space-between; display:flex;">
                <input type="text" id="action_hide_text" value="<?php echo $action_hide; ?>" hidden>
                <a href="addGovApp.php" class="btn btn-default color-green-dark" style="width:auto; font-size:large;">
                  <i class=" fas fa-plus"></i>&nbsp; Add Applicant
                </a>
                <button <?php echo $delete_disabled; ?> <?php echo $delete_hide; ?> type="button" data-toggle="modal"
                data-target="#delete" class="btn btn-default color-red-dark-auto">
                  <i class="fa fa-trash"></i>&nbsp; Delete
                </button>
                <?php include("delete_modalGOV.php"); ?>
              </div>
              <br>
              <div class="card card-primary" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                <div class="card-header" style="background: #67a2b2;">
                  <h3 class="card-title"></h3>
                </div>
                <div class="card-body">
                  <div class="col-sm-3">
                    <div class="input-group input-group-sm" style="padding:7px 7px 0px;">
                      <input type="text" class="form-control" id="checker" name="checker" onkeyup="checksample()"
                       value="<?php echo $_SESSION["now"]; ?>" placeholder="Search: Firstname / Lastname / Project Name">
                    </div>
                    <script>
                      function checksample() {
                          $("#loaderIcon").show();
                              jQuery.ajax({
                              url: "registration_search.php",
                              data: {'checkers': $("#checker").val()},
                              type: "POST",
                              success:function(data){
                                  $("#checkxxx").html(data);
                                  $("#loaderIcon").hide();
                              },
                              error:function (){
                              }
                          });
                      }
                    </script>
                  </div>
                  <div class="col-sm-12" id="checkxxx">
                  <div style="overflow:auto; height: 59vh; padding: 7px;">
                  <table id="projectTable" class="table table-bordered table-striped" style=" border-radius: 5px;">
                    <thead>
                    <tr>
                      <th <?php echo $delete_hide; ?> style="white-space: nowrap; " class="project-actions text-center"><input id="chk_all" name="chk_all" type="checkbox"></th>
                      <th>Date Submitted</th>
                      <th>Name of Project</th>
                      <th>Applicant's Name</th>
                      <th>Address</th>
                      <th>Contact No.</th>
                      <th>Submitted Documents</th>
                      <th>Remarks</th>
                      <th>Status</th>
                      <th>User/Operator</th>
                      <th>Full Details</th>
                    </tr>
                    </thead>
                    <tbody>
                      <?php
                      if(isset($_SESSION["now"])){
                        try{
                          $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                          $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                          if($login_office == 'Regional'){
                            $selectdata = [
                              ':reglogin_officeID' => $reglogin_officeID,
                              ':checkercount' => $_SESSION["now"],
                              ':checkercount2' => $_SESSION["now"],
                              ':checkercount3' => $_SESSION["now"],
                            ];
                            $select = "SELECT * FROM applications 
                                            LEFT JOIN applicant_info ON applications.applicant_id = applicant_info.applicant_id 
                                            LEFT JOIN applicant_spouse ON applications.spouse_id = applicant_spouse.spouse_id 
                                            LEFT JOIN employment_status ON applications.employment_id = employment_status.employment_id 
                                            LEFT JOIN requirements ON applications.req_id = requirements.req_id 
                                            LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
                                            WHERE project_info.archived != '1' AND uni_rank = '' AND project_info.region = :reglogin_officeID AND LNAME LIKE CONCAT('%', :checkercount, '%') 
                                            OR project_info.archived != '1' AND uni_rank = '' AND project_info.region = :reglogin_officeID AND FNAME LIKE CONCAT('%', :checkercount2, '%')
                                            OR project_info.archived != '1' AND uni_rank = '' AND project_info.region = :reglogin_officeID AND PJ_NAME LIKE CONCAT('%', :checkercount3, '%')
                                            ORDER BY APP_ID DESC";
                            $sthselect = $dbh->prepare($select);
                            $sthselect->execute($selectdata);
                            $count = $sthselect->rowCount(); 
                            $setcount = $count;
                            
                            if (isset($_GET['pageno'])) {
                              $pageno = $_GET['pageno'];
                              //$i = $_GET['pageno'];
                              if($_GET['pageno'] == 1){
                                $i = 2;
                              }
                            } 
                            else {
                              $pageno = 1;
                              $i = 2;
                            }

                            $no_of_records_per_page = 10;
                            $offset = ($pageno-1) * $no_of_records_per_page;

                            $total_pages = ceil($count / $no_of_records_per_page);

                            $cnt=1;
                            
                            $sql2data = [
                              ':reglogin_officeID' => $reglogin_officeID,
                              ':checkercount' => $_SESSION["now"],
                              ':checkercount2' => $_SESSION["now"],
                              ':checkercount3' => $_SESSION["now"],
                            ];
                            $sql2 = "$select LIMIT $offset, $no_of_records_per_page";

                            $sthsql2 = $dbh->prepare($sql2);
                            $sthsql2->execute($sql2data);
                            $sthsql2->setFetchMode(PDO::FETCH_ASSOC); 
                            $app_count = $sthsql2->rowCount();
                            $setcount = $app_count;
                          }
                          else {
                            $selectdata = [
                              ':checkercount' => $_SESSION["now"],
                              ':checkercount2' => $_SESSION["now"],
                              ':checkercount3' => $_SESSION["now"],
                            ];
                            $select = "SELECT * FROM applications 
                                            LEFT JOIN applicant_info ON applications.applicant_id = applicant_info.applicant_id 
                                            LEFT JOIN applicant_spouse ON applications.spouse_id = applicant_spouse.spouse_id 
                                            LEFT JOIN employment_status ON applications.employment_id = employment_status.employment_id 
                                            LEFT JOIN requirements ON applications.req_id = requirements.req_id 
                                            LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
                                            WHERE project_info.archived != '1' AND uni_rank = '' AND LNAME LIKE CONCAT('%', :checkercount, '%')
                                            OR project_info.archived != '1' AND uni_rank = '' AND FNAME LIKE CONCAT('%', :checkercount2, '%')
                                            OR project_info.archived != '1' AND uni_rank = '' AND PJ_NAME LIKE CONCAT('%', :checkercount3, '%')
                                            ORDER BY APP_ID DESC";
                            $sthselect = $dbh->prepare($select);
                            $sthselect->execute($selectdata);
                            $count = $sthselect->rowCount();
                            $setcount = $count;
                            
                            if (isset($_GET['pageno'])) {
                              $pageno = $_GET['pageno'];
                              //$i = $_GET['pageno'];
                              if($_GET['pageno'] == 1){
                                $i = 2;
                              }
                            } 
                            else {
                              $pageno = 1;
                              $i = 2;
                            }

                            $no_of_records_per_page = 10;
                            $offset = ($pageno-1) * $no_of_records_per_page;

                            $total_pages = ceil($count / $no_of_records_per_page);

                            $cnt=1;
                            
                            $sql2data = [
                              ':checkercount' => $_SESSION["now"],
                              ':checkercount2' => $_SESSION["now"],
                              ':checkercount3' => $_SESSION["now"],
                            ];
                            $sql2 = "$select LIMIT $offset, $no_of_records_per_page";

                            $sthsql2 = $dbh->prepare($sql2);
                            $sthsql2->execute($sql2data);
                            $sthsql2->setFetchMode(PDO::FETCH_ASSOC); 
                            $app_count = $sthsql2->rowCount();
                            $setcount = $app_count;
                          }
                            $i=0;
                            while ($row = $sthsql2->fetch(PDO::FETCH_ASSOC)) {
                            $idreq = $row["req_id"];
                            $LNAME = $row["LNAME"];
                            $FNAME = $row["FNAME"];
                            $MNAME = $row["MNAME"];
                            $BDATE = $row["BDATE"];
                            $PJ_CODE = $row["PJ_CODE"];
                            ?>
                            <tr>
                            <td <?php echo $delete_hide; ?> class="project-actions text-center" style="vertical-align: middle;"><input name="check[]" form="deleteForm" type="checkbox" class='chkbox' value="<?php echo $row['applicant_id']; ?>"/></td>
                            <td style="vertical-align: middle;"><?php echo date('F d, Y', strtotime($row["app_date"])) ?></td>
                            <td style="vertical-align: middle;"><?php echo $row["PJ_NAME"] ?></td>
                            <td style="vertical-align: middle;"><?php echo $row["LNAME"].", ".$row["FNAME"]." ". $row["MNAME"] ?></td>
                            <td style="vertical-align: middle;"><?php echo $row["ADDR1"] ?></td>
                            <td style="vertical-align: middle;"><?php echo $row["contact"] ?></td>
                            <td style="vertical-align: middle; min-width: 400px;">
                              <div class="col-sm-12">
                                <div class="row">
                                  <div class="col-sm-6">
                                    <div class="col-sm-12">
                                      <?php if( $row["app_form"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                                      &nbsp; Application Form
                                    </div>
                                    <div class="col-sm-12">
                                      <?php if( $row["compensation"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                                      &nbsp; Certificate of Compensation
                                    </div>
                                    <div class="col-sm-12">
                                      <?php if( $row["bir"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                                      &nbsp; BIR Certified Income Tax Return
                                    </div>
                                    <div class="col-sm-12">
                                      <?php if( $row["active_service"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                                      &nbsp; Certificate of Active in Service
                                    </div>
                                    <div class="col-sm-12">
                                      <?php if( $row["birth_cert"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                                      &nbsp; Birth Certificate (Single)
                                    </div>
                                    <div class="col-sm-12">
                                      <?php if( $row["marriage"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                                      &nbsp; Marriage Certificate
                                    </div>
                                    <div class="col-sm-12">
                                      <?php if( $row["separation"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                                      &nbsp; Affidavit of Separation-in-Fact
                                    </div>
                                    <div class="col-sm-12">
                                      <?php if( $row["gov_id"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                                      &nbsp; Two (2) Government-Issued ID
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="col-sm-12">
                                      <?php if( $row["income_tax"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                                      &nbsp; Latest Income Tax Return (ITR)
                                    </div>
                                    <div class="col-sm-12">
                                      <?php if( $row["pagibig_loan"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                                      &nbsp; PAG-IBIG Housing Loan Application
                                    </div>
                                    <div class="col-sm-12">
                                      <?php if( $row["pay_slip"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                                      &nbsp; One (1) Month Pay Slip
                                    </div>
                                    <div class="col-sm-12">
                                      <?php if( $row["sig_id"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                                      &nbsp; One (1) Valid ID with Signature
                                    </div>
                                    <div class="col-sm-12">
                                      <?php if( $row["borrower_confo"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                                      &nbsp; Notarized Borrower's Conformity
                                    </div>
                                    <div class="col-sm-12">
                                      <?php if( $row["amortization"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                                      &nbsp; Authority to Deduct Loan Amortization
                                    </div>
                                    <div class="col-sm-12">
                                      <?php if( $row["disinterested"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                                      &nbsp; Notarized Affidavit of Two Disinterested Person
                                    </div>
                                  </div>
                                </div>
                                <div style="margin-top: 5px;">
                                  <a style="width: 100%;" type="button" data-a="<?php echo $row['req_id'] ?>" data-toggle="modal" href="#reqAPP" style="color: white;" class="requirements btn btn-info btn-sm">
                                    <i class="fa fa-check-square">
                                    </i>&nbsp;
                                    Update Checklist
                                  </a>
                                </div>
                              </div>
                            </td>
                            <td style="vertical-align: middle;">
                              <?php 
                                $remarks = $row["remarks"];
                                if($remarks == "") {
                                  $hideRemarks = "hidden";
                                }
                                if($remarks != "") {
                                  $hideRemarks = "";
                                }
                              ?>
                              <b <?php echo $hideRemarks ?>><?php echo $row["remarks"] ?></b>
                              <br <?php echo $hideRemarks ?>><br <?php echo $hideRemarks ?>>
                              <?php
                              try{
                                $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                                $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                $querysearchdata = [
                                  ':LNAME' => $LNAME,
                                  ':FNAME' => $FNAME,
                                  ':MNAME' => $MNAME,
                                  ':BDATE' => $BDATE,
                                  ':PJ_CODE' => $PJ_CODE
                                ];
                                $querysearch = "SELECT FNAME, project_info.PJ_NAME, table_ro_.RO FROM applications 
                                                LEFT JOIN applicant_info ON applications.applicant_id = applicant_info.applicant_id
                                                LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
                                                LEFT JOIN table_ro_ ON project_info.region = table_ro_.region_id
                                                LEFT JOIN employment_status ON applications.employment_id = employment_status.employment_id
                                                WHERE project_info.archived != '1' AND uni_rank = '' AND LNAME = :LNAME AND FNAME = :FNAME
                                                AND MNAME = :MNAME AND BDATE = :BDATE AND applications.PJ_CODE != :PJ_CODE";
                                $sthquerysearch = $dbh->prepare($querysearch);
                                $sthquerysearch->execute($querysearchdata);
                                $sthquerysearch->setFetchMode(PDO::FETCH_ASSOC); 
                                $app_counts = $sthquerysearch->rowCount();
                                if($app_counts>0) {

                                  echo "<i>";
                                  echo "has other application(s) in:";
                                  echo "<br>";

                                  for($i=0;$i<$app_count;$i++){
                                    $select = $sthquerysearch->fetch(PDO::FETCH_ASSOC);
                                    $list = $select["PJ_NAME"];
                                    echo "<p>".$list."</p>";
                                    
                                  }
                                  echo "</i>";
                                  $dbh = null;
                                }
                                else {
                                  echo "<i>";
                                  echo "has no other application(s)";
                                  echo "</i>";
                                  $dbh = null;
                                }
                              }
                              catch(PDOException $e){
                                  error_log('PDOException - ' . $e->getMessage(), 0);
                                  http_response_code(500);
                                  die('Error establishing connection with database');
                              }
                              ?>
                            </td>
                              <?php 
                                if($row["app_status"] == "Pending"){
                                  echo "<td style='color: white; vertical-align: middle;'><span style='text-align: center; background: orange; padding: 3px 6px; border-radius: 5px;'><b>";
                                  echo $row["app_status"];
                                  echo"</b></span></td>";
                                }
                                if($row["app_status"] == "Qualified"){
                                  echo "<td style='color: white; vertical-align: middle;'><span style='text-align: center; background: green; padding: 3px 6px; border-radius: 5px;'><b>";
                                  echo $row["app_status"];
                                  echo"</b></span></td>";
                                }
                                if($row["app_status"] == "Disqualified"){
                                  echo "<td style='color: white; vertical-align: middle;'><span style='text-align: center; background: red; padding: 3px 6px; border-radius: 5px;'><b>";
                                  echo $row["app_status"];
                                  echo"</b></span></td>";
                                }
                                if($row["app_status"] == "New Applicant") 
                                {
                                  echo "<td style='vertical-align: middle;'><b>";
                                  echo $row["app_status"];
                                  echo"</b></td>";
                                }
                              ?>
                            <td style="vertical-align: middle; text-align: center;">
                              <a class="modal-user" data-toggle="modal" data-a="<?php echo $row['username'] ?>" href="#modal_user"><?php echo $row["username"] ?></a>
                              <style>
                                .modal-user{
                                  cursor: pointer;
                                }
                              </style>
                            </td>
                            <td class="project-actions text-center" style="vertical-align: middle; white-space: nowrap;">
                              <button id="tooltip" type="button" data-a="<?php echo $row['APP_ID'] ?>"
                              data-toggle='modal' data-keyboard='false' href='#modalview' style="font-size:large;" class="btn_reg_modal btn btn-default btn-sm color-blue-dark">
                                <i class="fa fa-eye">
                                </i>
                                <span class="tooltiptext">View</span>
                              </button>
                            </td>
                            </tr>
                            <?php 
                            $i++;
                          } 
                          $dbh = null;
                        }
                        catch(PDOException $e){
                            error_log('PDOException - ' . $e->getMessage(), 0);
                            http_response_code(500);
                            die('Error establishing connection with database');
                        }
                      }
                      ?>
                    </form>
                    </tbody>
                  </table>
                  </div>
                  <?php include("paging.php"); ?>
                  <?php include("delete_script.php");?>
                  <?php include("requirements_modal.php"); ?>
                  <?php include("disqualified_modal_alt.php"); ?>
                  <?php include("qualified_modal_alt.php"); ?>
                  <?php include("remarks_modal_alt.php"); ?>
                  <?php include("users_modal.php"); ?>
                  <?php include("registration_modal_alt.php"); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
  </div>
  <footer class="main-footer">
    <?php include('footer.php'); ?>
  </footer>

  <aside class="control-sidebar control-sidebar-dark">
  </aside>
</div>
</body>
</html>


<style>
  .content {
    -webkit-animation-duration: 0.7s;
    animation-duration: 0.7s;
  }
  
  @-webkit-keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  
  @keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  
  .content {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
  }

  td {
    background: #f5f5f5;
  }
  th {
    cursor: pointer;
  }
</style>

<script>
  $('.btn_reg_modal').click(function(){
      var ID=$(this).attr('data-a');
      acH = document.getElementById("action_hide_text").value;
      $.ajax({url:"registration_modal_result?ID="+ID+"&acH="+acH,cache:false,success:function(result){
          $(".registration-modal").html(result);
      }});
  });

  $('.modal-user').click(function(){
      var username=$(this).attr('data-a');
      $.ajax({url:"users_modal_result?username="+username,cache:false,success:function(result){
          $(".users-modal").html(result);
      }});
  });

  $('.requirements').click(function(){
      var req_id=$(this).attr('data-a');
      $.ajax({url:"reqGOV_modal_result?req_id="+req_id,cache:false,success:function(result){
          $(".req-modal").html(result);
      }});
  });
</script>

<script src="../../plugins/jquery/jquery.min.js"></script>
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script src="../../dist/js/adminlte.min.js"></script>
<script src="../../dist/js/demo.js"></script>

<script>
  $(function () {
    $("#projectTable").DataTable({
      "paging": false,
      "lengthChange": true,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": false,
      "responsive": false,
      //"buttons": ["excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#projectTable_wrapper .col-md-6:eq(0)');
  });

  
</script>

