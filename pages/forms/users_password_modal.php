<div class="modal fade" id="password">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 5px;">
      <div class="modal-header" style="background: #67a2b2;">
        <h4 class="modal-title" style="color: white;">Change Password</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: white;">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!--Form for editing existing account password-->
      <form method="POST" action="users_passwordEdit.php">
        <div class="modal-body">
          <div class="col-sm-12">
            <div class="col-lg-12">
              <div class="form-group">
                <label for="new_password">Old Password</label>&nbsp&nbsp&nbsp<span id="old_pass-status"></span>
                <input class="form-control" type="password" name="old_password" id="old_password" onkeyup="checkOldpass()" maxlength="20"
                oninput="this.value = this.value.replace(/[^0-9 a-z A-Z@_.]/g, '').replace(/(\*)\./g, '$1');" required>
              </div>
              <div class="form-group">
                <label for="new_password">New Password</label>&nbsp&nbsp&nbsp
                <input class="form-control" type="password" name="new_password" id="new_password" onkeyup="validate()" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@_.]).{8,}" maxlength="20" 
                        title="Must contain at least one number, one uppercase and lowercase letter, one special character such as @, _ , and at least 8 or more characters"
                        oninput="this.value = this.value.replace(/[^0-9 a-z A-Z@_.]/g, '').replace(/(\*)\./g, '$1');" required disabled>
              </div>
              <div class="form-group">
                <label for="validate">Confirm Password</label>
                <input class="form-control" type="password" name="confirm_password" id="confirm_password" onkeyup="validate()" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@_.]).{8,}" maxlength="20" 
                        title="Must contain at least one number, one uppercase and lowercase letter, one special character such as @, _ , and at least 8 or more characters"
                        oninput="this.value = this.value.replace(/[^0-9 a-z A-Z@_.]/g, '').replace(/(\*)\./g, '$1');" required disabled>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <span id="password-check-status"></span>
          <button type="submit" id="submitform" class="btn btn-default color-cyan" style="font-size:large; width:auto;" disabled><i class="fa fa-paper-plane"></i>&nbsp; Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php include("userOldpass_script.php"); ?>

<script>
  function validate(){

    var a = document.getElementById("new_password").value;
    var b = document.getElementById("confirm_password").value;
    if(a=='' && b==''){
      document.getElementById('submitform').disabled = true;
      document.getElementById('password-check-status').innerText = '';
      document.getElementById('new_password').style.border = '';
      document.getElementById('confirm_password').style.border = '';
    }
    else if(b==''){
      document.getElementById('submitform').disabled = true;
      document.getElementById('password-check-status').innerText = '';
      document.getElementById('new_password').style.border = '';
      document.getElementById('confirm_password').style.border = '';
    }
    else if(a==''){
      document.getElementById('submitform').disabled = true;
      document.getElementById('password-check-status').innerText = '';
      document.getElementById('new_password').style.border = '';
      document.getElementById('confirm_password').style.border = '';
    }
    else {
      if (a!=b) {
        document.getElementById('submitform').disabled = true;
        document.getElementById('password-check-status').innerText = 'Password did not match!';
        document.getElementById('password-check-status').style.color = 'red';
        document.getElementById('new_password').style.border = '2px solid red';
        document.getElementById('confirm_password').style.border = '2px solid red';
      }
      else {
        document.getElementById('submitform').disabled = false;
        document.getElementById('password-check-status').innerText = '';
        document.getElementById('new_password').style.border = '';
        document.getElementById('confirm_password').style.border = '';
      }
    } 
  }
</script>

<style>
  input:hover, select:hover, textarea:hover {
    border: 1px solid #67a2b2;
  }
</style>