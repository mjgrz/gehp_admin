<?php
include("../../../../dbcon.php");
$applyid = 2;
if (is_numeric($applyid) == true){
  try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $bannerselect2 = "SELECT * FROM banner where id = :applyid";
    $sthbannerselect2 = $dbh->prepare($bannerselect2);
    $sthbannerselect2->bindParam(':applyid', $applyid);
    $sthbannerselect2->execute();
    $sthbannerselect2->setFetchMode(PDO::FETCH_ASSOC); 
    if($sthbannerselect2->rowCount() > 0){
      while ($bannerrow2 = $sthbannerselect2->fetch(PDO::FETCH_ASSOC)) {
        $banner_title2 = $bannerrow2['banner_title'];
        $banner_desc2 = $bannerrow2['banner_desc'];
        $banner_image2 = $bannerrow2['banner_image'];
      } 
      $dbh = null;
    }
    else
    {
      $dbh = null;
    }
  }
  catch(PDOException $e){
    error_log('PDOException - ' . $e->getMessage(), 0);
    http_response_code(500);
    die('Error establishing connection with database');
  }
} 
else{
http_response_code(400);
die('Error processing bad or malformed request');
}
?>
<div class="card card-primary" style="box-shadow: none; border: 1px solid #67a2b2;">
  <!--Submit form for the page of how to apply-->
  <form method="POST" action="apply_page_update.php" enctype="multipart/form-data">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-4">
          <div class="form-group">
            <label for="apply_banner">Title</label>
            <input type="text" class="form-control" id="apply_banner" name="apply_banner"
            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $banner_title2 ?>">
          </div>
          <div class="form-group">
            <label for="apply_desc">Description</label>
            <textarea class="form-control" id="apply_desc" name="apply_desc" style="resize: none; padding: 10px;" rows="10"
            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $banner_desc2 ?>"><?php echo $banner_desc2 ?></textarea>
          </div>
        </div>

        <input type="text" class="form-control" id="app_uploadImage_text" name="app_uploadImage_text" value="<?php echo $banner_image2 ?>" hidden>
        <div class="col-sm-8">
          <div class="col-md-12" style="margin-top: 32px;">
            <div class="col-md-12">
              <div class="form-group" id="app_uploadDiv">
                <input type="file" id="app_uploadImage" name="app_uploadImage" title="Click in this area to upload an image." onchange="previewupdate_apply()">
                <p id="app_uploadText"></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="card-footer">
      <div class="project-actions text-right">
        <button style="width:auto; font-size:large;" type="submit" class="btn btn-default color-cyan-dark"><i class="fa fa-paper-plane"></i>&nbsp; Submit</button>
      </div>
    </div>
  </form>
  <!--End of Submit form for the page of how to apply-->
</div>
<script>
$(function () {
  bsCustomFileInput.init();
});
</script>
<style>
  #app_uploadDiv {
    width: 100%;
    height: 348px;
    border: 2px dashed #67a2b2;
    border-radius: 5px;
  }
  #app_uploadImage {
    position: absolute;
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    outline: none;
    opacity: 0;
  }
  #app_uploadImage:hover {
    cursor: pointer;
  }
  #app_uploadText {
    text-align: center;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 5px;
  }
  #app_uploadText {
    background-image: url(uploads/banner/<?php echo $banner_image2 ?>);
    background-position: center;
    background-repeat: no-repeat;
    background-size: 100% auto;
  }
  .content {
    -webkit-animation-duration: 0.7s;
    animation-duration: 0.7s;
  }
  @-webkit-keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  @keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  .content {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
  }
  input:hover, select:hover, textarea:hover {
    border: 1px solid #67a2b2;
  }
</style>
<script>
  function previewupdate_apply() {
    var urlimage = URL.createObjectURL(event.target.files[0]);
    document.getElementById("app_uploadText").style.backgroundImage = "url("+urlimage+")";
    document.getElementById("app_uploadText").style.backgroundSize = "100% auto";
    document.getElementById("app_uploadText").style.backgroundRepeat = "no-repeat";
    document.getElementById("app_uploadText").style.backgroundPosition = "center";
    document.getElementById("app_uploadText").innerHTML = "";
  }
</script>