
<?php
session_start();
include("../../../../dbcon.php");
date_default_timezone_set("Asia/Hong_Kong");
$time = date("h:i a");
$date = date('F d, Y', strtotime(date("Y-m-d")));
$datetime = $date." ".$time;
//$URL = $_POST["URL"];

error_reporting(E_ERROR | E_PARSE);
  $req_id = $_POST["req_id"];

  $check1 = $_POST["app_form"];
  if($check1 == "") {
    $app_form = "0";
  }
  if($check1 != "") {
    $app_form = "1";
  }

  $check2 = $_POST["compensation"];
  if($check2 == "") {
    $compensation = "0";
  }
  if($check2 != "") {
    $compensation = "1";
  }

  $check3 = $_POST["bir"];
  if($check3 == "") {
    $bir = "0";
  }
  if($check3 != "") {
    $bir = "1";
  }

  $check4 = $_POST["active_service"];
  if($check4 == "") {
    $active_service = "0";
  }
  if($check4 != "") {
    $active_service = "1";
  }

  $check5 = $_POST["birth_cert"];
  if($check5 == "") {
    $birth_cert = "0";
  }
  if($check5 != "") {
    $birth_cert = "1";
  }

  $check6 = $_POST["marriage"];
  if($check6 == "") {
    $marriage = "0";
  }
  if($check6 != "") {
    $marriage = "1";
  }

  $check7 = $_POST["separation"];
  if($check7 == "") {
    $separation = "0";
  }
  if($check7 != "") {
    $separation = "1";
  }

  $check8 = $_POST["gov_id"];
  if($check8 == "") {
    $gov_id = "0";
  }
  if($check8 != "") {
    $gov_id = "1";
  }

  $check9 = $_POST["income_tax"];
  if($check9 == "") {
    $income_tax = "0";
  }
  if($check9 != "") {
    $income_tax = "1";
  }

  $check10 = $_POST["pagibig_loan"];
  if($check10 == "") {
    $pagibig_loan = "0";
  }
  if($check10 != "") {
    $pagibig_loan = "1";
  }

  $check11 = $_POST["pay_slip"];
  if($check11 == "") {
    $pay_slip = "0";
  }
  if($check11 != "") {
    $pay_slip = "1";
  }

  $check12 = $_POST["sig_id"];
  if($check12 == "") {
    $sig_id = "0";
  }
  if($check12 != "") {
    $sig_id = "1";
  }

  $check13 = $_POST["borrower_confo"];
  if($check13 == "") {
    $borrower_confo = "0";
  }
  if($check13 != "") {
    $borrower_confo = "1";
  }

  $check14 = $_POST["amortization"];
  if($check14 == "") {
    $amortization = "0";
  }
  if($check14 != "") {
    $amortization = "1";
  }

  $check15 = $_POST["disinterested"];
  if($check15 == "") {
    $disinterested = "0";
  }
  if($check15 != "") {
    $disinterested = "1";
  }

  if(is_numeric($req_id)){
    try{
      $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $sqlreqdata = [
        ':app_form' => $app_form,
        ':compensation' => $compensation,
        ':bir' => $bir,
        ':active_service' => $active_service,
        ':birth_cert' => $birth_cert,
        ':marriage' => $marriage,
        ':separation' => $separation,
        ':gov_id' => $gov_id,
        ':income_tax' => $income_tax,
        ':pagibig_loan' => $pagibig_loan,
        ':pay_slip' => $pay_slip,
        ':sig_id' => $sig_id,
        ':borrower_confo' => $borrower_confo,
        ':amortization' => $amortization,
        ':disinterested' => $disinterested,
        ':req_id' => $req_id
      ];

      $sqlreq = "UPDATE requirements_ofw SET app_form = :app_form, compensation = :compensation, bir = :bir, active_service = :active_service, 
      birth_cert = :birth_cert, marriage = :marriage, separation = :separation, gov_id = :gov_id, income_tax = :income_tax, 
      pagibig_loan = :pagibig_loan, pay_slip = :pay_slip, sig_id = :sig_id, borrower_confo = :borrower_confo, 
      amortization = :amortization, disinterested = :disinterested WHERE req_id = :req_id";
      $sthsqlreq = $dbh->prepare($sqlreq);

      if($sthsqlreq->execute($sqlreqdata)){
        $auditdata = [
          ':activity' => "Updated the Requirements $req_id (OFW Application)",
          ':username' => $_SESSION['login_user'],
          ':datetime' => $datetime
        ];
        $audit = "INSERT INTO audit_trail_applications (activity, username, date) VALUES (:activity, :username, :datetime)";
        $sthaudit = $dbh->prepare($audit);
        $sthaudit->execute($auditdata);
        $_SESSION["status"] = "Your data have been saved successfully.";
        header('Location: registration_OFW.php');
        $dbh = null;
      } 
      else {
        $_SESSION["error"] = "Sorry, your data were not saved.";
        header('Location: registration_OFW.php');
        $dbh = null;
      }
    }
    catch(PDOException $e){
        error_log('PDOException - ' . $e->getMessage(), 0);
        http_response_code(500);
        die('Error establishing connection with database');
    }
  }
  if(!is_numeric($req_id)){
    http_response_code(400);
    die('Error processing bad or malformed request');
  }
?>