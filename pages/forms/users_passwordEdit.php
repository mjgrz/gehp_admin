<?php
  include('login_session.php');
  include("../../../../dbcon.php");
  $password = $_POST["new_password"];  
  $hashed_password = password_hash($password, PASSWORD_DEFAULT);
  if(is_numeric($login_id)){
    try{
      $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $sqluser = "UPDATE users SET user_password = '$hashed_password' WHERE user_id = :login_id";
      $stheditpass = $dbh->prepare($sqluser);
      $stheditpass->bindParam(':login_id', $login_id);
      if ($stheditpass->execute()) {
          $_SESSION["status"] = "Your password has been changed successfully.";
          header('Location: users_profile.php');
          $dbh = null;
      }
      else {
          $_SESSION["error"] = "Sorry, unable to change your password.";
          header('Location: users_profile.php');
          $dbh = null;
      }
    }
    catch(PDOException $e){
        error_log('PDOException - ' . $e->getMessage(), 0);
        http_response_code(500);
        die('Error establishing connection with database');
    }
  }
  else{
    http_response_code(400);
    die('Error processing bad or malformed request');
  }
?>