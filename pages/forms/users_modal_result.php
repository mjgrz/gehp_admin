
<?php

$username_modal = $_GET['username'];
include("../../../../dbcon.php");

?>
<?php
  try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $user_select_modal = "SELECT * FROM users WHERE username = :username_modal";
    $sthusermodal = $dbh->prepare($user_select_modal);
    $sthusermodal->bindParam(':username_modal', $username_modal);
    $sthusermodal->execute();
    $sthusermodal->setFetchMode(PDO::FETCH_ASSOC); 
    if($sthusermodal->rowCount() > 0){
      while ($user_modal = $sthusermodal->fetch(PDO::FETCH_ASSOC)) {
        $user_image = $user_modal["user_image"];
        $username_modal = $user_modal["username"];
        $userlevel_modal = $user_modal["userlevel"];
        $office_modal = $user_modal["office"];
        $office_id_modal = $user_modal["office_id"];
        $lastname_modal = $user_modal["lastname"];
        $firstname_modal = $user_modal["firstname"];
        $middlename_modal = $user_modal["middlename"];
        $suffix_modal = $user_modal["suffix"];
        $status_modal = $user_modal["status"];
      }
      $dbh = null;
    }
    else{
      $user_image = "user.png";
      $username_modal = "";
      $userlevel_modal = "";
      $office_modal = "";
      $office_id_modal = "";
      $lastname_modal = "";
      $firstname_modal = "";
      $middlename_modal = "";
      $suffix_modal = "";
      $status_modal = "";
    }
  }
  catch(PDOException $e){
      error_log('PDOException - ' . $e->getMessage(), 0);
      http_response_code(500);
      die('Error establishing connection with database');
  }
?>
      <div class="modal-header" style="background: #67a2b2;">
        <h4 class="modal-title" style="color: white;"><?php echo $status_modal; ?> User</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: white;">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="col-sm-12">
          <div class="col-sm-12" style="height: 350px; padding: 5px; max-height: 350px; min-height: 350px;">
            <?php
            $target_dir = "uploads/users/";
            $target_file = $target_dir . basename("$user_image");
            if(file_exists($target_file)){
              $profilepicmodal = $user_image;
            } 
            else {
              $profilepicmodal = 'user.png';
            }
            ?>
            <p id="profilepicmodal"></p>
            <style>
              #profilepicmodal{
                background-image: url(uploads/users/<?php echo $profilepicmodal ?>);
                background-position: center;
                background-repeat: no-repeat;
                background-size:contain;
                border: 2px solid #67a2b2;
                border-radius: 5px;
                text-align: center;
                height: 100%;
                width: 100%;
                display: flex;
                justify-content: center;
                align-items: center;
              }
            </style>
          </div>
          
          <div class="col-sm-12">
            <span style="font-size: 40px;"><b><?php echo $username_modal ?></b></span>
            <p style="font-size: 20px;"><?php echo $userlevel_modal ?></p>
            <hr>
            <label for="name" style="font-size: 20px;">Name:</label>
            <div>
              <?php
                if($lastname_modal == '' && $firstname_modal == ''){
                  echo "Information unavailable.";
                  $hideinfo = "hidden";
                }
                if($lastname_modal != '' && $firstname_modal != ''){
                  $hideinfo = "";
                }
              ?>
              <p <?php echo $hideinfo; ?> id="name" style="font-size: 20px;"><?php echo $lastname_modal ?><?php if($suffix_modal == 'N/A' || $suffix_modal== ''){echo '';} else{echo ' '.$suffix_modal;}?>, <?php echo $firstname_modal ?> <?php echo $middlename_modal ?></p>
            </div>
            <hr>
            <label for="ro_id" style="font-size: 20px;">Location:</label>
            <div>
              <?php
                if($office_modal == ''){
                  echo "Information unavailable.";
                  $hideinfo2 = "hidden";
                }
                if($office_modal != ''){
                  $hideinfo2 = "";
                }
              ?>
              <p <?php echo $hideinfo2; ?> id="ro_id" style="font-size: 20px;">
              <?php
                echo $office_modal;
              ?>
              </p>
            </div>
            <hr>
            <label <?php echo $hideinfo2; ?> for="do_id" style="font-size: 20px;">
              <?php
                if($office_modal == "Regional"){
                  echo "Region Office:";
                }
                if($office_modal == "Head Office"){
                  echo "Department:";
                }
              ?>
            </label>
            <div <?php echo $hideinfo2; ?>>
              <p id="do_id" style="font-size: 20px;">
              <?php
              include("../../../../dbcon.php");
              try{
                $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                if($office_modal == "Regional"){
                    
                  $office_idselect = "SELECT RO FROM table_ro_ WHERE ro_id = :office_id_modal";
                  $sthoffice_idselect = $dbh->prepare($office_idselect);
                  $sthoffice_idselect->bindParam(':office_id_modal', $office_id_modal);
                  $sthoffice_idselect->execute();
                  $sthoffice_idselect->setFetchMode(PDO::FETCH_ASSOC); 
                  $office_idresultcount=$sthoffice_idselect->rowCount();
                  if($office_idresultcount > 0){
                    while ($office_idrow = $sthoffice_idselect->fetch(PDO::FETCH_ASSOC)) {
                      echo $office_idrow['RO'];
                    } 
                    $dbh = null;
                  }
                  else{}
                }
                if($office_modal == "Head Office"){
                    
                  $depselect = "SELECT description FROM department WHERE office_id = :office_id_modal";
                  $sthdepselect = $dbh->prepare($depselect);
                  $sthdepselect->bindParam(':office_id_modal', $office_id_modal);
                  $sthdepselect->execute();
                  $sthdepselect->setFetchMode(PDO::FETCH_ASSOC); 
                  $depresultcount=$sthdepselect->rowCount();
                  if($depresultcount > 0){
                    while ($deprow = $sthdepselect->fetch(PDO::FETCH_ASSOC)) {
                      echo $deprow['description'];
                    } 
                  }
                  else{}
                }
              }
              catch(PDOException $e){
                  error_log('PDOException - ' . $e->getMessage(), 0);
                  http_response_code(500);
                  die('Error establishing connection with database');
              }
              ?>
              </p>
            </div>
          </div>
        </div>
      </div>