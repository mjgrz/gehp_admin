<?php
  session_start();
  date_default_timezone_set("Asia/Hong_Kong");
  $time = date("h:i a");
  $date = date('F d, Y', strtotime(date("Y-m-d")));
  $datetime = $date." ".$time;

  include('../../../../dbcon.php');
  if(isset($_POST['deleteBtn'])){
    if(isset($_POST['check'])) {
      $checkbox = $_POST['check'];
      for($i=0;$i<count($checkbox);$i++){
        try{
          $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
          $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $del_id = $checkbox[$i]; 
          if(is_numeric($del_id)){
            $userdeletequery = "DELETE FROM users WHERE user_id = :del_id";
            $sthuserdelete = $dbh->prepare($userdeletequery);
            $sthuserdelete->bindParam(':del_id', $del_id);
            
            if($sthuserdelete->execute()){
                $auditdata = [
                  ':activity' => "User(s) Deleted",
                  ':username' => $_SESSION['login_user'],
                  ':datetime' => $datetime
                ];
                $audit = "INSERT INTO audit_trail (activity, username, date) VALUES (:activity, :username, :datetime)";
                $sthaudit = $dbh->prepare($audit);
                $sthaudit->execute($auditdata);
              
                $_SESSION["status"] = "Record(s) have been deleted successfully.";
                header('Location: users.php');
                $dbh = null;
            }
            else{
                $_SESSION["error"] = "Sorry, record(s) were not deleted.";
                header('Location: ../../pages/forms/users.php');
                $dbh = null;
            }
          }
          else{
            http_response_code(400);
            die('Error processing bad or malformed request');
            $dbh = null;
          }
        }
        catch(PDOException $e){
            error_log('PDOException - ' . $e->getMessage(), 0);
            http_response_code(500);
            die('Error establishing connection with database');
        }
      }
    }
    if(!isset($_POST['check'])) {
      $_SESSION["info"] = "Sorry, no record(s) to be deleted.";
      header('Location: ../../pages/forms/users.php');
    }
  }
  else{
    header('Location: ../../pages/forms/users.php');
  }
?>