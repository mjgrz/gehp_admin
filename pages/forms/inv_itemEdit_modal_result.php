<?php
  $EDITID = $SLCT_PJID;
  $EDITFLR = $SLCT_FLOOR;
  $EDITSTR = $slot;
  $EDITPJNAME = $name;
?>
<div class="modal-header" style="background:#67a2b2;">
  <h4 class="modal-title" style="color:white;">Edit Map Inventory - <?php echo $EDITPJNAME ?> (Floor <?php echo $EDITFLR; ?>)</h4>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true" style="color: white;">&times;</span>
  </button>
</div>
<form method="POST" action="inv_edit_slots.php">
  <div class="modal-body" style="background: #f6f6f6;">
    <div class="col-sm-12">

      <div class="col-lg-12">
        <div class="row">
          <div class="col-lg-3 mt-3">
            <input type="checkbox" id="checkMap" name="checkMap" onclick="check()">&nbsp;
            <label>Use Current Built Map Image: </label>
            <div class="input-group">
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="builtMapFile" name="builtMapFile" required>
                <label class="custom-file-label" for="exampleInputFile">Choose file</label>
              </div>
              <script type="text/javascript">
                $(document).ready(function () {
                  bsCustomFileInput.init();
                });
              </script>
            </div>
          </div>
          <div class="col-lg-9 mt-3" hidden>
            <label>list: </label>
            <textarea class="form-control" id="listEditTxt" name="listEditTxt"></textarea>
            <input type="text" id="editID" name="editID" value="<?php echo $EDITID ?>">
            <input type="text" id="editFLR" name="editFLR" value="<?php echo $EDITFLR ?>">
          </div>
        </div>
      </div>

      <hr>

      <div class="col-lg-12 mt-3">
        <div class="row" id="nextEdit">

        </div>
        <div class="col-12 mt-2 pr-3">
          <div class="row" style="justify-content:space-between;">
            <div>
              <button class="btn btn-default color-green-dark" type="button" id="addInput" style="width:auto;"
              onclick="addEdit()">Add Block</button>
              <button class="btn btn-default color-red-dark" type="button" id="removeInputEdit" style="width:auto;"
              onclick="removeEdit()" hidden>Remove Last Block</button>
            </div>
            <div>
              <!-- <button class="btn btn-default color-cyan-dark" type="button" id="preview" style="width:auto;"
              onclick="showEdit()">View</button> -->
            </div>
          </div>
        </div>
      </div>

      <div class="col-12" id="table">
        <div style="overflow:auto; height: auto; padding:15px 10px;">
          <table style="border-radius:5px; margin:auto;">
            <thead>
              <tr id="theaderEdit">
              </tr>
            </thead>
            <tbody id="tobodyEdit">
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  
  <div class="modal-footer justify-content-right">
    <button type="submit" class="btn btn-default color-cyan"
    style="font-size:large; width:auto;">
    <i class="fa fa-paper-plane"></i>&nbsp; Submit</button>
  </div>
</form>

<script>
  var editstr = '<?php echo $EDITSTR; ?>';
  var strdisplay = JSON.parse(editstr)
  document.getElementById('listEditTxt').value = editstr;
  length  = strdisplay.length;
  var i = strdisplay.length;;
  var el;
  var editNumlot = [];
  for(var tb=1;tb<=strdisplay.length;tb++){
    editNumlot.push(strdisplay[tb-1].items.length);
  }
  var edithightNumlot = Math.max(...editNumlot);
  for(var tb=1;tb<=strdisplay.length;tb++){
    if(strdisplay.length > 1){
      document.getElementById('removeInputEdit').hidden = false;
    }
    for(var th=0;th<=edithightNumlot;th++) {
      if(tb === 1){
        if(th === 0){
          var header = $('#theaderEdit').append(
            '<th class="text-center theadEdit">'+
              '<div style="border-radius:5px; background-color:lightgray; width:102px;" class="btn btn-default btn-md">'+
                'BLOCK/LOT'+
              '</div>'+
            '</th>'
          );
        }
        if(th != 0){
          var header = $('#theaderEdit').append(
            '<th class="text-center theadEdit" style="padding:4px; cursor:default;">'+
              '<div style="border-radius:5px; background-color:lightgray; width:75px;" class="btn btn-default btn-md">'+
                th
              +'</div>'+
            '</th>'
          );
        }
      }
      else{}
    };
    var body = $('#tobodyEdit').append(
      '<tr class="tbodyrowEdit" id="tbodyrowEdit'+tb+'">'+
        '<td class="text-center">'+
          '<div style="border-radius:5px; background-color:lightgray; width:102px; height:41px; cursor:default;" class="btn btn-default btn-md">'+
            tb
          +'</div>'+
        '</td>'+
      '</tr>'
    );
    for(var th=1;th<=editNumlot[tb-1];th++) {
      var classNameStrEdit = "btn btn-info btn-md";
      if(strdisplay[tb-1].items[th-1].prop === "Available"){
        classNameStrEdit = "btn btn-info btn-md";
      }
      if(strdisplay[tb-1].items[th-1].prop === "Awarded"){
        classNameStrEdit = "btn btn-success btn-md";
      }
      if(strdisplay[tb-1].items[th-1].prop === "Reserved"){
        classNameStrEdit = "btn btn-info-dark btn-md";
      }
      if(strdisplay[tb-1].items[th-1].prop === "Under Construction"){
        classNameStrEdit = "btn btn-danger";
      }
      var data = $('#tbodyrowEdit'+tb+'').append(
        '<td class="text-center">'+
          '<button style="font-size:large; margin:3px; width:75px; pointer-events:none;" type="button" class="'+classNameStrEdit+'">'+
            '<span>B'+ tb +'L'+ th +'</span>'+
          '</button>'+
        '</td>'
      );
    };
    var editrow = $('#nextEdit').append(
      '<div class="col-lg-3" id="addedEdit'+tb+'">'+
        '<div class="row">'+
          '<div class="col-3" hidden>'+
            '<label>Items: </label>'+
            '<input class="sl" type="text" name="slno[]" id="slno" value="'+tb+'"  readonly="">'+
          '</div>'+
          '<div class="col-6" hidden>'+
            '<label>Block/Building: </label>'+
            '<input type="text" name="blockTxt[]" id="blockTxt'+tb+'" disabled min="1" value="1" class="form-control">'+
          '</div>'+
          '<div class="col-12">'+
            '<div class="row">'+
              '<div class="col-2">'+

              '</div>'+
              '<div class="col-12">'+
                '<label>Number of Lot/Unit for Block '+tb+': </label>'+
                '<input type="number" name="lotTxt[]" oninput="showEdit()"'+
                'value="'+editNumlot[tb-1]+'"'+
                'id="lotTxt'+tb+'" min="1" class="form-control" required>'+
              '</div>'+
            '</div>'+
          '</div>'+
        '</div>'+
      '</div>'
    );
  }

  function check() {
    var checked = document.getElementById("checkMap").checked;
    if(checked) {
      document.getElementById("builtMapFile").disabled = true;
    }
    else {
      document.getElementById("builtMapFile").disabled = false;
    }
  }

  function addEdit(){
    document.getElementById('removeInputEdit').hidden = false;
		length  = $('.sl').length;
		i       = parseInt(length)+parseInt(1);
		var newrow = $('#nextEdit').append(
      '<div class="col-lg-3" id="addedEdit'+i+'">'+
        '<div class="row">'+
          '<div class="col-3" hidden>'+
            '<label>Items: </label>'+
            '<input class="sl" type="text" name="slno[]" id="slno" value="'+i+'"  readonly="">'+
          '</div>'+
          '<div class="col-6" hidden>'+
            '<label>Block/Building: </label>'+
            '<input type="text" name="blockTxt[]" id="blockTxt'+i+'" disabled min="1" value="1" class="form-control">'+
          '</div>'+
          '<div class="col-12">'+
            '<div class="row">'+
              '<div class="col-2">'+

              '</div>'+
              '<div class="col-12">'+
                '<label>Number of Lot/Unit for Block '+i+': </label>'+
                '<input type="number" name="lotTxt[]" oninput="showEdit()" id="lotTxt'+i+'" min="1" class="form-control" required>'+
              '</div>'+
            '</div>'+
          '</div>'+
        '</div>'+
      '</div>'
    );
    document.getElementById('lotTxt'+i).focus();
    showEdit()
  };
  
  function removeEdit() {
    if(i <= 1){

    }
    else{
      $('#addedEdit'+i+'').remove()
      i  = parseInt(i)-parseInt(1)
      if(i <= 1){
        document.getElementById('removeInputEdit').hidden = true;
      }
      else{
        document.getElementById('removeInputEdit').hidden = false;
      }
      document.getElementById('lotTxt'+i).focus();
    }
    showEdit()
  };

  var array = [];
  function showEdit(){
    array = [];
    $('.theadEdit').remove()
    $('.tbodyrowEdit').remove()
		var length = $('.sl').length;
    var numblk = [];
    var numlot = [];
    for(var i=1;i<=length;i++){
      var items = [];
      var lot = $('#lotTxt'+i+'').val();
      var blk = $('#blkTxt'+i+'').val();
      for(var ii=1;ii<=lot;ii++){
          var propValueEdit = "Available";
        if(strdisplay[i-1].items[ii-1] !== undefined){
          propValueEdit = strdisplay[i-1].items[ii-1].prop;
        }
        else{
          propValueEdit = "Available";
        }
        items.push({id:ii,blocknum:i,lotnum:ii,prop:propValueEdit});
      }
      array.push({items});
      numlot.push(lot);
      numblk.push(i);
    }
    strdisplay = array;
    var stringToEdit = JSON.stringify(strdisplay);
    document.getElementById('listEditTxt').value = stringToEdit;
    var hightNumlot = Math.max(...numlot);
    var hightNumblk = Math.max(...numblk);
    console.log(strdisplay)

    for(var tb=1;tb<=hightNumblk;tb++){
      for(var th=0;th<=hightNumlot;th++) {
        if(tb === 1){
          if(th === 0){
            var header = $('#theaderEdit').append(
              '<th class="text-center theadEdit">'+
                '<div style="border-radius:5px; background-color:lightgray; width:102px;" class="btn btn-default btn-md">'+
                  'BLOCK/LOT'+
                '</div>'+
              '</th>'
            );
          }
          if(th != 0){
            var header = $('#theaderEdit').append(
              '<th class="text-center theadEdit" style="padding:4px; cursor:default;">'+
                '<div style="border-radius:5px; background-color:lightgray; width:75px;" class="btn btn-default btn-md">'+
                  th
                +'</div>'+
              '</th>'
            );
          }
        }
        else{}
      };
      var body = $('#tobodyEdit').append(
        '<tr class="tbodyrowEdit" id="tbodyrowEdit'+tb+'">'+
          '<td class="text-center">'+
            '<div style="border-radius:5px; background-color:lightgray; width:102px; height:41px; cursor:default;" class="btn btn-default btn-md">'+
              tb
            +'</div>'+
          '</td>'+
        '</tr>'
      );
      for(var th=1;th<=numlot[tb-1];th++) {
        var classNameStrEdit = "btn btn-info btn-md";
        if(strdisplay[tb-1].items[th-1].prop === "Available"){
          classNameStrEdit = "btn btn-info btn-md";
        }
        if(strdisplay[tb-1].items[th-1].prop === "Awarded"){
          classNameStrEdit = "btn btn-success btn-md";
        }
        if(strdisplay[tb-1].items[th-1].prop === "Reserved"){
          classNameStrEdit = "btn btn-info-dark btn-md";
        }
        if(strdisplay[tb-1].items[th-1].prop === "Under Construction"){
          classNameStrEdit = "btn btn-danger";
        }
        var data = $('#tbodyrowEdit'+tb+'').append(
          '<td class="text-center">'+
            '<button style="font-size:large; margin:3px; width:75px; pointer-events:none;" type="button" class="'+classNameStrEdit+'">'+
              '<span>B'+ tb +'L'+ th +'</span>'+
            '</button>'+
          '</td>'
        );
      };
    }
  };
</script>