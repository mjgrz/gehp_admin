<?php
$acH = $_GET['acH'];
$ID = $_GET['ID'];
include("../../../../dbcon.php");
if (is_numeric($ID) == true){
  try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $idselect = "SELECT * FROM ofw_applications 
                  LEFT JOIN ofw_applicant_info ON ofw_applications.applicant_id = ofw_applicant_info.applicant_id 
                  LEFT JOIN ofw_applicant_spouse ON ofw_applications.spouse_id = ofw_applicant_spouse.spouse_id 
                  LEFT JOIN ofw_employment_status ON ofw_applications.employment_id = ofw_employment_status.employment_id 
                  LEFT JOIN requirements_ofw ON ofw_applications.req_id = requirements_ofw.req_id 
                  LEFT JOIN project_info ON ofw_applications.PJ_CODE = project_info.PJ_CODE
                  WHERE APP_ID = :ID";
    $sthidselect = $dbh->prepare($idselect);
    $sthidselect->bindParam(':ID', $ID);
    $sthidselect->execute();
    $sthidselect->setFetchMode(PDO::FETCH_ASSOC); 
    while ($idrow = $sthidselect->fetch(PDO::FETCH_ASSOC)) {
      $PJ_NAME = $idrow['PJ_NAME'];
      $PJ_CODE = $idrow['PJ_CODE'];
      $payment = $idrow['payment'];
      $LNAME = $idrow['LNAME'];
      $MNAME = $idrow['MNAME'];
      $FNAME = $idrow['FNAME'];
      $type = $idrow['house_type'];
      $employee = $idrow['employee'];
      $motherSurname = $idrow['motherSurname'];
      $ADDR1 = $idrow['ADDR1'];
      $email = $idrow['email'];
      $BDATE = $idrow['BDATE'];
      $birthplace = $idrow['birthplace'];
      $CSTATUS = $idrow['CSTATUS'];
      $sex = $idrow['sex'];
      $citizenship = $idrow['citizenship'];
      $contact = $idrow['contact'];
      $office = $idrow['office'];
      $tin = $idrow['tin'];
      $pagibig = $idrow['pagibig'];
      $fam_income = $idrow['fam_income'];
      $non_violation = $idrow['non_violation'];
      $alt_violation = $idrow['alt_violation'];
      $SLNAME = $idrow['SLNAME'];
      $SFNAME = $idrow['SFNAME'];
      $SMNAME = $idrow['SMNAME'];
      $SmotherSurname = $idrow['SmotherSurname'];
      $SBDATE = $idrow['SBDATE'];
      $Sbirthplace = $idrow['Sbirthplace'];
      $nature = $idrow['nature'];
      $EMPLOYER = $idrow['EMPLOYER'];
      $emp_address = $idrow['emp_address'];
      $MINCOME = $idrow['MINCOME'];
      $dependent_id = $idrow['dependent_id'];
      $APP_ID = $idrow['APP_ID'];
    }
    $dbh = null;
  }
  catch(PDOException $e){
      error_log('PDOException - ' . $e->getMessage(), 0);
      http_response_code(500);
      die('Error establishing connection with database');
  }
}
if (!is_numeric($ID) == true){
  http_response_code(400);
  die('Error processing bad or malformed request');
}
?>
<div class="modal-header" style="background: #67a2b2; color: white;">
  <h4 class="modal-title">Applicant (Overseas Filipino Worker) - Full Details</h4>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true" style="color: white;">&times;</span>
  </button>
</div>
<div class="modal-body" style="overflow:auto; height: 870px;">
  <div class="card-body">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label style="border-bottom: 2px solid #67a2b2;">A.1 FOR PURCHASE OF HOUSE AND LOT PACKAGE <span class="h6">under the GEHP thru</span></label>
          <div class="form-group col-sm-12">
            <input type="text" class="form-control" id="payment" name="payment" placeholder="" value="<?php echo $payment ?>" readonly>
          </div>
        </div>
        <div class="form-group col-sm-12">
          <label for="project">Project Name</label>
          <?php
          if (is_numeric($PJ_CODE) == true){
            try{
              $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
              $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
              $regionselect = "SELECT * FROM project_info 
                              LEFT JOIN units_floor ON project_info.project_id = units_floor.project_id
                              LEFT JOIN region ON project_info.region = region.psgc_reg
                              WHERE PJ_CODE = :PJ_CODE";
                $sthregionselect = $dbh->prepare($regionselect);
                $sthregionselect->bindParam(':PJ_CODE', $PJ_CODE);
                $sthregionselect->execute();
                $sthregionselect->setFetchMode(PDO::FETCH_ASSOC);
                while ($regionrow = $sthregionselect->fetch(PDO::FETCH_ASSOC)) {
                  $Region = $regionrow['Region'];
                  $PJ_NAME = $regionrow['PJ_NAME'];
                  $location = $regionrow['location'];
                }
            }
            catch(PDOException $e){
                error_log('PDOException - ' . $e->getMessage(), 0);
                http_response_code(500);
                die('Error establishing connection with database');
            }
          }
          if (!is_numeric($PJ_CODE) == true){
            http_response_code(400);
            die('Error processing bad or malformed request');
          }
          ?>
          <input type="text" class="form-control" value="<?php echo $Region ?> | <?php echo $PJ_NAME ?> (<?php echo $location ?>)" readonly>
        </div>
        <div class="form-group">
          <label style="border-bottom: 2px solid #67a2b2;">A.2 TYPE OF HOUSING <span class="h6">under the GEHP thru</span></label>
          <div class="form-group col-sm-12">
            <input type="text" class="form-control" id="type" name="type" placeholder="" value="<?php echo $type ?>" readonly>
          </div>
        </div>

        <div class="form-group">
          <label style="border-bottom: 2px solid #67a2b2;">A.3 As Government Employee of </label>
          <div class="form-group col-sm-12">
            <input type="text" class="form-control" id="employee" name="employee"
            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="" value="<?php echo $employee ?>" readonly>
          </div>
        </div>
        <br>
        <label style="border-bottom: 2px solid #67a2b2;">I. APPLICANT'S IDENTITY <span class="h6 text-muted">(For female applicant/spouse, give complete maiden name)</span> </label>
        <div class="col-sm-12">
          <label>Full Name</label>
          <div class="row">
            <div class="form-group col-sm-3">
              <input type="text" class="form-control" id="lname" name="lname"
              oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Last Name" value="<?php echo $LNAME ?>" readonly>
            </div>
            <div class="form-group col-sm-3">
              <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name"
              oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $FNAME ?>" readonly>
            </div>
            <div class="form-group col-sm-3">
              <input type="text" class="form-control" id="mname" name="mname" placeholder="Middle Name"
              oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $MNAME ?>" readonly>
            </div>
            <div class="form-group col-sm-3">
              <input type="text" class="form-control" id="msurname" name="msurname"
              oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Mother's Surname" value="<?php echo $motherSurname ?>" readonly>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="row">
            <div class="form-group col-sm-4">
              <label for="address">Residence / Address</label> 
              <input type="text" class="form-control" id="address" name="address"
              oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="" value="<?php echo $ADDR1 ?>" readonly>
            </div>
            <div class="form-group col-sm-4">
              <label for="dplace">Place of Birth</label>
              <input type="text" class="form-control" id="dplace" name="dplace"
              oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="" value="<?php echo $birthplace ?>" readonly>
            </div>
            <div class="form-group col-sm-4">
              <label for="email">Email Address</label>
              <input type="email" class="form-control" id="email" name="email" placeholder="" value="<?php echo $email ?>" readonly>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="row">
            <div class="form-group col-sm-4">
              <label for="dbirth">Date of Birth</label> 
              <input type="text" class="form-control" id="dbirth" name="dbirth" placeholder="" value="<?php echo date('F d, Y',strtotime($BDATE)) ?>" readonly>
            </div>
            <div class="form-group col-sm-4">
              <label for="sex">Sex</label>
              <input type="text" class="form-control" id="sex" name="sex"
              oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="" value="<?php echo $sex ?>" readonly>
            </div>
            <div class="form-group col-sm-4">
              <label for="status">Civil Status</label>
              <input type="text" class="form-control" id="status" name="status"
              oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="" value="<?php echo $CSTATUS ?>" readonly>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="row">
            <div class="form-group col-sm-4">
              <label for="citizen">Citizenship</label>
              <input type="text" class="form-control" id="citizen" name="citizen"
              oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="" value="<?php echo $citizenship ?>" readonly>
            </div>
            <div class="form-group col-sm-4">
              <label for="contact">Contact Number</label> 
              <input type="text" class="form-control" id="contact" name="contact"
              oninput="this.value = this.value.replace(/[^0-9-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="" value="<?php echo $contact ?>" readonly>
            </div>
            <div class="form-group col-sm-4">
              <label for="office">Office Number</label>
              <input type="text" class="form-control" id="office" name="office"
              oninput="this.value = this.value.replace(/[^0-9-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="" value="<?php echo $office ?>" readonly>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="row">
            <div class="form-group col-sm-6">
              <label for="tin">TIN Number</label>
              <input type="text" class="form-control" id="tin" name="tin"
              oninput="this.value = this.value.replace(/[^0-9 a-z A-Z-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="" value="<?php echo $tin ?>" readonly>
            </div>
            <div class="form-group col-sm-6">
              <label for="pagibig">PAG-IBIG ID</label>
              <input type="text" class="form-control" id="pagibig" name="pagibig"
              oninput="this.value = this.value.replace(/[^0-9 a-z A-Z-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="" value="<?php echo $pagibig ?>" readonly>
            </div>
          </div>
        </div>
        
        <div class="col-sm-12">
          <label for="userMname">Name of Spouse</label>
          <div class="row">
            <div class="form-group col-sm-3">
              <input type="text" class="form-control" id="s_lname" name="s_lname"
              oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Last Name" value="<?php echo $SLNAME ?>" readonly>
            </div>
            <div class="form-group col-sm-3">
              <input type="text" class="form-control" id="s_fname" name="s_fname"
              oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="First Name" value="<?php echo $SFNAME ?>" readonly>
            </div>
            <div class="form-group col-sm-3">
              <input type="text" class="form-control" id="s_mname" name="s_mname"
              oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Middle Name" value="<?php echo $SMNAME ?>" readonly>
            </div>
            <div class="form-group col-sm-3">
              <input type="text" class="form-control" id="s_msurname" name="s_msurname"
              oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Mother's Surname" value="<?php echo $SmotherSurname ?>" readonly>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="row">
            <div class="form-group col-sm-6">
              <label for="s_dbirth">Date of Birth</label> 
              <input type="text" class="form-control" id="s_dbirth" name="s_dbirth" placeholder="" value="<?php if($SBDATE == ''){}else{ echo date('F d, Y',strtotime($SBDATE));} ?>" readonly>
            </div>
            <div class="form-group col-sm-6">
              <label for="s_dplace">Place of Birth</label>
              <input type="text" class="form-control" id="s_dplace" name="s_dplace"
              oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="" value="<?php echo $Sbirthplace ?>" readonly>
            </div>
          </div>
        </div>
      </div>
      <br>
      <div class="col-md-6">
        <label style="border-bottom: 2px solid #67a2b2;">II. APPLICANT’S EMPLOYMENTS STATUS</label>
        <div class="col-sm-12">
          <div class="row">
            <div class="form-group col-sm-4">
              <label for="nature">Nature of Employment</label> 
              <input type="text" class="form-control" id="nature" name="nature"
              oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $nature ?>"  readonly>
            </div>
            <div class="form-group col-sm-4">
              <label for="agency">Name of Agency/Corporation</label>
              <input type="text" class="form-control" id="agency" name="agency"
              oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $EMPLOYER ?>"  readonly>
            </div>
            <div class="form-group col-sm-4">
              <label for="income">Income</label>
              <input type="text" class="form-control" id="income" name="income" value="<?php echo $MINCOME ?>"  readonly>
            </div>
          </div>
        </div>
        
        <div class="form-group col-sm-12">
          <label for="a_address">Address of Agency/Corporation</label> 
          <input type="text" class="form-control" id="a_address" name="a_address"
              oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $emp_address ?>" placeholder="" readonly>
        </div>
        <br>
        <span>
          <label style="border-bottom: 2px solid #67a2b2;">III. APPLICANT'S FAMILY COMPOSITION <span class=" h6 text-muted"></span></label>
          <?php
          if (is_numeric($dependent_id) == true){
            try{
              $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
              $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
              $dependentquerymodal = "SELECT * FROM ofw_family WHERE dependent_id = :dependent_id";
              $sthdependentquerymodal = $dbh->prepare($dependentquerymodal);
              $sthdependentquerymodal->bindParam(':dependent_id', $dependent_id);
              $sthdependentquerymodal->execute();
              $sthdependentquerymodal->setFetchMode(PDO::FETCH_ASSOC);
              if($sthdependentquerymodal->rowCount() > 0){
              while ($derow1 = $sthdependentquerymodal->fetch(PDO::FETCH_ASSOC)) {
              ?>
            <span>
              <div class="col-sm-12">
                <label>Full Name</label>
                <div class="row">
                  <div class="form-group col-sm-4">
                    <input class="sl"type="text" name="slno[]" id="slno" value="1" style="border-radius: 5px; width: 100%; height: 50px; border-color: #f1f1f1;" hidden readonly="">
                    <input type="text" class="form-control" id="fam-lname" name="dlname[]"
                    oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Last Name" value="<?php echo $derow1['DLNAME']; ?>" readonly>
                  </div>
                  <div class="form-group col-sm-4">
                    <input type="text" class="form-control" id="fam-fname" name="dfname[]"
                    oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="First Name" value="<?php echo $derow1['DFNAME']; ?>" readonly>
                  </div>
                  <div class="form-group col-sm-4">
                    <input type="text" class="form-control" id="fam-fname" name="dmname[]"
                    oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Middle Name" value="<?php echo $derow1['DMNAME']; ?>" readonly>
                  </div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="row">
                  <div class="form-group col-sm-3">
                    <label for="s_dbirth">Relation to Applicant</label> 
                    <input type="text" class="form-control" id="fam-relation" name="relation[]"
                    oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $derow1['relation']; ?>" readonly>
                  </div>
                  <div class="form-group col-sm-3">
                    <label for="s_dplace">Civil Status</label>
                    <input type="text" class="form-control" id="cstatus" name="dcstatus[]"
                    oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $derow1['DCSTATUS']; ?>" readonly>
                  </div>
                  <div class="form-group col-sm-3">
                    <label for="s_dplace">Age</label>
                    <input type="number" class="form-control" max="150" id="fam-age" name="age[]"
                    oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $derow1['age']; ?>" readonly>
                  </div>
                  <div class="form-group col-sm-3">
                    <label for="s_dplace">Source of Income</label>
                    <input type="text" class="form-control" id="fam-age" name="source_income[]"
                    oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $derow1['source_income']; ?>" readonly>
                  </div>
                </div>
              </div>
              <hr style="border-bottom: 1px solid #67a2b2;">
            </span>
            <?php
                }
              }
            if($sthdependentquerymodal->rowCount() == 0){
              echo "<div><i>No Dependents</i></div>";
            }
            }
            catch(PDOException $e){
                error_log('PDOException - ' . $e->getMessage(), 0);
                http_response_code(500);
                die('Error establishing connection with database');
            }
          }
          if (!is_numeric($dependent_id) == true){
            http_response_code(400);
            die('Error processing bad or malformed request');
          }
          ?>
        </span>
        <br>
        <div class="form-group col-sm-12">
          <div class="row">
            <label style="margin-top:auto; border-bottom: 2px solid #67a2b2;">IV. APPLICANT'S TOTAL FAMILY INCOME PER MONTH:</label>&nbsp;&nbsp;&nbsp;
            <input type="text" style="width:auto;" value="<?php echo $fam_income ?>" class="form-control" id="famIncome" name="famIncome" readonly>
          </div>
        </div>
        <div class="form-group">
          <label style="border-bottom: 2px solid #67a2b2;">V. FAMILY REAL PROPERTY HOLDINGS:</label>
          <div class="form-group col-sm-12">
            <label>
              Have never availed of any form of government housing assistance
            </label>
            <input type="text" class="form-control col-sm-3" value="<?php echo $non_violation ?>" id="noViolation" name="noViolation" readonly>
          </div>
          <div class="form-group col-sm-12">
            <label>
              Have sold, alienated, conveyed, encumbered or leased the socialized housing, including imporovements
              or rights thereon, except to qualified beneficiary as determined by the Government Agency
            </label>
            <input type="text" class="form-control col-sm-3" value="<?php echo $alt_violation ?>" id="altViolation" name="altViolation" readonly>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer justify-content-right">
  <span>
    <a id="tooltip" type="button" class="btn btn-default color-gray" target="_blank" href="print_AppOFW.php?id=<?php echo $APP_ID ?>">
      <i class="fa fa-print">
      </i>
      <span class="tooltiptext">Print</span>
    </a>
    <a id="tooltip" type="button" <?php echo $acH; ?> class="btn btn-default color-blue" href="editOfwApp.php?id=<?php echo $APP_ID ?>">
      <i class="fas fa-edit">
      </i>
      <span class="tooltiptext">Edit</span>
    </a>
    <a id="tooltip" type="button" <?php echo $acH; ?> data-a="<?php echo $APP_ID ?>" class="remarks btn btn-default color-orange" data-toggle="modal" data-dismiss="modal" href="#remarksmodal">
      <i class="fas fa-exclamation">
      </i>
      <span class="tooltiptext">Pend</span>
    </a>
    <a id="tooltip" type="button" <?php echo $acH; ?> class="qualified btn btn-default color-green" data-a="<?php echo $APP_ID ?>" data-toggle="modal" data-dismiss="modal" href="#qualified">
      <i class="fa fa-check">
      </i>
      <span class="tooltiptext">Qualify</span>
    </a>
    <a id="tooltip" type="button" <?php echo $acH; ?> class="disqualified btn btn-default color-red" data-a="<?php echo $APP_ID ?>" data-toggle="modal" data-dismiss="modal" href="#disqualified">
      <i class="fas fa-ban">
      </i>
      <span class="tooltiptext">Disqualify</span>
    </a>
  </span>
</div>


<script>
  
  $('.remarks').click(function(){
      var remarks_id=$(this).attr('data-a');
      $.ajax({url:"remarksOFW_modal_result?remarks_id="+remarks_id,cache:false,success:function(result){
          $(".remarks-modal").html(result);
      }});
  });

  $('.qualified').click(function(){
      var qualified_id=$(this).attr('data-a');
      $.ajax({url:"qualifiedOFW_modal_result?qualified_id="+qualified_id,cache:false,success:function(result){
          $(".qualified-modal").html(result);
      }});
  });

  $('.disqualified').click(function(){
      var disqualified_id=$(this).attr('data-a');
      $.ajax({url:"disqualifiedOFW_modal_result?disqualified_id="+disqualified_id,cache:false,success:function(result){
          $(".disqualified-modal").html(result);
      }});
  });
  
</script>