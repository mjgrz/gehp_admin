<?php
include('login_session.php');
$_SESSION["projsearch"] = $_POST["checkers"];
error_reporting(E_ERROR | E_PARSE);
?>
<span>
<div style="overflow:auto; height: 650px; padding: 7px;">
<table id="projectTable" class="table table-bordered table-striped">
  <thead>
  <tr>
    <th <?php echo $delete_hide; ?> style="white-space: nowrap; " class="project-actions text-center"><input id="chk_all" name="chk_all" type="checkbox"></th>
    <th>Name</th>
    <th>Overview</th>
    <th>Location</th>
    <th>Availability</th>
    <th style="width:60px;">On Hold</th>
    <th>Island</th>
    <th>Region</th>
    <th>Status</th>
    <th>Image</th>
    <th>Action</th>
  </tr>
  </thead>
  <tbody>
    <?php
      try{
        $selectdata=[
          ':projsearch' => $_SESSION["projsearch"],
          ':projsearch2' => $_SESSION["projsearch"],
        ];
        $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $projectselect = "SELECT * FROM project_info LEFT JOIN units_floor ON project_info.project_id = units_floor.project_id 
                          LEFT JOIN nearby_estab ON project_info.project_id = nearby_estab.project_id
                          LEFT JOIN other_files ON project_info.project_id = other_files.project_id
                          WHERE PJ_NAME LIKE CONCAT('%', :projsearch, '%') OR island LIKE CONCAT('%', :projsearch2, '%')
                          ORDER BY PJ_NAME";
        $sthselect = $dbh->prepare($projectselect);
          $sthselect->execute($selectdata);
          $count = $sthselect->rowCount();
          $setcount = $count;
          if (isset($_GET['pageno'])) {
            $pageno = $_GET['pageno'];
            //$i = $_GET['pageno'];
            if($_GET['pageno'] == 1){
              $i = 2;
            }
          } 
          else {
            $pageno = 1;
            $i = 2;
          }
          $no_of_records_per_page = 5;
          $offset = ($pageno-1) * $no_of_records_per_page;
    
          $total_pages = ceil($count / $no_of_records_per_page);
    
          $cnt=1;
          
          $sql2 = "$projectselect LIMIT $offset, $no_of_records_per_page";
    
          $sthsql2 = $dbh->prepare($sql2);
          $sthsql2->execute($selectdata);
          $sthsql2->setFetchMode(PDO::FETCH_ASSOC); 
          $app_count = $sthsql2->rowCount();
          $setcount = $app_count;
        $i=0;
        while ($projectrow = $sthsql2->fetch(PDO::FETCH_ASSOC)) {
          $region = $projectrow['region'];
          $regionselect = "SELECT Region FROM region WHERE psgc_reg = :region";
          $sthregionselect = $dbh->prepare($regionselect);
          $sthregionselect->bindParam(':region', $region);
          $sthregionselect->execute();
          $sthregionselect->setFetchMode(PDO::FETCH_ASSOC); 
          while ($regionrow = $sthregionselect->fetch(PDO::FETCH_ASSOC)) {$regionshow = $regionrow["Region"];}
          ?>
          <tr>
            <td <?php echo $delete_hide; ?> class="project-actions text-center" style="vertical-align: middle;"><input name="check[]" type="checkbox" class='chkbox' value="<?php echo $projectrow['project_id']; ?>"/></td>
            <td style="vertical-align: middle;"> <?php echo $projectrow['PJ_NAME'] ?> </td>
            <td style="vertical-align: middle;"> <?php echo $projectrow['overview'] ?> </td>
            <td style="vertical-align: middle;"> <?php echo $projectrow['location'] ?> </td>
            <td style="vertical-align: middle; text-align:center"> <?php include('availability.php'); ?> </td>
            <td style="vertical-align: middle; text-align:center"> <?php include('archived.php'); ?> </td>
            <td style="vertical-align: middle;"> <?php echo $projectrow['island'] ?> </td>
            <td style="vertical-align: middle;"> <?php echo $regionshow ?> </td>
            <td style="vertical-align: middle;"> <?php echo $projectrow['project_status'] ?> </td>
            <td class="project-actions text-center" style="vertical-align: middle;"> 
            <?php 
            $main_image = $projectrow['main_image'];
            $target_dir = "uploads/projects/main_image/";
            $target_file = $target_dir . basename("$main_image");
            if(file_exists($target_file)){
            ?>
            <img src="uploads/projects/main_image/<?php echo $main_image ?>" style="border-radius: 5px;" width="150px" height="150px"> 
            <?php
            } 
            else
            {
              echo "No image available.";
            }
            ?>
            </td>
            <td style="white-space: nowrap; vertical-align: middle;" class="project-actions text-center">
                
              <a id="tooltip" type="button" href="EditProjects.php?id=<?php echo $projectrow['project_id'] ?>" style="font-size:large;" name="submit" class="btn btn-default btn-sm color-cyan-dark">
                <i class="fas fa-edit">
                </i>
                <span class="tooltiptext">Edit</span>
              </a>
              <a id="tooltip" type="button" href="" data-toggle="modal" data-target="#modal-xl<?php echo $projectrow['project_id'] ?>" style="font-size:large;" class="btn btn-default btn-sm color-blue-dark">
                <i class="fa fa-eye">
                </i>
                <span class="tooltiptext">View</span>
              </a>
            </td>
            <?php include("projects_modal.php");?>
          </tr>
          <?php    
          $i++;
        } 
      }
      catch(PDOException $e){
          error_log('PDOException - ' . $e->getMessage(), 0);
          http_response_code(500);
          die('Error establishing connection with database');
      }
    ?>
  </tbody>
</table>
</div>
<?php include("paging.php"); ?>
<?php include("delete_script.php");?>
</span>
<script>
  $(function () {
    $("#projectTable").DataTable({
      "destroy": true,
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": false,
      "responsive": false,
      //"buttons": ["excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#userTable_wrapper .col-md-6:eq(0)');
  });
</script>