
                <p style="font-size: 20px;">Remarks</p>
                <table border="1" cellpadding="5" cellspacing="2">
                  <thead>
                    <tr>
                      <th>
                        CODE
                      </th>
                      <th>
                        EMPLOYEE
                      </th>
                      <th>
                        FILE
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                <?php
                  include("dbcon.php");
                  $select = "SELECT * FROM applications";
                  $result = mysqli_query($conn, $select);
                  $count = mysqli_num_rows($result);
                  for($i=0;$i<$count;$i++){
                    $row = mysqli_fetch_assoc($result);
                    $depID = $row['dependent_id'];
                  ?>
                        <tr>
                          <td>
                            <?php date_default_timezone_set("Asia/Hong_Kong"); echo date("h:i a"); ?>
                          </td>
                          <td>
                            <?php echo $row["employee"] ?>
                          </td>
                          <td>
                            <?php
                            $file_select = "SELECT * FROM applicant_family WHERE dependent_id = $depID";
                            $file_result = mysqli_query($conn, $file_select);
                            while($file_row = mysqli_fetch_assoc($file_result)){
                              echo $file_row["DFNAME"].'<br>';
                            }
                            ?>
                          </td>
                        </tr>
                  <?php
                  }
                  ?>
                  </tbody>
                </table>
              