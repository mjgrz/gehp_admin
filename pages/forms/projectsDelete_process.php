<?php
session_start();
include('login_session.php');
if(isset($_POST['submit'])){
  if(isset($_POST['check'])) {
    $checkbox = $_POST['check'];
    $username = $_POST['username'];
    date_default_timezone_set("Asia/Hong_Kong");
    $time = date("h:i a");
    $date = date('F d, Y', strtotime(date("Y-m-d")));
    $datetime = $date." ".$time;
    for($i=0;$i<count($checkbox);$i++){
      $del_id = $checkbox[$i]; 
      if ( is_numeric($del_id) == true){
        try{
          $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
          $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $sqldata = [
            ':del_id' => $del_id
          ];
          $project_info = "DELETE FROM project_info WHERE project_id=:del_id";
          $sthproject_info = $dbh->prepare($project_info);
          $nearby_estab = "DELETE FROM nearby_estab WHERE project_id=:del_id";
          $sthnearby_estab = $dbh->prepare($nearby_estab);
          $other_files = "DELETE FROM other_files WHERE project_id=:del_id";
          $sthother_files = $dbh->prepare($other_files);
          $units_floor = "DELETE FROM units_floor WHERE project_id=:del_id";
          $sthunits_floor = $dbh->prepare($units_floor);
          if($sthproject_info->execute($sqldata) && $sthnearby_estab->execute($sqldata) && $sthother_files->execute($sqldata) && $sthunits_floor->execute($sqldata)){
              $auditdata = [
                ':activity' => "Project(s) Deleated'",
                ':username' => $_SESSION['login_user'],
                ':datetime' => $datetime
              ];
              $audit = "INSERT INTO audit_trail (activity, username, date) VALUESS (:activity, :username, :datetime)";
              $sthaudit = $dbh->prepare($audit);
              $sthaudit->execute($auditdata);
              $_SESSION["status"] = "Record(s) have been deleted successfully.";
              header('Location: ../../pages/forms/project_view.php');
              $dbh = null;
          }
          else{
              $_SESSION["error"] = "Sorry, record(s) were not deleted.";
              header('Location: ../../pages/forms/project_view.php');
              $dbh = null;
          }
        }
        catch(PDOException $e){
            error_log('PDOException - ' . $e->getMessage(), 0);
            http_response_code(500);
            die('Error establishing connection with database');
        }
      }
      else{
        http_response_code(400);
        die('Error processing bad or malformed request');
      }
    }
  }
  if(!isset($_POST['check'])) {
    $_SESSION["info"] = "Sorry, no record(s) to be deleted.";
    header('Location: ../../pages/forms/project_view.php');
  }
}
else{
  header('Location: ../../pages/forms/project_view.php');
}
?>