<?php
session_start();
include("../../../../dbcon.php");
date_default_timezone_set("Asia/Hong_Kong");
$s_id = date("YmdHis").$_POST["counter"];
$a_id = date("YmdHis").$_POST["counter"];
$e_id = date("YmdHis").$_POST["counter"];
$f_id = date("YmdHis").$_POST["counter"];
$r_id = date("YmdHis").$_POST["counter"];

if (is_numeric($r_id) == true && is_numeric($f_id) == true && is_numeric($e_id) == true && is_numeric($a_id) == true && is_numeric($s_id) == true){
  try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $payment = $_POST["payment"]; 
    if(!isset($_POST["type"])){
        $type = "";   
    }
    if(isset($_POST["type"])){
        $type = $_POST["type"];  
    }
    $employee = $_POST["employee"]; 
    $project = $_POST["project"];
    $lname = $_POST["lname"];
    $fname = $_POST["fname"];
    $mname = $_POST["mname"];
    $msurname = $_POST["msurname"];
    $address = $_POST["address"];
    $email = $_POST["email"];
    $dbirth = $_POST["dbirth"];
    $dplace = $_POST["dplace"];
    $status = $_POST["status"];
    $sex = $_POST["sex"];
    $citizen = $_POST["citizen"];
    $contact = $_POST["contact"];
    $office = $_POST["office"];
    $tin = $_POST["tin"];
    $pagibig = $_POST["pagibig"];
    $s_lname = $_POST["s_lname"];
    $s_fname = $_POST["s_fname"];
    $s_mname = $_POST["s_mname"];
    $s_msurname = $_POST["s_msurname"];
    $s_dbirth = $_POST["s_dbirth"];
    $s_dplace = $_POST["s_dplace"];
    $nature = $_POST["nature"];
    $agency = $_POST["agency"];
    $a_address = $_POST["a_address"];
    $famIncome = $_POST["famIncome"];
    $noViolation = $_POST["noViolation"];
    $altViolation = $_POST["altViolation"];
    $SG = $_POST["income"];
    $usernam = $_POST["username"];
    $date = date("Y-m-d");
    $app_date = $_POST["app_date"];
    $s_final = "$s_id";
    $a_final = "$a_id";
    $e_final = "$e_id";
    $f_final = "$f_id";
    $r_final = "$r_id";
    $time = date("h:i a");
    $dates = date('F d, Y', strtotime(date("Y-m-d")));
    $datetime = $dates." ".$time;
    if ( is_numeric($project) == true){
      if(!isset($_POST["checkerOEC"])){
        $target_dir = "uploads/OEC_uploads/";
        $target_filevideo = $target_dir . basename("$project-$lname-$s_id.pdf");
        $uploadOkvideo = 1;
        $video = "$project-$lname-$s_id.pdf";
        $imageFileTypevideo = strtolower(pathinfo($target_filevideo,PATHINFO_EXTENSION));

        if ($_FILES["file"]["size"] > 20000000) {
          $_SESSION['ofw'] = "Sorry, your file is too large.";
          $uploadOkvideo = 0;
        }

        if($imageFileTypevideo != "pdf") {
          $_SESSION['ofw'] = "Sorry, only pdf files are allowed.";
          $uploadOkvideo = 0;
        }

        if ($uploadOkvideo == 0) {
          header('Location: ../forms/registration_OFW.php');
        } 
        
        else {
          $querysearchdata = [
            ':project' => $project,
            ':lname' => $lname,
            ':fname' => $fname,
            ':mname' => $mname,
            ':dbirth' => $dbirth
          ];
          $querysearch = "SELECT PJ_CODE FROM ofw_applications 
                        LEFT JOIN ofw_applicant_info ON ofw_applications.applicant_id = ofw_applicant_info.applicant_id
                        WHERE PJ_CODE = :project AND LNAME = :lname AND FNAME = :fname AND MNAME = :mname AND BDATE = :dbirth";
          $sthquerysearch = $dbh->prepare($querysearch);
          $sthquerysearch->execute($querysearchdata);
          if($sthquerysearch->rowCount() > 0) {
            $_SESSION['error'] = 'Sorry, this application is still on process';
            header('Location: ../forms/registration_OFW.php');
          }
          else {
            $sql_applicant_infodata = [
              ':a_final' => $a_final,
              ':lname' => $lname,
              ':fname' => $fname,
              ':mname' => $mname,
              ':msurname' => $msurname,
              ':address' => $address,
              ':email' => $email,
              ':dbirth' => $dbirth,
              ':dplace' => $dplace,
              ':status' => $status,
              ':sex' => $sex,
              ':citizen' => $citizen,
              ':contact' => $contact,
              ':office' => $office,
              ':tin' => $tin,
              ':pagibig' => $pagibig,
              ':famIncome' => $famIncome,
              ':noViolation' => $noViolation,
              ':altViolation' => $altViolation
            ];
            $sql_applicant_info = "INSERT INTO ofw_applicant_info (applicant_id, LNAME, FNAME, MNAME, motherSurname, ADDR1, email, BDATE, birthplace, CSTATUS, sex, citizenship, contact, office, tin, pagibig, fam_income, non_violation, alt_violation) 
            VALUES 
            (:a_final, :lname, :fname, :mname, :msurname, :address, :email, :dbirth, :dplace, :status, :sex, :citizen, :contact, :office, :tin, :pagibig, :famIncome, :noViolation, :altViolation)";
            $sthsql_applicant_info = $dbh->prepare($sql_applicant_info);
            $sql_applicant_spousedata = [
              ':s_final' => $s_final,
              ':s_lname' => $s_lname,
              ':s_fname' => $s_fname,
              ':s_mname' => $s_mname,
              ':s_msurname' => $s_msurname,
              ':s_dbirth' => $s_dbirth,
              ':s_dplace' => $s_dplace
            ];
            $sql_applicant_spouse = "INSERT INTO ofw_applicant_spouse (spouse_id, SLNAME, SFNAME, SMNAME, SmotherSurname, SBDATE, Sbirthplace) 
            VALUES 
            (:s_final, :s_lname, :s_fname, :s_mname, :s_msurname, :s_dbirth, :s_dplace)";
            $sthsql_applicant_spouse = $dbh->prepare($sql_applicant_spouse);
            $sql_employment_statusdata = [
              ':e_final' => $e_final,
              ':nature' => $nature,
              ':agency' => $agency,
              ':a_address' => $a_address,
              ':SG' => $SG
            ];
            $sql_employment_status = "INSERT INTO ofw_employment_status (employment_id, nature, EMPLOYER, emp_address, MINCOME) 
            VALUES 
            (:e_final, :nature, :agency, :a_address, :SG)";
            $sthsql_employment_status = $dbh->prepare($sql_employment_status);
            $stat = "New Applicant";
            $source = "Website/Backend";
            $sql_applicationsdata = [
              ':payment' => $payment,
              ':type' => $type,
              ':employee' => $employee,
              ':app_date' => $app_date,
              ':project' => $project,
              ':a_final' => $a_final,
              ':s_final' => $s_final,
              ':e_final' => $e_final,
              ':f_final' => $f_final,
              ':r_final' => $r_final,
              ':username' => $usernam,
              ':stat' => $stat,
              ':video' => $video,
              ':source' => $source
            ];
            $sql_applications = "INSERT INTO ofw_applications (payment, house_type, employee, app_date, PJ_CODE, applicant_id, spouse_id, employment_id, dependent_id, req_id, username, app_status, fileUpload, source) 
            VALUES (:payment, :type, :employee, :app_date, :project, :a_final, :s_final, :e_final, :f_final, :r_final, :username, :stat, :video, :source)";
            $sthsql_applications = $dbh->prepare($sql_applications);
            $sql_requirementsdata = [
              ':r_final' => $r_final
            ];
            $sql_requirements = "INSERT INTO requirements_ofw (req_id) VALUES (:r_final)"; 
            $sthsql_requirements = $dbh->prepare($sql_requirements);
            if ($sthsql_applicant_info->execute($sql_applicant_infodata) && $sthsql_applicant_spouse->execute($sql_applicant_spousedata) && $sthsql_employment_status->execute($sql_employment_statusdata) && $sthsql_applications->execute($sql_applicationsdata) && $sthsql_requirements->execute($sql_requirementsdata)) {
              if(isset($_POST['dlname'])){
                for($i=0;$i<count($_POST['slno']);$i++){
                  $DLNAME = $_POST['dlname'][$i];
                  $DFNAME = $_POST['dfname'][$i];
                  $DMNAME = $_POST['dmname'][$i];
                  $relation = $_POST['relation'][$i];
                  $DCSTATUS = $_POST['dcstatus'][$i];
                  $age = $_POST['age'][$i];
                  $source_income = $_POST['source_income'][$i];
                  $sql_familydata = [
                          ':f_final' => $f_final,
                          ':DLNAME' => $DLNAME,
                          ':DFNAME' => $DFNAME,
                          ':DMNAME' => $DMNAME,
                          ':relation' => $relation,
                          ':DCSTATUS' => $DCSTATUS,
                          ':age' => $age,
                          ':source_income' => $source_income
                  ];
                  if($DLNAME!="" && $DFNAME!="" && $DMNAME!="" && $relation!="" && $DCSTATUS!="" && $age!=""){
                    $sql_family = "INSERT INTO ofw_family (dependent_id, DLNAME, DFNAME, DMNAME, relation, DCSTATUS, age, source_income)
                    VALUES
                    (:f_final, :DLNAME, :DFNAME, :DMNAME, :relation, :DCSTATUS, :age, :source_income)";
                    $sthsql_family = $dbh->prepare($sql_family);
                    $sthsql_family->execute($sql_familydata);
                  }
                  else {
                  }
                }
              }
              if(!isset($_POST['dlname'])){

              }

              $auditdata = [
                      ':activity' => 'Added an Application (OFW_Website/Backend)',
                      ':username' => $_SESSION['login_user'],
                      ':datetime' => $datetime
              ];
              $audit = "INSERT INTO audit_trail_applications (activity, username, date) VALUES (:activity, :username, :datetime)";
              $sthaudit = $dbh->prepare($audit);
              $sthaudit->execute($auditdata);

              $countAT = "SELECT COUNT(*) as CountTodel FROM audit_trail_applications";
              $sthcountAT = $dbh->prepare($countAT);
              $sthcountAT->execute();
              $sthcountAT->setFetchMode(PDO::FETCH_ASSOC);
              while ($rowCountAT = $sthcountAT->fetch(PDO::FETCH_ASSOC))  {   
                      $CountATres = $rowCountAT['CountTodel']; 
              }   
              if($CountATres > 1000){
                      $delList = $CountATres - 1000;
                      $reduceAT = "DELETE FROM audit_trail_applications ORDER BY id ASC LIMIT ".$delList."";
                      $sthreduceAT = $dbh->prepare($reduceAT);
                      $sthreduceAT->execute();
              }
              if($CountATres <= 1000){

              }

              $_SESSION['status'] = 'Application was submitted successfully.';
              header('Location: ../forms/registration_OFW.php');
              $dbh = null;
            }
            else {
              $_SESSION['error'] = 'Sorry, something went wrong and application was not submitted.';
              header('Location: ../forms/registration_OFW.php');
              $dbh = null;
            }
          }
        }
      }
      if(isset($_POST["checkerOEC"])){
        $checkerOEC = $_POST["checkerOEC"];
        $querysearchdata = [
          ':project' => $project,
          ':lname' => $lname,
          ':fname' => $fname,
          ':mname' => $mname,
          ':dbirth' => $dbirth
        ];
        $querysearch = "SELECT PJ_CODE FROM ofw_applications 
                        LEFT JOIN ofw_applicant_info ON ofw_applications.applicant_id = ofw_applicant_info.applicant_id
                        WHERE PJ_CODE = :project AND LNAME = :lname AND FNAME = :fname AND MNAME = :mname AND BDATE = :dbirth";
        $sthquerysearch = $dbh->prepare($querysearch);
        $sthquerysearch->execute($querysearchdata);

        if($sthquerysearch->rowCount() > 0) {
                
          $_SESSION['error'] = 'Sorry, this application is still on process';
          header('Location: ../forms/registration_OFW.php');
        }
        else {
          $sql_applicant_infodata = [
            ':a_final' => $a_final,
            ':lname' => $lname,
            ':fname' => $fname,
            ':mname' => $mname,
            ':msurname' => $msurname,
            ':address' => $address,
            ':email' => $email,
            ':dbirth' => $dbirth,
            ':dplace' => $dplace,
            ':status' => $status,
            ':sex' => $sex,
            ':citizen' => $citizen,
            ':contact' => $contact,
            ':office' => $office,
            ':tin' => $tin,
            ':pagibig' => $pagibig,
            ':famIncome' => $famIncome,
            ':noViolation' => $noViolation,
            ':altViolation' => $altViolation
          ];
          $sql_applicant_info = "INSERT INTO ofw_applicant_info (applicant_id, LNAME, FNAME, MNAME, motherSurname, ADDR1, email, BDATE, birthplace, CSTATUS, sex, citizenship, contact, office, tin, pagibig, fam_income, non_violation, alt_violation) 
          VALUES 
          (:a_final, :lname, :fname, :mname, :msurname, :address, :email, :dbirth, :dplace, :status, :sex, :citizen, :contact, :office, :tin, :pagibig, :famIncome, :noViolation, :altViolation)";
          $sthsql_applicant_info = $dbh->prepare($sql_applicant_info);
          $sql_applicant_spousedata = [
            ':s_final' => $s_final,
            ':s_lname' => $s_lname,
            ':s_fname' => $s_fname,
            ':s_mname' => $s_mname,
            ':s_msurname' => $s_msurname,
            ':s_dbirth' => $s_dbirth,
            ':s_dplace' => $s_dplace
          ];
          $sql_applicant_spouse = "INSERT INTO ofw_applicant_spouse (spouse_id, SLNAME, SFNAME, SMNAME, SmotherSurname, SBDATE, Sbirthplace) 
          VALUES 
          (:s_final, :s_lname, :s_fname, :s_mname, :s_msurname, :s_dbirth, :s_dplace)";
          $sthsql_applicant_spouse = $dbh->prepare($sql_applicant_spouse);
          $sql_employment_statusdata = [
            ':e_final' => $e_final,
            ':nature' => $nature,
            ':agency' => $agency,
            ':a_address' => $a_address,
            ':SG' => $SG
          ];
          $sql_employment_status = "INSERT INTO ofw_employment_status (employment_id, nature, EMPLOYER, emp_address, MINCOME) 
          VALUES 
          (:e_final, :nature, :agency, :a_address, :SG)";
          $sthsql_employment_status = $dbh->prepare($sql_employment_status);
          $stat = "New Applicant";
          $source = "Website/Backend";
          $sql_applicationsdata = [
            ':payment' => $payment,
            ':type' => $type,
            ':employee' => $employee,
            ':app_date' => $app_date,
            ':project' => $project,
            ':a_final' => $a_final,
            ':s_final' => $s_final,
            ':e_final' => $e_final,
            ':f_final' => $f_final,
            ':r_final' => $r_final,
            ':username' => $usernam,
            ':stat' => $stat,
            ':checkerOEC' => $checkerOEC,
            ':source' => $source
          ];
          $sql_applications = "INSERT INTO ofw_applications (payment, house_type, employee, app_date, PJ_CODE, applicant_id, spouse_id, employment_id, dependent_id, req_id, username, app_status, fileUpload, source) 
          VALUES
          (:payment, :type, :employee, :app_date, :project, :a_final, :s_final, :e_final, :f_final, :r_final, :username, :stat, :checkerOEC, :source)";
          $sthsql_applications = $dbh->prepare($sql_applications);
          $sql_requirementsdata = [
            ':r_final' => $r_final
          ];
          $sql_requirements = "INSERT INTO requirements_ofw (req_id) VALUES (:r_final)"; 
          $sthsql_requirements = $dbh->prepare($sql_requirements);

          if ($sthsql_applicant_info->execute($sql_applicant_infodata) && $sthsql_applicant_spouse->execute($sql_applicant_spousedata) && $sthsql_employment_status->execute($sql_employment_statusdata) && $sthsql_applications->execute($sql_applicationsdata) && $sthsql_requirements->execute($sql_requirementsdata)) {
              if(isset($_POST['dlname'])){
                for($i=0;$i<count($_POST['slno']);$i++){
                  $DLNAME = $_POST['dlname'][$i];
                  $DFNAME = $_POST['dfname'][$i];
                  $DMNAME = $_POST['dmname'][$i];
                  $relation = $_POST['relation'][$i];
                  $DCSTATUS = $_POST['dcstatus'][$i];
                  $age = $_POST['age'][$i];
                  $source_income = $_POST['source_income'][$i];
                  $sql_familydata = [
                    ':f_final' => $f_final,
                    ':DLNAME' => $DLNAME,
                    ':DFNAME' => $DFNAME,
                    ':DMNAME' => $DMNAME,
                    ':relation' => $relation,
                    ':DCSTATUS' => $DCSTATUS,
                    ':age' => $age,
                    ':source_income' => $source_income
                  ];
                  if($DLNAME!="" && $DFNAME!="" && $DMNAME!="" && $relation!="" && $DCSTATUS!="" && $age!=""){
                    $sql_family = "INSERT INTO ofw_family (dependent_id, DLNAME, DFNAME, DMNAME, relation, DCSTATUS, age, source_income)
                    VALUES
                    (:f_final, :DLNAME, :DFNAME, :DMNAME, :relation, :DCSTATUS, :age, :source_income)";
                    $sthsql_family = $dbh->prepare($sql_family);
                    $sthsql_family->execute($sql_familydata);
                  }
                  else {
                  }
                }
              }
              else{}
              $auditdata = [
                      ':activity' => 'Added an Application (OFW_Website/Backend)',
                      ':username' => $_SESSION['login_user'],
                      ':datetime' => $datetime
              ];
              $audit = "INSERT INTO audit_trail_applications (activity, username, date) VALUES (:activity, :username, :datetime)";
              $sthaudit = $dbh->prepare($audit);
              $sthaudit->execute($auditdata);
              $_SESSION['status'] = 'Application was submitted successfully.';
              header('Location: ../forms/registration_OFW.php');
              $dbh = null;
          }
          else {
            $_SESSION['error'] = 'Sorry, something went wrong and application was not submitted.';
            header('Location: ../forms/registration_OFW.php');
            $dbh = null;
          }
        }
      }
    }
    else{
    http_response_code(400);
    die('Error processing bad or malformed request');
    } 
  }
  catch(PDOException $e){
    error_log('PDOException - ' . $e->getMessage(), 0);
    http_response_code(500);
    die('Error establishing connection with database');
  }
} 
else{
http_response_code(400);
die('Error processing bad or malformed request');
}    
?>
