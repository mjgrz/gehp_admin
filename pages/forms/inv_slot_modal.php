<div class="modal fade" id="modalviewSlot" role="dialog">
  <div class="modal-dialog modal-md" style="border-radius: 5px;">
    <div class="modal-content" id="modalBodySlot">
      <div class="slot_modal"></div>
    </div>
  </div>
</div>
<style>
  
  @media (min-width: 992px) {
    .modal-xxl {
      max-width: 80%;
    }
  }
  @media (min-width: 1200px) {
    .modal-xxl {
      max-width: 80%;
    }
  }

</style>