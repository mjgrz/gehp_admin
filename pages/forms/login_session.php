<?php
include("../../../../dbcon.php");
session_start();
$deploy = '';
if(isset($_SESSION['login_id'])){
    $user_check = $_SESSION['login_id'];
    if(is_numeric($user_check)){
        try{
            $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query_check = "SELECT * FROM users WHERE user_id = :user_check";
            $sthquery_check = $dbh->prepare($query_check);
            $sthquery_check->bindParam(':user_check', $user_check);
            $sthquery_check->execute();
            $sthquery_check->setFetchMode(PDO::FETCH_ASSOC); 
            while ($row = $sthquery_check->fetch(PDO::FETCH_ASSOC)) {
                $login_session = $row['username'];
                $login_id = $row['user_id'];
                $email_sender = $row['email'];
                $login_office = $row['office'];
                $login_officeID = $row['office_id'];
                $login_image = $row['user_image'];
                $login_userlevel = $row['userlevel'];
                //Head Office User
                if($login_office == 'Head Office' && $login_userlevel == 'User'){
                    $user_hide = 'hidden';
                    $pages_hide = 'hidden';
                    $element_hide = 'hidden';
                    $project_hide = 'hidden';
                    $drop_hide = 'hidden';
                    $delete_hide = 'hidden';
                    $delete_disabled = 'disabled';
                    $hideEMDreport = 'hidden';
                    $action_hide = '';
                    $inv_hide = 'hidden';
                    if($login_officeID == 'h60000000') {
                        $pages_hide = '';
                        $project_hide = '';
                        $action_hide = 'hidden';
                    }
                    if($login_officeID == 'h10000000') {
                        $element_hide = '';
                        $hideEMDreport = '';
                    }
                    if($login_officeID == 'h20000000' || $login_officeID == 'h30000000' || $login_officeID == 'h40000000') {
                        $action_hide = 'hidden';
                    }
                }
                //Head Office Administrator
                if($login_office == 'Head Office' && $login_userlevel == 'Administrator'){
                    $user_hide = '';
                    $pages_hide = 'hidden';
                    $element_hide = '';
                    $project_hide = 'hidden';
                    $drop_hide = 'hidden';
                    $delete_hide = 'hidden';
                    $delete_disabled = 'disabled';
                    $hideEMDreport = 'hidden';
                    $action_hide = '';
                    $inv_hide = 'hidden';
                    if($login_officeID == 'h20000000' || $login_officeID == 'h30000000' || $login_officeID == 'h40000000') {
                        $drop_hide = '';
                        $hideEMDreport = '';
                        $action_hide = 'hidden';
                    }
                    if($login_officeID == 'h10000000') {
                        $drop_hide = '';
                        $hideEMDreport = '';
                        $delete_hide = '';
                        $delete_disabled = '';
                    }
                    if($login_officeID == 'h50000000') {
                        $drop_hide = '';
                        $delete_hide = '';
                        $delete_disabled = '';
                        $hideEMDreport = '';
                        $pages_hide = '';
                        $project_hide = '';
                        $inv_hide = '';
                    }
                    if($login_officeID == 'h60000000') {
                        $pages_hide = '';
                        $project_hide = '';
                        $drop_hide = '';
                        $hideEMDreport = 'hidden';
                        $action_hide = 'hidden';
                        $inv_hide = '';
                    }
                }
                //Regional User
                if($login_office == 'Regional' && $login_userlevel == 'User'){
                    include("../../../../dbcon.php");
                    try{
                        $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        $query = "SELECT region_id FROM table_ro_ WHERE ro_id = :login_officeID";
                        $sthquery = $dbh->prepare($query);
                        $sthquery->bindParam(':login_officeID', $login_officeID);
                        $sthquery->execute();
                        $sthquery->setFetchMode(PDO::FETCH_ASSOC); 
                        while ($reg_row = $sthquery->fetch(PDO::FETCH_ASSOC)) {
                            $reglogin_officeID = $reg_row['region_id'];
                        }
                        $dbh = null;
                        $delete_hide = 'hidden';
                        $delete_disabled = 'disabled';
                        $user_hide = 'hidden';
                        $pages_hide = 'hidden';
                        $element_hide = 'hidden';
                        $project_hide = 'hidden';
                        $drop_hide = 'hidden';
                        $hideEMDreport = 'hidden';
                        $action_hide = '';
                        $inv_hide = 'hidden';
                    }
                    catch(PDOException $e){
                        error_log('PDOException - ' . $e->getMessage(), 0);
                        http_response_code(500);
                        die('Error establishing connection with database');
                    }
                }
                //Regional Administrator
                if($login_office == 'Regional' && $login_userlevel == 'Administrator'){
                    include("../../../../dbcon.php");
                    try{
                        $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        $query = "SELECT region_id FROM table_ro_ WHERE ro_id = :login_officeID";
                        $sthquery = $dbh->prepare($query);
                        $sthquery->bindParam(':login_officeID', $login_officeID);
                        $sthquery->execute();
                        $sthquery->setFetchMode(PDO::FETCH_ASSOC); 
                        while ($reg_row = $sthquery->fetch(PDO::FETCH_ASSOC)) {
                            $reglogin_officeID = $reg_row['region_id'];
                        }
                        $dbh = null;
                        $delete_hide = '';
                        $delete_disabled = '';
                        $user_hide = '';
                        $pages_hide = 'hidden';
                        $element_hide = '';
                        $project_hide = 'hidden';
                        $drop_hide = 'hidden';
                        $hideEMDreport = 'hidden';
                        $action_hide = '';
                        $inv_hide = 'hidden';
                    }
                    catch(PDOException $e){
                        error_log('PDOException - ' . $e->getMessage(), 0);
                        http_response_code(500);
                        die('Error establishing connection with database');
                    }
                }
            }
            if(!isset($_SESSION['login_user'])){
                $dbh = null;
                header("location:login.php");
                die();
            }
        }
        catch(PDOException $e){
            error_log('PDOException - ' . $e->getMessage(), 0);
            http_response_code(500);
            die('Error establishing connection with database');
        }
    }
    else{
    http_response_code(400);
    die('Error processing bad or malformed request');
    }
}
else{
    header("location:login.php");
    die();
}
include("session_expire.php");
?>
