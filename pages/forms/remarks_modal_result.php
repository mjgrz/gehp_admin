<?php
$remarks_id = $_GET['remarks_id'];
include("../../../../dbcon.php");
session_start();
?>
<?php
if(is_numeric($remarks_id)){
  try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $remarks_select_modal = "SELECT remarks FROM applications WHERE APP_ID = :remarks_id";
    $sthremarks_select_modal = $dbh->prepare($remarks_select_modal);
    $sthremarks_select_modal->bindParam(':remarks_id', $remarks_id);
    $sthremarks_select_modal->execute();
    $sthremarks_select_modal->setFetchMode(PDO::FETCH_ASSOC); 
    while ($remarks_modal = $sthremarks_select_modal->fetch(PDO::FETCH_ASSOC)) {
      $remarks = $remarks_modal["remarks"];
    }
    $dbh = null;
  }
  catch(PDOException $e){
    error_log('PDOException - ' . $e->getMessage(), 0);
    http_response_code(500);
    die('Error establishing connection with database');
  }
}
else{
  http_response_code(400);
  die('Error processing bad or malformed request');
}
?>

<div class="modal-header" style="background:#67a2b2;">
  <h4 class="modal-title" style="color:white;">Pending</h4>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true" style="color: white;">&times;</span>
  </button>
</div>
<form method="POST" action="remarks_process.php">
  <div class="modal-body" style="background: #f6f6f6;">
    <div class="col-sm-12">
      <div class="col-lg-12">
        <div class="form-group">
          <p style="font-size: 20px;">Remarks</p>
          <input type="text" name="APP_ID" id="APP_ID" value="<?php echo $remarks_id ?>" hidden>
          <input type="text" name="username" id="username" value="<?php echo $_SESSION['login_user']?>" hidden>
          <input type="text" name="status" id="status" value="Pending" hidden>
          <textarea class="form-control" style="resize: none;" name ="remarks" value="<?php echo $remarks ?>"
          oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" rows="6" required width="100%"><?php echo $remarks ?></textarea>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer justify-content-right">
    <button type="submit" onclick="SubRemarksGov()" class="btn btn-default color-orange" style="font-size:large; width:auto;"><i class="fa fa-paper-plane"></i>&nbsp; Submit</button>
  </div>
</form>

<script>
  function SubRemarksGov() {
var remarks = document.getElementById("remarks").value;
// Returns successful data submission message when the entered information is stored in database.
var dataString = 'remarks=' + remarks;
  if (remarks == '') {
  } 
  else {
    $.ajax({
    type: "POST",
    url: "remarks_process.php",
    data: dataString,
    cache: false,
    success: function(html) {
    }
    });
  }
}
</script>