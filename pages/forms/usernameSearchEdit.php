<?php
include("login_session.php");
$unameEditsearchmatch = $_POST["username"];

if($_SESSION["validity_mail"] == 0){
  echo "<script>document.getElementById('submitform').disabled = true;</script>";
  echo "<script>document.getElementById('edit_password').disabled = true;</script>";
  echo "<script>document.getElementById('edit_confirm_password').disabled = true;</script>";
  $validity_mail = 1;
  $_SESSION["validity_username"] = $validity_mail;

  if(!empty($_POST["username"])) {
    try{
      $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      if($_POST['userid'] == "$login_id"){
        $unameEditmatchquery = "SELECT username FROM users WHERE username = :unameEditsearchmatch";
        $sthunameEditmatchquery = $dbh->prepare($unameEditmatchquery);
        $sthunameEditmatchquery->bindParam(':unameEditsearchmatch', $unameEditsearchmatch);
        

        if($sthunameEditmatchquery->execute()) {
          if($sthunameEditmatchquery->rowCount()>0) {
            if($_POST["username"] != $login_session){
              echo "<span style='color:red'> (Username already exists.)</span>";
              echo "<script>document.getElementById('username').style.border = '2px solid red';</script>";
              $dbh = null;
              $validity_mail = 0;
              $_SESSION["validity_username"] = $validity_mail;
            }
            if($_POST["username"] == $login_session){
              echo "<span style='color:green'> (Username currently in use.)</span>";
              echo "<script>document.getElementById('username').style.border = '';</script>";
              echo "<script>
                    if(document.getElementById('edit_password').style.border == '2px solid red'){
                    }
                    else {
                    }
                    </script>";
              $dbh = null;
            }
          }
          if($sthunameEditmatchquery->rowCount() == 0) {
            echo "<span style='color:green'> (Username available.)</span>";
            echo "<script>document.getElementById('username').style.border = '';</script>";
            echo "<script>
                  if(document.getElementById('edit_password').style.border == '2px solid red'){
                  }
                  else {
                  }
                  </script>";
            $dbh = null;
          }
        }
        else 
        {
          echo "<span style='color:red'> (Invalid input.)</span>";
          $dbh = null;
          $validity_mail = 0;
          $_SESSION["validity_username"] = $validity_mail;
        }
      }
      else 
      {
        $result2query = "SELECT user_id FROM users WHERE username=:unameEditsearchmatch";
        $sthresult2query = $dbh->prepare($result2query);
        $sthresult2query->bindParam(':unameEditsearchmatch', $unameEditsearchmatch);
        $sthresult2query->execute();
        $sthresult2query->setFetchMode(PDO::FETCH_ASSOC); 

        $result1query = "SELECT username FROM users WHERE username=:unameEditsearchmatch";
        $sthresult1query = $dbh->prepare($result1query);
        $sthresult1query->bindParam(':unameEditsearchmatch', $unameEditsearchmatch);
        $sthresult1query->execute();
        
        if($sthresult1query->execute()) {
          if($sthresult1query->rowCount() > 0) {
            while ($selectrow = $sthresult2query->fetch(PDO::FETCH_ASSOC)) {
              $user_id = $selectrow['user_id'];
            }
            if($user_id == $_POST['userid']) {
              echo "<span style='color:green'> (Username currently in use.)</span>";
              echo "<script>document.getElementById('username').style.border = '';</script>";
              $dbh = null;
            }
            else 
            {
              echo "<span style='color:red'> (Username already exists.)</span>";
              echo "<script>document.getElementById('username').style.border = '2px solid red';</script>";
              $dbh = null;
              $validity_mail = 0;
              $_SESSION["validity_username"] = $validity_mail;
            }
          }
          else {
            echo "<span style='color:green'> (Username available.)</span>";
            echo "<script>document.getElementById('username').style.border = '';</script>";
            $dbh = null;
          }
        }
        else 
        {
          echo "<span style='color:red'> (Invalid input.)</span>";
          $dbh = null;
          $validity_mail = 0;
          $_SESSION["validity_username"] = $validity_mail;
        }
      }
    }
    catch(PDOException $e){
        error_log('PDOException - ' . $e->getMessage(), 0);
        http_response_code(500);
        die('Error establishing connection with database');
    }
  }
  else {
    echo "<script>document.getElementById('username').style.border = '';</script>";
  }

}
else{
  
  if(!empty($_POST["username"])) {
    $validity_mail = 1;
    $_SESSION["validity_username"] = $validity_mail;
    try{
      $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      if($_POST['userid'] == "$login_id"){
        $unameEditmatchquery = "SELECT username FROM users WHERE username = :unameEditsearchmatch";
        $sthunameEditmatchquery = $dbh->prepare($unameEditmatchquery);
        $sthunameEditmatchquery->bindParam(':unameEditsearchmatch', $unameEditsearchmatch);
        

        if($sthunameEditmatchquery->execute()) {
          if($sthunameEditmatchquery->rowCount()>0) {
            if($_POST["username"] != $login_session){
              echo "<span style='color:red'> (Username already exists.)</span>";
              echo "<script>document.getElementById('username').style.border = '2px solid red';</script>";
              echo "<script>document.getElementById('submitform').disabled = true;</script>";
              $dbh = null;
              $validity_mail = 0;
              $_SESSION["validity_username"] = $validity_mail;
            }
            if($_POST["username"] == $login_session){
              echo "<span style='color:green'> (Username currently in use.)</span>";
              echo "<script>document.getElementById('username').style.border = '';</script>";
              echo "<script>document.getElementById('submitform').disabled = false;</script>";
              $dbh = null;
            }
          }
          if($sthunameEditmatchquery->rowCount() == 0) {
            echo "<span style='color:green'> (Username available.)</span>";
            echo "<script>document.getElementById('username').style.border = '';</script>";
            echo "<script>document.getElementById('submitform').disabled = false;</script>";
            $dbh = null;
          }
        }
        else 
        {
          echo "<span style='color:red'> (Invalid input.)</span>";
          $dbh = null;
          $validity_mail = 0;
          $_SESSION["validity_username"] = $validity_mail;
        }
      }
      else 
      {
        $result2query = "SELECT user_id FROM users WHERE username=:unameEditsearchmatch";
        $sthresult2query = $dbh->prepare($result2query);
        $sthresult2query->bindParam(':unameEditsearchmatch', $unameEditsearchmatch);
        $sthresult2query->execute();
        $sthresult2query->setFetchMode(PDO::FETCH_ASSOC); 

        $result1query = "SELECT username FROM users WHERE username=:unameEditsearchmatch";
        $sthresult1query = $dbh->prepare($result1query);
        $sthresult1query->bindParam(':unameEditsearchmatch', $unameEditsearchmatch);
        $sthresult1query->execute();
        
        if($sthresult1query->execute()) {
          if($sthresult1query->rowCount() > 0) {
            while ($selectrow = $sthresult2query->fetch(PDO::FETCH_ASSOC)) {
              $user_id = $selectrow['user_id'];
            }
            if($user_id == $_POST['userid']) {
              echo "<span style='color:green'> (Username currently in use.)</span>";
              echo "<script>document.getElementById('username').style.border = '';</script>";
              echo "<script>document.getElementById('submitform').disabled = false;</script>";
              $dbh = null;
            }
            else 
            {
              echo "<span style='color:red'> (Username already exists.)</span>";
              echo "<script>document.getElementById('username').style.border = '2px solid red';</script>";
              echo "<script>document.getElementById('submitform').disabled = true;</script>";
              $dbh = null;
              $validity_mail = 0;
              $_SESSION["validity_username"] = $validity_mail;
            }
          }
          else {
            echo "<span style='color:green'> (Username available.)</span>";
            echo "<script>document.getElementById('username').style.border = '';</script>";
            echo "<script>document.getElementById('submitform').disabled = false;</script>";
            $dbh = null;
          }
        }
        else 
        {
          echo "<span style='color:red'> (Invalid input.)</span>";
          $dbh = null;
          $validity_mail = 0;
          $_SESSION["validity_username"] = $validity_mail;
        }
      }
    }
    catch(PDOException $e){
        error_log('PDOException - ' . $e->getMessage(), 0);
        http_response_code(500);
        die('Error establishing connection with database');
    }
  }
  else {
    echo "<script>document.getElementById('username').style.border = '';</script>";
    echo "<script>document.getElementById('submitform').disabled = false;</script>";
  }
}
?>