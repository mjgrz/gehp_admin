<?php
$target_dir = "uploads/users/";
$target_file = $target_dir . basename("$login_image");
if(file_exists($target_file)){
  $profile = $login_image;
} 
else {
  $profile = 'user.png';
}
?>
<div class="modal fade" id="profile">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 5px;">
      <div class="modal-header" style="background: #67a2b2;">
        <h4 class="modal-title" style="color: white;">Change Profile Picture</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: white;">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!--Form for changing account's image/picture-->
      <form method="POST" action="user_profilepic.php" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="col-sm-12">
            <div class="col-lg-12">
              <div class="form-group" id="profilediv">
                <input type="text" name="usernameprofile" value="<?php echo $username ?>" hidden>
                <input type="text" name="officeidprofile" value="<?php echo $office_id ?>" hidden>
                <input type="text" name="useridprofile" value="<?php echo $user_id ?>" hidden>
                <input type="file" id="profilepic" name="profilepic" title="Click in this area to upload an image." onchange="preview()">
                <p id="profileimage"></p>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-right">
          <button type="submit" class="btn btn-default color-cyan" style="font-size:large; width:auto;"><i class="fa fa-paper-plane"></i>&nbsp; Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
  $(function () {
    bsCustomFileInput.init();
  });
  function preview() {
    var urlimage = URL.createObjectURL(event.target.files[0]);
    document.getElementById("profileimage").style.backgroundImage = "url("+urlimage+")";
    document.getElementById("profileimage").style.backgroundSize = "100% auto";
    document.getElementById("profileimage").style.backgroundRepeat = "no-repeat";
    document.getElementById("profileimage").style.backgroundPosition = "center";
    document.getElementById("profileimage").innerHTML = "";
  }
</script>

<style>
  #profilediv {
    width: 100%;
    height: 350px;
    border: 2px dashed #67a2b2;
    border-radius: 5px;  
  }
  #profilepic {
    position: absolute;
    margin: 0;
    padding: 0;
    width: 98%;
    height: 100%;
    outline: none;
    opacity: 0;
  }
  #profilepic:hover {
    cursor: pointer;
  }
  #profileimage {
    text-align: center;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 5px;
  }
  #profileimage {
    background-image: url(uploads/users/<?php echo $profile ?>);
    background-position: center;
    background-repeat: no-repeat;
    background-size: contain;
  }
  input:hover, select:hover, textarea:hover {
    border: 1px solid #67a2b2;
  }
</style>