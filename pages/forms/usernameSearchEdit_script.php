<script>
    function checkusernameAvailabilityEdit() {
        $("#loaderIcon").show();
            jQuery.ajax({
            url: "usernameSearchEdit.php",
            data: {'username': $("#username").val(), 'userid': $("#userid").val()},
            type: "POST",
            success:function(data){
                $("#username-availability-status").html(data);
                $("#loaderIcon").hide();
            },
            error:function (){
            }
        });
    }
</script>