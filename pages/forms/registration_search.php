<?php
include('login_session.php');
$_SESSION["now"] = $_POST["checkers"];
error_reporting(E_ERROR | E_PARSE);
?>
<span>
<div style="overflow:auto; height: 650px; padding: 7px;">
<table id="projectTable" class="table table-bordered table-striped" style=" border-radius: 5px;">
  <thead>
  <tr>
    <th <?php echo $delete_hide; ?> style="white-space: nowrap; " class="project-actions text-center"><input id="chk_all" name="chk_all" type="checkbox"></th>
    <th>Date Submitted</th>
    <th>Name of Project</th>
    <th>Applicant's Name</th>
    <th>Address</th>
    <th>Contact No.</th>
    <th>Submitted Documents</th>
    <th>Remarks</th>
    <th>Status</th>
    <th>User/Operator</th>
    <th>Full Details</th>
  </tr>
  </thead>
  <tbody>
    <?php
    try{
      $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      
      if($login_office == 'Regional'){
        $selectdata = [
          ':reglogin_officeID' => $reglogin_officeID,
          ':checkercount' => $_SESSION["now"],
          ':checkercount2' => $_SESSION["now"],
          ':checkercount3' => $_SESSION["now"],
        ];
        $select = "SELECT * FROM applications 
                        LEFT JOIN applicant_info ON applications.applicant_id = applicant_info.applicant_id 
                        LEFT JOIN applicant_spouse ON applications.spouse_id = applicant_spouse.spouse_id 
                        LEFT JOIN employment_status ON applications.employment_id = employment_status.employment_id 
                        LEFT JOIN requirements ON applications.req_id = requirements.req_id 
                        LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
                        WHERE project_info.archived != '1' AND uni_rank = '' AND project_info.region = :reglogin_officeID AND LNAME LIKE CONCAT('%', :checkercount, '%') 
                        OR project_info.archived != '1' AND uni_rank = '' AND project_info.region = :reglogin_officeID AND FNAME LIKE CONCAT('%', :checkercount2, '%')
                        OR project_info.archived != '1' AND uni_rank = '' AND project_info.region = :reglogin_officeID AND PJ_NAME LIKE CONCAT('%', :checkercount3, '%')
                        ORDER BY APP_ID DESC";
        $sthselect = $dbh->prepare($select);
        //$sthselect->bindParam(':reglogin_officeID', $reglogin_officeID);
        $sthselect->execute($selectdata);
        $count = $sthselect->rowCount(); 
        $setcount = $count;
        
        if (isset($_GET['pageno'])) {
          $pageno = $_GET['pageno'];
          //$i = $_GET['pageno'];
          if($_GET['pageno'] == 1){
            $i = 2;
          }
        } 
        else {
          $pageno = 1;
          $i = 2;
        }

        $no_of_records_per_page = 10;
        $offset = ($pageno-1) * $no_of_records_per_page;

        $total_pages = ceil($count / $no_of_records_per_page);

        $cnt=1;
        
        $sql2data = [
          ':reglogin_officeID' => $reglogin_officeID,
          ':checkercount' => $_SESSION["now"],
          ':checkercount2' => $_SESSION["now"],
          ':checkercount3' => $_SESSION["now"],
        ];
        $sql2 = "$select LIMIT $offset, $no_of_records_per_page";

        $sthsql2 = $dbh->prepare($sql2);
        //$sthsql2->bindParam(':reglogin_officeID', $reglogin_officeID);
        $sthsql2->execute($sql2data);
        $sthsql2->setFetchMode(PDO::FETCH_ASSOC); 
        $app_count = $sthsql2->rowCount();
        $setcount = $app_count;
      }
      else {
        $selectdata = [
          ':checkercount' => $_SESSION["now"],
          ':checkercount2' => $_SESSION["now"],
          ':checkercount3' => $_SESSION["now"],
        ];
        $select = "SELECT * FROM applications 
                        LEFT JOIN applicant_info ON applications.applicant_id = applicant_info.applicant_id 
                        LEFT JOIN applicant_spouse ON applications.spouse_id = applicant_spouse.spouse_id 
                        LEFT JOIN employment_status ON applications.employment_id = employment_status.employment_id 
                        LEFT JOIN requirements ON applications.req_id = requirements.req_id 
                        LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
                        WHERE project_info.archived != '1' AND uni_rank = '' AND LNAME LIKE CONCAT('%', :checkercount, '%')
                        OR project_info.archived != '1' AND uni_rank = '' AND FNAME LIKE CONCAT('%', :checkercount2, '%')
                        OR project_info.archived != '1' AND uni_rank = '' AND PJ_NAME LIKE CONCAT('%', :checkercount3, '%')
                        ORDER BY APP_ID DESC";
        $sthselect = $dbh->prepare($select);
        $sthselect->execute($selectdata);
        $count = $sthselect->rowCount();
        $setcount = $count;
        
        if (isset($_GET['pageno'])) {
          $pageno = $_GET['pageno'];
          //$i = $_GET['pageno'];
          if($_GET['pageno'] == 1){
            $i = 2;
          }
        } 
        else {
          $pageno = 1;
          $i = 2;
        }

        $no_of_records_per_page = 10;
        $offset = ($pageno-1) * $no_of_records_per_page;

        $total_pages = ceil($count / $no_of_records_per_page);

        $cnt=1;

        $sql2data = [
          ':checkercount' => $_SESSION["now"],
          ':checkercount2' => $_SESSION["now"],
          ':checkercount3' => $_SESSION["now"],
        ];
        
        $sql2 = "$select LIMIT $offset, $no_of_records_per_page";

        $sthsql2 = $dbh->prepare($sql2);
        $sthsql2->execute($sql2data);
        $sthsql2->setFetchMode(PDO::FETCH_ASSOC); 
        $app_count = $sthsql2->rowCount();
        $setcount = $app_count;
      }
        $i=0;
        while ($row = $sthsql2->fetch(PDO::FETCH_ASSOC)) {
        $idreq = $row["req_id"];
        $LNAME = $row["LNAME"];
        $FNAME = $row["FNAME"];
        $MNAME = $row["MNAME"];
        $BDATE = $row["BDATE"];
        $PJ_CODE = $row["PJ_CODE"];
        ?>
        <tr>
        <td <?php echo $delete_hide; ?> class="project-actions text-center" style="vertical-align: middle;"><input name="check[]" form="deleteForm" type="checkbox" class='chkbox'
          value="<?php echo $row['applicant_id']; ?>"/></td>
        <td style="vertical-align: middle;"><?php echo date('F d, Y', strtotime($row["app_date"])) ?></td>
        <td style="vertical-align: middle;"><?php echo $row["PJ_NAME"] ?></td>
        <td style="vertical-align: middle;"><?php echo $row["LNAME"].", ".$row["FNAME"]." ". $row["MNAME"] ?></td>
        <td style="vertical-align: middle;"><?php echo $row["ADDR1"] ?></td>
        <td style="vertical-align: middle;"><?php echo $row["contact"] ?></td>
        <td style="vertical-align: middle; min-width: 400px;">
          <div class="col-sm-12">
            <div class="row">
              <div class="col-sm-6">
                <div class="col-sm-12">
                  <?php if( $row["app_form"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                  &nbsp; Application Form
                </div>
                <div class="col-sm-12">
                  <?php if( $row["compensation"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                  &nbsp; Certificate of Compensation
                </div>
                <div class="col-sm-12">
                  <?php if( $row["bir"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                  &nbsp; BIR Certified Income Tax Return
                </div>
                <div class="col-sm-12">
                  <?php if( $row["active_service"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                  &nbsp; Certificate of Active in Service
                </div>
                <div class="col-sm-12">
                  <?php if( $row["birth_cert"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                  &nbsp; Birth Certificate (Single)
                </div>
                <div class="col-sm-12">
                  <?php if( $row["marriage"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                  &nbsp; Marriage Certificate
                </div>
                <div class="col-sm-12">
                  <?php if( $row["separation"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                  &nbsp; Affidavit of Separation-in-Fact
                </div>
                <div class="col-sm-12">
                  <?php if( $row["gov_id"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                  &nbsp; Two (2) Government-Issued ID
                </div>
              </div>
              <div class="col-sm-6">
                <div class="col-sm-12">
                  <?php if( $row["income_tax"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                  &nbsp; Latest Income Tax Return (ITR)
                </div>
                <div class="col-sm-12">
                  <?php if( $row["pagibig_loan"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                  &nbsp; PAG-IBIG Housing Loan Application
                </div>
                <div class="col-sm-12">
                  <?php if( $row["pay_slip"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                  &nbsp; One (1) Month Pay Slip
                </div>
                <div class="col-sm-12">
                  <?php if( $row["sig_id"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                  &nbsp; One (1) Valid ID with Signature
                </div>
                <div class="col-sm-12">
                  <?php if( $row["borrower_confo"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                  &nbsp; Notarized Borrower's Conformity
                </div>
                <div class="col-sm-12">
                  <?php if( $row["amortization"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                  &nbsp; Authority to Deduct Loan Amortization
                </div>
                <div class="col-sm-12">
                  <?php if( $row["disinterested"] == 1){echo "<i class='fa fa-check' style='color: green;'></i>";} else{echo "<i class='fa fa-times' style='color: red;'></i>";} ?>
                  &nbsp; Notarized Affidavit of Two Disinterested Person
                </div>
              </div>
            </div>
            <div style="margin-top: 5px;">
              <a style="width: 100%;" type="button" data-a="<?php echo $row['req_id'] ?>" data-toggle="modal" href="#reqAPP" style="color: white;" class="requirements btn btn-info btn-sm">
                <i class="fa fa-check-square">
                </i>&nbsp;
                Update Checklist
              </a>
            </div>
          </div>
        </td>
        <td style="vertical-align: middle;">
          <?php 
            $remarks = $row["remarks"];
            if($remarks == "") {
              $hideRemarks = "hidden";
            }
            if($remarks != "") {
              $hideRemarks = "";
            }
          ?>
          <b <?php echo $hideRemarks ?>><?php echo $row["remarks"] ?></b>
          <br <?php echo $hideRemarks ?>><br <?php echo $hideRemarks ?>>
          <?php
          try{
            $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $querysearchdata = [
              ':LNAME' => $LNAME,
              ':FNAME' => $FNAME,
              ':MNAME' => $MNAME,
              ':BDATE' => $BDATE,
              ':PJ_CODE' => $PJ_CODE
            ];
            $querysearch = "SELECT FNAME, project_info.PJ_NAME, table_ro_.RO FROM applications 
                            LEFT JOIN applicant_info ON applications.applicant_id = applicant_info.applicant_id
                            LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
                            LEFT JOIN table_ro_ ON project_info.region = table_ro_.region_id
                            LEFT JOIN employment_status ON applications.employment_id = employment_status.employment_id
                            WHERE project_info.archived != '1' AND uni_rank = '' AND LNAME = :LNAME AND FNAME = :FNAME
                            AND MNAME = :MNAME AND BDATE = :BDATE AND applications.PJ_CODE != :PJ_CODE";
            $sthquerysearch = $dbh->prepare($querysearch);
            $sthquerysearch->execute($querysearchdata);
            $sthquerysearch->setFetchMode(PDO::FETCH_ASSOC); 
            $app_count = $sthquerysearch->rowCount();
            if($app_count>0) {

              echo "<i>";
              echo "has other application(s) in:";
              echo "<br>";

              for($i=0;$i<$app_count;$i++){
                $select = $sthquerysearch->fetch(PDO::FETCH_ASSOC);
                $list = $select["PJ_NAME"];
                echo "<p>".$list."</p>";
                
              }
              echo "</i>";
              $dbh = null;
            }
            else {
              echo "<i>";
              echo "has no other application(s)";
              echo "</i>";
              $dbh = null;
            }
          }
          catch(PDOException $e){
              error_log('PDOException - ' . $e->getMessage(), 0);
              http_response_code(500);
              die('Error establishing connection with database');
          }
          ?>
        </td>
          <?php 
            if($row["app_status"] == "Pending"){
              echo "<td style='color: white; vertical-align: middle;'><span style='text-align: center; background: orange; padding: 3px 6px; border-radius: 5px;'><b>";
              echo $row["app_status"];
              echo"</b></span></td>";
            }
            if($row["app_status"] == "Qualified"){
              echo "<td style='color: white; vertical-align: middle;'><span style='text-align: center; background: green; padding: 3px 6px; border-radius: 5px;'><b>";
              echo $row["app_status"];
              echo"</b></span></td>";
            }
            if($row["app_status"] == "Disqualified"){
              echo "<td style='color: white; vertical-align: middle;'><span style='text-align: center; background: red; padding: 3px 6px; border-radius: 5px;'><b>";
              echo $row["app_status"];
              echo"</b></span></td>";
            }
            if($row["app_status"] == "New Applicant") 
            {
              echo "<td style='vertical-align: middle;'><b>";
              echo $row["app_status"];
              echo"</b></td>";
            }
          ?>
        <td style="vertical-align: middle; text-align: center;">
          <a class="modal-user" data-toggle="modal" data-a="<?php echo $row['username'] ?>" href="#modal_user"><?php echo $row["username"] ?></a>
          <style>
            .modal-user{
              cursor: pointer;
            }
          </style>
        </td>
        <td class="project-actions text-center" style="vertical-align: middle; white-space: nowrap;">
          <button id="tooltip" type="button" data-a="<?php echo $row['APP_ID'] ?>"
          data-toggle='modal' data-keyboard='false' href='#modalview' style="font-size:large;" class="btn_reg_modal btn btn-default btn-sm color-blue-dark">
            <i class="fa fa-eye">
            </i>
            <span class="tooltiptext">View</span>
          </button>
        </td>
        </tr>
        <?php 
        $i++;
      } 
      $dbh = null;
    }
    catch(PDOException $e){
        error_log('PDOException - ' . $e->getMessage(), 0);
        http_response_code(500);
        die('Error establishing connection with database');
    }
    ?>
    </form>
  </tbody>
</table>
  </div>

<?php include("paging.php"); ?>
<?php include("delete_script.php");?>
<?php include("requirements_modal.php"); ?>
<?php include("disqualified_modal_alt.php"); ?>
<?php include("qualified_modal_alt.php"); ?>
<?php include("remarks_modal_alt.php"); ?>
<?php include("users_modal.php"); ?>
<?php include("registration_modal_alt.php"); ?>
</span>

<script>
  $(function () {
    $("#projectTable").DataTable({
      "destroy": true,
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": false,
      "responsive": false,
      //"buttons": ["excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#userTable_wrapper .col-md-6:eq(0)');
  });
</script>

<script>
  $('.btn_reg_modal').click(function(){
      var ID=$(this).attr('data-a');
      acH = document.getElementById("action_hide_text").value;
      $.ajax({url:"registration_modal_result?ID="+ID+"&acH="+acH,cache:false,success:function(result){
          $(".registration-modal").html(result);
      }});
  });

  $('.modal-user').click(function(){
      var username=$(this).attr('data-a');
      $.ajax({url:"users_modal_result?username="+username,cache:false,success:function(result){
          $(".users-modal").html(result);
      }});
  });

  $('.requirements').click(function(){
      var req_id=$(this).attr('data-a');
      $.ajax({url:"reqGOV_modal_result?req_id="+req_id,cache:false,success:function(result){
          $(".req-modal").html(result);
      }});
  });
  
</script>