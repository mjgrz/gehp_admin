<!DOCTYPE html>
<html lang="en">
<head>
<?php

use Google\Service\CloudSearch\PushItem;

include('login_session.php');
// include("../../../../dbcon.php"); 
if($login_officeID != "h60000000" || $login_officeID != "h50000000"){
  if($login_officeID == "h60000000"){
    
  }
  else if($login_officeID == "h50000000"){
    
  }
  else{
    header("location:../../index.php");
  }
}
error_reporting(E_ERROR | E_PARSE);

$SLCT_PJID = $_GET['ID'];
$SLCT_FLOOR = $_GET['FLOOR'];
$dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$querypjselect = "SELECT * FROM inventory WHERE project_id = '$SLCT_PJID' AND floor_num = '$SLCT_FLOOR'";
$sthpj = $dbh->prepare($querypjselect);
$sthpj->execute();
$cntpj = $sthpj->rowCount();
$sthpj->setFetchMode(PDO::FETCH_ASSOC); 
if($cntpj > 0) {
  while ($pjrowslct = $sthpj->fetch(PDO::FETCH_ASSOC)) {
    $id = $pjrowslct['id'];
    $slot = $pjrowslct['slot_list'];
    $slot_img = $pjrowslct['slot_map'];
  }
}
else{
  $id = "Null";
  $slot = "Null";
  $slot_img = "Null";
}
?>

<link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
<link rel="stylesheet" href="../../plugins/toastr/toastr.min.css">
<link rel="stylesheet" href="../../dist/css/adminlte.min.css">

<script src="../../plugins/jquery/jquery.min.js"></script>
<script src="../../plugins/toastr/toastr.min.js"></script>

<?php
if (isset($_SESSION["status"]) && $_SESSION["status"] !='') {
  $msg = $_SESSION["status"];
  echo "<span><script type='text/javascript'>toastr.success('$msg')</script></span>";
  
  unset($_SESSION["status"]);
}
if (isset($_SESSION["error"]) && $_SESSION["error"] !='') {
  $msg = $_SESSION["error"];
  echo "<span><script type='text/javascript'>toastr.error('$msg')</script></span>";
  
  unset($_SESSION["error"]);
}
if (isset($_SESSION["info"]) && $_SESSION["info"] !='') {
  $msg = $_SESSION["info"];
  echo "<span><script type='text/javascript'>toastr.info('$msg')</script></span>";
  
  unset($_SESSION["info"]);
}
else {

}
?>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>NHA | Government Employee's Housing Program</title>
  <link rel="shortcut icon" type="image/png" href="../../dist/img/nha_logo_circle.png">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
</head>
<body class="hold-transition sidebar-mini">
  
<script language="javascript">
           var message="This function is not allowed here.";
           function clickIE4(){
                 if (event.button==2){
                     alert(message);
                     return false;
                 }
           }
           function clickNS4(e){
                if (document.layers||document.getElementById&&!document.all){
                        if (e.which==2||e.which==3){
                                  alert(message);
                                  return false;
                        }
                }
           }
           if (document.layers){
                 document.captureEvents(Event.MOUSEDOWN);
                 document.onmousedown=clickNS4;
           }
           else if (document.all&&!document.getElementById){
                 document.onmousedown=clickIE4;
           }
           document.oncontextmenu=new Function("alert(message);return false;")
</script>

<div class="wrapper">
  <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="font-weight: bold;">
    <?php include("header.php"); ?>
  </nav>

  <aside class="main-sidebar sidebar-dark-primary elevation-4" style="background: #002d32; position: fixed;">
    <a href="../../index.php" class="brand-link">
      <img src="../../dist/img/nha_logo_circle.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"><b>NHA | GEHP</b></span>
    </a>

    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <?php
          $target_dir = "uploads/users/";
          $target_file = $target_dir . basename("$login_image");
          if(file_exists($target_file)){
            echo "<img src='uploads/users/$login_image' class='img-circle elevation-3' alt='User Image' style='height: 33px;'>";
          } 
          else {
            echo "<img src='uploads/users/user.png' width='150px'>";
          }
          ?>
        </div>
        <div class="info">
          <a href="users_profile.php" class="d-block"><?php echo $_SESSION['login_user'] ?></a>
        </div>
      </div>

      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
          <li class="nav-item">
            <a href="../../index.php" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-briefcase"></i>
              <p>
                Applicants
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <input type="text" class="form-control" id="sessionempty" name="sessionempty" value="" placeholder="Search" hidden>
              <li class="nav-item">
                <a href="../forms/registration.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Government Employees</p>
                </a>
              </li>
              <li class="nav-item" <?php echo $deploy;?>>
                <a href="../forms/registration_Uni.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Uniformed Personnel</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/registration_OFW.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>OFW</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item" <?php echo $project_hide ?>>
            <a href="../forms/project_view.php" onclick="sessionempty()" class="nav-link">
              <i class="nav-icon fas fa-building"></i>
              <p>
                Projects
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $user_hide ?>>
            <a href="../forms/users.php" onclick="sessionempty()" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Users
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $element_hide ?>>
            <a href="../forms/report_gen.php" class="nav-link">
              <i class="nav-icon fas fa-scroll"></i>
              <p>
                Reports
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $pages_hide ?>>
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-clone"></i>
              <p>
                Pages
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              
              <li class="nav-item">
                <a href="../forms/home.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Home</p>
                </a>
              </li>
              <li class="nav-item" hidden>
                <a href="../forms/howToApply.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>How To Apply</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/about.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>About</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/banner.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Banner</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="../forms/bulletin.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Bulletin</p>
                </a>
              </li>
            </ul>
          </li>
          
          <li class="nav-item" <?php echo $inv_hide;?>>
            <a href="../forms/inventory.php" class="nav-link active" style="background: #67a2b2;">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                Inventory
              </p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>
  <script>
    function sessionempty() {
      $("#loaderIcon").show();
          jQuery.ajax({
          url: "session_clear.php",
          data: {'sessionempty': $("#sessionempty").val()},
          type: "POST",
          success:function(data){
              $("#").html(data);
              $("#loaderIcon").hide();
          },
          error:function (){
          }
      });
    }
  </script>
  
  <?php
    $PJ_ID = $_GET['ID'];
    $query = "SELECT * FROM project_info 
              WHERE availability = 'Available' 
              AND project_id = '$PJ_ID' ORDER BY PJ_NAME";
    $sth = $dbh->prepare($query);
    $sth->execute();
    $sth->setFetchMode(PDO::FETCH_ASSOC); 
    while ($pjrow = $sth->fetch(PDO::FETCH_ASSOC)) {
      $project_id = $pjrow['project_id'];
      $name = $pjrow['PJ_NAME'];
    }
  ?>

  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?php echo $name; ?></h1>
          </div>
          <?php
            $queryflrChk = "SELECT * FROM inventory WHERE project_id = '$PJ_ID' ORDER BY floor_num";
            $sthflrChk = $dbh->prepare($queryflrChk);
            $sthflrChk->execute();
            $sthflrChk->setFetchMode(PDO::FETCH_ASSOC); 
            while ($flrChk = $sthflrChk->fetch(PDO::FETCH_ASSOC)) {
              $flrChkLast = $flrChk['floor_num'];
            }
          ?>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right"
              <?php if($flrChkLast != $SLCT_FLOOR){echo "hidden";}else{} ?>>
              <button class="btn btn-default color-red-dark delete_item"  style="width:auto; font-size:large;"
              data-toggle='modal' data-keyboard='false' href='#modalDelItem' 
              id="mapBtnDel">
                <i class=" fas fa-trash"></i>&nbsp; Delete Map Inventory
              </button>
            </ol>

            <ol class="breadcrumb float-sm-right">
              <button class="btn btn-default color-green-dark edit_item"  style="width:auto; font-size:large;"
              data-toggle='modal' data-keyboard='false' href='#modalEditItem' 
              id="mapBtnEdt">
                <i class=" fas fa-plus"></i>&nbsp; Edit Map Inventory
              </button>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <?php include("inv_itemEdit_modal.php"); ?>
    <?php include("inv_del_modal.php"); ?>
    <?php include('custom_btn.php'); ?>

    <section class="content">
      <div class="container-fluid">
        <div class="col-12 project-actions text-center img-magnifier-container" id="zoom-img" style="justify-content:center; display:flex;">
          <div id="span-image-container">
            <?php
            $target_dir = "uploads/inventory/";
            $target_file = $target_dir . basename("$slot_img");
            if(file_exists($target_file)){
              // echo "<button class='col-12' id='zoomC' style='background:url(uploads/inventory/$slot_img)'></button>";
              echo "<img id='myimage' src='uploads/inventory/$slot_img' height='900vh' width='auto'>";
            } 
            else {
              echo "<div class='col-12'>No Map Available.</div>";
            }
            ?>
          </div>
        </div>
      </div>
    </section>
    <style>
      #zoomC {
        width: 75vw;
        height: 80vh;
        max-height: 100vh;
        overflow: auto;
        border: none;
        cursor: crosshair;
        background-position: center;
        background-size:contain;
        outline: 0;
      }

      .img-magnifier-container {
        position:relative;
      }

      .img-magnifier-glass {
        position: absolute;
        border: 3px solid #000;
        border-radius: 50%;
        cursor: none;
        /*Set the size of the magnifier glass:*/
        width: 300px;
        height: 300px;
      }
    </style>
    <script>
      // var addZoom = target => {
      //   // (A) GET CONTAINER + IMAGE SOURCE
      //   let container = document.getElementById(target),
      //       imgsrc = container.currentStyle || window.getComputedStyle(container, false);
      //       imgsrc = imgsrc.backgroundImage.slice(4, -1).replace(/"/g, "");
      
      //   // (B) LOAD IMAGE + ATTACH ZOOM
      //   let img = new Image();
      //   img.src = imgsrc;
      //   img.onload = () => {
      //     // (B1) CALCULATE ZOOM RATIO
      //     let ratio = img.naturalHeight / img.naturalWidth,
      //         percentage = ratio * 100 + "%";
      
      //     let finalWidth = img.naturalWidth + 600;
          
      //     Object.assign(container.style, {
      //       backgroundPosition: "center",
      //       backgroundSize: "contain"
      //     });
      //     // (B2) ATTACH ZOOM ON MOUSE MOVE
      //     container.onmousemove = e => {
      //       let rect = e.target.getBoundingClientRect(),
      //           xPos = e.clientX - rect.left,
      //           yPos = e.clientY - rect.top,
      //           xPercent = xPos / (container.clientWidth / 100) + "%",
      //           yPercent = yPos / ((container.clientWidth * ratio) / 100) + "%";
      
      //       Object.assign(container.style, {
      //         backgroundPosition: xPercent + " " + yPercent,
      //         backgroundSize: finalWidth + "px",
      //       });
      //     };
      
      //     // (B3) RESET ZOOM ON MOUSE LEAVE
      //     container.onmouseleave = e => {
      //       Object.assign(container.style, {
      //         backgroundPosition: "center",
      //         backgroundSize: "contain"
      //       });
      //     };
      //   }
      // };
      // // (C) ATTACH FOLLOW ZOOM
      // addZoom("zoomC");

      let container = document.getElementById("span-image-container");
      var glass = document.createElement("DIV");
      glass.setAttribute("class", "img-magnifier-glass");

      container.onmousemove = e => {
        magnify("myimage", 2);
      };
      
      container.onmouseleave = e => {
        $('.img-magnifier-glass').hide();
      };

      function magnify(imgID, zoom) {
        var img, w, h, bw;
        img = document.getElementById(imgID);
        /*create magnifier glass:*/
        $('.img-magnifier-glass').show();
        /*insert magnifier glass:*/
        img.parentElement.insertBefore(glass, img);
        /*set background properties for the magnifier glass:*/
        glass.style.backgroundImage = "url('" + img.src + "')";
        glass.style.backgroundRepeat = "repeat";
        glass.style.backgroundSize = (img.width * zoom) + "px " + (img.height * zoom) + "px";
        bw = 2;
        w = glass.offsetWidth / 18;
        h = glass.offsetHeight / 2;
        /*execute a function when someone moves the magnifier glass over the image:*/
        glass.addEventListener("mousemove", moveMagnifier);
        img.addEventListener("mousemove", moveMagnifier);
        /*and also for touch screens:*/
        glass.addEventListener("touchmove", moveMagnifier);
        img.addEventListener("touchmove", moveMagnifier);
        function moveMagnifier(e) {
          var pos, x, y;
          /*prevent any other actions that may occur when moving over the image*/
          e.preventDefault();
          /*get the cursor's x and y positions:*/
          pos = getCursorPos(e);
          x = pos.x;
          y = pos.y;
          /*prevent the magnifier glass from being positioned outside the image:*/
          if (x > img.width - (w / zoom)) {x = img.width - (w / zoom);}
          if (x < w / zoom) {x = w / zoom;}
          if (y > img.height - (h / zoom)) {y = img.height - (h / zoom);}
          if (y < h / zoom) {y = h / zoom;}
          /*set the position of the magnifier glass:*/
          glass.style.left = (x - w) + "px";
          glass.style.top = (y - h) + "px";
          /*display what the magnifier glass "sees":*/
          glass.style.backgroundPosition = "-" + ((x * zoom) - w + bw) + "px -" + ((y * zoom) - h + bw) + "px";
        }
        function getCursorPos(e) {
          var a, x = 0, y = 0;
          e = e || window.event;
          /*get the x and y positions of the image:*/
          a = img.getBoundingClientRect();
          /*calculate the cursor's x and y coordinates, relative to the image:*/
          x = e.pageX - a.left;
          y = e.pageY - a.top;
          /*consider any page scrolling:*/
          x = x - window.pageXOffset;
          y = y - window.pageYOffset;
          return {x : x, y : y};
        }
      }
    </script>
    <br>

    <section class="content">
      <div class="container-fluid">
        <div class="col-sm-12">
          <div class="col-sm-10" style="margin:auto;">
            <div class="col-sm-2">
              <label>Select Floor:</label>
              <select class="form-control" id="floorSelector" onchange="selectFloor()">
              <?php
                $queryflr = "SELECT * FROM inventory WHERE project_id = '$PJ_ID' ORDER BY floor_num";
                $sthflr = $dbh->prepare($queryflr);
                $sthflr->execute();
                $flrCountToDsply = $sthflr->rowCount();
                $sthflr->setFetchMode(PDO::FETCH_ASSOC); 
                while ($flrrow = $sthflr->fetch(PDO::FETCH_ASSOC)) {
                  if($flrrow['floor_num'] != $SLCT_FLOOR){
                    echo '<option value="'.$flrrow['floor_num'].'">Floor '.$flrrow['floor_num'].'</option>';
                  }
                  else{
                    echo '<option selected value="'.$flrrow['floor_num'].'">Floor '.$flrrow['floor_num'].'</option>';
                  }
                }
              ?>
              </select>
            </div>
            <br>
            <div class="col-sm-12">
              <div class="col-sm-6">
                <b>Legend:</b>
                <div class="row mt-1">
                  <div class="col-sm-3">
                    <i class="fa fa-circle" style="color:#67a2b2;"></i>&nbsp;&nbsp;
                    <span>
                      <span id="AvlLgnd"></span> Available Unit(s)
                    </span>
                  </div>
                  
                  <div class="col-sm-3">
                    <i class="fa fa-circle" style="color:#014047;"></i>&nbsp;&nbsp;
                    <span>
                      <span id="RsrLgnd"></span> Reserved Unit(s)
                    </span>
                  </div>
                  
                  <div class="col-sm-3">
                    <i class="fa fa-circle" style="color:#28a745;"></i>&nbsp;&nbsp;
                    <span>
                      <span id="AwrLgnd"></span> Awarded Unit(s)
                    </span>
                  </div>
                  
                  <div class="col-sm-3">
                    <i class="fa fa-circle" style="color:#dc3545;"></i>&nbsp;&nbsp;
                    <span>
                      <span id="UndLgnd"></span> Under Construction
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container-fluid mb-4">
        <div class="col-sm-11" id="table" style="justify-content:center; margin:auto;">
          <div style="overflow:auto; height: auto; padding:15px 10px;">
            <table style="border-radius:5px; margin:auto;">
              <thead>
                <tr id="theader">
                </tr>
              </thead>
              <tbody id="tobody">
              </tbody>
            </table>
          </div>
        </div>
        <?php include("inv_slot_modal.php"); ?>
      </div>
    </section>
  </div>
  <footer class="main-footer">
    <?php include('footer.php'); ?>
  </footer>

  <aside class="control-sidebar control-sidebar-dark">
  </aside>
</div>
</body>
</html>

<script>
  var string = '<?php echo $slot; ?>';
  $(document).ready(function () {
    var arrayStr = JSON.parse(string);
    var lotArray = [];
    var blkArray = [];
    var AvlLgnd = 0;
    var RsrLgnd = 0;
    var AwrLgnd = 0;
    var UndLgnd = 0;
    for(var i=0; i<arrayStr.length;i++){
      lotArray.push(arrayStr[i].items.length)
      blkArray.push(i+1)
    }
    var highLotNum = Math.max(...lotArray)
    var highBlkNum = Math.max(...blkArray)

    for(var tb=1;tb<=highBlkNum;tb++){
      for(var th=0;th<=highLotNum;th++) {
        if(tb === 1){
          if(th === 0){
            var thTxt = 'BLK/LOT';
            if(<?php echo $flrCountToDsply; ?> > 1) {
              thTxt = 'BLD/UNIT';
            }
            else {
              thTxt = 'BLK/LOT';
            }
            var header = $('#theader').append(
              '<th class="text-center thead" style="cursor:default; pointer-events:none;">'+
                '<div style="border-radius:5px; background-color:lightgray; width:102px;" class="btn btn-default btn-md">'+
                  thTxt+
                '</div>'+
              '</th>'
            );
          }
          if(th != 0){
            var header = $('#theader').append(
              '<th class="text-center thead" style="padding:4px; cursor:default; pointer-events:none;">'+
                '<div style="border-radius:5px; background-color:lightgray; width:75px;" class="btn btn-default btn-md">'+
                  th
                +'</div>'+
              '</th>'
            );
          }
        }
        else{}
      };
      var body = $('#tobody').append(
        '<tr class="tbodyrow" id="tbodyrow'+tb+'">'+
          '<td class="text-center">'+
            '<div style="border-radius:5px; background-color:lightgray; width:102px; height:41px; cursor:default;" class="btn btn-default btn-md">'+
              tb
            +'</div>'+
          '</td>'+
        '</tr>'
      );
      for(var th=1;th<=lotArray[tb-1];th++) {
        var classNameStr = "btn btn-info btn-md";
        var hdrClas = "btn-info";
        if(arrayStr[tb-1].items[th-1].prop === "Available"){
          AvlLgnd = AvlLgnd + 1;
          classNameStr = "btn btn-info btn-md";
          hdrClas = "btn-info";
        }
        if(arrayStr[tb-1].items[th-1].prop === "Awarded"){
          AwrLgnd = AwrLgnd + 1;
          classNameStr = "btn btn-success btn-md";
          hdrClas = "btn-success";
        }
        if(arrayStr[tb-1].items[th-1].prop === "Reserved"){
          RsrLgnd = RsrLgnd + 1;
          classNameStr = "btn btn-info-dark btn-md";
          hdrClas = "btn-info-dark";
        }
        if(arrayStr[tb-1].items[th-1].prop === "Under Construction"){
          UndLgnd = UndLgnd + 1;
          classNameStr = "btn btn-danger";
          hdrClas = "btn-danger";
        }
        
        document.getElementById("AvlLgnd").innerHTML = AvlLgnd;
        document.getElementById("AwrLgnd").innerHTML = AwrLgnd;
        document.getElementById("RsrLgnd").innerHTML = RsrLgnd;
        document.getElementById("UndLgnd").innerHTML = UndLgnd;

        var tdTxt1 = 'B';
        var tdTxt2 = 'L';
        if(<?php echo $flrCountToDsply; ?> > 1) {
          tdTxt1 = 'B';
          tdTxt2 = 'U';
        }
        else {
          tdTxt1 = 'B';
          tdTxt2 = 'L';
        }
        
        var data = $('#tbodyrow'+tb+'').append(
          '<td class="text-center">'+
            '<button style="font-size:large; margin:3px; width:75px;"'+
            'data-toggle="modal" data-keyboard="false" href="#modalviewSlot"'+
            'data-id="'+tdTxt1+tb+tdTxt2+th+'"'+
            'data-blk="'+tb+'"'+
            'data-lot="'+th+'"'+
            'data-prop="'+arrayStr[tb-1].items[th-1].prop+'"'+
            'data-hdrClass="'+hdrClas+'"'+
            'data-pjID="<?php echo $project_id ?>"'+
            'data-pjName="<?php echo $name ?>"'+
            'data-idselect="<?php echo $id ?>"'+
            'id="slotBtn'+tb+th+'"'+
            'type="button" class="'+classNameStr+'" onclick="slotBtn('+tb+th+')">'+
              '<span>'+ tdTxt1 + tb + tdTxt2 + th +'</span>'+
            '</button>'+
          '</td>'
        );
      };
    }
  }); 
  
  function slotBtn(value) {
    var array = [];
    var floorVal = document.getElementById('floorSelector').value
    var PJID=$("#slotBtn"+value).attr('data-pjID');
    var PJNAME=$("#slotBtn"+value).attr('data-pjName');
    var ID=$("#slotBtn"+value).attr('data-id');
    var BLK=$("#slotBtn"+value).attr('data-blk');
    var LOT=$("#slotBtn"+value).attr('data-lot');
    var PROP=$("#slotBtn"+value).attr('data-prop');
    var HDRCLASS=$("#slotBtn"+value).attr('data-hdrClass');
    var IDSELECT=$("#slotBtn"+value).attr('data-idselect');
    $.ajax({url:"inv_slot_modal_result.php?ID="+ID+"&BLK="+BLK+
      "&LOT="+LOT+"&PROP="+PROP+"&HDRCLASS="+HDRCLASS+"&FLOOR="+floorVal+
      "&PJID="+PJID+"&PJNAME="+PJNAME+"&IDSELECT="+IDSELECT,
      cache:false,success:function(result){
        $(".slot_modal").html(result);
        document.getElementById('listTxtEditSlot').value = string;
    }});
  };

  function selectFloor() {
  var floorVal = document.getElementById('floorSelector').value
  window.location.href = "?ID=<?php echo $SLCT_PJID?>&FLOOR="+floorVal+"";
  }
</script>

<style>
  .content {
    -webkit-animation-duration: 0.7s;
    animation-duration: 0.7s;
  }
  
  @-webkit-keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  
  @keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  
  .content {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
  }

  td {
    background: #f5f5f5;
  }
  th {
    cursor: pointer;
  }
  
  .btn-info-dark {
  color: #fff;
  background-color: #014047;
  border-color: #014047;
  box-shadow: none;
  }

  .btn-info-dark:hover {
    color: #fff;
    background-color: #003035;
    border-color: #003035;
  }

  .btn-info-dark:focus, .btn-info-dark.focus {
    color: #fff;
    background-color: #014047;
    border-color: #002c31;
    box-shadow: 0 0 0 0 rgba(58, 176, 195, 0.5);
  }

  .btn-info-dark.disabled, .btn-info-dark:disabled {
    color: #fff;
    background-color: #17a2b8;
    border-color: #17a2b8;
  }

  .btn-info-dark:not(:disabled):not(.disabled):active, .btn-info-dark:not(:disabled):not(.disabled).active,
  .show > .btn-info-dark.dropdown-toggle {
    color: #fff;
    background-color: #002c31;
    border-color: #002c31;
  }

  .btn-info-dark:not(:disabled):not(.disabled):active:focus, .btn-info-dark:not(:disabled):not(.disabled).active:focus,
  .show > .btn-info-dark.dropdown-toggle:focus {
    box-shadow: 0 0 0 0 rgba(58, 176, 195, 0.5);
  }
</style>

<script src="../../plugins/jquery/jquery.min.js"></script>
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script src="../../dist/js/adminlte.min.js"></script>
<script src="../../dist/js/demo.js"></script>
<script src="../../plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>

<script>
  $(function () {
    $("#projectTable").DataTable({
      "paging": false,
      "lengthChange": true,
      "searching": false,
      "ordering": false,
      "info": false,
      "autoWidth": false,
      "responsive": false,
      //"buttons": ["excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#projectTable_wrapper .col-md-6:eq(0)');
  });
</script>

