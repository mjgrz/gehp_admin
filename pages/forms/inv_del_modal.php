<div class="modal fade" id="modalDelItem" role="dialog">
  <div class="modal-dialog modal-md" style="border-radius: 5px;">
    <div class="modal-content">
      <div class="item_modal">
        <?php include('inv_del_modal_result.php'); ?>
      </div>
    </div>
  </div>
</div>
<style>
  
  @media (min-width: 992px) {
    .modal-xxl {
      max-width: 80%;
    }
  }
  @media (min-width: 1200px) {
    .modal-xxl {
      max-width: 80%;
    }
  }

</style>

