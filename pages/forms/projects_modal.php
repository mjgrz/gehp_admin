<div class="modal fade" id="modal-xl<?php echo $projectrow['project_id'] ?>">
  <div class="modal-dialog modal-xl" style="border-radius: 5px;">
    <div class="modal-content" id="modalBody">
      <div class="modal-header" style="background: #67a2b2; color: white;">
        <h4 class="modal-title"><?php echo $projectrow['PJ_NAME'] ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: white;">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="overflow:auto; height: 870px;">
        <div class="col-sm-12">
          <div class="row">
            <div class="col-sm-4" style="margin-bottom: 20px;">
              <?php 
              $main_image = $projectrow['main_image'];
              $target_dir = "uploads/projects/main_image/";
              $target_file = $target_dir . basename("$main_image");
              if(file_exists($target_file)){
                echo "<img src='uploads/projects/main_image/$main_image' style='box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 
                0 6px 20px 0 rgba(0, 0, 0, 0.19); border-radius: 5px;' width='100%'>";
              } 
              else
              {
                echo "No image available.";
              }
              ?>
            </div>
            <div class="col-sm-8">
              <label for="PO">Project Overview</label>
              <div name="PO" class="col-sm-12">
                <p><?php echo $projectrow['overview'] ?></p>
              </div>
              <hr style="border-color: #CFCFCF;">
              <label for="PO">Nearby Establishments</label>
              <div name="PO" class="col-sm-12">
                <ul>
                  <?php
                  if ($projectrow['estab1'] != "") {
                    echo "<li>$projectrow[estab1]</li>";
                  }
                  if ($projectrow['estab2'] != "") {
                    echo "<li>$projectrow[estab2]</li>";
                  }
                  if ($projectrow['estab3'] != "") {
                    echo "<li>$projectrow[estab3]</li>";
                  }
                  if ($projectrow['estab4'] != "") {
                    echo "<li>$projectrow[estab4]</li>";
                  }
                  if ($projectrow['estab5'] != "") {
                    echo "<li>$projectrow[estab5]</li>";
                  }
                  if ($projectrow['estab6'] != "") {
                    echo "<li>$projectrow[estab6]</li>";
                  }
                  else {
                  }
                  ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <hr style="border-color: #CFCFCF;">
        <div class="col-sm-12">
          <div class="row">
            <div class="col-sm-4">
            <label for="PO">Units and Floor Plans</label>
              <div name="PO" class="col-sm-12">
                <h5 style="font-size: 16px;"><b>Location: </b><?php echo $projectrow['location'] ?></h5>
                <h5 style="font-size: 16px;"><b>Bedroom: </b><?php echo $projectrow['bedroom'] ?></h5>
                <h5 style="font-size: 16px;"><b>Bathroom: </b><?php echo $projectrow['bathroom'] ?></h5>
                <h5 style="font-size: 16px;"><b>Lot Area: </b><?php echo $projectrow['lot_area'] ?></h5>
                <h5 style="font-size: 16px;"><b>Floor Area: </b><?php echo $projectrow['floor_area'] ?></h5>
                <h5 style="font-size: 16px;"><b>Specification: </b><?php echo $projectrow['specification'] ?></h5>
              </div>
            </div>
            <div class="col-sm-8">
              <div class="project-actions text-center">
                <div class="row">
                  <div class="col-sm-6">
                    <?php 
                    $floor_img1 = $projectrow['floor_img1'];
                    $target_dir = "uploads/projects/units/";
                    $target_file = $target_dir . basename("$floor_img1");
                    if(file_exists($target_file)){
                      echo "<img src='uploads/projects/units/$floor_img1' style='box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 
                      0 6px 20px 0 rgba(0, 0, 0, 0.19); border-radius: 5px;' width='50%'>";
                    } 
                    else
                    {
                      echo "No image available.";
                    }
                    ?>
                  </div>
                  <div class="col-sm-6">
                    <?php 
                    $floor_img2 = $projectrow['floor_img2'];
                    $target_dir = "uploads/projects/units/";
                    $target_file = $target_dir . basename("$floor_img2");
                    if(file_exists($target_file)){
                      echo "<img src='uploads/projects/units/$floor_img2' style='box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 
                      0 6px 20px 0 rgba(0, 0, 0, 0.19); border-radius: 5px;' width='50%'>";
                    } 
                    else
                    {
                      echo "No image available.";
                    }
                    ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr style="border-color: #CFCFCF;">
        <div class="col-sm-12">
        <label for="maps">Maps</label>
          <div class="project-actions text-center">
            <div class="row" name="maps">
              <div class="col-sm-6">
                <?php 
                $location_map = $projectrow['location_map'];
                $target_dir = "uploads/projects/maps/";
                $target_file = $target_dir . basename("$location_map");
                if(file_exists($target_file)){
                  echo "<img src='uploads/projects/maps/$location_map' style='box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 
                  0 6px 20px 0 rgba(0, 0, 0, 0.19); border-radius: 5px;' width='70%'>";
                } 
                else
                {
                  echo "No image available.";
                }
                ?>
              </div>
              <div class="col-sm-6">
                <?php 
                $vicinity_map = $projectrow['vicinity_map'];
                $target_dir = "uploads/projects/maps/";
                $target_file = $target_dir . basename("$vicinity_map");
                if(file_exists($target_file)){
                  echo "<img src='uploads/projects/maps/$vicinity_map' style='box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 
                  0 6px 20px 0 rgba(0, 0, 0, 0.19); border-radius: 5px;' width='70%'>";
                } 
                else
                {
                  echo "No image available.";
                }
                ?>
              </div>
            </div>
          </div>
        </div>
        <hr style="border-color: #CFCFCF;">
        <div class="col-sm-12">
        <label for="gallery">Photo Gallery</label>
          <div class="project-actions text-center">
            <div class="row" name="gallery">
              <div class="col-sm-2">
                <?php 
                $unit_img1 = $projectrow['unit_img1'];
                $target_dir = "uploads/projects/gallery/";
                $target_file = $target_dir . basename("$unit_img1");
                if(file_exists($target_file)){
                  echo "<img src='uploads/projects/gallery/$unit_img1' style='box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 
                  0 6px 20px 0 rgba(0, 0, 0, 0.19); border-radius: 5px;' width='100%'>";
                } 
                else
                {
                  echo "No image available.";
                }
                ?>
              </div>
              <div class="col-sm-2">
                <?php 
                $unit_img2 = $projectrow['unit_img2'];
                $target_dir = "uploads/projects/gallery/";
                $target_file = $target_dir . basename("$unit_img2");
                if(file_exists($target_file)){
                  echo "<img src='uploads/projects/gallery/$unit_img2' style='box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 
                  0 6px 20px 0 rgba(0, 0, 0, 0.19); border-radius: 5px;' width='100%'>";
                } 
                else
                {
                  echo "No image available.";
                }
                ?>
              </div>
              <div class="col-sm-2">
                <?php 
                $unit_img3 = $projectrow['unit_img3'];
                $target_dir = "uploads/projects/gallery/";
                $target_file = $target_dir . basename("$unit_img3");
                if(file_exists($target_file)){
                  echo "<img src='uploads/projects/gallery/$unit_img3' style='box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 
                  0 6px 20px 0 rgba(0, 0, 0, 0.19); border-radius: 5px;' width='100%'>";
                } 
                else
                {
                  echo "No image available.";
                }
                ?>
              </div>
              <div class="col-sm-2">
                <?php 
                $unit_img4 = $projectrow['unit_img4'];
                $target_dir = "uploads/projects/gallery/";
                $target_file = $target_dir . basename("$unit_img4");
                if(file_exists($target_file)){
                  echo "<img src='uploads/projects/gallery/$unit_img4' style='box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 
                  0 6px 20px 0 rgba(0, 0, 0, 0.19); border-radius: 5px;' width='100%'>";
                } 
                else
                {
                  echo "No image available.";
                }
                ?>
              </div>
              <div class="col-sm-2">
                <?php 
                $unit_img5 = $projectrow['unit_img5'];
                $target_dir = "uploads/projects/gallery/";
                $target_file = $target_dir . basename("$unit_img5");
                if(file_exists($target_file)){
                  echo "<img src='uploads/projects/gallery/$unit_img5' style='box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 
                  0 6px 20px 0 rgba(0, 0, 0, 0.19); border-radius: 5px;' width='100%'>";
                } 
                else
                {
                  echo "No image available.";
                }
                ?>
              </div>
              <div class="col-sm-2">
                <?php 
                $unit_img6 = $projectrow['unit_img6'];
                $target_dir = "uploads/projects/gallery/";
                $target_file = $target_dir . basename("$unit_img6");
                if(file_exists($target_file)){
                  echo "<img src='uploads/projects/gallery/$unit_img6' style='box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 
                  0 6px 20px 0 rgba(0, 0, 0, 0.19); border-radius: 5px;' width='100%'>";
                } 
                else
                {
                  echo "No image available.";
                }
                ?>
              </div>
            </div>
          </div>
        </div>
        <hr style="border-color: #CFCFCF;">
        <div class="col-sm-12">
            <?php 
              if(substr($projectrow['video'], 0, strpos($projectrow['video'], "=")) == "https://www.youtube.com/watch?v"){
                  $displayvideolink = "https://www.youtube.com/embed/".substr($projectrow['video'], strpos($projectrow['video'], "=") + 1);
                  echo '<label for="video">Walkthrough</label>
                        <div class="project-actions text-center">';
                  echo '<iframe id="video" width="100%" height="600vh" src="'.$displayvideolink.'" title="" frameborder="0" onload="onMyFrameLoad(this)"
                          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                        </iframe>';
                  echo '</div>';
              }
              else{
                  $target_dirvid = "gehpbackend/pages/forms/uploads/projects/videos/";
                  $target_pjvideo = $target_dirvid . basename($projectrow['video']);
                  if(file_exists($target_pjvideo)){
              ?>
                  
                  <label for="video">Walkthrough</label>
                  <div class="project-actions text-center">;
                    <video controls class="video" id="video" style="width: 100%; border-radius: 5px;">
                    <source src="gehpbackend/pages/forms/uploads/projects/videos/<?php echo $projectrow['video'] ?>" type="video/mp4">
                    </video>
                  </div>
              <?php
                  }
                  else{

                  }
              }
            ?>
        </div>
      </div>
      <div class="modal-footer justify-content-right">
        <a type="button" href="EditProjects.php?id=<?php echo $projectrow['project_id'] ?>" style="width: auto; font-size: large;" name="submit" class="btn btn-default color-cyan">
          <i class="fas fa-edit">
          </i>&nbsp;Edit
        </a>
      </div>
    </div>
  </div>
</div>

