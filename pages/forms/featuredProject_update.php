<?php
  session_start();
  date_default_timezone_set("Asia/Hong_Kong");
  $username = $_SESSION['login_user'];
  $time = date("h:i a");
  $date = date('F d, Y', strtotime(date("Y-m-d")));
  $datetime = $date." ".$time;
  include('featuredProject_update/featured_image1.php'); 
  include('featuredProject_update/featured_image2.php'); 
  include('featuredProject_update/featured_image3.php'); 
  include('featuredProject_update/featured_image4.php'); 
  include('dbcon.php'); 
  $featured_title = $_POST["featured_title"];
  $featured_desc = $_POST["featured_desc"];
  $featured_id = 1;
  if ( is_numeric($featured_id) == true){
    try{
      $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $sqlfeatureddata = [
        ':featured_title' => $featured_title,
        ':featured_desc' => $featured_desc,
        ':filename1' => $filename1,
        ':filename2' => $filename2,
        ':filename3' => $filename3,
        ':filename4' => $filename4,
        ':featured_id' => $featured_id,
      ];
      $sqlfeatured = "UPDATE featured_project SET featured_title=:featured_title, featured_desc=:featured_desc, featured_image1=:filename1, 
                      featured_image2=:filename2, featured_image3=:filename3, featured_image4=:filename4 WHERE featured_id=:featured_id";
      $sthsqlfeatured = $dbh->prepare($sqlfeatured);
      if($sthsqlfeatured->execute()){
        $auditdata = [
          ':activity' => "Edited the Featured Project",
          ':username' => $_SESSION['login_user'],
          ':datetime' => $datetime
        ];
        $audit = "INSERT INTO audit_trail (activity, username, date) VALUES (:activity, :username, :datetime)";
        $sthaudit = $dbh->prepare($audit);
        $sthaudit->execute($auditdata);
        $_SESSION["status"] = "Your data have been saved successfully.";
        header('Location: ../forms/home.php');
        $dbh = null;
      }
      else{
        $_SESSION["error"] = "Sorry, your data were not saved.";
        header('Location: ../forms/projects.php');
        $dbh = null;
      }
    }
    catch(PDOException $e){
      error_log('PDOException - ' . $e->getMessage(), 0);
      http_response_code(500);
      die('Error establishing connection with database');
    }
  } 
  else{
  http_response_code(400);
  die('Error processing bad or malformed request');
  }
?>