<!DOCTYPE html>
<html lang="en">
<head>
<?php

use Google\Service\CloudSearch\PushItem;

include('login_session.php');
include('pgcon.php');
error_reporting(E_ERROR | E_PARSE);

?>

<link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
<link rel="stylesheet" href="../../plugins/toastr/toastr.min.css">
<link rel="stylesheet" href="../../dist/css/adminlte.min.css">

<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet@1.3.3/dist/leaflet.css">

<script src="../../plugins/jquery/jquery.min.js"></script>
<script src="../../plugins/toastr/toastr.min.js"></script>

<?php
if (isset($_SESSION["status"]) && $_SESSION["status"] !='') {
  $msg = $_SESSION["status"];
  echo "<span><script type='text/javascript'>toastr.success('$msg')</script></span>";
  
  unset($_SESSION["status"]);
}
if (isset($_SESSION["error"]) && $_SESSION["error"] !='') {
  $msg = $_SESSION["error"];
  echo "<span><script type='text/javascript'>toastr.error('$msg')</script></span>";
  
  unset($_SESSION["error"]);
}
if (isset($_SESSION["info"]) && $_SESSION["info"] !='') {
  $msg = $_SESSION["info"];
  echo "<span><script type='text/javascript'>toastr.info('$msg')</script></span>";
  
  unset($_SESSION["info"]);
}
else {

}
?>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>NHA | Government Employee's Housing Program</title>
  <link rel="shortcut icon" type="image/png" href="../../dist/img/nha_logo_circle.png">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
</head>
<body class="hold-transition sidebar-mini">
  
<!-- <script language="javascript">
           var message="This function is not allowed here.";
           function clickIE4(){
                 if (event.button==2){
                     alert(message);
                     return false;
                 }
           }
           function clickNS4(e){
                if (document.layers||document.getElementById&&!document.all){
                        if (e.which==2||e.which==3){
                                  alert(message);
                                  return false;
                        }
                }
           }
           if (document.layers){
                 document.captureEvents(Event.MOUSEDOWN);
                 document.onmousedown=clickNS4;
           }
           else if (document.all&&!document.getElementById){
                 document.onmousedown=clickIE4;
           }
           document.oncontextmenu=new Function("alert(message);return false;")
</script> -->

<div class="wrapper">
  <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="font-weight: bold;">
    <?php include("header.php"); ?>
  </nav>

  <aside class="main-sidebar sidebar-dark-primary elevation-4" style="background: #002d32; position: fixed;">
    <a href="../../index.php" class="brand-link">
      <img src="../../dist/img/nha_logo_circle.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"><b>NHA | GEHP</b></span>
    </a>

    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <?php
          $target_dir = "uploads/users/";
          $target_file = $target_dir . basename("$login_image");
          if(file_exists($target_file)){
            echo "<img src='uploads/users/$login_image' class='img-circle elevation-3' alt='User Image' style='height: 33px;'>";
          } 
          else {
            echo "<img src='uploads/users/user.png' width='150px'>";
          }
          ?>
        </div>
        <div class="info">
          <a href="users_profile.php" class="d-block"><?php echo $_SESSION['login_user'] ?></a>
        </div>
      </div>

      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
          <li class="nav-item">
            <a href="../../index.php" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-briefcase"></i>
              <p>
                Applicants
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <input type="text" class="form-control" id="sessionempty" name="sessionempty" value="" placeholder="Search" hidden>
              <li class="nav-item">
                <a href="../forms/registration.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Government Employees</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/registration_Uni.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Uniformed Personnel</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/registration_OFW.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>OFW</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item" <?php echo $project_hide ?>>
            <a href="../forms/project_view.php" onclick="sessionempty()" class="nav-link">
              <i class="nav-icon fas fa-building"></i>
              <p>
                Projects
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $user_hide ?>>
            <a href="../forms/users.php" onclick="sessionempty()" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Users
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $element_hide ?>>
            <a href="../forms/report_gen.php" class="nav-link">
              <i class="nav-icon fas fa-scroll"></i>
              <p>
                Reports
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $pages_hide ?>>
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-clone"></i>
              <p>
                Pages
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              
              <li class="nav-item">
                <a href="../forms/home.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Home</p>
                </a>
              </li>
              <li class="nav-item" hidden>
                <a href="../forms/howToApply.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>How To Apply</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/about.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>About</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/banner.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Banner</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="../forms/bulletin.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Bulletin</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
    </div>
  </aside>
  <script>
    function sessionempty() {
      $("#loaderIcon").show();
          jQuery.ajax({
          url: "session_clear.php",
          data: {'sessionempty': $("#sessionempty").val()},
          type: "POST",
          success:function(data){
              $("#").html(data);
              $("#loaderIcon").hide();
          },
          error:function (){
          }
      });
    }
  </script>

  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>ACACIA GREEN FIELD HOMES<span style="color: gray;"><?php //echo $CAT ?></span></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="col-sm-12 project-actions text-center">
          <script src='https://unpkg.com/leaflet@1.3.3/dist/leaflet.js'></script>
          <div id="map"></div>
        </div>
      </div>
    </section>
    <br>

    <section class="content">
      <div class="container-fluid">
        <div class="col-sm-12">
          <div class="col-sm-10" style="margin:auto;">
            <div class="col-sm-2">
              <label>Select Floor:</label>
              <select class="form-control">
                <option>Floor 1</option>
                <option>Floor 2</option>
              </select>
            </div>
            <br>
            <div class="col-sm-12">
              <b>Legend:</b>&nbsp;&nbsp;&nbsp;
              <i class="fa fa-circle" style="color:#67a2b2;"></i>&nbsp;&nbsp;<span>0 Available Unit(s)</span>&nbsp;&nbsp;&nbsp;
              <i class="fa fa-circle" style="color:#014047;"></i>&nbsp;&nbsp;<span>0 Reserved Unit(s)</span>&nbsp;&nbsp;&nbsp;
              <i class="fa fa-circle" style="color:#28a745;"></i>&nbsp;&nbsp;<span>0 Awarded Unit(s)</span>&nbsp;&nbsp;&nbsp;
              <i class="fa fa-circle" style="color:#dc3545;"></i>&nbsp;&nbsp;<span>Under Construction</span>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="col-sm-12">
          <div style="overflow:auto; height: auto; padding:15px 10px;">
            <table style="border-radius:5px; margin:auto;">
              <thead>
                <tr>
                  <th style="width:100px; cursor:default;" class="text-center">BLOCK/LOT</th>
                  <?php
                    for($i=1;$i<=14;$i++){
                  ?>
                    <th class="text-center" style="padding:4px; cursor:default;">
                      <div style="border-radius:5px; background-color:lightgray; padding:5px;">
                        <?php echo $i ?>
                      </div>
                    </th>
                  <?php
                    }
                  ?>
                </tr>
              </thead>
              <tbody>
                <?php for($b=1;$b<=5;$b++){ ?>
                  <tr>
                    <td class="text-center">
                      <div style="border-radius:5px; background-color:lightgray; padding:8px; cursor:default;">
                        <b><?php echo $b ?></b>
                      </div>
                    </td>
                    <?php for($l=1;$l<=14;$l++){ ?>
                      <?php
                        $class = "btn btn-info btn-md"; 
                        if($b == 2){
                          $class = "btn btn-info btn-md";  
                        }
                        if($b == 1 && $l < 7){
                          $class = "btn btn-success btn-md";  
                        }
                        if($b >= 3 && $l > 1 && $l < 7){
                          $class = "btn btn-info-dark btn-md";  
                        }
                        if($b == 2 && $l > 4){
                          $class = "btn btn-danger btn-md";  
                        }
                      ?>
                      <td class="text-center">
                        <button style="font-size:large; margin:3px;" class="slotBtn <?php echo $class; ?>" type="button"
                        data-toggle='modal' data-keyboard='false' href='#modalview' data-a="B<?php echo $b ?>L<?php echo $l ?>">
                          <span>B<?php echo $b ?>L<?php echo $l ?></span>
                        </button>
                      </td>
                    <?php } ?>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
        <?php include("slot_modal.php"); ?>
      </div>
    </section>
  </div>
  <footer class="main-footer">
    <?php include('footer.php'); ?>
  </footer>

  <aside class="control-sidebar control-sidebar-dark">
  </aside>
</div>
</body>
</html>

<script>
  $('.slotBtn').click(function(){
      var ID=$(this).attr('data-a');
      $.ajax({url:"inv_slot_modal_result.php?ID="+ID,cache:false,success:function(result){
          $(".slot_modal").html(result);
      }});
  });
</script>

<style>
  .content {
    -webkit-animation-duration: 0.7s;
    animation-duration: 0.7s;
  }
  
  @-webkit-keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  
  @keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  
  .content {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
  }

  td {
    background: #f5f5f5;
  }
  th {
    cursor: pointer;
  }
  
  .btn-info-dark {
  color: #fff;
  background-color: #014047;
  border-color: #014047;
  box-shadow: none;
  }

  .btn-info-dark:hover {
    color: #fff;
    background-color: #003035;
    border-color: #003035;
  }

  .btn-info-dark:focus, .btn-info-dark.focus {
    color: #fff;
    background-color: #014047;
    border-color: #002c31;
    box-shadow: 0 0 0 0 rgba(58, 176, 195, 0.5);
  }

  .btn-info-dark.disabled, .btn-info-dark:disabled {
    color: #fff;
    background-color: #17a2b8;
    border-color: #17a2b8;
  }

  .btn-info-dark:not(:disabled):not(.disabled):active, .btn-info-dark:not(:disabled):not(.disabled).active,
  .show > .btn-info-dark.dropdown-toggle {
    color: #fff;
    background-color: #002c31;
    border-color: #002c31;
  }

  .btn-info-dark:not(:disabled):not(.disabled):active:focus, .btn-info-dark:not(:disabled):not(.disabled).active:focus,
  .show > .btn-info-dark.dropdown-toggle:focus {
    box-shadow: 0 0 0 0 rgba(58, 176, 195, 0.5);
  }
  
  #map {
    height: 500px
  }
</style>

<script src="../../plugins/jquery/jquery.min.js"></script>
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script src="../../dist/js/adminlte.min.js"></script>
<script src="../../dist/js/demo.js"></script>

<script>
  $(function () {
    $("#projectTable").DataTable({
      "paging": false,
      "lengthChange": true,
      "searching": false,
      "ordering": false,
      "info": false,
      "autoWidth": false,
      "responsive": false,
      //"buttons": ["excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#projectTable_wrapper .col-md-6:eq(0)');
  });
</script>

<script>
  var map = L.map('map').setView([6.98596611780583, 122.165898218613], 17);

  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  }).addTo(map);

  var latlngs =  [[6.985888855,122.166279453],[6.985924595,122.166343927],[6.985844004,122.166389479],[6.985806861,122.166326407],[6.985888855,122.166279453]]
  var polygon = L.polygon(latlngs, {color: 'blue'});
  polygon.addTo(map);

  // L.marker([6.98596611780583, 122.165898218613]).addTo(map)
  //   .bindPopup('A pretty CSS3 popup.<br> Easily customizable.')
  //   .openPopup();
</script>

