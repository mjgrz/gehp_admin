<div class="modal fade" id="modal<?php echo $modalID ?>">
  <div class="modal-dialog modal-default">
    <div class="modal-content">
      <div class="modal-header" style="background: #67a2b2; color: white;">
        <span>
        <h4 class="modal-title"><?php echo $modalTitle ?></h4>
        <p><?php echo date('F d, Y', strtotime($modalUpload)) ?></p>
        </span>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: white;">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="overflow:auto; max-height: 72vh; min-height: fit-content;">
        <div class="col-sm-12">
          <div class="project-actions text-center">
            <img src="uploads/bulletin/<?php echo $modalImage ?>" width="80%" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); border-radius: 5px;">
          </div>
          <div>
            <br>
            <p style="text-align:justify;"><?php echo nl2br($modalDesc); ?></p>
          </div>
        </div>
      </div>
      <div class="modal-footer justify-content-right">
        <a style="width:auto; font-size:large;" type="button" href="bulletin_edit.php?id=<?php echo $modalID ?>" class="btn btn-default color-cyan">
          <i class="fas fa-edit">
          </i>&nbsp
          Edit
        </a>
      </div>
    </div>
  </div>
</div>