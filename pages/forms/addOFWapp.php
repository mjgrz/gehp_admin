
<?php 
  include("login_session.php");
  error_reporting(E_ERROR | E_PARSE);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>NHA | Government Employee's Housing Program</title>
  <link rel="shortcut icon" type="image/png" href="../../dist/img/nha_logo_circle.png">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="font-weight: bold;">
    <?php include("header.php"); ?>
  </nav>

  <aside class="main-sidebar sidebar-dark-primary elevation-4" style="background: #002d32; position: fixed;">
    <a href="../../index.php" class="brand-link">
      <img src="../../dist/img/nha_logo_circle.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"><b>NHA | GEHP</b></span>
    </a>

    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <?php
          $target_dir = "uploads/users/";
          $target_file = $target_dir . basename("$login_image");
          if(file_exists($target_file)){
            echo "<img src='uploads/users/$login_image' class='img-circle elevation-3' alt='User Image' style='height: 33px;'>";
          } 
          else {
            echo "<img src='uploads/users/user.png' width='150px'>";
          }
          ?>
        </div>
        <div class="info">
          <a href="users_profile.php" class="d-block"><?php echo $_SESSION['login_user'] ?></a>
        </div>
      </div>

      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
          <li class="nav-item">
            <a href="../../index.php" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>

          <li class="nav-item menu-open">
            <a href="#" class="nav-link active" style="background: #67a2b2;">
              <i class="nav-icon fas fa-briefcase"></i>
              <p>
                Applicants
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <input type="text" class="form-control" id="sessionempty" name="sessionempty" value="" placeholder="Search" hidden>
              <li class="nav-item">
                <a href="../forms/registration.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Government Employees</p>
                </a>
              </li>
              <li class="nav-item" <?php echo $deploy;?>>
                <a href="../forms/registration_Uni.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Uniformed Personnel</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/registration_OFW.php" onclick="sessionempty()" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>OFW</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item" <?php echo $project_hide ?>>
            <a href="../forms/project_view.php" onclick="sessionempty()" class="nav-link">
              <i class="nav-icon fas fa-building"></i>
              <p>
                Projects
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $user_hide ?>>
            <a href="../forms/users.php" onclick="sessionempty()" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Users
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $element_hide ?>>
            <a href="../forms/report_gen.php" class="nav-link">
              <i class="nav-icon fas fa-scroll"></i>
              <p>
                Reports
              </p>
            </a>
          </li>
          
          <li class="nav-item" <?php echo $pages_hide ?>>
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-clone"></i>
              <p>
                Pages
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              
              <li class="nav-item">
                <a href="../forms/home.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Home</p>
                </a>
              </li>
              <li class="nav-item" hidden>
                <a href="../forms/howToApply.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>How To Apply</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/about.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>About</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/banner.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Banner</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="../forms/bulletin.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Bulletin</p>
                </a>
              </li>
            </ul>
          </li>
          
          <li class="nav-item" <?php echo $inv_hide;?>>
            <a href="../forms/inventory.php" class="nav-link">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                Inventory
              </p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>
  <script>
    function sessionempty() {
      $("#loaderIcon").show();
          jQuery.ajax({
          url: "session_clear.php",
          data: {'sessionempty': $("#sessionempty").val()},
          type: "POST",
          success:function(data){
              $("#").html(data);
              $("#loaderIcon").hide();
          },
          error:function (){
          }
      });
    }
  </script>
  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Overseas Filipino Worker - Application</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
              <div class="card-header" style="background: #67a2b2;">
                <h3 class="card-title"></h3>
              </div>
              <!--Submit form for OFW Application-->
              <form method="POST" action="addOFWapp_submit.php" enctype="multipart/form-data">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label style="border-bottom: 2px solid #67a2b2;">DATE OF APPLICATION</label>
                        <div class="col-sm-12">
                          <input type="date" class="form-control col-sm-6" name="app_date" id="app_date" value="<?php echo date('Y-m-d'); ?>" max="<?php echo date('Y-m-d'); ?>">
                        </div>
                      </div>
                      <?php 
                      include("../../../../dbcon.php");
                      try{
                        $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        $query = "SELECT * FROM ofw_applications";
                        $sthquery = $dbh->prepare($query);
                        $sthquery->execute();
                        $count = $sthquery->rowCount() + 535;
                        $dbh = null;
                      }
                      catch(PDOException $e){
                        error_log('PDOException - ' . $e->getMessage(), 0);
                        http_response_code(500);
                        die('Error establishing connection with database');
                      }
                      ?>
                      <input type="text" name="counter" value="<?php echo $count ?>" hidden>
                      <input type="text" name="username" value="<?php echo $_SESSION['login_user'] ?>" hidden>
                      <div class="form-group">
                        <label style="border-bottom: 2px solid #67a2b2;">A.1 FOR PURCHASE OF HOUSE AND LOT PACKAGE <span class="h6">under the GEHP thru</span></label>
                        <div class="col-sm-12">
                          <div class="row">
                            <div style="padding-right: 30px;">
                              <label> 
                                <input type="radio" name="payment" value="End-User Financing (MTO-Pag-IBIG)" required>
                                End-User Financing (MTO-Pag-IBIG) 
                              </label>
                            </div>
                            <div style="padding-right: 50px;">
                              <label>
                                <input type="radio" name="payment" value="Cash Sale" required>
                                Cash Sale 
                              </label>
                            </div>
                            <div style="padding-right: 50px;">
                              <label>
                                <input type="radio" name="payment" value="Staggered Cash" required>
                                Staggered Cash 
                              </label>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-12">
                            <div style="padding: 3px;">
                              <div class="row">
                                <label>In-House Financing (Installment Payment): &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                <div style="padding-right: 30px;">
                                  <label> 
                                    <input type="radio" name="payment" value="Straight Amortization" required>
                                    Straight Amortization 
                                  </label>
                                </div>
                                <div style="padding-right: 50px;">
                                  <label>
                                    <input type="radio" name="payment" value="Escalating Amortization" required>
                                    Escalating Amortization
                                  </label>
                                </div>
                              </div>
                            </div>
                        </div>
                      </div>
                      <div class="form-group col-sm-12">
                        <label for="project">Project Name</label>
                          <select class="form-control" name="project" id="project" style="padding: 8px;" required>
                            <option disabled selected value="">Select Housing Project</option>
                            <?php
                            include("../../../../dbcon.php");
                            try{
                              $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                              $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                              $regionselect = "SELECT * FROM project_info 
                                              LEFT JOIN units_floor ON project_info.project_id = units_floor.project_id
                                              LEFT JOIN region ON project_info.region = region.psgc_reg";
                              $sthregionselect = $dbh->prepare($regionselect);
                              $sthregionselect->execute();
                              $sthregionselect->setFetchMode(PDO::FETCH_ASSOC); 

                              if($sthregionselect->rowCount() > 0){
                                while ($regionrow = $sthregionselect->fetch(PDO::FETCH_ASSOC)) {
                                ?>
                                <option value="<?php echo $regionrow['PJ_CODE'] ?>"><?php echo $regionrow['Region'] ?> | <?php echo $regionrow['PJ_NAME'] ?> (<?php echo $regionrow['location'] ?>)</option>
                                <?php 
                                } 
                                $dbh = null;
                              }
                              else { 
                                $dbh = null;
                              }
                            }
                            catch(PDOException $e){
                              error_log('PDOException - ' . $e->getMessage(), 0);
                              http_response_code(500);
                              die('Error establishing connection with database');
                            }
                            ?>
                          </select>
                      </div>
                      <div class="form-group">
                        <label style="border-bottom: 2px solid #67a2b2;">A.2 TYPE OF HOUSING <span class="h6">under the GEHP thru</span></label>
                        <div class="col-sm-12">
                          <div class="row">
                            <div style="padding-right: 30px;">
                              <label> 
                                <input type="radio" name="type" value="2-Storey Duplex">
                                2-Storey Duplex 
                              </label>
                            </div>
                            <div style="padding-right: 30px;">
                              <label>
                                <input type="radio" name="type" value="1-Storey Duplex">
                                1-Storey Duplex 
                              </label>
                            </div>
                            <div style="padding-right: 30px;">
                              <label>
                                <input type="radio" name="type" value="Low-Rise Building/Condominium">
                                Low-Rise Building/Condominium 
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label style="border-bottom: 2px solid #67a2b2;">A.3 As Overseas Filipino Worker in </label>
                        <div class="form-group col-sm-12">
                          <input type="text" class="form-control" id="employee" name="employee"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Please specify" required>
                        </div>
                      </div>
                      <br>
                      <label style="border-bottom: 2px solid #67a2b2;">I. APPLICANT'S IDENTITY <span class="h6 text-muted">(For female applicant/spouse, give complete maiden name)</span> </label>
                      <div class="col-sm-12">
                        <label for="userMname">Full Name</label>
                        <div class="row">
                          <div class="form-group col-sm-3">
                            <input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" required>
                          </div>
                          <div class="form-group col-sm-3">
                            <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" required>
                          </div>
                          <div class="form-group col-sm-3">
                            <input type="text" class="form-control" id="mname" name="mname" placeholder="Middle Name"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" required>
                          </div>
                          <div class="form-group col-sm-3">
                            <input type="text" class="form-control" id="msurname" name="msurname" placeholder="Mother's Surname"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" required>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="row">
                          <div class="form-group col-sm-4">
                            <label for="address">Residence / Address</label> 
                            <input type="text" class="form-control" id="address" name="address"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="" required>
                          </div>
                          <div class="form-group col-sm-4">
                            <label for="dplace">Place of Birth</label>
                            <input type="text" class="form-control" id="dplace" name="dplace"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="" required>
                          </div>
                          <div class="form-group col-sm-4">
                            <label for="email">Email Address</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="">
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="row">
                          <div class="form-group col-sm-4">
                            <label for="dbirth">Date of Birth</label> 
                            <input type="date" class="form-control" id="dbirth" name="dbirth" min="<?php echo Date('Y-m-d', strtotime('-65 year')); ?>" max="<?php echo date('Y-m-d', strtotime('-18 year')); ?>" placeholder="" required>
                          </div>
                          <div class="form-group col-sm-4">
                            <label for="sex">Sex</label>
                            <select class="form-control" name="sex" id="sex" style="padding: 8px;" required>
                              <option style="color: #808080;" value="" disabled selected>- Select -</option>
                              <option value="male">Male</option>
                              <option value="female">Female</option>
                            </select>
                          </div>
                          <div class="form-group col-sm-4">
                            <label for="status">Civil Status</label>
                            <select class="form-control" name="status" id="status" style="padding: 8px;" required>
                              <option style="color: #808080;" value="" disabled selected>- Select -</option>
                              <option value="single">Single</option>
                              <option value="married">Married</option>
                              <option value="widowed">Widowed</option>
                              <option value="separated-in-fact">Separated-in-Fact</option>
                              <option value="legally-separated">Legally-Separated</option>
                              <option value="single-HOF">Single-HOF</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="row">
                          
                          <div class="form-group col-sm-4">
                            <label for="citizen">Citizenship</label>
                            <input type="text" class="form-control" id="citizen" name="citizen"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="" required>
                          </div>
                          <div class="form-group col-sm-4">
                            <label for="contact">Contact Number</label> 
                            <input type="text" class="form-control" id="contact"  pattern="[0-9]+" oninput="this.value = this.value.replace(/[^0-9-]/g, '').replace(/(\..*)\./g, '$1');" maxlength="11" name="contact" placeholder="" required>
                          </div>
                          <div class="form-group col-sm-4">
                            <label for="office">Office Number</label>
                            <input type="text" class="form-control" id="office"  pattern="[0-9]+" oninput="this.value = this.value.replace(/[^0-9-]/g, '').replace(/(\..*)\./g, '$1');" maxlength="11" name="office" placeholder="">
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="row">
                          <div class="form-group col-sm-6">
                            <label for="tin">TIN Number</label>
                            <input type="text" class="form-control" id="tin" name="tin" placeholder=""
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z-]/g, '').replace(/(\..*)\./g, '$1');" required>
                          </div>
                          <div class="form-group col-sm-6">
                            <label for="pagibig">GSIS/SSS/PAG-IBIG ID</label>
                            <input type="text" class="form-control" id="pagibig" name="pagibig" placeholder=""
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z-]/g, '').replace(/(\..*)\./g, '$1');" required>
                          </div>
                        </div>
                      </div>

                      <div class="col-sm-12">
                        <label for="userMname">Name of Spouse</label>
                        <div class="row">
                          <div class="form-group col-sm-3">
                            <input type="text" class="form-control" id="s_lname" name="s_lname"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Last Name">
                          </div>
                          <div class="form-group col-sm-3">
                            <input type="text" class="form-control" id="s_fname" name="s_fname"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="First Name">
                          </div>
                          <div class="form-group col-sm-3">
                            <input type="text" class="form-control" id="s_mname" name="s_mname"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Middle Name">
                          </div>
                          <div class="form-group col-sm-3">
                            <input type="text" class="form-control" id="s_msurname" name="s_msurname"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Mother's Surname">
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="row">
                          <div class="form-group col-sm-6">
                            <label for="s_dbirth">Date of Birth</label> 
                            <input type="date" class="form-control" id="s_dbirth" name="s_dbirth" max="<?php echo date('Y-m-d', strtotime('-18 year')); ?>" placeholder="">
                          </div>
                          <div class="form-group col-sm-6">
                            <label for="s_dplace">Place of Birth</label>
                            <input type="text" class="form-control" id="s_dplace" name="s_dplace"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="">
                          </div>
                        </div>
                      </div>
                    </div>
                    <br>
                    <div class="col-md-6">
                      <label style="border-bottom: 2px solid #67a2b2;">II. APPLICANT’S EMPLOYMENTS STATUS</label>
                      <div class="col-sm-12">
                        <div class="row">
                          <div class="form-group col-sm-4">
                            <label for="nature">Nature of Employment</label> 
                            <input type="text" class="form-control" id="nature" name="nature"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="" required>
                          </div>
                          <div class="form-group col-sm-4">
                            <label for="agency">Name of Agency/Corporation</label>
                            <input type="text" class="form-control" id="agency" name="agency"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="" required>
                          </div>
                          <div class="form-group col-sm-4">
                            <label for="income">Income</label>
                            <input type="text" class="form-control" id="income" name="income" maxlength="15" placeholder="" required>
                          </div>
                        </div>
                      </div>
                      <div class="form-group col-sm-12">
                        <label for="a_address">Address of Agency/Corporation</label> 
                        <input type="text" class="form-control" id="a_address" name="a_address"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="" required>
                      </div>
                      <br>
                      <span>
                        <label style="border-bottom: 2px solid #67a2b2;">III. APPLICANT'S FAMILY COMPOSITION <span class=" h6 text-muted"> (add additional fields if necssary) </span></label>
                        <div class="col-sm-12">
                          <label>Full Name</label>
                          <button class="close" style="background: none; border: none; float: right;"><i class="fa fa-times"></i></button>
                          <div class="row">
                            <div class="form-group col-sm-4">
                              <input class="sl"type="text" name="slno[]" id="slno" value="1" style="border-radius: 5px; width: 100%; height: 50px; border-color: #f1f1f1;" hidden readonly="">
                              <input type="text" class="form-control" id="fam-lname" name="dlname[]"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Last Name">
                            </div>
                            <div class="form-group col-sm-4">
                              <input type="text" class="form-control" id="fam-fname" name="dfname[]"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="First Name">
                            </div>
                            <div class="form-group col-sm-4">
                              <input type="text" class="form-control" id="fam-fname" name="dmname[]"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Middle Name">
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-12">
                          <div class="row">
                            <div class="form-group col-sm-3">
                              <label for="s_dbirth">Relation to Applicant</label> 
                              <input type="text" class="form-control" id="fam-relation" name="relation[]"
                              oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="">
                            </div>
                            <div class="form-group col-sm-3">
                              <label for="status">Civil Status</label>
                              <select class="form-control" name="dcstatus[]" id="fam-status" style="padding: 8px;">
                                <option style="color: #808080;" value="" disabled selected>- Select -</option>
                                <option value="single">Single</option>
                                <option value="married">Married</option>
                                <option value="widowed">Widowed</option>
                                <option value="separated-in-fact">Separated-in-Fact</option>
                                <option value="legally-separated">Legally-Separated</option>
                                <option value="single-HOF">Single-HOF</option>
                              </select>
                            </div>
                            <div class="form-group col-sm-3">
                              <label for="s_dplace">Age</label>
                              <input type="number" class="form-control" min="0" max="150" id="fam-age" name="age[]" placeholder="">
                            </div>
                            <div class="form-group col-sm-3">
                              <label for="s_dplace">Source of Income</label>
                              <input type="text" class="form-control" id="fam-age" name="source_income[]"
                              oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="">
                            </div>
                          </div>
                        </div>
                        <hr style="border-bottom: 1px solid #67a2b2;">
                      </span>
                      <div id="next"></div>
                      <button type="button" id="addrow" class="btn btn-default btn-sm color-cyan" style="font-size:medium; width:auto; float:right;"><i class="fa fa-plus"></i>&nbsp; Add member </button>
                      <br><br>
                      <div class="form-group col-sm-12">
                        <div class="row">
                          <label style="margin-top:auto; border-bottom: 2px solid #67a2b2;">IV. APPLICANT'S TOTAL FAMILY INCOME PER MONTH:</label>&nbsp;&nbsp;&nbsp;
                            <input type="text" style="width:auto;" pattern="[0-9]+" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');" maxlength="15" class="form-control" id="famIncome" name="famIncome" required>
                          </div>
                      </div>

                      <div class="form-group">
                          <label style="border-bottom: 2px solid #67a2b2;">V. FAMILY REAL PROPERTY HOLDINGS:</label>
                          <div class="form-group col-sm-12">
                            <p>
                              Have never availed of any form of government housing assistance
                              &nbsp;&nbsp;
                              <input type="radio" required id="noViolation" name="noViolation" value="YES" required> <b>YES</b>
                              &nbsp;/&nbsp;
                              <input type="radio" required id="noViolation" name="noViolation" value="NO" required> <b>NO</b>
                            </p>
                          </div>
                          <div class="form-group col-sm-12">
                            <p>
                              Have sold, alienated, conveyed, encumbered or leased the socialized housing, including imporovements
                              or rights thereon, except to qualified beneficiary as determined by the Government Agency
                              &nbsp;&nbsp;
                              <input type="radio" required id="altViolation" name="altViolation" value="YES"> <b>YES</b>
                              &nbsp;/&nbsp;
                              <input type="radio" required id="altViolation" name="altViolation" value="NO"> <b>NO</b>
                            </p>
                          </div>
                      </div>
                      
                      <label style="border-bottom: 2px solid #67a2b2; margin-top: 17px;">VI. ATTACH OEC FILE <span class=" h6 text-muted"> (Overseas Employment Certificate) </span></label>
                      <div class="col-sm-12">
                        <div class="custom-control custom-checkbox">
                          <input class="custom-control-input" type="checkbox" name="checkerOEC" id="customCheckbox1" value="Already submitted OEC File (Hardcopy)">
                          <label for="customCheckbox1" class="custom-control-label">Already submitted OEC File (Hardcopy - PDF)</label>
                        </div>
                        <div class="custom-file col-sm-12" style="margin-top: 5px;">
                          <input type="file" class="custom-file-input" id="file" name="file" required>
                          <label class="custom-file-label" for="file">Choose file</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card-footer">
                  <div class="project-actions text-right">
                    <button type="submit" style="font-size:large; width:auto;" class="btn btn-default color-cyan-dark">
                      <i class="fa fa-paper-plane">
                      </i>&nbsp; Submit
                    </button>
                  </div>
                </div>
              </form>
              <!--End of Submit form for OFW Application-->
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <footer class="main-footer">
    <?php include('footer.php'); include('custom_btn.php');?>
  </footer>
  <aside class="control-sidebar control-sidebar-dark">
  </aside>
</div>
<script src="../../plugins/jquery/jquery.min.js"></script>
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="../../dist/js/adminlte.min.js"></script>
<script src="../../dist/js/demo.js"></script>
<script>
$(function () {
  bsCustomFileInput.init();
});
</script>
</body>
</html>
<style>
  .content {
    -webkit-animation-duration: 0.7s;
    animation-duration: 0.7s;
  }
  @-webkit-keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  @keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  .content {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
  }
  #file {
    cursor: pointer;
  }
  input:hover, select:hover {
    border: 1px solid #67a2b2;
  }
</style>
<script>
$('#addrow').click(function(){
		var length = $('.sl').length;
		var i   = parseInt(length)+parseInt(1);
		var newrow = $('#next').append(
              '<span>'+
                '<div class="col-sm-12">'+
                  '<label for="userMname">Full Name</label>'+
                  '<button class="close" style="background: none; border: none; float: right;"><i class="fa fa-times"></i></button>'+
                  '<div class="row">'+
                    '<div class="form-group col-sm-4">'+
                      '<input class="sl"type="text" name="slno[]" id="slno" value="'+i+'" style="border-radius: 5px; width: 100%; height: 50px; border-color: #f1f1f1;" hidden readonly="">'+
                      '<input type="text" class="form-control" id="fam-lname'+i+'" name="dlname[]" placeholder="Last Name" required>'+
                    '</div>'+
                    '<div class="form-group col-sm-4">'+
                      '<input type="text" class="form-control" id="fam-fname'+i+'" name="dfname[]" placeholder="First Name" required>'+
                    '</div>'+
                    '<div class="form-group col-sm-4">'+
                      '<input type="text" class="form-control" id="fam-fname'+i+'" name="dmname[]" placeholder="Middle Name" required>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
                '<div class="col-sm-12">'+
                  '<div class="row">'+
                    '<div class="form-group col-sm-3">'+
                      '<label for="s_dbirth">Relation to Applicant</label>'+
                      '<input type="text" class="form-control" id="fam-relation'+i+'" name="relation[]" placeholder="" required>'+
                    '</div>'+
                    '<div class="form-group col-sm-3">'+
                      '<label for="s_dplace">Civil Status</label>'+
                      '<select class="form-control" name="dcstatus[]" id="fam-status'+i+'" style="padding: 8px;" required>'+
                        '<option style="color: #808080;" value="" disabled selected>- Select -</option>'+
                        '<option value="single">Single</option>'+
                        '<option value="married">Married</option>'+
                        '<option value="widowed">Widowed</option>'+
                        '<option value="separated-in-fact">Separated-in-Fact</option>'+
                        '<option value="legally-separated">Legally-Separated</option>'+
                        '<option value="single-HOF">Single-HOF</option>'+
                      '</select>'+
                    '</div>'+
                    '<div class="form-group col-sm-3">'+
                      '<label for="s_dplace">Age</label>'+
                      '<input type="number" class="form-control" max="150" id="fam-age'+i+'" name="age[]" placeholder="" required>'+
                    '</div>'+
                    '<div class="form-group col-sm-3">'+
                      '<label for="s_dplace">Source of Income</label>'+
                      '<input type="text" class="form-control" id="fam-age'+i+'" name="source_income[]" placeholder="" required>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
                '<hr style="border-bottom: 1px solid #67a2b2;">'+
              '</span>'
            );
            jQuery('#fam-age'+i+'').attr('maxvalue','3');
      $('#fam-lname'+i+'').focus();  
		});
  $('body').on('click','.close',function() {
       $(this).closest('span').remove()
  });
  $(function () {
      $("#customCheckbox1").click(function () {
          if ($(this).is(":checked")) {
            document.getElementById("file").disabled = true;
            document.getElementById("file").required = false;
            document.getElementById("file").title = "";
          } else {
            document.getElementById("file").disabled = false;
            document.getElementById("file").required = true;
            
          }
      });
  });
  $(function () {
    bsCustomFileInput.init();
  });
</script>                      
<script language="javascript">
  var message="This function is not allowed here.";
  function clickIE4(){
        if (event.button==2){
            alert(message);
            return false;
        }
  }
  function clickNS4(e){
      if (document.layers||document.getElementById&&!document.all){
              if (e.which==2||e.which==3){
                        alert(message);
                        return false;
              }
      }
  }
  if (document.layers){
        document.captureEvents(Event.MOUSEDOWN);
        document.onmousedown=clickNS4;
  }
  else if (document.all&&!document.getElementById){
        document.onmousedown=clickIE4;
  }
  document.oncontextmenu=new Function("alert(message);return false;")
</script>