<?php
include('login_session.php');
if($login_userlevel != "Administrator"){
  header("location:../../index.php");
}
error_reporting(0);
?>

<!DOCTYPE html>
<html lang="en">
<head>

<link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
<link rel="stylesheet" href="../../plugins/toastr/toastr.min.css">
<link rel="stylesheet" href="../../dist/css/adminlte.min.css">

<script src="../../plugins/jquery/jquery.min.js"></script>
<script src="../../plugins/toastr/toastr.min.js"></script>

<?php

if (isset($_SESSION["status"]) && $_SESSION["status"] !='') {
  $msg = $_SESSION["status"];
  echo "<span><script type='text/javascript'>toastr.success('$msg')</script></span>";
  
  unset($_SESSION["status"]);
}
if (isset($_SESSION["error"]) && $_SESSION["error"] !='') {
  $msg = $_SESSION["error"];
  echo "<span><script type='text/javascript'>toastr.error('$msg')</script></span>";
  
  unset($_SESSION["error"]);
}
if (isset($_SESSION["info"]) && $_SESSION["info"] !='') {
  $msg = $_SESSION["info"];
  echo "<span><script type='text/javascript'>toastr.info('$msg')</script></span>";
  
  unset($_SESSION["info"]);
}
else {

}
?>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>NHA | Government Employee's Housing Program</title>
  <link rel="shortcut icon" type="image/png" href="../../dist/img/nha_logo_circle.png">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="font-weight: bold;">
    <?php include("header.php"); ?>
  </nav>

  <aside class="main-sidebar sidebar-dark-primary elevation-4" style="background: #002d32; position: fixed;">
    <a href="../../index.php" class="brand-link">
      <img src="../../dist/img/nha_logo_circle.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"><b>NHA | GEHP</b></span>
    </a>

    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <?php
          $target_dir = "uploads/users/";
          $target_file = $target_dir . basename("$login_image");
          if(file_exists($target_file)){
            echo "<img src='uploads/users/$login_image' class='img-circle elevation-3' alt='User Image' style='height: 33px;'>";
          } 
          else {
            echo "<img src='uploads/users/user.png' width='150px'>";
          }
          ?>
        </div>
        <div class="info">
          <a href="users_profile.php" class="d-block"><?php echo $_SESSION['login_user'] ?></a>
        </div>
      </div>

      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
          <li class="nav-item">
            <a href="../../index.php" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-briefcase"></i>
              <p>
                Applicants
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <input type="text" class="form-control" id="sessionempty" name="sessionempty" value="" placeholder="Search" hidden>
              <li class="nav-item">
                <a href="../forms/registration.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Government Employees</p>
                </a>
              </li>
              <li class="nav-item" <?php echo $deploy;?>>
                <a href="../forms/registration_Uni.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Uniformed Personnel</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/registration_OFW.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>OFW</p>
                </a>
              </li>
            </ul>
          </li>
          
          <li class="nav-item" <?php echo $project_hide ?>>
            <a href="../forms/project_view.php" onclick="sessionempty()" class="nav-link">
              <i class="nav-icon fas fa-building"></i>
              <p>
                Projects
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $user_hide ?>>
            <a href="../forms/users.php" class="nav-link active" onclick="sessionempty()" style="background: #67a2b2;">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Users
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $element_hide ?>>
            <a href="../forms/report_gen.php" class="nav-link">
              <i class="nav-icon fas fa-scroll"></i>
              <p>
                Reports
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $pages_hide ?>>
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-clone"></i>
              <p>
                Pages
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              
              <li class="nav-item">
                <a href="../forms/home.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Home</p>
                </a>
              </li>
              <li class="nav-item" hidden>
                <a href="../forms/howToApply.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>How To Apply</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/about.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>About</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/banner.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Banner</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="../forms/bulletin.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Bulletin</p>
                </a>
              </li>
            </ul>
          </li>
          
          <li class="nav-item" <?php echo $inv_hide;?>>
            <a href="../forms/inventory.php" class="nav-link">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                Inventory
              </p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>
  <script>
    function sessionempty() {
      $("#loaderIcon").show();
          jQuery.ajax({
          url: "session_clear.php",
          data: {'sessionempty': $("#sessionempty").val()},
          type: "POST",
          success:function(data){
              $("#").html(data);
              $("#loaderIcon").hide();
          },
          error:function (){
          }
      });
    }
  </script>

  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Users</h1>
          </div>
          <div class="col-sm-6">
          </div>
        </div>
      </div>
    </section>
    <form method="POST" action="usersDelete_process.php" id="deleteForm">
      <section class="content" id="form">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div>
                <a href="addUsers.php" class="btn btn-default color-green-dark" style="float:right; width:auto; font-size:large;">
                  <i class="fas fa-plus"></i>&nbsp; Add User
                </a>
                <button <?php echo $delete_disabled; ?> type="button" data-toggle="modal" data-target="#delete" class="btn btn-default color-red-dark-auto">
                  <i class="fa fa-trash"></i>&nbsp; Delete
                </button>
                <?php include("delete_modal.php"); ?>
              </div><br>
              <div class="card card-primary" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                <div class="card-header" style="background: #67a2b2;">
                  <h3 class="card-title"></h3>
                </div>
                <div class="card-body">
                  <div class="col-sm-3">
                    <div class="input-group input-group-sm" style="padding: 7px 10px 0px;">
                      <input type="text" class="form-control" id="checker" name="checker" onkeyup="checksample()"
                       value="<?php echo $_SESSION["userSearch"]; ?>" placeholder="Search: Firstname / Lastname">
                    </div>
                    <script>
                      function checksample() {
                          $("#loaderIcon").show();
                              jQuery.ajax({
                              url: "users_search.php",
                              data: {'checkers': $("#checker").val()},
                              type: "POST",
                              success:function(data){
                                  $("#checkxxx").html(data);
                                  $("#loaderIcon").hide();
                              },
                              error:function (){
                              }
                          });
                      }
                    </script>
                  </div>
                  <div class="col-sm-12" id="checkxxx">
                  <div style="overflow:auto; height: 650px; padding: 7px 10px;">
                    <table id="userTable" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th <?php echo $delete_hide; ?> style="white-space: nowrap; " class="project-actions text-center"><input id="chk_all" name="chk_all" type="checkbox"></th>
                        <th>Image</th>
                        <th>Email Address</th>
                        <th>Username</th>
                        <th>Status</th>
                        <th>Full Name</th>
                        <th>Region Office</th>
                        <th>District Office</th>
                        <th>User Level</th>
                        <th>Action</th>
                      </tr>
                      </thead>
                      <tbody>
                      
                      <?php
                      if(isset($_SESSION["userSearch"])){
                        try{
                          $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                          $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                          //Regional User
                          if($login_office == 'Regional'){
                            $selectdata=[
                              ':userSearch' => $_SESSION["userSearch"],
                              ':login_office' => $login_office,
                              ':login_officeID' => $login_officeID,
                              ':status' => 'Active'
                            ];
                            $select = "SELECT * FROM users
                                          LEFT JOIN table_ro_ ON users.office_id = table_ro_.ro_id
                                          WHERE lastname LIKE CONCAT('%', :userSearch, '%') AND office = :login_office AND office_id = :login_officeID AND status = :status
                                          OR firstname LIKE CONCAT('%', :userSearch, '%') AND office = :login_office AND office_id = :login_officeID AND status = :status
                                          ORDER BY userlevel";
                          }
                          //Head Office Any department except COSDD
                          if($login_office == 'Head Office' && $login_officeID != 'h50000000' OR $login_officeID == 'h10000000'){
                            $selectdata=[
                              ':userSearch' => $_SESSION["userSearch"],
                              ':login_office' => $login_office,
                              ':login_officeID' => $login_officeID,
                              ':login_officeIDx' => "h50000000",
                              ':status' => 'Active'
                            ];
                            $select = "SELECT * FROM users
                                          WHERE lastname LIKE CONCAT('%', :userSearch, '%') AND office = :login_office AND office_id = :login_officeID AND office_id != :login_officeIDx AND status = :status
                                          OR firstname LIKE CONCAT('%', :userSearch, '%') AND office = :login_office AND office_id = :login_officeID AND office_id != :login_officeIDx AND status = :status
                                          ORDER BY userlevel";
                          }
                          //Head Office COSDD
                          if($login_office == 'Head Office' && $login_officeID == 'h50000000'){
                            $selectdata=[
                              ':userSearch' => $_SESSION["userSearch"],
                            ];
                            $select = "SELECT * FROM users WHERE lastname LIKE CONCAT('%', :userSearch, '%') OR firstname LIKE CONCAT('%', :userSearch, '%') ORDER BY userlevel";
                          }
                          //Head Office EMD
                          if($login_office == 'Head Office' && $login_officeID == 'h10000000'){
                            $selectdata=[
                              ':userSearch' => $_SESSION["userSearch"],
                              ':login_officeIDx1' => "h60000000",
                              ':login_officeIDx2' => "h50000000",
                              ':login_officeIDx3' => "h40000000",
                              ':login_officeIDx4' => "h30000000",
                              ':login_officeIDx5' => "h20000000",
                              ':status' => 'Active'
                            ];
                            $select = "(SELECT * FROM users WHERE lastname LIKE CONCAT('%', :userSearch, '%') AND office_id != :login_officeIDx1
                                       AND office_id != :login_officeIDx2 AND office_id != :login_officeIDx3 AND office_id != :login_officeIDx4 AND office_id != :login_officeIDx5
                                       AND status = :status
                                       OR firstname LIKE CONCAT('%', :userSearch, '%') AND office_id != :login_officeIDx1
                                       AND office_id != :login_officeIDx2 AND office_id != :login_officeIDx3 AND office_id != :login_officeIDx4 AND office_id != :login_officeIDx5
                                       AND status = :status) ORDER BY office, userlevel";
                          }
                          $sthselect = $dbh->prepare($select);
                          $sthselect->execute($selectdata);
                          $count = $sthselect->rowCount();
                          $setcount = $count;
                          
                          if (isset($_GET['pageno'])) {
                            $pageno = $_GET['pageno'];
                            if($_GET['pageno'] == 1){
                              $i = 2;
                            }
                          } 
                          else {
                            $pageno = 1;
                            $i = 2;
                          }
                    
                          $no_of_records_per_page = 5;
                          $offset = ($pageno-1) * $no_of_records_per_page;
                    
                          $total_pages = ceil($count / $no_of_records_per_page);
                    
                          $cnt=1;
                          
                          $sql2 = "$select LIMIT $offset, $no_of_records_per_page";
                    
                          $sthsql2 = $dbh->prepare($sql2);
                          $sthsql2->execute($selectdata);
                          $sthsql2->setFetchMode(PDO::FETCH_ASSOC); 
                          $app_count = $sthsql2->rowCount();
                          $setcount = $app_count;

                            $i=0;
                            while ($userrow = $sthsql2->fetch(PDO::FETCH_ASSOC)) {
                              $image = $userrow['user_image'];
                              ?>
                              <tr>
                              <td <?php echo $delete_hide; ?> class="project-actions text-center" style="vertical-align: middle;"><input name="check[]" type="checkbox" class='chkbox' value="<?php echo $userrow['user_id']; ?>"/></td>
                              <td class="project-actions text-center" style="vertical-align: middle;"> 
                                <?php
                                $target_dir = "uploads/users/";
                                $target_file = $target_dir . basename("$image");
                                if(file_exists($target_file)){
                                  echo "<img src='uploads/users/$image' width='150px'>";
                                } 
                                else {
                                  echo "<img src='uploads/users/user.png' width='150px'>";
                                }
                                ?>
                              </td>
                              <td style="vertical-align: middle;"> <?php echo $userrow['email'] ?> </td>
                              <td style="vertical-align: middle;"> <?php echo $userrow['username'] ?> </td>
                              <td class="project-actions text-center" style="vertical-align: middle;">
                                <span <?php if($userrow['status']=='Active'){echo 'style="background:green; padding: 3px 6px; color: white; border-radius: 5px;"';} 
                                else {echo 'style="background:red; padding: 3px 6px; color: white; border-radius: 5px;"';}?>>
                                  <?php echo $userrow['status'] ?>
                                </span>
                              </td>
                              <td style="vertical-align: middle;"> <?php echo $userrow['lastname'] ?><?php if($userrow['suffix'] == 'N/A' || $userrow['suffix'] == ''){echo '';} else{echo ' '.$userrow['suffix'];}?>, <?php echo $userrow['firstname'] ?> <?php echo $userrow['middlename'] ?> </td>
                              <td style="vertical-align: middle;"> 
                                <?php 
                                echo $userrow['office'];
                                
                                ?> 
                              </td>
                              <td style="vertical-align: middle;"> 
                                <?php 
                                if($userrow['office'] == 'Head Office'){
                                  $officeselect = "SELECT description FROM department WHERE office_id = :office_id";
                                  $sthofficeselect = $dbh->prepare($officeselect);
                                  $sthofficeselect->bindParam(':office_id', $userrow['office_id']);
                                  $sthofficeselect->execute();
                                  $sthofficeselect->setFetchMode(PDO::FETCH_ASSOC); 
                                  while ($officerow = $sthofficeselect->fetch(PDO::FETCH_ASSOC)) {
                                    echo $officerow['description'];
                                  }
                                }
                                if($userrow['office'] == 'Regional'){
                                  $officeselect = "SELECT RO FROM table_ro_ WHERE ro_id = :office_id";
                                  $sthofficeselect = $dbh->prepare($officeselect);
                                  $sthofficeselect->bindParam(':office_id', $userrow['office_id']);
                                  $sthofficeselect->execute();
                                  $sthofficeselect->setFetchMode(PDO::FETCH_ASSOC); 
                                  while ($officerow = $sthofficeselect->fetch(PDO::FETCH_ASSOC)) {
                                    echo $officerow['RO'];
                                  }
                                }
                                ?> 
                              </td>
                              <td style="vertical-align: middle;"> <?php echo $userrow['userlevel'] ?> </td>
                              <td class="project-actions text-center" style="vertical-align: middle;">
                              <div>
                                <a id="tooltip" href="users_edit.php?id=<?php echo $userrow['user_id'] ?>&office=<?php echo $userrow['office'] ?>" style="font-size:large;" class="btn btn-default btn-sm color-cyan-dark">
                                  <i class="fas fa-edit">
                                  </i>
                                  <span class="tooltiptext">Edit</span>
                                </a>
                              </div>
                              </td>
                              </tr>
                              <?php 
                              $i++; 
                            }
                            $dbh = null; 
                        }
                        catch(PDOException $e){
                            error_log('PDOException - ' . $e->getMessage(), 0);
                            http_response_code(500);
                            die('Error establishing connection with database');
                        }
                      }
                      ?>
                      </tbody>
                    </table>
                  </div>
                  <?php include("paging.php"); ?>
                  <?php include("delete_script.php");?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </form>
  </div>
  <footer class="main-footer">
    <?php include('footer.php'); include('custom_btn.php'); ?>
  </footer>

  <aside class="control-sidebar control-sidebar-dark">
  </aside>
</div>

<script src="../../plugins/jquery/jquery.min.js"></script>
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script src="../../dist/js/adminlte.min.js"></script>
<script src="../../dist/js/demo.js"></script>
<script src="../../plugins/sweetalert2/sweetalert2.min.js"></script>
<script src="../../plugins/toastr/toastr.min.js"></script>

<script>
  $(function () {
    $("#userTable").DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": false,
      "responsive": false,
      //"buttons": ["excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#userTable_wrapper .col-md-6:eq(0)');
  });
</script>

</body>
</html>

<style>
  #form {
    -webkit-animation-duration: 0.7s;
    animation-duration: 0.7s;
  }
  @-webkit-keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  @keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  #form {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
  }
  td {
    background: #f5f5f5;
  }
</style>
<script language="javascript">
  var message="This function is not allowed here.";
  function clickIE4(){
        if (event.button==2){
            alert(message);
            return false;
        }
  }
  function clickNS4(e){
      if (document.layers||document.getElementById&&!document.all){
              if (e.which==2||e.which==3){
                        alert(message);
                        return false;
              }
      }
  }
  if (document.layers){
        document.captureEvents(Event.MOUSEDOWN);
        document.onmousedown=clickNS4;
  }
  else if (document.all&&!document.getElementById){
        document.onmousedown=clickIE4;
  }
  document.oncontextmenu=new Function("alert(message);return false;")
</script>