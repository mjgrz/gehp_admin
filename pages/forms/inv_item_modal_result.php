<?php
  include("../../../../dbcon.php"); 
  $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
  $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $querypjselect = "SELECT * FROM project_info WHERE availability = 'Available' ORDER BY PJ_NAME";
  $sthpj = $dbh->prepare($querypjselect);
  $sthpj->execute();
  $sthpj->setFetchMode(PDO::FETCH_ASSOC); 
?>
<div class="modal-header" style="background:#67a2b2;">
  <h4 class="modal-title" style="color:white;">Add Map Inventory</h4>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true" style="color: white;">&times;</span>
  </button>
</div>
<form method="POST" action="inv_insert_stat.php" enctype="multipart/form-data">
  <div class="modal-body" style="background: #f6f6f6;">
    <div class="col-sm-12">

      <div class="col-lg-12">
        <div class="row">
          <div class="col-sm-3 mt-3">
            <label>Project Name: </label>
            <select class="form-control" required onchange="getPrjct(this.value)" id="prjTxt" name="prjTxt">
              <option disabled selected value=""> - Select Project Name -</option>
              <?php while ($pjrowslct = $sthpj->fetch(PDO::FETCH_ASSOC)) { ?>
                <option value="<?php echo $pjrowslct['project_id']; ?>"><?php echo $pjrowslct['PJ_NAME']; ?></option>
              <?php } ?>
            </select>
            <script type="text/javascript">
              var flrCountToDsply = 0;
              function getPrjct(val){
                $.ajax({
                  type:'POST',
                  url:'inv_ajax_floor.php',
                  data:{getOption:val},
                  success:function(response){
                    document.getElementById("floorNum").value="Floor "+response;
                    flrCountToDsply = response;
                  }
                })
              }
            </script>
          </div>
          <div class="col-sm-3 mt-3" hidden>
            <label>Status: </label>
            <select class="form-control">
              <option disabled selected value=""> - Select Status -</option>
              <option value="Available">Available</option>
              <option value="Reserved">Reserved</option>
              <option value="Awarded">Awarded</option>
              <option value="Under Construction">Under Construction</option>
            </select>
          </div>
          <div class="col-sm-3 mt-3" hidden>
            <label>Floor Area: </label>
            <input type="text" class="form-control">
          </div>
          <div class="col-sm-3 mt-3" hidden>
            <label>Lot Area: </label>
            <input type="text" class="form-control">
          </div>
          <div class="col-sm-3 mt-3">
            <label>Floor Number: </label>
            <input type="text" id="floorNum" name="floorNum" class="form-control" value="" readonly>
            <!-- <select class="form-control" id="floorNum" name="floorNum" required>
              <option disabled selected value="">- Select Floor Number -</option>
            </select> -->
          </div>
          <div class="col-sm-3 mt-3">
            <label>Built Map: </label>
            <div class="input-group">
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="builtMapFile" name="builtMapFile" required>
                <label class="custom-file-label" for="exampleInputFile">Choose file</label>
              </div>
              <script type="text/javascript">
                $(document).ready(function () {
                  bsCustomFileInput.init();
                });
              </script>
            </div>
          </div>
        </div>
      </div>

      <hr>

      <div class="col-lg-12 mt-3"><textarea id="listTxt" name="listTxt" class="form-control" hidden></textarea>
        <div class="row" id="next">
          <div class="col-lg-3">
            <div class="row">
              <div class="col-12" hidden>
                <label>Items: </label>
                <input class="sl" type="text" name="slno[]" id="slno" value="1" hidden readonly="">
              </div>
              <div class="col-12" hidden>
                <label>Block/Building: </label>
                <input type="text" name="blockTxt[]" id="blockTxt1" disabled min="1" value="1" class="form-control">
              </div>
              <div class="col-12">
                <div class="row">
                  <div class="col-2">

                  </div>
                  <div class="col-sm-12">
                    <label>Number of Lot/Unit for Block 1: </label>
                    <input type="number" name="lotTxt[]" oninput="show()"
                    id="lotTxt1" min="1" class="form-control" required>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 mt-2 pr-3">
          <div class="row" style="justify-content:space-between;">
            <div>
              <button class="btn btn-default color-green-dark" type="button" id="addInput" style="width:auto;"
              onclick="add()">Add Block</button>
              <button class="btn btn-default color-red-dark" type="button" id="removeInput" style="width:auto;"
              onclick="remove()" hidden>Remove Last Block</button>
            </div>
            <div>
              <!-- <button class="btn btn-default color-cyan-dark" type="button" id="preview" style="width:auto;"
              onclick="show()">View</button> -->
            </div>
          </div>
        </div>
      </div>

      <div class="col-12" id="table">
        <div style="overflow:auto; height: auto; padding:15px 10px;">
          <table style="border-radius:5px; margin:auto;">
            <thead>
              <tr id="theader">
              </tr>
            </thead>
            <tbody id="tobody">
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  
  <div class="modal-footer justify-content-right">
    <button type="submit" class="btn btn-default color-cyan"
    style="font-size:large; width:auto;">
    <i class="fa fa-paper-plane"></i>&nbsp; Submit</button>
  </div>
</form>

<script>
  var length = 0;
  var i = 0;
  var el;

  function add()  {
    document.getElementById('removeInput').hidden = false;
		length  = $('.sl').length;
		i       = parseInt(length)+parseInt(1);
		var newrow = $('#next').append(
      '<div class="col-lg-3" id="added'+i+'">'+
        '<div class="row">'+
          '<div class="col-3" hidden>'+
            '<label>Items: </label>'+
            '<input class="sl" type="text" name="slno[]" id="slno" value="'+i+'"  readonly="">'+
          '</div>'+
          '<div class="col-6" hidden>'+
            '<label>Block/Building: </label>'+
            '<input type="text" name="blockTxt[]" id="blockTxt'+i+'" disabled min="1" value="1" class="form-control">'+
          '</div>'+
          '<div class="col-12">'+
            '<div class="row">'+
              '<div class="col-2">'+

              '</div>'+
              '<div class="col-12">'+
                '<label>Number of Lot/Unit for Block '+i+': </label>'+
                '<input type="number" name="lotTxt[]" oninput="show()" id="lotTxt'+i+'" min="1" class="form-control" focus required>'+
              '</div>'+
            '</div>'+
          '</div>'+
        '</div>'+
      '</div>'
    );
    document.getElementById('lotTxt'+i).focus();
    show()
  };
  
  function remove() {
    if(i <= 1){

    }
    else{
      $('#added'+i+'').remove()
      i  = parseInt(i)-parseInt(1)
      if(i <= 1){
        document.getElementById('removeInput').hidden = true;
      }
      else{
        document.getElementById('removeInput').hidden = false;
      }
      document.getElementById('lotTxt'+i).focus();
    }
    show()
  };

  var array = [];
  function show(){
    array = [];
    $('.thead').remove()
    $('.tbodyrow').remove()
		var length = $('.sl').length;
    var numblk = [];
    var numlot = [];
    for(var i=1;i<=length;i++){
      var items = [];
      var lot = $('#lotTxt'+i+'').val();
      var blk = $('#blkTxt'+i+'').val();
      for(var ii=1;ii<=lot;ii++){
        items.push({id:ii,blocknum:i,lotnum:ii,prop:'Available'});
      }
      array.push({items});
      numlot.push(lot);
      numblk.push(i);
    }
    var string = JSON.stringify(array);
    document.getElementById('listTxt').value = string;
    var hightNumlot = Math.max(...numlot);
    var hightNumblk = Math.max(...numblk);
    console.log(array)

    for(var tb=1;tb<=hightNumblk;tb++){
      for(var th=0;th<=hightNumlot;th++) {
        if(tb === 1){
          if(th === 0){
            var thTxt = 'BLK/LOT';
            if(flrCountToDsply > 1) {
              thTxt = 'BLD/UNIT';
            }
            else {
              thTxt = 'BLK/LOT';
            }
            var header = $('#theader').append(
              '<th class="text-center thead">'+
                '<div style="border-radius:5px; background-color:lightgray; width:102px;" class="btn btn-default btn-md">'+
                  thTxt+
                '</div>'+
              '</th>'
            );
          }
          if(th != 0){
            var header = $('#theader').append(
              '<th class="text-center thead" style="padding:4px; cursor:default;">'+
                '<div style="border-radius:5px; background-color:lightgray; width:75px;" class="btn btn-default btn-md">'+
                  th
                +'</div>'+
              '</th>'
            );
          }
        }
        else{}
      };
      var body = $('#tobody').append(
        '<tr class="tbodyrow" id="tbodyrow'+tb+'">'+
          '<td class="text-center">'+
            '<div style="border-radius:5px; background-color:lightgray; width:102px; height:41px; cursor:default;" class="btn btn-default btn-md">'+
              tb
            +'</div>'+
          '</td>'+
        '</tr>'
      );
      for(var th=1;th<=numlot[tb-1];th++) {
        var tdTxt1 = 'B';
        var tdTxt2 = 'L';
        if(flrCountToDsply > 1) {
          tdTxt1 = 'B';
          tdTxt2 = 'U';
        }
        else {
          tdTxt1 = 'B';
          tdTxt2 = 'L';
        }
        var data = $('#tbodyrow'+tb+'').append(
          '<td class="text-center">'+
            '<button style="font-size:large; margin:3px; width:75px; pointer-events:none;" type="button" class="btn btn-info btn-md">'+
              '<span>'+ tdTxt1 + tb + tdTxt2 + th +'</span>'+
            '</button>'+
          '</td>'
        );
      };
    }
  };
</script>

<!-- <script>
  var array = [];
  function myFunction() {
    var items = [];
    for(var i=1;i<=5;i++){
      items.push({id:i,blk:i,lot:i,prop:'Available'});
    }
    array.push({items});
    var string = JSON.stringify(array);
    document.getElementById('listTxt').value = string;
    console.log(JSON.parse(string));
  }
</script> -->