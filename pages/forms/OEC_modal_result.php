<?php
$username_modal = $_GET['app_id'];
include("../../../../dbcon.php");
session_start();
?>
<?php
if (is_numeric($username_modal) == true){
  try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $OEC_select_modal = "SELECT * FROM ofw_applications 
                         LEFT JOIN ofw_applicant_info ON ofw_applications.applicant_id = ofw_applicant_info.applicant_id   
                         WHERE APP_ID = :username_modal";
    $sthOEC_select_modal = $dbh->prepare($OEC_select_modal);
    $sthOEC_select_modal->bindParam(':username_modal', $username_modal);
    $sthOEC_select_modal->execute();
    $sthOEC_select_modal->setFetchMode(PDO::FETCH_ASSOC); 
    while ($OEC_modal = $sthOEC_select_modal->fetch(PDO::FETCH_ASSOC)) {
      $PJ_CODE = $OEC_modal["PJ_CODE"];
      $LNAME = $OEC_modal["LNAME"];
      $applicant_id = $OEC_modal["applicant_id"];
      if($OEC_modal["fileUpload"] == "Already submitted OEC File (Hardcopy)"){
        $checker = "checked";
        $require = "";
        $disable = "disabled";
      }
      if($OEC_modal["fileUpload"] != "Already submitted OEC File (Hardcopy)"){
        $checker = "";
        $require = "required";
        $disable = "";
      }
      $fileUpload = $PJ_CODE."-".$LNAME."-".$applicant_id.".pdf";
    }
    $dbh = null;
  }
  catch(PDOException $e){
    error_log('PDOException - ' . $e->getMessage(), 0);
    http_response_code(500);
    die('Error establishing connection with database');
  }
} 
else{
http_response_code(400);
die('Error processing bad or malformed request');
}
?>
<div class="modal-header" style="background: #67a2b2; color: white;">
  <h4 class="modal-title" style="color: white;">Change OEC File (PDF format)</h4>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true" style="color: white;">&times;</span>
  </button>
</div>
<!--Submit Form for updating OEC-->
<form method="POST" action="OEC_process.php" enctype="multipart/form-data">
  <div class="modal-body">
    <div class="col-sm-12">
      <div class="col-lg-12">
        <div class="custom-control custom-checkbox">
          <input class="custom-control-input" type="checkbox" name="checkerOEC" id="customCheckbox1" value="Already submitted OEC File (Hardcopy)" <?php echo $checker ?>>
          <label for="customCheckbox1" class="custom-control-label">Already submitted OEC File (Hardcopy)</label>
        </div>
        <br>
        <div class="form-group" id="grouptohide">
          <input type="text" name="filename" id="filename" value="<?php echo $fileUpload ?>" hidden>
          <input type="text" name="APP_ID" id="APP_ID" value="<?php echo $username_modal ?>" hidden>
          <input type="text" name="username" id="username" value="<?php echo $_SESSION['login_user']?>" hidden>
          <div class="custom-file">
            <input type="file" class="custom-file-input" id="file" name="file" <?php echo $require ?> <?php echo $disable ?>>
            <label class="custom-file-label" for="file">Choose file</label>
          </div>  
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer justify-content-right" style="background:#f6f6f6;">
    <button type="submit" class="btn btn-default color-cyan-dark" style="font-size:large; width:auto;"><i class="fa fa-paper-plane"></i>&nbsp; Submit</button>
  </div>
</form>
<!--End of Submit Form for updating OEC-->
<script>
  $(function () {
      $("#customCheckbox1").click(function () {
          if ($(this).is(":checked")) {
            document.getElementById("file").disabled = true;
            document.getElementById("file").required = false;

          } else {
            document.getElementById("file").disabled = false;
            document.getElementById("file").required = true;
          }
      });
  });
  $(function () {
    bsCustomFileInput.init();
  });
</script>
<style>
  #file {
    cursor: pointer;
  }
</style>