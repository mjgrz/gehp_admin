<?php
session_start();
date_default_timezone_set("Asia/Hong_Kong");
$username = $_SESSION['login_user'];
$time = date("h:i a");
$date = date('F d, Y', strtotime(date("Y-m-d")));
$datetime = $date." ".$time;
include('dbcon.php');
$URL = $_POST['URL'];
if(isset($_POST['submit'])){
  if(isset($_POST['check'])) {
    $checkbox = $_POST['check'];
    for($i=0;$i<count($checkbox);$i++){
    $del_id = $checkbox[$i]; 
      if (is_numeric($del_id) == true){
        try{
          $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
          $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $bulletindelete = "DELETE FROM bulletin WHERE bulletin_id=:del_id";
          $sthbulletindelete = $dbh->prepare($bulletindelete);
          $sthbulletindelete->bindParam(':del_id', $del_id);
            if($sthbulletindelete->execute()){
                $auditdata = [
                  ':activity' => "Bulletin Item Deleted",
                  ':username' => $_SESSION['login_user'],
                  ':datetime' => $datetime
                ];
                $audit = "INSERT INTO audit_trail (activity, username, date) VALUES (:activity, :username, :datetime)";
                $sthaudit = $dbh->prepare($audit);
                $sthaudit->execute($auditdata);
                $_SESSION["status"] = "Record(s) have been deleted successfully.";
                header('Location: '. $URL .'');
                $dbh = null;
            }
            else{
                $_SESSION["error"] = "Sorry, record(s) were not deleted.";
                header('Location: '. $URL .'');
                $dbh = null;
            }
        }
        catch(PDOException $e){
          error_log('PDOException - ' . $e->getMessage(), 0);
          http_response_code(500);
          die('Error establishing connection with database');
        }
      } 
      else{
      http_response_code(400);
      die('Error processing bad or malformed request');
      }
    }
  }
  if(!isset($_POST['check'])) {
    $_SESSION["info"] = "Sorry, no record(s) to be deleted.";
    header('Location: '. $URL .'');
  }
}
else{
  header('Location: '. $URL .'');
}
?>