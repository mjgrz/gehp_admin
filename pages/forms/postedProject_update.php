<?php
  session_start();
  include('postedProject_update/posted_image1.php'); 
  include('postedProject_update/posted_image2.php'); 
  include('postedProject_update/posted_image3.php'); 
  include('postedProject_update/posted_image4.php'); 
  include('postedProject_update/posted_image5.php'); 
  include('../../../../dbcon.php'); 

  $post_title = $_POST["posted_title"];
  $project_name = $_POST["project_name"];
  $post_desc = $_POST["posted_desc"];
  date_default_timezone_set("Asia/Hong_Kong");
  $time = date("h:i a");
  $date = date('F d, Y', strtotime(date("Y-m-d")));
  $datetime = $date." ".$time;
  $postedprojid = 1;
  if ( is_numeric($postedprojid) == true){
    try{
      $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $sqlposteddata = [
        ':post_title' => $post_title,
        ':project_name' => $project_name,
        ':post_desc' => $post_desc,
        ':filename1' => $filename1,
        ':filename2' => $filename2,
        ':filename3' => $filename3,
        ':filename4' => $filename4,
        ':filename5' => $filename5,
        ':postedprojid' => $postedprojid
      ];

      $sqlposted = "UPDATE posted_project SET post_title=:post_title, project_name=:project_name, post_desc=:post_desc, 
      post_image1=:filename1, post_image2=:filename2, post_image3=:filename3, post_image4=:filename4, post_image5=:filename5 
      WHERE posted_id=:postedprojid";
      $sthsqlposted = $dbh->prepare($sqlposted);
      
      if($sthsqlposted->execute($sqlposteddata)){
        $auditdata = [
          ':activity' => "Edited the Posted Project",
          ':username' => $_SESSION['login_user'],
          ':datetime' => $datetime
        ];
        $audit = "INSERT INTO audit_trail (activity, username, date) VALUES (:activity, :username, :datetime)";
        $sthaudit = $dbh->prepare($audit);
        $sthaudit->execute($auditdata);
        
        $_SESSION["status"] = "Your data have been saved successfully.";
        header('Location: ../forms/home.php');
        $dbh = null;
      }
      else{
        $_SESSION["error"] = "Sorry, your data were not saved.";
        header('Location: ../forms/projects.php');
        $dbh = null;
      }
    }
    catch(PDOException $e){
      error_log('PDOException - ' . $e->getMessage(), 0);
      http_response_code(500);
      die('Error establishing connection with database');
    }
  } 
  else{
  http_response_code(400);
  die('Error processing bad or malformed request');
  }
?>