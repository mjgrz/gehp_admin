<?php
  session_start();
  include("../../../../dbcon.php");
  try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $userLname = $_POST["userLname"];
    $userFname = $_POST["userFname"];
    $userMname = $_POST["userMname"];
    $userSuffix = $_POST["userSuffix"];
    $location = $_POST["office"];
    $office_id = $_POST["office_id"];
    $userlevel = $_POST["userlevel"];
    $email = $_POST["email"];
    $usernamex = $_POST["username"];
    $password = $_POST["password"];
    $hashed_password = password_hash($_POST["password"], PASSWORD_DEFAULT);
    $user_image = $usernamex."-".$office_id.".png";
    date_default_timezone_set("Asia/Hong_Kong");
    $time = date("h:i a");
    $date = date('F d, Y', strtotime(date("Y-m-d")));
    $datetime = $date." ".$time;
    $sqluserdata = [
      ':email' => $email,
      ':username' => $usernamex,
      ':password' => $hashed_password,
      ':userLname' => $userLname,
      ':userFname' => $userFname,
      ':userMname' => $userMname,
      ':userSuffix' => $userSuffix,
      ':location' => $location,
      ':office_id' => $office_id,
      ':userlevel' => $userlevel,
      ':user_image' => $user_image,
      ':status' => 'Active'
    ];
    $sqluser = "INSERT INTO users (email, username, user_password, lastname, firstname, middlename, suffix, office, office_id, userlevel, user_image, status) 
                  VALUES (:email, :username, :password, :userLname, :userFname, :userMname, :userSuffix, :location, :office_id, :userlevel, :user_image, :status)";
    $sthsqluser = $dbh->prepare($sqluser);
    if ($sthsqluser->execute($sqluserdata)) {
        $auditdata = [
          ':activity' => "Added an User (User = $usernamex/$office_id)",
          ':username' => $_SESSION['login_user'],
          ':datetime' => $datetime
        ];  
        $audit = "INSERT INTO audit_trail (activity, username, date) VALUES (:activity, :username, :datetime)";
        $sthaudit = $dbh->prepare($audit);
        $sthaudit->execute($auditdata);

        $_SESSION["status"] = "Your data have been saved successfully.";
        header('Location: users.php');
        $dbh = null;
    }
    else {
      $_SESSION["error"] = "Sorry, your data were not saved.";
        header('Location: users.php');
        $dbh = null;
    }
  }
  catch(PDOException $e){
    error_log('PDOException - ' . $e->getMessage(), 0);
    http_response_code(500);
    die('Error establishing connection with database');
  }
?>