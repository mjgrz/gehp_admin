<?php
include("../../../../dbcon.php");
$idproj = 1;
if ( is_numeric($idproj) == true){
  try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $bannerselect1 = "SELECT banner_title, banner_desc, banner_image FROM banner where id = :idproj";
    $sthbannerselect1 = $dbh->prepare($bannerselect1);
    $sthbannerselect1->bindParam(':idproj', $idproj);
    $sthbannerselect1->execute();
    $sthbannerselect1->setFetchMode(PDO::FETCH_ASSOC); 
    $idproj_count=$sthbannerselect1->rowCount();
    if($idproj_count > 0){
      while ($bannerrow1 = $sthbannerselect1->fetch(PDO::FETCH_ASSOC)) {
        $banner_title1 = $bannerrow1['banner_title'];
        $banner_desc1 = $bannerrow1['banner_desc'];
        $banner_image1 = $bannerrow1['banner_image'];
      } 
    }
    else
    {
              $banner_title1 = "";
              $banner_desc1 = "";
              $banner_image1 = "";
    }
    $dbh = null;
  }
  catch(PDOException $e){
    error_log('PDOException - ' . $e->getMessage(), 0);
    http_response_code(500);
    die('Error establishing connection with database');
  }
} 
else{
http_response_code(400);
die('Error processing bad or malformed request');
}
?>
<div class="card card-primary" style="box-shadow: none; border: 1px solid #67a2b2;">
  <form method="POST" action="project_page_update.php" enctype="multipart/form-data">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-4">
          <div class="form-group">
            <label for="project_banner">Title</label>
            <input type="text" class="form-control" id="project_banner" name="project_banner"
      oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $banner_title1 ?>">
          </div>
          <div class="form-group">
            <label for="project_desc">Description</label>
            <textarea class="form-control" id="project_desc" name="project_desc" style="resize: none; padding: 10px;" rows="10" value="<?php echo $banner_desc1 ?>"><?php echo $banner_desc1 ?></textarea>
          </div>
        </div>
        <input type="text" class="form-control" id="pro_uploadImage_text" name="pro_uploadImage_text"
      oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $banner_image1 ?>" hidden>
        
        <div class="col-sm-8">
          <div class="col-md-12" style="margin-top: 32px;">
            <div class="col-md-12">
              <div class="form-group" id="pro_uploadDiv">
                <input type="file" id="pro_uploadImage" name="pro_uploadImage" title="Click in this area to upload an image." onchange="previewupdate_project()">
                <p id="pro_uploadText" ></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="card-footer">
      <div class="project-actions text-right">
        <button style="width:auto; font-size:large;" type="submit" class="btn btn-default color-cyan-dark"><i class="fa fa-paper-plane"></i>&nbsp; Submit</button>
      </div>
    </div>
  </form>
</div>
<style>
  #pro_uploadDiv {
    width: 100%;
    height: 348px;
    border: 2px dashed #67a2b2;
    border-radius: 5px;
  }
  #pro_uploadImage {
    position: absolute;
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    outline: none;
    opacity: 0;
  }
  #pro_uploadImage:hover {
    cursor: pointer;
  }
  #pro_uploadText {
    text-align: center;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 5px;
  }
  #pro_uploadText {
    background-image: url(uploads/banner/<?php echo $banner_image1 ?>);
    background-position: center;
    background-repeat: no-repeat;
    background-size: 100% auto;
  }
  .content {
    -webkit-animation-duration: 0.7s;
    animation-duration: 0.7s;
  }
  @-webkit-keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  @keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  .content {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
  }
  input:hover, select:hover, textarea:hover {
    border: 1px solid #67a2b2;
  }
</style>
<script>
$(function () {
  bsCustomFileInput.init();
});
function previewupdate_project() {
  var urlimage = URL.createObjectURL(event.target.files[0]);
  document.getElementById("pro_uploadText").style.backgroundImage = "url("+urlimage+")";
  document.getElementById("pro_uploadText").style.backgroundSize = "100% auto";
  document.getElementById("pro_uploadText").style.backgroundRepeat = "no-repeat";
  document.getElementById("pro_uploadText").style.backgroundPosition = "center";
  document.getElementById("pro_uploadText").innerHTML = "";
}
</script>