<?php 
  include("login_session.php");if($login_userlevel != "Administrator"){
    header("location:../../index.php");
  }
  error_reporting(E_ERROR | E_PARSE);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>NHA | Government Employee's Housing Program</title>
<link rel="shortcut icon" type="image/png" href="../../dist/img/nha_logo_circle.png">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
<link rel="stylesheet" href="../../dist/css/adminlte.min.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="font-weight: bold;">
    <?php include("header.php"); ?>
  </nav>

  <aside class="main-sidebar sidebar-dark-primary elevation-4" style="background: #002d32; position: fixed;">
    <a href="../../index.php" class="brand-link">
      <img src="../../dist/img/nha_logo_circle.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"><b>NHA | GEHP</b></span>
    </a>

    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <?php
          $target_dir = "uploads/users/";
          $target_file = $target_dir . basename("$login_image");
          if(file_exists($target_file)){
            echo "<img src='uploads/users/$login_image' class='img-circle elevation-3' alt='User Image' style='height: 33px;'>";
          } 
          else {
            echo "<img src='uploads/users/user.png' width='150px'>";
          }
          ?>
        </div>
        <div class="info">
          <a href="users_profile.php" class="d-block"><?php echo $_SESSION['login_user'] ?></a>
        </div>
      </div>

      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
          <li class="nav-item">
            <a href="../../index.php" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-briefcase"></i>
              <p>
                Applicants
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <input type="text" class="form-control" id="sessionempty" name="sessionempty" value="" placeholder="Search" hidden>
              <li class="nav-item">
                <a href="../forms/registration.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Government Employees</p>
                </a>
              </li>
              <li class="nav-item" <?php echo $deploy;?>>
                <a href="../forms/registration_Uni.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Uniformed Personnel</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/registration_OFW.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>OFW</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item" <?php echo $project_hide ?>>
            <a href="../forms/project_view.php" onclick="sessionempty()" class="nav-link">
              <i class="nav-icon fas fa-building"></i>
              <p>
                Projects
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $user_hide ?>>
            <a href="../forms/users.php" class="nav-link active" onclick="sessionempty()" style="background: #67a2b2;">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Users
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $element_hide ?>>
            <a href="../forms/report_gen.php" class="nav-link">
              <i class="nav-icon fas fa-scroll"></i>
              <p>
                Reports
              </p>
            </a>
          </li>
          
          <li class="nav-item" <?php echo $pages_hide ?>>
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-clone"></i>
              <p>
                Pages
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              
              <li class="nav-item">
                <a href="../forms/home.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Home</p>
                </a>
              </li>
              <li class="nav-item" hidden>
                <a href="../forms/howToApply.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>How To Apply</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/about.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>About</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/banner.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Banner</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="../forms/bulletin.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Bulletin</p>
                </a>
              </li>
            </ul>
          </li>
          
          <li class="nav-item" <?php echo $inv_hide;?>>
            <a href="../forms/inventory.php" class="nav-link">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                Inventory
              </p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>
  <script>
    function sessionempty() {
      $("#loaderIcon").show();
          jQuery.ajax({
          url: "session_clear.php",
          data: {'sessionempty': $("#sessionempty").val()},
          type: "POST",
          success:function(data){
              $("#").html(data);
              $("#loaderIcon").hide();
          },
          error:function (){
          }
      });
    }
  </script>
  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add User</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
              <div class="card-header" style="background: #67a2b2;">
                <h3 class="card-title"></h3>
              </div>
              <!--Submit form for Adding User Account-->
              <form method="POST" action="addUsers_process.php">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="userLname">Last Name</label>
                        <input type="text" class="form-control" id="login_office" name="login_office" value="<?php echo $login_office ?>" hidden required>
                        
                        <input type="text" class="form-control" id="userLname" name="userLname" placeholder="Enter Last Name" required>
                      </div>
                      <div class="form-group">
                        <label for="userFname">First Name</label>
                        <input type="text" class="form-control" id="userFname" name="userFname" placeholder="Enter First Name" required>
                      </div>
                      <div class="form-group">
                        <label for="userMname">Middle Name</label>
                        <input type="text" class="form-control" id="userMname" name="userMname" placeholder="Enter Middle Name" required>
                      </div>
                      <div class="form-group">
                        <label for="userSuffix">Suffix</label>
                        <input type="text" class="form-control" id="userSuffix" name="userSuffix" placeholder="Enter Suffix">
                      </div>
                      <div class="form-group" <?php echo $drop_hide ?>>
                        <label for="office">Location</label>
                        <select class="form-control" name="office" id="office" onchange="getOffice(this.value)" required>
                          <option value="" disabled>- Select Location -</option>
                          <option <?php if($login_office == "Regional"){echo"selected"; } else {} ?> value="Regional">Regional</option>
                          <option <?php if($login_office == "Head Office"){echo"selected"; } else {} ?> value="Head Office">Head Office</option>
                        </select>
                        <script type="text/javascript">
                        function getOffice(val){
                          $.ajax({
                            type:'POST',
                            url:'ajax.php',
                            data:{getOption:val},
                            success:function(response){
                              document.getElementById("office_id").innerHTML=response;
                            }
                          })
                        }
                      </script>
                      </div>
                      <div class="form-group" <?php echo $drop_hide ?>>
                        <label for="office_id">Office</label>
                        <select class="form-control" name="office_id" id="office_id" required>
                          <option value='' disabled>- Select Office -</option>
                          <?php
                            if($login_office == "Regional"){
                              try{
                                $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                                $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                $regionsql = "SELECT ro_id, RO FROM table_ro_ ORDER BY RO";
                                $sthregionsql = $dbh->prepare($regionsql);
                                $sthregionsql->execute();
                                $sthregionsql->setFetchMode(PDO::FETCH_ASSOC); 
                                while ($regionrow = $sthregionsql->fetch(PDO::FETCH_ASSOC)) {
                                  if($regionrow['ro_id'] == $login_officeID) {
                                    $set = "selected";
                                  }
                                  else{
                                    $set = "";
                                  }
                                    echo "<option $set value='" . $regionrow['ro_id'] . "'>" . strtoupper($regionrow['RO']) . "</option>";
                                }
                                $dbh = null;
                              }
                              catch(PDOException $e){
                                error_log('PDOException - ' . $e->getMessage(), 0);
                                http_response_code(500);
                                die('Error establishing connection with database');
                              }
                            }
                            if($login_office == "Head Office"){
                              try{
                                $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                                $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                $depsql = "SELECT office_id, description FROM department";
                                $sthdepsql = $dbh->prepare($depsql);
                                $sthdepsql->execute();
                                $sthdepsql->setFetchMode(PDO::FETCH_ASSOC); 
                                while ($deprow = $sthdepsql->fetch(PDO::FETCH_ASSOC)) {
                                  if($deprow['office_id'] == $login_officeID) {
                                    $set = "selected";
                                  }
                                  else{
                                    $set = "";
                                  }
                                    echo "<option $set value='" . $deprow['office_id'] . "'>" . $deprow['description'] . "</option>";
                                }
                                $dbh = null;
                              }
                              catch(PDOException $e){
                                error_log('PDOException - ' . $e->getMessage(), 0);
                                http_response_code(500);
                                die('Error establishing connection with database');
                              }
                            }
                          ?>
                        </select>
                      </div>

                      <div class="form-group">
                        <label>User Level</label>
                        <select class="form-control" name="userlevel" id="userlevel" required>
                          <option value="" disabled>- Select User Level -</option>
                          <option value="Administrator">Administrator</option>
                          <option value="User">User</option>
                        </select>
                      </div>
                    </div>
                    <?php
                      $_SESSION["validity_mailadd"] = 1;
                      $_SESSION["validity_usernameadd"] = 1;
                    ?>        
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="email">Email Address</label>&nbsp&nbsp&nbsp<span id="user_email-availability-status" style="color: red;">*Active email address required</span>
                        <input type="email" class="form-control" id="email" name="email" onkeyup="checkuser_emailAvailabilityEdit()"
                         placeholder="Enter Email" required>
                      </div>
                      
                      <div class="form-group">
                        <label for="username">Username</label>&nbsp&nbsp&nbsp<span id="username-availability-status"></span>
                        <input type="text" class="form-control" id="username" name="username" onkeyup="checkusernameAvailability()"
                        oninput="this.value = this.value.replace(/[^0-9 a-z A-Z@_.]/g, '').replace(/(\*)\./g, '$1');" placeholder="Enter Username" required>
                        
                      </div>
                      <?php include("usernameSearch_script.php"); ?>
                      <?php include("user_emailSearch_script.php"); ?>

                      <div class="form-group">
                        <label for="password_div">Password</label>&nbsp&nbsp&nbsp<span id="password-check-status"></span>
                        <input class="form-control" type="password" id="password" name="password" onkeyup="validate()"
                        pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@_.]).{8,}" oninput="this.value = this.value.replace(/[^0-9 a-z A-Z@_.]/g, '').replace(/(\*)\./g, '$1');" maxlength="20" 
                        title="Must contain at least one number, one uppercase and lowercase letter, one special character such as +, @, _ , and at least 8 or more characters" required>
                      </div>

                      <div class="form-group">
                        <label for="password_div">Confirm Password</label>
                        <input class="form-control" type="password" id="confirm_password" name="confirm_password" onkeyup="validate()"
                        pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@_.]).{8,}" oninput="this.value = this.value.replace(/[^0-9 a-z A-Z@_.]/g, '').replace(/(\*)\./g, '$1');" maxlength="20" 
                        title="Must contain at least one number, one uppercase and lowercase letter, one special character such as +, @, _ , and at least 8 or more characters" required>
                      </div>
                      
                      <script>
                        function validate(){
                          var a = document.getElementById("password").value;
                          var b = document.getElementById("confirm_password").value;
                          if (document.getElementById('username').style.border == '2px solid red'){
                            if (a!=b) {
                            document.getElementById('submitform').disabled = true;
                            document.getElementById('password-check-status').innerText = 'Password do not match!';
                            document.getElementById('password-check-status').style.color = 'red';
                            document.getElementById('password').style.border = '2px solid red';
                            document.getElementById('confirm_password').style.border = '2px solid red';
                            }
                            else {
                              document.getElementById('submitform').disabled = true;
                              document.getElementById('password-check-status').innerText = '';
                              document.getElementById('password').style.border = '';
                              document.getElementById('confirm_password').style.border = '';
                            }
                          }
                          else {
                            if (a!=b) {
                            document.getElementById('submitform').disabled = true;
                            document.getElementById('password-check-status').innerText = 'Password do not match!';
                            document.getElementById('password-check-status').style.color = 'red';
                            document.getElementById('password').style.border = '2px solid red';
                            document.getElementById('confirm_password').style.border = '2px solid red';
                            }
                            else {
                              document.getElementById('submitform').disabled = false;
                              document.getElementById('password-check-status').innerText = '';
                              document.getElementById('password').style.border = '';
                              document.getElementById('confirm_password').style.border = '';
                            }
                          }
                        }
                      </script>
                    </div>
                  </div>
                </div>

                <div class="card-footer">
                  <div class="project-actions text-right">
                    <button type="submit" id="submitform" style="width:auto; font-size:large;" class="btn btn-default btn-sm color-cyan-dark">
                      <i class="fa fa-paper-plane">
                      </i>&nbsp; Submit
                    </button>
                  </div>
                </div>
              </form>
              <!--End of Submit form for Adding User Account-->
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <footer class="main-footer">
    <?php include('footer.php'); include('custom_btn.php'); ?>
  </footer>
  <aside class="control-sidebar control-sidebar-dark">
  </aside>
</div>
<script src="../../plugins/jquery/jquery.min.js"></script>
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="../../dist/js/adminlte.min.js"></script>
<script src="../../dist/js/demo.js"></script>
<script>
$(function () {
  bsCustomFileInput.init();
});
</script>
</body>
</html>
<style>
  .content {
    -webkit-animation-duration: 0.7s;
    animation-duration: 0.7s;
  }
  @-webkit-keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  @keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  .content {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
  }
  input:hover, select:hover, textarea:hover {
    border: 1px solid #67a2b2;
  }
</style>
<script language="javascript">
  var message="This function is not allowed here.";
  function clickIE4(){
        if (event.button==2){
            alert(message);
            return false;
        }
  }
  function clickNS4(e){
      if (document.layers||document.getElementById&&!document.all){
              if (e.which==2||e.which==3){
                        alert(message);
                        return false;
              }
      }
  }
  if (document.layers){
        document.captureEvents(Event.MOUSEDOWN);
        document.onmousedown=clickNS4;
  }
  else if (document.all&&!document.getElementById){
        document.onmousedown=clickIE4;
  }
  document.oncontextmenu=new Function("alert(message);return false;")
</script>
