<style>
    .invContainer {
    position: relative;
    }

    .invItem {
    opacity: 1;
    display: block;
    width: 100%;
    height: auto;
    transition: .5s ease;
    backface-visibility: hidden;
    }

    .invMiddle {
    transition: .5s ease;
    opacity: 0;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    text-align: center;
    }

    .invContainer:hover .invItem {
    opacity: 0.3;
    }

    .invContainer:hover .invMiddle {
    opacity: 1;
    }

    .invText {
    background-color: #67a2b2;
    color: white;
    font-size: 16px;
    padding: 16px 20px;
    border-radius: 10px;
    cursor: default;
    }
</style>