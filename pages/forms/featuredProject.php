
<?php
  include("../../../../dbcon.php");
  try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $featuredselect = "SELECT * FROM featured_project";
    $sthfeaturedselect = $dbh->prepare($featuredselect);
    $sthfeaturedselect->execute();
    $sthfeaturedselect->setFetchMode(PDO::FETCH_ASSOC); 
    if($sthfeaturedselect->rowCount() > 0){
      while ($featuredrow = $sthfeaturedselect->fetch(PDO::FETCH_ASSOC)) {
        $featured_title = $featuredrow['featured_title'];
        $featured_desc = $featuredrow['featured_desc'];
        $featured_image1 = $featuredrow['featured_image1'];
        $featured_image2 = $featuredrow['featured_image2'];
        $featured_image3 = $featuredrow['featured_image3'];
        $featured_image4 = $featuredrow['featured_image4'];
      } 
      $dbh = null;
    }
    if($sthfeaturedselect->rowCount() == 0){
      $featured_title = "";
      $featured_desc = "";
      $featured_image1 = "";
      $featured_image2 = "";
      $featured_image3 = "";
      $featured_image4 = "";
      $dbh = null;
    }
  }
  catch(PDOException $e){
    error_log('PDOException - ' . $e->getMessage(), 0);
    http_response_code(500);
    die('Error establishing connection with database');
  }
?>             
  <div class="card-body">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-primary" style="box-shadow: none; border: 1px solid #67a2b2;"> 
          <!--Submit Form for editing the content of featured project on home page-->
          <form method="POST" action="featuredProject_update.php" enctype="multipart/form-data">
            <div class="card-body">
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                    <label for="featured_title">Title</label>
                    <input type="text" class="form-control" id="featured_title" name="featured_title"
                    oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $featured_title ?>">
                  </div>
                  <div class="form-group">
                    <label for="featured_desc">Description</label>
                    <textarea class="form-control" id="featured_desc" name="featured_desc" style="resize: none; padding: 10px; height: 345px;"
                    oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" rows="14"value="<?php echo $featured_desc ?>"><?php echo $featured_desc ?></textarea>
                  </div>
                </div>

                <div class="col-sm-8">
                  <div class="row" style="margin-top: 32px;">
                    <div class="col-md-6">
                      <div class="col-md-12">
                        <div class="form-group" id="uploadDiv3">
                          <input type="text" class="form-control" id="featuredImage1text" name="featuredImage1text" value="<?php echo $featured_image1 ?>"hidden>
                          <input type="file" id="featuredImage1" name="featuredImage1" title="Click in this area to upload an image." onchange="featuredPreview1()">
                          <p id="featuredText1"></p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="col-md-12">
                        <div class="form-group" id="uploadDiv3">
                          <input type="text" class="form-control" id="featuredImage2text" name="featuredImage2text" value="<?php echo $featured_image2 ?>"hidden>
                          <input type="file" id="featuredImage2" name="featuredImage2" title="Click in this area to upload an image." onchange="featuredPreview2()">
                          <p id="featuredText2"></p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row" style="margin-top: 15px;">
                    <div class="col-md-6">
                      <div class="col-md-12">
                        <div class="form-group" id="uploadDiv3">
                          <input type="text" class="form-control" id="featuredImage3text" name="featuredImage3text" value="<?php echo $featured_image3 ?>"hidden>
                          <input type="file" id="featuredImage3" name="featuredImage3" title="Click in this area to upload an image." onchange="featuredPreview3()">
                          <p id="featuredText3"></p>
                        </div>
                      </div>
                    </div>
                    
                    <div class="col-md-6">
                      <div class="col-md-12">
                        <div class="form-group" id="uploadDiv3">
                          <input type="text" class="form-control" id="featuredImage4text" name="featuredImage4text" value="<?php echo $featured_image4 ?>"hidden>
                          <input type="file" id="featuredImage4" name="featuredImage4" title="Click in this area to upload an image." onchange="featuredPreview4()">
                          <p id="featuredText4"></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-footer">
              <div class="project-actions text-right">
                <button style="width:auto; font-size:large;" type="submit" class="btn btn-default color-cyan-dark"><i class="fa fa-paper-plane"></i>&nbsp; Submit</button>
              </div>
            </div>
          </form>
          <!--End of Submit Form for editing the content of featured project on home page-->
        </div>
      </div>
    </div>
  </div>
<style>
  #uploadDiv3 {
    width: 100%;
    height: 200px;
    border: 2px dashed #67a2b2;
    border-radius: 5px;
  }
  #featuredImage1, #featuredImage2, #featuredImage3, #featuredImage4 {
    position: absolute;
    margin: 0;
    padding: 0;
    width: 96%;
    height: 100%;
    outline: none;
    opacity: 0;
  }
  #featuredImage1, #featuredImage2, #featuredImage3, #featuredImage4 {
    cursor: pointer;
  }
  #featuredText1, #featuredText2, #featuredText3, #featuredText4 {
    text-align: center;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  #featuredText1 {
    padding: 5px;
    background-image: url(uploads/featuredProject/<?php echo $featured_image1 ?>);
    background-position: center;
    background-repeat: no-repeat;
    background-size: auto 100%;
  }
  #featuredText2 {
    padding: 5px;
    background-image: url(uploads/featuredProject/<?php echo $featured_image2 ?>);
    background-position: center;
    background-repeat: no-repeat;
    background-size: auto 100%;
  }
  #featuredText3 {
    padding: 5px;
    background-image: url(uploads/featuredProject/<?php echo $featured_image3 ?>);
    background-position: center;
    background-repeat: no-repeat;
    background-size: auto 100%;
  }
  #featuredText4 {
    padding: 5px;
    background-image: url(uploads/featuredProject/<?php echo $featured_image4 ?>);
    background-position: center;
    background-repeat: no-repeat;
    background-size: auto 100%;
  }
  .content {
    -webkit-animation-duration: 0.7s;
    animation-duration: 0.7s;
  }
  @-webkit-keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  @keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  .content {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
  }
  input:hover, select:hover, textarea:hover {
    border: 1px solid #67a2b2;
  }
</style>
<script>
$(function () {
  bsCustomFileInput.init();
});
function featuredPreview1() {
  var urlimage = URL.createObjectURL(event.target.files[0]);
  document.getElementById("featuredText1").style.backgroundImage = "url("+urlimage+")";
  document.getElementById("featuredText1").style.backgroundSize = "auto 100%";
  document.getElementById("featuredText1").style.backgroundRepeat = "no-repeat";
  document.getElementById("featuredText1").style.backgroundPosition = "center";
  document.getElementById("featuredText1").innerHTML = "";
}
function featuredPreview2() {
  var urlimage = URL.createObjectURL(event.target.files[0]);
  document.getElementById("featuredText2").style.backgroundImage = "url("+urlimage+")";
  document.getElementById("featuredText2").style.backgroundSize = "auto 100%";
  document.getElementById("featuredText2").style.backgroundRepeat = "no-repeat";
  document.getElementById("featuredText2").style.backgroundPosition = "center";
  document.getElementById("featuredText2").innerHTML = "";
}
function featuredPreview3() {
  var urlimage = URL.createObjectURL(event.target.files[0]);
  document.getElementById("featuredText3").style.backgroundImage = "url("+urlimage+")";
  document.getElementById("featuredText3").style.backgroundSize = "auto 100%";
  document.getElementById("featuredText3").style.backgroundRepeat = "no-repeat";
  document.getElementById("featuredText3").style.backgroundPosition = "center";
  document.getElementById("featuredText3").innerHTML = "";
}
function featuredPreview4() {
  var urlimage = URL.createObjectURL(event.target.files[0]);
  document.getElementById("featuredText4").style.backgroundImage = "url("+urlimage+")";
  document.getElementById("featuredText4").style.backgroundSize = "auto 100%";
  document.getElementById("featuredText4").style.backgroundRepeat = "no-repeat";
  document.getElementById("featuredText4").style.backgroundPosition = "center";
  document.getElementById("featuredText4").innerHTML = "";
}
</script>