<?php
  include("../../../../dbcon.php");
  date_default_timezone_set("Asia/Hong_Kong");
  $time = date("h:i a");
  $dates = date('F d, Y', strtotime(date("Y-m-d")));
  $datetime = $dates." ".$time;
  $payment = $_POST["payment"]; 
  $type = $_POST["type"]; 
  $employee = $_POST["employee"]; 
  $project = $_POST["project"];
  $lname = $_POST["lname"];
  $fname = $_POST["fname"];
  $mname = $_POST["mname"];
  $msurname = $_POST["msurname"];
  $address = $_POST["address"];
  $email = $_POST["email"];
  $dbirth = $_POST["dbirth"];
  $dplace = $_POST["dplace"];
  $status = $_POST["status"];
  $sex = $_POST["sex"];
  $citizen = $_POST["citizen"];
  $contact = $_POST["contact"];
  $office = $_POST["office"];
  $tin = $_POST["tin"];
  $pagibig = $_POST["pagibig"];
  $s_lname = $_POST["s_lname"];
  $s_fname = $_POST["s_fname"];
  $s_mname = $_POST["s_mname"];
  $s_msurname = $_POST["s_msurname"];
  $s_dbirth = $_POST["s_dbirth"];
  $s_dplace = $_POST["s_dplace"];
  $nature = $_POST["nature"];
  $agency = $_POST["agency"];
  $a_address = $_POST["a_address"];
  $famIncome = $_POST["famIncome"];
  $noViolation = $_POST["noViolation"];
  $altViolation = $_POST["altViolation"];
  $SG = $_POST["income"];
  $date = date("Y-m-d");
  $APP_ID = $_POST["APP_ID"];
  $username = $_POST["username"];
  $app_status = $_POST["app_status"];
  $app_date = $_POST["app_date"];
  $s_id = $_POST["spouse_id"];
  $a_id = $_POST["applicant_id"];
  $e_id = $_POST["employment_id"];
  $f_id = $_POST["dependent_id"];
  $s_final = "$s_id";
  $a_final = "$a_id";
  $e_final = "$e_id";
  $f_final = "$f_id";
if (is_numeric($APP_ID) == true && is_numeric($s_id) == true && is_numeric($a_id) == true && is_numeric($e_id) == true && is_numeric($f_id) == true){
  try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql_applicant_infodata = [
      ':lname' => $lname,
      ':fname' => $fname,
      ':mname' => $mname,
      ':msurname' => $msurname,
      ':address' => $address,
      ':email' => $email,
      ':dbirth' => $dbirth,
      ':dplace' => $dplace,
      ':status' => $status,
      ':sex' => $sex,
      ':citizen' => $citizen,
      ':contact' => $contact,
      ':office' => $office,
      ':tin' => $tin,
      ':pagibig' => $pagibig,
      ':famIncome' => $famIncome,
      ':noViolation' => $noViolation,
      ':altViolation' => $altViolation,
      ':a_final' => $a_final
    ];
    $sql_applicant_info = "UPDATE ofw_applicant_info SET LNAME=:lname, FNAME=:fname, MNAME=:mname, motherSurname=:msurname, ADDR1=:address, 
    email=:email, BDATE=:dbirth, birthplace=:dplace, CSTATUS=:status, sex=:sex, citizenship=:citizen, contact=:contact, office=:office, 
    tin=:tin, pagibig=:pagibig, fam_income=:famIncome, non_violation=:noViolation, alt_violation=':altViolation WHERE applicant_id=:a_final";
    $sthsql_applicant_info = $dbh->prepare($sql_applicant_info);
    $sql_applicant_spousedata = [
      ':s_lname' => $s_lname,
      ':s_fname' => $s_fname,
      ':s_mname' => $s_mname,
      ':s_msurname' => $s_msurname,
      ':s_dbirth' => $s_dbirth,
      ':s_dplace' => $s_dplace,
      ':s_final' => $s_final
    ];
    $sql_applicant_spouse = "UPDATE ofw_applicant_spouse SET SLNAME=:s_lname, SFNAME=:s_fname, SMNAME=:s_mname, SmotherSurname=:s_msurname, 
    SBDATE=:s_dbirth, Sbirthplace=:s_dplace WHERE spouse_id=:s_final";
    $sthsql_applicant_spouse = $dbh->prepare($sql_applicant_spouse);

    $sql_employment_statusdata = [
      ':nature' => $nature,
      ':agency' => $agency,
      ':a_address' => $a_address,
      ':SG' => $SG,
      ':e_final' => $e_final
    ];
    $sql_employment_status = "UPDATE ofw_employment_status SET nature=:nature, EMPLOYER=:agency, emp_address=:a_address, MINCOME=:SG WHERE employment_id=:e_final";
    $sthsql_employment_status = $dbh->prepare($sql_employment_status);
    $sql_applicationsdata = [
      ':payment' => $payment,
      ':type' => $type,
      ':employee' => $employee,
      ':project' => $project,
      ':username' => $usernam,
      ':APP_ID' => $APP_ID
    ];
    $sql_applications = "UPDATE ofw_applications SET payment=:payment, house_type=:type, employee=:employee, PJ_CODE=:project, username=:username WHERE APP_ID=:APP_ID";
    $sthsql_applications = $dbh->prepare($sql_applications);
    if ($sthsql_applicant_info->execute($sql_applicant_infodata) && $sthsql_applicant_spouse->execute($sql_applicant_spousedata) && $sthsql_employment_status->execute($sql_employment_statusdata) && $sthsql_applications->execute($sql_applicationsdata)) {
          $sql_delete_dependent = "DELETE FROM ofw_family WHERE dependent_id=:f_final";
          $sthsql_delete_dependent = $dbh->prepare($sql_delete_dependent);
          $sthsql_delete_dependent->bindParam(':f_final', $f_final);
          $sthsql_delete_dependent->execute();
          if(isset($_POST['dlname'])){
            for($i=0;$i<count($_POST['dlname']);$i++){
              $DLNAME = $_POST['dlname'][$i];
              $DFNAME = $_POST['dfname'][$i];
              $DMNAME = $_POST['dmname'][$i];
              $relation = $_POST['relation'][$i];
              $DCSTATUS = $_POST['dcstatus'][$i];
              $age = $_POST['age'][$i];
              $source_income = $_POST['source_income'][$i];
              $sql_familydata = [
                      ':f_final' => $f_final,
                      ':DLNAME' => $DLNAME,
                      ':DFNAME' => $DFNAME,
                      ':DMNAME' => $DMNAME,
                      ':relation' => $relation,
                      ':DCSTATUS' => $DCSTATUS,
                      ':age' => $age,
                      ':source_income' => $source_income
              ];
              if($DLNAME!="" && $DFNAME!="" && $DMNAME!="" && $relation!="" && $DCSTATUS!="" && $age!=""){
                $sql_family = "INSERT INTO ofw_family (dependent_id, DLNAME, DFNAME, DMNAME, relation, DCSTATUS, age, source_income)
                VALUES
                (:f_final, :DLNAME, :DFNAME, :DMNAME, :relation, :DCSTATUS, :age, :source_income)";
                $sthsql_family = $dbh->prepare($sql_family);
                $sthsql_family->execute($sql_familydata);
              }
              else {
              }
            }
          }
          if(!isset($_POST['dlname'])){

          }
          $auditdata = [
            ':activity' => "Edited an Application (OFW_Website/Backend, APP_ID = $APP_ID)",
            ':username' => $_SESSION['login_user'],
            ':datetime' => $datetime
          ];
          $audit = "INSERT INTO audit_trail_applications (activity, username, date) VALUES (:activity, :username, :datetime)";
          $sthaudit = $dbh->prepare($audit);
          $sthaudit->execute($auditdata);

          $countAT = "SELECT COUNT(*) as CountTodel FROM audit_trail_applications";
          $sthcountAT = $dbh->prepare($countAT);
          $sthcountAT->execute();
          $sthcountAT->setFetchMode(PDO::FETCH_ASSOC);
          while ($rowCountAT = $sthcountAT->fetch(PDO::FETCH_ASSOC))  {   
                  $CountATres = $rowCountAT['CountTodel']; 
          }   
          if($CountATres > 1000){
                  $delList = $CountATres - 1000;
                  $reduceAT = "DELETE FROM audit_trail_applications ORDER BY id ASC LIMIT ".$delList."";
                  $sthreduceAT = $dbh->prepare($reduceAT);
                  $sthreduceAT->execute();
          }
          if($CountATres <= 1000){

          }

          $_SESSION['status'] = 'Application was submitted successfully.';
          header('Location: ../forms/registration_OFW.php');
          $dbh = null;
    }
    else {
      $_SESSION['error'] = 'Sorry, something went wrong and application was not submitted.';
      header('Location: ../forms/registration_OFW.php');
      $dbh = null;
    }
  }
  catch(PDOException $e){
    error_log('PDOException - ' . $e->getMessage(), 0);
    http_response_code(500);
    die('Error establishing connection with database');
  }
} 
else{
http_response_code(400);
die('Error processing bad or malformed request');
}
?>
