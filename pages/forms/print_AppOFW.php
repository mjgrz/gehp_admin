<?php
$app_id = $_GET["id"];
include("login_session.php");
include("../../../../dbcon.php");
if ( is_numeric($app_id) == true){
  try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $select = "SELECT * FROM ofw_applications 
                    LEFT JOIN ofw_applicant_info ON ofw_applications.applicant_id = ofw_applicant_info.applicant_id 
                    LEFT JOIN ofw_applicant_spouse ON ofw_applications.spouse_id = ofw_applicant_spouse.spouse_id 
                    LEFT JOIN ofw_employment_status ON ofw_applications.employment_id = ofw_employment_status.employment_id 
                    LEFT JOIN project_info ON ofw_applications.PJ_CODE = project_info.PJ_CODE
                    WHERE APP_ID = :app_id";
        $sthselect = $dbh->prepare($select);
        $sthselect->bindParam(':app_id', $app_id);
        $sthselect->execute();
        $sthselect->setFetchMode(PDO::FETCH_ASSOC); 
        if($sthselect->rowCount() > 0){
            while ($row = $sthselect->fetch(PDO::FETCH_ASSOC)) {
                $payment = $row["payment"];
                $type = $row["house_type"];
                $employee = $row["employee"];
                $PJ_CODE = $row["PJ_CODE"];
                $PJ_NAME = $row["PJ_NAME"];
                $LNAME = $row["LNAME"];
                $FNAME = $row["FNAME"];
                $MNAME = $row["MNAME"];
                $motherSurname = $row["motherSurname"];
                $ADDR1 = $row["ADDR1"];
                $email = $row["email"];
                $BDATE = date('m-d-Y', strtotime($row["BDATE"]));
                $birthplace = $row["birthplace"];
                $CSTATUS = ucfirst($row["CSTATUS"]);
                $citizenship = ucfirst($row["citizenship"]);
                $contact = $row["contact"];
                $office = $row["office"];
                $tin = $row["tin"];
                $pagibig = $row["pagibig"];
                $umid = $row["umid"];
                $SLNAME = $row["SLNAME"];
                $SFNAME = $row["SFNAME"];
                $SMNAME = $row["SMNAME"];
                $SmotherSurname = $row["SmotherSurname"];
                $SBDATE = date('m-d-Y', strtotime($row["SBDATE"]));
                $Sbirthplace = $row["Sbirthplace"];
                $nature = $row["nature"];
                $EMPLOYER = $row["EMPLOYER"];
                $MINCOME = $row["MINCOME"];
                $emp_address = $row["emp_address"];
                $APP_ID = $row["APP_ID"];
                $app_date = date('F d, Y', strtotime($row["app_date"]));
                $applicant_id = $row["applicant_id"];
                $spouse_id = $row["spouse_id"];
                $employment_id = $row["employment_id"];
                $dependent_id = $row["dependent_id"];
                $app_status = $row["app_status"];
                $pdfFile = $row["app_date"];
                $pdfFilefinal = $pdfFile."-".$LNAME."-"."OFW";
                $fam_income = $row["fam_income"];
                $non_violation = $row["non_violation"];
                $alt_violation = $row["alt_violation"];
            }
            $dbh = null;
        }  
    }
    catch(PDOException $e){
      error_log('PDOException - ' . $e->getMessage(), 0);
      http_response_code(500);
      die('Error establishing connection with database');
    }
} 
else{
http_response_code(400);
die('Error processing bad or malformed request');
}
  
  
require_once('TCPDF-main/tcpdf.php');

$pdf = new TCPDF('p', 'mm', 'A4', true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('GEHP');
$pdf->SetTitle('NHA | Government Employees Housing Program');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

$pdf->SetFont('dejavusans', '', 14, '', true);

$pdf->AddPage();

    $imageFile = K_PATH_IMAGES. 'header.png';
    $pdf->Image($imageFile, 10, 5, 189, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);

    $pdf->SetY(-272);
    $pdf->Ln(5);
    $pdf->SetFont('helvetica', '', 10);
    $pdf->SetX(79);
    $pdf->Cell(167, 1, $app_date, 0, 1, 'C');
    $pdf->SetY(-266);
    $pdf->SetX(79);
    $pdf->Cell(167, 1, '______________________', 0, 1, 'C');
    $pdf->SetX(79);
    $pdf->Cell(167, 1, 'Date of Application', 0, 1, 'C');
    $pdf->SetFont('helvetica', 'B', 12);
    $pdf->SetY(-254);
    $pdf->SetX(10);
    $pdf->MultiCell(189, 10, 'APPLICATION TO PURCHASE A HOUSE AND LOT PACKAGE/HOUSING UNIT
    UNDER THE GOVERNMENT EMPLOYEES HOUSING PROGRAM
    ', 0, 'C', 0 , 1, '', '', true);
    $pdf->SetFont('helvetica', 'B', 10);
    $pdf->SetY(-240);
    $pdf->Cell(167, 1, 'THE REGIONAL MANAGER', 0, 1, 'L');
    $pdf->SetFont('helvetica', '', 10);
    $pdf->Cell(167, 1, 'Thru:  The District Manager', 0, 1, 'L');
    $pdf->SetY(-228);
    $pdf->Cell(167, 1, 'Sir:/Madame:', 0, 1, 'L');
    $pdf->SetY(-222);
    $pdf->MultiCell(180, 10, '     In accordance with NHA rules and regulations which I and my family agree to comply with faithfully, I hereby apply: (please check)
    ', 0, 'J', 0 , 1, '', '', true);
    $pdf->SetFont('helvetica', 'B', 10);
    $pdf->Cell(167, 0, 'A.1.  FOR PURCHASE OF HOUSE AND LOT PACKAGE under the GEHP thru', 0, 1, 'L');
    
    $pdf->SetFont('helvetica', '', 10);
    $pdf->SetY(-203);
    $pdf->SetX(20);
    $pdf->Cell(5, 1, ' ', 1, 1, 'L');
    $pdf->SetY(-203);
    $pdf->SetX(25);
    $pdf->Cell(50, 3, 'Cash Sale', 0, 1, 'L');
    if ($payment == "Cash Sale") {
        $imageFile = K_PATH_IMAGES. 'check.png';
        $pdf->Image($imageFile, 21, 93, 5, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }
    if ($payment == "Staggered Cash") {
        $imageFile = K_PATH_IMAGES. 'check.png';
        $pdf->Image($imageFile, 49, 93, 5, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }
    if ($payment == "Straight Amortization") {
        $pdf->SetFont('helvetica', 'B', 14);
        $imageFile = K_PATH_IMAGES. 'check.png';
        $pdf->Image($imageFile, 88, 99, 5, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }
    if ($payment == "Escalating Amortization") {
        $pdf->SetFont('helvetica', 'B', 14);
        $imageFile = K_PATH_IMAGES. 'check.png';
        $pdf->Image($imageFile, 131, 99, 5, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }
    if ($payment == "End-User Financing (MTO-Pag-IBIG)") {
        $pdf->SetFont('helvetica', 'B', 14);
        $imageFile = K_PATH_IMAGES. 'check.png';
        $pdf->Image($imageFile, 116, 93, 5, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }
    
    $pdf->SetFont('helvetica', '', 10);
    $pdf->SetY(-203);
    $pdf->SetX(60);
    $pdf->Cell(5, 1, ' ', 1, 1, 'L');
    $pdf->SetY(-203);
    $pdf->SetX(65);
    $pdf->Cell(50, 3, 'Staggered Cash', 0, 1, 'L');

    $pdf->SetFont('helvetica', '', 10);
    $pdf->SetY(-203);
    $pdf->SetX(115);
    $pdf->Cell(5, 1, ' ', 1, 1, 'L');
    $pdf->SetY(-203);
    $pdf->SetX(120);
    $pdf->Cell(50, 3, 'End-User Financing (MTO-Pag-IBIG)', 0, 1, 'L');

    $pdf->SetFont('helvetica', '', 10);
    $pdf->SetY(-197);
    $pdf->Cell(5, 1, 'In-House Financing (Installment Payment): ', 0, 1, 'L');

    $pdf->SetY(-197);
    $pdf->SetX(87);
    $pdf->Cell(5, 1, ' ', 1, 1, 'L');
    $pdf->SetY(-197);
    $pdf->SetX(92);
    $pdf->Cell(50, 3, 'Straight Amortization', 0, 1, 'L');

    $pdf->SetY(-197);
    $pdf->SetX(130);
    $pdf->Cell(5, 1, ' ', 1, 1, 'L');
    $pdf->SetY(-197);
    $pdf->SetX(135);
    $pdf->Cell(50, 3, 'Escalating Amortization', 0, 1, 'L');

    $pdf->SetFont('helvetica', 'B', 10);
    $pdf->Cell(167, 10, 'PROJECT NAME (see attached Annex for list): ___________________________________________________', 0, 1, 'L');
    $pdf->Cell(167, 1, 'A.2.  TYPE OF HOUSING', 0, 1, 'L');
    $pdf->SetY(-190);
    $pdf->SetX(95);
    $pdf->Cell(98, 1, $PJ_NAME, 0, 1, 'C');

    $pdf->SetFont('helvetica', '', 10);
    $pdf->SetY(-174);
    $pdf->SetX(20);
    $pdf->Cell(5, 1, ' ', 1, 1, 'L');
    $pdf->SetY(-174);
    $pdf->SetX(25);
    $pdf->Cell(50, 3, '2-Storey Duplex', 0, 1, 'L');

    if($type == "2-Storey Duplex"){
        $imageFile = K_PATH_IMAGES. 'check.png';
        $pdf->Image($imageFile, 21, 122, 5, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }

    if($type == "1-Storey Duplex"){
        $imageFile = K_PATH_IMAGES. 'check.png';
        $pdf->Image($imageFile, 59, 122, 5, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }

    if($type == "Low-Rise Building/Condominium"){
        $imageFile = K_PATH_IMAGES. 'check.png';
        $pdf->Image($imageFile, 97, 122, 5, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }

    $pdf->SetY(-174);
    $pdf->SetX(58);
    $pdf->Cell(5, 1, ' ', 1, 1, 'L');
    $pdf->SetY(-174);
    $pdf->SetX(63);
    $pdf->Cell(50, 3, '1-Storey Duplex', 0, 1, 'L');

    $pdf->SetY(-174);
    $pdf->SetX(96);
    $pdf->Cell(5, 1, ' ', 1, 1, 'L');
    $pdf->SetY(-174);
    $pdf->SetX(101);
    $pdf->Cell(50, 3, 'Low-Rise Building/Condominium', 0, 1, 'L');

    $pdf->SetFont('helvetica', 'B', 10);
    $pdf->Cell(167, 10, 'A.3. As Overseas Filipino Worker in __________________________________________ (please specify)', 0, 1, 'L');

    $pdf->SetY(-167);
    $pdf->SetX(78);
    $pdf->Cell(79, 1, $employee, 0, 1, 'C'); 

    $pdf->Ln(5);
    $pdf->Cell(167, 1, "I. APPLICANT'S IDENTITY: (For female applicant/spouse, give complete maiden name)", 0, 1, 'L');

    $pdf->SetFont('helvetica', '', 10);
    $pdf->Cell(167, 10, 'NAME: _____________________________________________________________________________________', 0, 1, 'L');
    $pdf->SetY(-146);
    $pdf->SetX(42);
    $pdf->Cell(50, 3, '(Last)', 0, 1, 'L');
    $pdf->SetY(-151);
    $pdf->SetX(27);
    $pdf->Cell(41, 5, $LNAME, 0, 1, 'C');

    $pdf->SetY(-146);
    $pdf->SetX(83);
    $pdf->Cell(50, 3, '(First)', 0, 1, 'L');
    $pdf->SetY(-151);
    $pdf->SetX(68);
    $pdf->Cell(41, 5, $FNAME, 0, 1, 'C');

    $pdf->SetY(-146);
    $pdf->SetX(122);
    $pdf->Cell(50, 3, '(Middle)', 0, 1, 'L');
    $pdf->SetY(-151);
    $pdf->SetX(109);
    $pdf->Cell(41, 5, $MNAME, 0, 1, 'C');

    $pdf->SetY(-146);
    $pdf->SetX(155);
    $pdf->Cell(50, 3, "(Mother's Surname)", 0, 1, 'L');
    $pdf->SetY(-151);
    $pdf->SetX(151);
    $pdf->Cell(40, 5, $MNAME, 0, 1, 'C');
    
    $pdf->Ln(8);
    $pdf->Cell(167, 5, 'Residence/Address: ___________________________________________________________________________', 0, 1, 'L');
    $pdf->SetY(-138);
    $pdf->SetX(49);
    $pdf->Cell(144, 5, $ADDR1, 0, 1, 'C');

    $pdf->Cell(167, 5, 'Date of Birth:_____________ Place of Birth ____________________________________ Civil Status___________', 0, 1, 'L');
    $pdf->SetY(-133);
    $pdf->SetX(37);
    $pdf->Cell(25, 5, $BDATE, 0, 1, 'C');
    $pdf->SetY(-133);
    $pdf->SetX(86);
    $pdf->Cell(68, 5, $birthplace, 0, 1, 'C');
    $pdf->SetY(-133);
    $pdf->SetX(175);
    $pdf->Cell(20, 5, $CSTATUS, 0, 1, 'C');
    
    $pdf->Cell(167, 5, 'Citizenship: __________ Contact Numbers (Residence) _____________________ Office ____________________', 0, 1, 'L');
    $pdf->SetY(-128);
    $pdf->SetX(35);
    $pdf->Cell(20, 5, $citizenship, 0, 1, 'C');
    $pdf->SetY(-128);
    $pdf->SetX(105);
    $pdf->Cell(40, 5, $contact, 0, 1, 'C');
    $pdf->SetY(-128);
    $pdf->SetX(156);
    $pdf->Cell(40, 5, $office, 0, 1, 'C');

    $pdf->Cell(167, 5, 'TIN No. _______________________________ GSIS/SSS/Pag-IBIG Policy No. ____________________________', 0, 1, 'L');
    $pdf->SetY(-123);
    $pdf->SetX(35);
    $pdf->Cell(50, 5, $tin, 0, 1, 'C');
    $pdf->SetY(-123);
    $pdf->SetX(142);
    $pdf->Cell(50, 5, $pagibig, 0, 1, 'C');

    $pdf->Cell(167, 5, 'Name of Spouse: _____________________________________________________________________________', 0, 1, 'L');
    $pdf->SetY(-114);
    $pdf->SetX(57);
    $pdf->Cell(50, 5, '(Last)', 0, 1, 'L');
    $pdf->SetY(-118);
    $pdf->SetX(44);
    $pdf->Cell(37, 5, $SLNAME, 0, 1, 'C');

    $pdf->SetY(-114);
    $pdf->SetX(94);
    $pdf->Cell(50, 5, '(First)', 0, 1, 'L');
    $pdf->SetY(-118);
    $pdf->SetX(81);
    $pdf->Cell(37, 5, $SFNAME, 0, 1, 'C');

    $pdf->SetY(-114);
    $pdf->SetX(131);
    $pdf->Cell(50, 5, '(Middle)', 0, 1, 'L');
    $pdf->SetY(-118);
    $pdf->SetX(120);
    $pdf->Cell(37, 5, $SMNAME, 0, 1, 'C');

    $pdf->SetY(-114);
    $pdf->SetX(160);
    $pdf->Cell(50, 5, "(Mother's Surname)", 0, 1, 'L');
    $pdf->SetY(-118);
    $pdf->SetX(157);
    $pdf->Cell(37, 5, $SmotherSurname, 0, 1, 'C');


    $pdf->Ln(4);
    $pdf->Cell(167, 5, 'Date of Birth__________________________________ Place of Birth____________________________________', 0, 1, 'L');
    $pdf->SetY(-109);
    $pdf->SetX(38);
    $pdf->Cell(60, 5, $SBDATE, 0, 1, 'C');
    $pdf->SetY(-109);
    $pdf->SetX(127);
    $pdf->Cell(60, 5, $Sbirthplace, 0, 1, 'C');


    $pdf->SetFont('helvetica', 'B', 10);
    $pdf->Cell(167, 10, 'II. APPLICANT’S EMPLOYMENTS STATUS', 0, 1, 'L');

    $pdf->SetFont('helvetica', '', 10);
    $pdf->Cell(167, 3, 'Nature of Employment: ______________________________', 0, 1, 'L');
    $pdf->Cell(167, 3, 'Name of Agency/ Corporation: ________________________________________________', 0, 1, 'L');
    $pdf->Cell(167, 3, 'Address: ________________________________________________', 0, 1, 'L');
    $pdf->Cell(167, 3, 'Income: _______________________', 0, 1, 'L');
    $pdf->SetY(-94.5);
    $pdf->SetX(54);
    $pdf->Cell(58, 5, $nature, 0, 1, 'C');
    $pdf->SetY(-90);
    $pdf->SetX(64);
    $pdf->Cell(95, 5, $EMPLOYER, 0, 1, 'C');
    $pdf->SetY(-85.5);
    $pdf->SetX(31);
    $pdf->Cell(94, 5, $emp_address, 0, 1, 'C');
    $pdf->SetY(-81);
    $pdf->SetX(30);
    $pdf->Cell(45, 5, $MINCOME, 0, 1, 'C');

    $pdf->AddPage();
    $pdf->SetY(-287);
    $pdf->SetFont('helvetica', 'B', 10);
    $pdf->Cell(167, 10, "III. APPLICANT'S FAMILY COMPOSITION (add additional page if necessary) :", 0, 1, 'L');

    $pdf->SetFont('helvetica', '', 10);
    $pdf->Cell(176, 3, '                   NAME    	                       RELATION TO           CIVIL STATUS         AGE 	    EMPLOYMENT/SOURCE', 0, 1, 'L');
    $pdf->Cell(176, 3, '                                                           APPLICANT                                                                      OF INCOME', 0, 1, 'L');
    include("dbcon.php");
    $query = "SELECT * FROM ofw_family WHERE dependent_id = '$dependent_id'";
    $select = mysqli_query($conn, $query);
    $count = mysqli_num_rows($select);
    $array = array();
    while ($row = mysqli_fetch_assoc($select)) {
        $array[] = $row;
    }
    
    if($count > 0) {
        for($i = 0; $i < $count; $i++){
            $j = $i*5;
            $k = -269 + $j;

            $pdf->Cell(176, 5, '_________________________   ________________    _______________   _______   _____________________', 0, 1, 'L');
            $pdf->SetY($k);
            $pdf->SetX(16);
            $pdf->Cell(49, 5, $array[$i]['DFNAME']. " " . $array[$i]['DLNAME'], 0, 1, 'C');
            $pdf->SetY($k);
            $pdf->SetX(68);
            $pdf->Cell(31, 5, $array[$i]['relation'], 0, 1, 'C');
            $pdf->SetY($k);
            $pdf->SetX(103);
            $pdf->Cell(30, 5, $array[$i]['DCSTATUS'], 0, 1, 'C');
            $pdf->SetY($k);
            $pdf->SetX(135.5);
            $pdf->Cell(14, 5, $array[$i]['age'], 0, 1, 'C');
            $pdf->SetY($k);
            $pdf->SetX(152.5);
            $pdf->Cell(41, 5, $array[$i]['source_income'], 0, 1, 'C');
        }
    }
    else
    {
        $pdf->Cell(176, 5, '_________________________   ________________    _______________   _______   _____________________', 0, 1, 'L');
        $pdf->SetY(-269);
        $pdf->SetX(16);
        $pdf->Cell(49, 5, 'N/A', 0, 1, 'C');
        $pdf->SetY(-269);
        $pdf->SetX(68);
        $pdf->Cell(31, 5, 'N/A', 0, 1, 'C');
        $pdf->SetY(-269);
        $pdf->SetX(103);
        $pdf->Cell(30, 5, 'N/A', 0, 1, 'C');
        $pdf->SetY(-269);
        $pdf->SetX(135.5);
        $pdf->Cell(14, 5, 'N/A', 0, 1, 'C');
        $pdf->SetY(-269);
        $pdf->SetX(152.5);
        $pdf->Cell(41, 5, 'N/A', 0, 1, 'C');
    }

    $pdf->SetFont('helvetica', 'B', 10);
    if($fam_income == ""){
        $pdf->Cell(167, 10, "IV. APPLICANT'S TOTAL FAMILY INCOME PER MONTH: (P _________________ )", 0, 1, 'L');
    }
    if($fam_income != ""){
        $pdf->Cell(167, 10, "IV. APPLICANT'S TOTAL FAMILY INCOME PER MONTH: (P ". $fam_income . " )", 0, 1, 'L');
    }
    $pdf->Cell(167, 9, "V. FAMILY REAL PROPERTY HOLDINGS: (please underline)", 0, 1, 'L');
    
    $pdf->SetFont('helvetica', '', 10);
    if($non_violation == "YES"){
        $value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Have never availed of any form of government housing assistance (<u>YES</u> / NO)';
    }
    if($non_violation == "NO"){
        $value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Have never availed of any form of government housing assistance (YES / <u>NO</u>)';
    }
    if($non_violation == ""){
        $value = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Have never availed of any form of government housing assistance (YES / NO)';
    }

    if($alt_violation == "YES"){
        $value1 = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rights thereon, except to qualified beneficiary as determined by the Government Agency (<u>YES</u> / NO)';
    }
    if($alt_violation == "NO"){
        $value1 = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rights thereon, except to qualified beneficiary as determined by the Government Agency (YES / <u>NO</u>)';
    }
    if($alt_violation == ""){
        $value1 = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rights thereon, except to qualified beneficiary as determined by the Government Agency (YES / NO)';
    }

    $pdf->writeHTML($value, true, false, true, false);

    $pdf->Ln(2);

    $pdf->writeHTML('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Have sold, alienated, conveyed, encumbered or leased the socialized housing, including imporovements
    or ', true, false, true, false);
    $pdf->writeHTML($value1, true, false, true, false);

    $pdf->SetFont('helvetica', 'B', 10);
    $pdf->Cell(176, 9, "VI. CERTIFICATIONS ", 0, 1, 'L');

    $pdf->Ln(2);
    $pdf->SetFont('helvetica', '', 10);
    $pdf->MultiCell(180, 10, '    I/We certify that the foregoing information/statement is to my/our knowledge, true, correct, complete, and updated. The signature/s appearing above my/our name/names below is/are genuine.

    I/We authorize National Housing Authority (NHA) or its duly authorized representative to verify necessary information or data (i.e. certificate of employment, pay slips and income tax return) with the concerned government agencies or third parties including banks and other financial institutions from whom NHA had obtained information.

      I/We hereby agrees that any misrepresentation of a material fact is a ground for disapproval of the application, cancellation of the loan, and shall be a cause for the total outstanding obligation to be due and demandable and shall be subject to other sanctions provided in existing NHA guidelines. I/We agree to notify NHA of any material change affecting the information contained herein. I/We agree that all information obtained by NHA) shall remain its property whether or not the loan is granted.

    I/We further agree to be bounded by the current and general policies of NHA and those that the NHA may adopt in the future, that may have relation to or in any way affect my/our loan.

    I/We understand that the processing/service/filing fee, notarial and all other incidental fees pertaining to the registration of mortgage on property shall be for my/our account.
', 0, 'J', 0 , 1, '', '', true);

    $pdf->Ln(15);
    $pdf->Cell(176, 5, "___________________________________________          ___________________________________________ ", 0, 1, 'L');
    $pdf->SetFont('helvetica', '', 9);
    $pdf->Cell(176, 5, "   SIGNATURE OVER PRINTED NAME OF BORROWER                      SIGNATURE OVER PRINTED NAME OF SPOUSE ", 0, 1, 'L');

    $pdf->Ln(5);
    $pdf->Cell(176, 5, "             __________________________________                                       __________________________________  ", 0, 1, 'L');
    $pdf->Cell(176, 5, "                                          DATE                                                                                                 DATE ", 0, 1, 'L');

    $pdf->Ln(7);
    $pdf->SetFont('helvetica', '', 10);
    $pdf->MultiCell(180, 10, '    SUBSCRIBED AND SWORN to before me in_________________________Philippines, this ______ day of _____________________ 20____, AFFIANT, having exhibited to me, his/her ID No. __________________ issued on ________________ at _________________________.
    ', 0, 'J', 0 , 1, '', '', true);

    $pdf->Ln(2);
    $pdf->Cell(176, 5, "Doc. No. __________				                                                                                         				NOTARY PUBLIC", 0, 1, 'L');
    $pdf->Cell(176, 5, "Page No. __________", 0, 1, 'L');
    $pdf->Cell(176, 5, "Book No. __________", 0, 1, 'L');
    $pdf->Cell(176, 5, "Series of 20", 0, 1, 'L');

    $pdf->Ln(9);
    $pdf->SetFont('helvetica', 'B', 10);
    $pdf->Cell(176, 1, "Note:   Please provide complete  information and  write legibly in PRINT /BLOCK LETTERS", 0, 1, 'L');

ob_end_clean();
$pdf->Output($pdfFilefinal.'.pdf', 'I');
?>