<div class="modal fade" id="delete">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 5px;">
      <div class="modal-header" style="background: #67a2b2;">
        <h4 class="modal-title" style="color: white;">Delete</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: white;">&times;</span>
        </button>
      </div>
        <div class="modal-body">
          <div class="col-sm-12">
            <div class="col-lg-12">
              <div class="form-group">
                <p style="font-size: 20px;">Are you sure you want to delete the selected record(s)?</p>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-right" style="background: #f6f6f6;">
          <button type="submit" name="deleteOFWSubBtn" class="btn btn-default color-red-dark" style="font-size:large; width:auto;"><i class="fa fa-paper-plane"></i>&nbsp; Delete</button>
        </div>
    </div>
  </div>
</div>
