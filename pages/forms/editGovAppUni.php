<?php
$app_id = $_GET["id"];
?>
<?php 
  include("login_session.php");
  error_reporting(E_ERROR | E_PARSE);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>NHA | Government Employee's Housing Program</title>
<link rel="shortcut icon" type="image/png" href="../../dist/img/nha_logo_circle.png">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
<link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
<link rel="stylesheet" href="../../dist/css/adminlte.min.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="font-weight: bold;">
    <?php include("header.php"); ?>
  </nav>

  <aside class="main-sidebar sidebar-dark-primary elevation-4" style="background: #002d32; position: fixed;">
    <a href="../../index.php" class="brand-link">
      <img src="../../dist/img/nha_logo_circle.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"><b>NHA | GEHP</b></span>
    </a>

    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <?php
          $target_dir = "uploads/users/";
          $target_file = $target_dir . basename("$login_image");
          if(file_exists($target_file)){
            echo "<img src='uploads/users/$login_image' class='img-circle elevation-3' alt='User Image' style='height: 33px;'>";
          } 
          else {
            echo "<img src='uploads/users/user.png' width='150px'>";
          }
          ?>
        </div>
        <div class="info">
          <a href="users_profile.php" class="d-block"><?php echo $_SESSION['login_user'] ?></a>
        </div>
      </div>

      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
          <li class="nav-item">
            <a href="../../index.php" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>

          <li class="nav-item menu-open">
            <a href="#" class="nav-link active" style="background: #67a2b2;">
              <i class="nav-icon fas fa-briefcase"></i>
              <p>
                Applicants
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <input type="text" class="form-control" id="sessionempty" name="sessionempty" value="" placeholder="Search" hidden>
              <li class="nav-item">
                <a href="../forms/registration.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Government Employees</p>
                </a>
              </li>
              <li class="nav-item" <?php echo $deploy;?>>
                <a href="../forms/registration_Uni.php" onclick="sessionempty()" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Uniformed Personnel</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/registration_OFW.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>OFW</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item" <?php echo $project_hide ?>>
            <a href="../forms/project_view.php" onclick="sessionempty()" class="nav-link">
              <i class="nav-icon fas fa-building"></i>
              <p>
                Projects
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $user_hide ?>>
            <a href="../forms/users.php" onclick="sessionempty()" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Users
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $element_hide ?>>
            <a href="../forms/report_gen.php" class="nav-link">
              <i class="nav-icon fas fa-scroll"></i>
              <p>
                Reports
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $pages_hide ?>>
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-clone"></i>
              <p>
                Pages
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              
              <li class="nav-item">
                <a href="../forms/home.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Home</p>
                </a>
              </li>
              <li class="nav-item" hidden>
                <a href="../forms/howToApply.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>How To Apply</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/about.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>About</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/banner.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Banner</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="../forms/bulletin.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Bulletin</p>
                </a>
              </li>
            </ul>
          </li>
          
          <li class="nav-item" <?php echo $inv_hide;?>>
            <a href="../forms/inventory.php" class="nav-link">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                Inventory
              </p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>
  <script>
    function sessionempty() {
      $("#loaderIcon").show();
          jQuery.ajax({
          url: "session_clear.php",
          data: {'sessionempty': $("#sessionempty").val()},
          type: "POST",
          success:function(data){
              $("#").html(data);
              $("#loaderIcon").hide();
          },
          error:function (){
          }
      });
    }
  </script>
  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Uniformed Personnel - Application</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
              <div class="card-header" style="background: #67a2b2;">
                <h3 class="card-title"></h3>
              </div>
              <!--Submit Form for editing Government Application-->
              <form method="POST" action="editGovAppUni_submit.php">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6">
                    <?php
                    include("../../../../dbcon.php");
                    if (is_numeric($app_id) == true){
                      try{
                        $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        $select = "SELECT * FROM applications 
                                          LEFT JOIN applicant_info ON applications.applicant_id = applicant_info.applicant_id 
                                          LEFT JOIN applicant_spouse ON applications.spouse_id = applicant_spouse.spouse_id 
                                          LEFT JOIN employment_status ON applications.employment_id = employment_status.employment_id 
                                          LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
                                          WHERE APP_ID = :app_id";
                        $sthselect = $dbh->prepare($select);
                        $sthselect->bindParam(':app_id', $app_id);
                        $sthselect->execute();
                        $sthselect->setFetchMode(PDO::FETCH_ASSOC); 
                        if($sthselect->rowCount() > 0){
                          while ($row = $sthselect->fetch(PDO::FETCH_ASSOC)) {
                            $payment = $row["payment"];
                            $house_type = $row["house_type"];
                            $employee = $row["employee"];
                            $PJ_CODE = $row["PJ_CODE"];
                            $LNAME = $row["LNAME"];
                            $FNAME = $row["FNAME"];
                            $MNAME = $row["MNAME"];
                            $motherSurname = $row["motherSurname"];
                            $ADDR1 = $row["ADDR1"];
                            $email = $row["email"];
                            $BDATE = $row["BDATE"];
                            $birthplace = $row["birthplace"];
                            $CSTATUS = $row["CSTATUS"];
                            $sex = $row["sex"];
                            $citizenship = $row["citizenship"];
                            $contact = $row["contact"];
                            $office = $row["office"];
                            $tin = $row["tin"];
                            $pagibig = $row["pagibig"];
                            $SLNAME = $row["SLNAME"];
                            $SFNAME = $row["SFNAME"];
                            $SMNAME = $row["SMNAME"];
                            $SmotherSurname = $row["SmotherSurname"];
                            $SBDATE = $row["SBDATE"];
                            $Sbirthplace = $row["Sbirthplace"];
                            $nature = $row["nature"];
                            $EMPLOYER = $row["EMPLOYER"];
                            $a_address = $row["emp_address"];
                            $service_branch = $row["service_branch"];
                            $uni_rank = $row["uni_rank"];
                            $serial = $row["serial_no"];
                            $POSITION = $row["POSITION"];
                            $assign_place = $row["assign_place"];
                            $mother_unit = $row["mother_unit"];
                            $fam_income = $row["fam_income"];
                            $non_violation = $row["non_violation"];
                            $alt_violation = $row["alt_violation"];
                            $SG = $row["salary_grade"];
                            $MINCOME = $row["MINCOME"];
                            $dateHired = $row["dateHired"];
                            $years = $row["years"];
                            $APP_ID = $row["APP_ID"];
                            $app_date = $row["app_date"];
                            $applicant_id = $row["applicant_id"];
                            $spouse_id = $row["spouse_id"];
                            $employment_id = $row["employment_id"];
                            $dependent_id = $row["dependent_id"];
                            $app_status = $row["app_status"];
                          }
                          $dbh = null;
                        } 
                      }
                      catch(PDOException $e){
                        error_log('PDOException - ' . $e->getMessage(), 0);
                        http_response_code(500);
                        die('Error establishing connection with database');
                      }
                    } 
                    if(!is_numeric($app_id) == true){
                    http_response_code(400);
                    die('Error processing bad or malformed request');
                    }      
                    ?>
                    <input type="text" name="APP_ID" value="<?php echo $APP_ID ?>" hidden>
                    <input type="text" name="applicant_id" value="<?php echo $applicant_id ?>" hidden>
                    <input type="text" name="spouse_id" value="<?php echo $spouse_id ?>" hidden>
                    <input type="text" name="employment_id" value="<?php echo $employment_id ?>" hidden>
                    <input type="text" name="dependent_id" value="<?php echo $dependent_id ?>" hidden>
                    <input type="text" name="app_status" value="<?php echo $app_status ?>" hidden>
                    <input type="text" name="username" value="<?php echo $_SESSION['login_user'] ?>" hidden>

                      <div class="form-group">
                        <label style="border-bottom: 2px solid #67a2b2;">DATE OF APPLICATION</label>
                        <div class="col-sm-12">
                          <input type="date" class="form-control col-sm-6" name="app_date" id="app_date" min="<?php //echo Date('Y-m-d', strtotime('-65 year')); ?>"value="<?php echo date('Y-m-d', strtotime($app_date)); ?>" max="<?php echo date('Y-m-d'); ?>">
                        </div>
                      </div>

                      <div class="form-group">
                        <label style="border-bottom: 2px solid #67a2b2;">A.1 FOR PURCHASE OF HOUSE AND LOT PACKAGE <span class="h6">under the GEHP thru</span></label>
                        <div class="col-sm-12">
                          <div class="row">
                            <div style="padding-right: 30px;">
                              <label> 
                                <input type="radio" name="payment" <?php if($payment == 'End-User Financing (MTO-Pag-IBIG)'){echo 'checked';} ?> value="End-User Financing (MTO-Pag-IBIG)" required>
                                End-User Financing (MTO-Pag-IBIG) 
                              </label>
                            </div>
                            <div style="padding-right: 50px;">
                              <label>
                                <input type="radio" name="payment" <?php if($payment == 'Cash Sale'){echo 'checked';} ?> value="Cash Sale" required>
                                Cash Sale 
                              </label>
                            </div>
                            <div style="padding-right: 50px;">
                              <label>
                                <input type="radio" name="payment" <?php if($payment == 'Staggered Cash'){echo 'checked';} ?> value="Staggered Cash" required>
                                Staggered Cash 
                              </label>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-12">
                            <div style="padding: 3px;">
                              <div class="row">
                                <label>In-House Financing (Installment Payment): &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                <div style="padding-right: 30px;">
                                  <label> 
                                    <input type="radio" name="payment" <?php if($payment == 'Straight Amortization'){echo 'checked';} ?> value="Straight Amortization" required>
                                    Straight Amortization 
                                  </label>
                                </div>
                                <div style="padding-right: 50px;">
                                  <label>
                                    <input type="radio" name="payment" <?php if($payment == 'Escalating Amortization'){echo 'checked';} ?> value="Escalating Amortization" required>
                                    Escalating Amortization
                                  </label>
                                </div>
                              </div>
                            </div>
                        </div>
                      </div>
                      <div class="form-group col-sm-12">
                        <label for="project">Project Name</label>
                          <select class="form-control" name="project" id="project" style="padding: 8px;" required>
                            <option disabled value="">Select Housing Project</option>
                            <?php
                            include("../../../../dbcon.php");
                            try{
                              $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                              $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                              $regionselect = "SELECT * FROM project_info 
                                              LEFT JOIN units_floor ON project_info.project_id = units_floor.project_id
                                              LEFT JOIN region ON project_info.region = region.psgc_reg";
                              $sthregionselect = $dbh->prepare($regionselect);
                              $sthregionselect->execute();
                              if($sthregionselect->rowCount() > 0){
                                while ($regionrow = $sthregionselect->fetch(PDO::FETCH_ASSOC)) {
                                  if($regionrow['PJ_CODE'] == $PJ_CODE){
                                    $setProj = 'selected';
                                  }
                                  else
                                  {
                                    $setProj = '';
                                  }
                                ?>
                                <option <?php echo $setProj ?> value="<?php echo $regionrow['PJ_CODE'] ?>"><?php echo $regionrow['Region'] ?> | <?php echo $regionrow['PJ_NAME'] ?> (<?php echo $regionrow['location'] ?>)</option>
                                <?php 
                                } 
                                $dbh = null;
                              }
                              else {
                                $dbh = null;
                              }
                            }
                            catch(PDOException $e){
                              error_log('PDOException - ' . $e->getMessage(), 0);
                              http_response_code(500);
                              die('Error establishing connection with database');
                            }
                            ?>
                          </select>
                      </div>

                      <div class="form-group">
                        <label style="border-bottom: 2px solid #67a2b2;">A.2 TYPE OF HOUSING <span class="h6">under the GEHP thru</span></label>
                        <div class="row">
                          <div style="padding-right: 30px;">
                            <label> 
                              <input type="radio" name="type" value="2-Storey Duplex" <?php if($house_type == '2-Storey Duplex') {echo 'checked';} ?>>
                              2-Storey Duplex 
                            </label>
                          </div>
                          <div style="padding-right: 30px;">
                            <label>
                              <input type="radio" name="type" value="1-Storey Duplex" <?php if($house_type == '1-Storey Duplex') {echo 'checked';} ?>>
                              1-Storey Duplex 
                            </label>
                          </div>
                          <div style="padding-right: 30px;">
                            <label>
                              <input type="radio" name="type" value="Low-Rise Building/Condominium" <?php if($house_type == 'Low-Rise Building/Condominium') {echo 'checked';} ?>>
                              Low-Rise Building/Condominium 
                            </label>
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label style="border-bottom: 2px solid #67a2b2;">A.3 As Government Employee of </label>
                        <div class="form-group col-sm-12">
                          <input type="text" class="form-control" id="employee" name="employee" value="<?php echo $employee ?>"
                          oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Please specify" required>
                        </div>
                      </div>
                      <br>
                      <label style="border-bottom: 2px solid #67a2b2;">I. APPLICANT'S IDENTITY <span class="h6 text-muted">(For female applicant/spouse, give complete maiden name)</span> </label>
                      <div class="col-sm-12">
                        <label>Full Name</label>
                        <div class="row">
                          <div class="form-group col-sm-3">
                            <input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name" value="<?php echo $LNAME ?>"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" required>
                          </div>
                          <div class="form-group col-sm-3">
                            <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" value="<?php echo $FNAME ?>"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" required>
                          </div>
                          <div class="form-group col-sm-3">
                            <input type="text" class="form-control" id="mname" name="mname" placeholder="Middle Name" value="<?php echo $MNAME ?>"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" required>
                          </div>
                          <div class="form-group col-sm-3">
                            <input type="text" class="form-control" id="msurname" name="msurname" placeholder="Mother's Surname"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $motherSurname ?>" required>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="row">
                          <div class="form-group col-sm-4">
                            <label for="address">Residence / Address</label> 
                            <input type="text" class="form-control" id="address" name="address" value="<?php echo $ADDR1 ?>"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" required>
                          </div>
                          <div class="form-group col-sm-4">
                            <label for="dplace">Place of Birth</label>
                            <input type="text" class="form-control" id="dplace" name="dplace" value="<?php echo $birthplace ?>"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" required>
                          </div>
                          <div class="form-group col-sm-4">
                            <label for="email">Email Address</label>
                            <input type="email" class="form-control" id="email" name="email" value="<?php echo $email ?>">
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="row">
                          <div class="form-group col-sm-4">
                            <label for="dbirth">Date of Birth</label> 
                            <input type="date" class="form-control" id="dbirth" name="dbirth" value="<?php echo $BDATE ?>" required>
                          </div>
                          <div class="form-group col-sm-4">
                            <label for="sex">Sex</label>
                            <select class="form-control" name="sex" id="sex" style="padding: 8px;" required>
                              <option <?php if($sex == ''){echo 'selected';} ?> style="color: #808080;" value="" disabled>- Select -</option>
                              <option <?php if($sex == 'male'){echo 'selected';} ?> value="male">Male</option>
                              <option <?php if($sex == 'female'){echo 'selected';} ?> value="female">Female</option>
                            </select>
                          </div>
                          <div class="form-group col-sm-4">
                            <label for="status">Civil Status</label>
                            <select class="form-control" name="status" id="status" style="padding: 8px;" required>
                              <option <?php if($CSTATUS == ''){echo 'selected';} ?> style="color: #808080;" value="" disabled>- Select -</option>
                              <option <?php if($CSTATUS == 'single'){echo 'selected';} ?> value="single">Single</option>
                              <option <?php if($CSTATUS == 'married'){echo 'selected';} ?> value="married">Married</option>
                              <option <?php if($CSTATUS == 'widowed'){echo 'selected';} ?> value="widowed">Widowed</option>
                              <option <?php if($CSTATUS == 'separated-in-fact'){echo 'selected';} ?> value="separated-in-fact">Separated-in-Fact</option>
                              <option <?php if($CSTATUS == 'legally-separated'){echo 'selected';} ?> value="legally-separated">Legally-Separated</option>
                              <option <?php if($CSTATUS == 'single-HOF'){echo 'selected';} ?> value="single-HOF">Single-HOF</option>
                            </select>
                          </div>
                        </div>
                      </div>

                      <div class="col-sm-12">
                        <div class="row">
                          <div class="form-group col-sm-4">
                            <label for="citizen">Citizenship</label>
                            <input type="text" class="form-control" id="citizen" name="citizen" value="<?php echo $citizenship ?>"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" required>
                          </div>
                          <div class="form-group col-sm-4">
                            <label for="contact">Contact Number</label> 
                            <input type="text" class="form-control" id="contact" pattern="[0-9]+" oninput="this.value = this.value.replace(/[^0-9-]/g, '').replace(/(\..*)\./g, '$1');" maxlength="11" name="contact" value="<?php echo $contact ?>" required>
                          </div>
                          <div class="form-group col-sm-4">
                            <label for="office">Office Number</label>
                            <input type="text" class="form-control" id="office" pattern="[0-9]+" oninput="this.value = this.value.replace(/[^0-9-]/g, '').replace(/(\..*)\./g, '$1');" maxlength="11" name="office" value="<?php echo $office ?>">
                          </div>
                        </div>
                      </div>

                      <div class="col-sm-12">
                        <div class="row">
                          <div class="form-group col-sm-6">
                            <label for="tin">TIN Number</label>
                            <input type="text" class="form-control" id="tin" name="tin" value="<?php echo $tin ?>"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="" required>
                          </div>
                          <div class="form-group col-sm-6">
                            <label for="pagibig">GSIS/SSS/PAG-IBIG ID</label>
                            <input type="text" class="form-control" id="pagibig" name="pagibig" value="<?php echo $pagibig ?>"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z-]/g, '').replace(/(\..*)\./g, '$1');" required>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <label for="userMname">Name of Spouse</label>
                        <div class="row">
                          <div class="form-group col-sm-3">
                            <input type="text" class="form-control" id="s_lname" name="s_lname" placeholder="Last Name"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $SLNAME ?>" >
                          </div>
                          <div class="form-group col-sm-3">
                            <input type="text" class="form-control" id="s_fname" name="s_fname" placeholder="First Name"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $SFNAME ?>" >
                          </div>
                          <div class="form-group col-sm-3">
                            <input type="text" class="form-control" id="s_mname" name="s_mname" placeholder="Middle Name"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $SMNAME ?>" >
                          </div>
                          <div class="form-group col-sm-3">
                            <input type="text" class="form-control" id="s_msurname" name="s_msurname" placeholder="Mother's Surname"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $SmotherSurname ?>" >
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="row">
                          <div class="form-group col-sm-6">
                            <label for="s_dbirth">Date of Birth</label> 
                            <input type="date" class="form-control" id="s_dbirth" name="s_dbirth" value="<?php echo $SBDATE ?>" >
                          </div>
                          <div class="form-group col-sm-6">
                            <label for="s_dplace">Place of Birth</label>
                            <input type="text" class="form-control" id="s_dplace" name="s_dplace"
                          oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $Sbirthplace ?>" >
                          </div>
                        </div>
                      </div>
                    </div>
                    <br>
                    <div class="col-md-6">
                      <label style="border-bottom: 2px solid #67a2b2;">II. APPLICANT’S EMPLOYMENTS STATUS</label>
                      <div class="col-sm-12">
                        <div class="row">
                          <div class="form-group col-sm-6">
                            <label for="s_dbirth">Nature of Employment</label> 
                            <input type="text" class="form-control" id="nature" name="nature" value="<?php echo $nature ?>"
                          oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" required>
                          </div>
                          <div class="form-group col-sm-6">
                            <label for="s_dplace">Name of Agency/Corporation</label>
                            <input type="text" class="form-control" id="agency" name="agency" value="<?php echo $EMPLOYER ?>"
                          oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" required>
                          </div>
                        </div>
                      </div>
                      <div class="form-group col-sm-12">
                        <label for="a_address">Address of Agency/Corporation</label> 
                        <input type="text" class="form-control" id="a_address" name="a_address" value="<?php echo $a_address ?>"
                          oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="" required>
                      </div>
                      <div class="col-sm-12">
                        <div class="row">
                          <div class="form-group col-sm-4">
                            <label for="BoS">Branch of Service</label> <span id="username-availability-status"></span>
                            <select class="form-control" name="BoS" id="BoS" style="padding: 8px;" required>
                              <option <?php if($service_branch == ''){echo 'selected';} ?> style="color: #808080;" value="" disabled>- Select -</option>
                              <option <?php if($service_branch == 'Air Force'){echo 'selected';} ?> value="Air Force">Air Force</option>
                              <option <?php if($service_branch == 'Army'){echo 'selected';} ?> value="Army">Army</option>
                              <option <?php if($service_branch == 'Navy'){echo 'selected';} ?> value="Navy">Navy</option>
                            </select>
                          </div>
                          <script type="text/javascript">
                            function getBranch()
                            {
                                jQuery.ajax({
                                type:'POST',
                                url:'ajax_branch.php',
                                data:'BoS='+$("#BoS").val(),
                                success:function(data){
                                  $("#username-availability-status").html(data);
                                }
                              })
                            }
                          </script>
                          <div class="form-group col-sm-4">
                            <label for="rank">Rank</label> 
                            <input type="text" class="form-control" id="rank" name="rank" placeholder="e.g. " required
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $uni_rank ?>" >
                          </div>
                          <div class="form-group col-sm-4">
                            <label for="serial">Serial Number / Badge Number</label> 
                            <input type="text" class="form-control" value="<?php echo $serial ?>" required
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z-]/g, '').replace(/(\..*)\./g, '$1');" id="serial" name="serial" >
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="row">
                          <div class="form-group col-sm-6">
                            <label for="assignPlace">Place of Assignment</label> 
                            <input type="text" class="form-control" value="<?php echo $assign_place ?>" required
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" id="assignPlace" name="assignPlace" >
                          </div>
                          <div class="form-group col-sm-6">
                            <label for="motherUnit">Mother Unit</label> 
                            <select class="form-control" name="motherUnit" id="motherUnit" style="padding: 8px;" required>
                              <option <?php if($mother_unit == ''){echo 'selected';} ?> style="color: #808080;" value="" disabled>- Select -</option>
                              <option <?php if($mother_unit == 'AFP'){echo 'selected';} ?> value="AFP">Armed Forces of the Philippines</option>
                              <option <?php if($mother_unit == 'PNP'){echo 'selected';} ?> value="PNP">Philippine National Police</option>
                              <option <?php if($mother_unit == 'BJMP'){echo 'selected';} ?> value="BJMP">Bureau of Jail Management and Penology</option>
                              <option <?php if($mother_unit == 'BFP'){echo 'selected';} ?> value="BFP">Bureau of Fire Protection</option>
                              <option <?php if($mother_unit == 'BuCor'){echo 'selected';} ?> value="BuCor">Bureau of Corrections</option>
                              <option <?php if($mother_unit == 'PCG'){echo 'selected';} ?> value="PCG">Philippine Coast Guard</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="row">
                          <div class="form-group col-sm-4">
                            <label for="position">Position</label>
                            <input type="text" class="form-control" id="position" name="position"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $POSITION ?>" required>
                          </div>
                          <div class="form-group col-sm-4">
                            <label for="SG">Salary Grade</label>
                            <select class="form-control" name="SG" id="SG" style="padding: 8px;" required>
                              <option <?php if($SG == ''){ echo 'selected';} ?> style="color: #808080;" value="" disabled>- Select -</option>
                              <option <?php if($SG == 'SG 1'){ echo 'selected';} ?> value="SG 1">Salary Grade 1</option>
                              <option <?php if($SG == 'SG 2'){ echo 'selected';} ?> value="SG 2">Salary Grade 2</option>
                              <option <?php if($SG == 'SG 3'){ echo 'selected';} ?> value="SG 3">Salary Grade 3</option>
                              <option <?php if($SG == 'SG 4'){ echo 'selected';} ?> value="SG 4">Salary Grade 4</option>
                              <option <?php if($SG == 'SG 5'){ echo 'selected';} ?> value="SG 5">Salary Grade 5</option>
                              <option <?php if($SG == 'SG 6'){ echo 'selected';} ?> value="SG 6">Salary Grade 6</option>
                              <option <?php if($SG == 'SG 7'){ echo 'selected';} ?> value="SG 7">Salary Grade 7</option>
                              <option <?php if($SG == 'SG 8'){ echo 'selected';} ?> value="SG 8">Salary Grade 8</option>
                              <option <?php if($SG == 'SG 9'){ echo 'selected';} ?> value="SG 9">Salary Grade 9</option>
                              <option <?php if($SG == 'SG 10'){ echo 'selected';} ?> value="SG 10">Salary Grade 10</option>
                              <option <?php if($SG == 'SG 11'){ echo 'selected';} ?> value="SG 11">Salary Grade 11</option>
                              <option <?php if($SG == 'SG 12'){ echo 'selected';} ?> value="SG 12">Salary Grade 12</option>
                              <option <?php if($SG == 'SG 13'){ echo 'selected';} ?> value="SG 13">Salary Grade 13</option>
                              <option <?php if($SG == 'SG 14'){ echo 'selected';} ?> value="SG 14">Salary Grade 14</option>
                              <option <?php if($SG == 'SG 15'){ echo 'selected';} ?> value="SG 15">Salary Grade 15</option>
                              <option <?php if($SG == 'SG 16'){ echo 'selected';} ?> value="SG 16">Salary Grade 16</option>
                              <option <?php if($SG == 'SG 17'){ echo 'selected';} ?> value="SG 17">Salary Grade 17</option>
                              <option <?php if($SG == 'SG 18'){ echo 'selected';} ?> value="SG 18">Salary Grade 18</option>
                              <option <?php if($SG == 'SG 19'){ echo 'selected';} ?> value="SG 19">Salary Grade 19</option>
                              <option <?php if($SG == 'SG 20'){ echo 'selected';} ?> value="SG 20">Salary Grade 20</option>
                              <option <?php if($SG == 'SG 21'){ echo 'selected';} ?> value="SG 21">Salary Grade 21</option>
                              <option <?php if($SG == 'SG 22'){ echo 'selected';} ?> value="SG 22">Salary Grade 22</option>
                              <option <?php if($SG == 'SG 23'){ echo 'selected';} ?> value="SG 23">Salary Grade 23</option>
                              <option <?php if($SG == 'SG 24'){ echo 'selected';} ?> value="SG 24">Salary Grade 24</option>
                              <option <?php if($SG == 'SG 25'){ echo 'selected';} ?> value="SG 25">Salary Grade 25</option>
                              <option <?php if($SG == 'SG 26'){ echo 'selected';} ?> value="SG 26">Salary Grade 26</option>
                              <option <?php if($SG == 'SG 27'){ echo 'selected';} ?> value="SG 27">Salary Grade 27</option>
                              <option <?php if($SG == 'SG 28'){ echo 'selected';} ?> value="SG 28">Salary Grade 28</option>
                              <option <?php if($SG == 'SG 29'){ echo 'selected';} ?> value="SG 29">Salary Grade 29</option>
                              <option <?php if($SG == 'SG 30'){ echo 'selected';} ?> value="SG 30">Salary Grade 30</option>
                              <option <?php if($SG == 'SG 31'){ echo 'selected';} ?> value="SG 31">Salary Grade 31</option>
                              <option <?php if($SG == 'SG 32'){ echo 'selected';} ?> value="SG 32">Salary Grade 32</option>
                              <option <?php if($SG == 'SG 33'){ echo 'selected';} ?> value="SG 33">Salary Grade 33</option>
                            </select>
                          </div>
                          <div class="form-group col-sm-4">
                            <label for="income">Gross Monthly Income</label>
                            <input type="text" class="form-control" maxlength="15" value="<?php echo $MINCOME ?>" id="income" name="income" placeholder="" required>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="row">
                          <div class="form-group col-sm-6">
                            <label for="dHired">Date Hired</label> 
                            <input type="date" class="form-control" id="dHired" name="dHired" value="<?php echo $dateHired ?>" required>
                          </div>
                          <div class="form-group col-sm-6">
                            <label for="years">No. of years in the Government</label>
                            <input type="number" class="form-control" id="years" max="60" min="0" name="years" value="<?php echo $years ?>" required>
                          </div>
                        </div>
                      </div>
                      <br>
                      <span>
                        <label style="border-bottom: 2px solid #67a2b2;">III. APPLICANT'S FAMILY COMPOSITION <span class=" h6 text-muted"> (add additional fields if necssary) </span></label>
                        <?php
                        include('../../../../dbcon.php');
                        if (is_numeric($dependent_id) == true){
                          try{
                            $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                            $dependentquerylimit1 = "SELECT * FROM applicant_family WHERE dependent_id = :dependent_id LIMIT 1";
                            $sthdependentquerylimit1 = $dbh->prepare($dependentquerylimit1);
                            $sthdependentquerylimit1->bindParam(':dependent_id', $dependent_id);
                            $sthdependentquerylimit1->execute();
                            $sthdependentquerylimit1->setFetchMode(PDO::FETCH_ASSOC); 
                            while ($derow1 = $sthdependentquerylimit1->fetch(PDO::FETCH_ASSOC)) {
                            ?>
                            <span>
                              <div class="col-sm-12">
                                <label>Full Name</label>
                                <button class="close" style="background: none; border: none; float: right;"><i class="fa fa-times"></i></button>
                                <div class="row">
                                  <div class="form-group col-sm-4">
                                    <input class="sl"type="text" name="slno[]" id="slno" value="1" style="border-radius: 5px; width: 100%; height: 50px; border-color: #f1f1f1;" hidden readonly="">
                                    <input type="text" class="form-control" id="fam-lname" name="dlname[]" placeholder="Last Name"
                                    oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $derow1['DLNAME']; ?>">
                                  </div>
                                  <div class="form-group col-sm-4">
                                    <input type="text" class="form-control" id="fam-fname" name="dfname[]" placeholder="First Name"
                                    oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $derow1['DFNAME']; ?>">
                                  </div>
                                  <div class="form-group col-sm-4">
                                    <input type="text" class="form-control" id="fam-fname" name="dmname[]" placeholder="Middle Name"
                                    oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $derow1['DMNAME']; ?>">
                                  </div>
                                </div>
                              </div>
                              <div class="col-sm-12">
                                <div class="row">
                                  <div class="form-group col-sm-3">
                                    <label for="s_dbirth">Relation to Applicant</label> 
                                    <input type="text" class="form-control" id="fam-relation" name="relation[]"
                                    oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $derow1['relation']; ?>">
                                  </div>
                                  <div class="form-group col-sm-3">
                                    <label for="status">Civil Status</label>
                                    <select class="form-control" name="dcstatus[]" id="fam-status" style="padding: 8px;">
                                      <option <?php if($derow1['DCSTATUS'] == ""){echo 'selected';} ?> style="color: #808080;" value="" disabled>- Select -</option>
                                      <option <?php if($derow1['DCSTATUS'] == "single"){echo 'selected';} ?> value="single">Single</option>
                                      <option <?php if($derow1['DCSTATUS'] == "married"){echo 'selected';} ?> value="married">Married</option>
                                      <option <?php if($derow1['DCSTATUS'] == "widowed"){echo 'selected';} ?> value="widowed">Widowed</option>
                                      <option <?php if($derow1['DCSTATUS'] == "separated-in-fact"){echo 'selected';} ?> value="separated-in-fact">Separated-in-Fact</option>
                                      <option <?php if($derow1['DCSTATUS'] == "legally-separated"){echo 'selected';} ?> value="legally-separated">Legally-Separated</option>
                                      <option <?php if($derow1['DCSTATUS'] == "single-HOF"){echo 'selected';} ?> value="single-HOF">Single-HOF</option>
                                    </select>
                                  </div>
                                  <div class="form-group col-sm-3">
                                    <label for="s_dplace">Age</label>
                                    <input type="number" class="form-control" min="0" max="150" id="fam-age" name="age[]" value="<?php echo $derow1['age']; ?>">
                                  </div>
                                  <div class="form-group col-sm-3">
                                    <label for="s_dplace">Source of Income</label>
                                    <input type="text" class="form-control" id="fam-age" name="source_income[]"
                                    oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $derow1['source_income']; ?>">
                                  </div>
                                </div>
                              </div>
                              <hr style="border-bottom: 1px solid #67a2b2;">
                            </span>
                            <?php
                            }
                          }
                          catch(PDOException $e){
                            error_log('PDOException - ' . $e->getMessage(), 0);
                            http_response_code(500);
                            die('Error establishing connection with database');
                          }
                        } 
                        if(!is_numeric($dependent_id) == true){
                        http_response_code(400);
                        die('Error processing bad or malformed request');
                        }
                        include('../../../../dbcon.php');
                        if (is_numeric($dependent_id) == true){
                          try{
                            $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                            $dependentqueryoffset = "SELECT * FROM applicant_family WHERE dependent_id = :dependent_id LIMIT 100 OFFSET 1";
                            $sthdependentqueryoffset = $dbh->prepare($dependentqueryoffset);
                            $sthdependentqueryoffset->bindParam(':dependent_id', $dependent_id);
                            $sthdependentqueryoffset->execute();
                            $sthdependentqueryoffset->setFetchMode(PDO::FETCH_ASSOC); 
                            while ($derow2 = $sthdependentqueryoffset->fetch(PDO::FETCH_ASSOC)){
                            ?>
                            <span>
                              <div class="col-sm-12">
                                <label>Full Name</label>
                                <button class="close" style="background: none; border: none; float: right;"><i class="fa fa-times"></i></button>
                                <div class="row">
                                  <div class="form-group col-sm-4">
                                    <input class="sl"type="text" name="slno[]" id="slno" value="1" style="border-radius: 5px; width: 100%; height: 50px; border-color: #f1f1f1;" hidden readonly="">
                                    <input type="text" class="form-control" id="fam-lname" name="dlname[]" placeholder="Last Name" value="<?php echo $derow2['DLNAME']; ?>" required>
                                  </div>
                                  <div class="form-group col-sm-4">
                                    <input type="text" class="form-control" id="fam-fname" name="dfname[]" placeholder="First Name" value="<?php echo $derow2['DFNAME']; ?>" required>
                                  </div>
                                  <div class="form-group col-sm-4">
                                    <input type="text" class="form-control" id="fam-fname" name="dmname[]" placeholder="Middle Name" value="<?php echo $derow2['DMNAME']; ?>" required>
                                  </div>
                                </div>
                              </div>
                              <div class="col-sm-12">
                                <div class="row">
                                  <div class="form-group col-sm-3">
                                    <label for="s_dbirth">Relation to Applicant</label> 
                                    <input type="text" class="form-control" id="fam-relation" name="relation[]" value="<?php echo $derow2['relation']; ?>" required>
                                  </div>
                                  <div class="form-group col-sm-3">
                                    <label for="status">Civil Status</label>
                                    <select class="form-control" name="dcstatus[]" id="fam-status" style="padding: 8px;" required>
                                      <option <?php if($derow1['DCSTATUS'] == ""){echo 'selected';} ?> style="color: #808080;" value="" disabled>- Select -</option>
                                      <option <?php if($derow1['DCSTATUS'] == "single"){echo 'selected';} ?> value="single">Single</option>
                                      <option <?php if($derow1['DCSTATUS'] == "married"){echo 'selected';} ?> value="married">Married</option>
                                      <option <?php if($derow1['DCSTATUS'] == "widowed"){echo 'selected';} ?> value="widowed">Widowed</option>
                                      <option <?php if($derow1['DCSTATUS'] == "separated-in-fact"){echo 'selected';} ?> value="separated-in-fact">Separated-in-Fact</option>
                                      <option <?php if($derow1['DCSTATUS'] == "legally-separated"){echo 'selected';} ?> value="legally-separated">Legally-Separated</option>
                                      <option <?php if($derow1['DCSTATUS'] == "single-HOF"){echo 'selected';} ?> value="single-HOF">Single-HOF</option>
                                    </select>
                                  </div>
                                  <div class="form-group col-sm-3">
                                    <label for="s_dplace">Age</label>
                                    <input type="number" class="form-control" min="0" max="150" id="fam-age" name="age[]" value="<?php echo $derow2['age']; ?>" required>
                                  </div>
                                  <div class="form-group col-sm-3">
                                    <label for="s_dplace">Source of Income</label>
                                    <input type="text" class="form-control" id="fam-age" name="source_income[]" value="<?php echo $derow2['source_income']; ?>" required>
                                  </div>
                                </div>
                              </div>
                              <hr style="border-bottom: 1px solid #67a2b2;">
                            </span>
                            <?php
                            }
                          }
                          catch(PDOException $e){
                            error_log('PDOException - ' . $e->getMessage(), 0);
                            http_response_code(500);
                            die('Error establishing connection with database');
                          }
                        } 
                        if(!is_numeric($dependent_id) == true){
                        http_response_code(400);
                        die('Error processing bad or malformed request');
                        }
                        ?>
                        <div id="next"></div>
                      </span>
                      <button type="button" id="addrow" class="btn btn-default btn-sm color-cyan" style="font-size:medium; width:auto; float:right;"><i class="fa fa-plus"></i>&nbsp; Add member </button>
                      <br><br>
                      <div class="form-group col-sm-12">
                        <div class="row">
                          <label style="margin-top:auto; border-bottom: 2px solid #67a2b2;">IV. APPLICANT'S TOTAL FAMILY INCOME PER MONTH:</label>&nbsp;&nbsp;&nbsp;
                            <input type="text" style="width:auto;" pattern="[0-9]+" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');" maxlength="15" class="form-control" id="famIncome" name="famIncome" required>
                          </div>
                      </div>
                      <div class="form-group">
                          <label style="border-bottom: 2px solid #67a2b2;">V. FAMILY REAL PROPERTY HOLDINGS:</label>
                          <div class="form-group col-sm-12">
                            <label>
                              Have never availed of any form of government housing assistance
                              &nbsp;&nbsp;
                              <input type="radio" id="noViolation" name="noViolation" <?php if($non_violation == 'YES'){echo 'checked';} ?> value="YES" > YES
                              &nbsp;/&nbsp;
                              <input type="radio" id="noViolation" name="noViolation" <?php if($non_violation == 'NO'){echo 'checked';} ?> value="NO" > NO
                            </label>
                          </div>
                          <div class="form-group col-sm-12">
                            <label>
                              Have sold, alienated, conveyed, encumbered or leased the socialized housing, including imporovements
                              or rights thereon, except to qualified beneficiary as determined by the Government Agency
                              &nbsp;&nbsp;
                              <input type="radio" required id="altViolation" name="altViolation" <?php if($alt_violation == 'YES'){echo 'checked';} ?> value="YES"> YES
                              &nbsp;/&nbsp;
                              <input type="radio" required id="altViolation" name="altViolation" <?php if($alt_violation == 'YES'){echo 'checked';} ?> value="NO"> NO
                            </label>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="project-actions text-right">
                    <button type="submit" style="font-size:large; width:auto;" class="btn btn-default color-cyan-dark">
                      <i class="fa fa-paper-plane">
                      </i>&nbsp; Submit
                    </button>
                  </div>
                </div>
              </form>
              <!--End of Submit Form for editing Government Application-->
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <footer class="main-footer">
    <?php include('footer.php'); include('custom_btn.php'); ?>
  </footer>
  <aside class="control-sidebar control-sidebar-dark">
  </aside>
</div>
<script src="../../plugins/jquery/jquery.min.js"></script>
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="../../dist/js/adminlte.min.js"></script>
<script src="../../dist/js/demo.js"></script>
<script>
$(function () {
  bsCustomFileInput.init();
});
</script>
</body>
</html>
<style>
  .content {
    -webkit-animation-duration: 0.7s;
    animation-duration: 0.7s;
  }
  @-webkit-keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  @keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  .content {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
  }
  input:hover, select:hover, textarea:hover {
    border: 1px solid #67a2b2;
  }
</style>
<script>
$('#addrow').click(function(){
		var length = $('.sl').length;
		var i   = parseInt(length)+parseInt(1);
		var newrow = $('#next').append(
        '<span id="removemember">'+
          '<div class="col-sm-12">'+
            '<label for="userMname">Full Name</label>'+
            '<button class="close" style="background: none; border: none; float: right;"><i class="fa fa-times"></i></button>'+
            '<div class="row">'+
              '<div class="form-group col-sm-4">'+
                '<input class="sl"type="text" name="slno[]" id="slno" value="'+i+'" style="border-radius: 5px; width: 100%; height: 50px; border-color: #f1f1f1;" hidden readonly="">'+
                '<input type="text" class="form-control" id="fam-lname'+i+'" name="dlname[]" placeholder="Last Name" required>'+
              '</div>'+
              '<div class="form-group col-sm-4">'+
                '<input type="text" class="form-control" id="fam-fname" name="dfname[]" placeholder="First Name" required>'+
              '</div>'+
              '<div class="form-group col-sm-4">'+
                '<input type="text" class="form-control" id="fam-fname" name="dmname[]" placeholder="Middle Name" required>'+
              '</div>'+
            '</div>'+
          '</div>'+
          '<div class="col-sm-12">'+
            '<div class="row">'+
              '<div class="form-group col-sm-3">'+
                '<label for="s_dbirth">Relation to Applicant</label>'+
                '<input type="text" class="form-control" id="fam-relation" name="relation[]" placeholder="" required>'+
              '</div>'+
              '<div class="form-group col-sm-3">'+
                '<label for="s_dplace">Civil Status</label>'+
                '<select class="form-control" name="dcstatus[]" id="fam-status'+i+'" style="padding: 8px;" required>'+
                  '<option style="color: #808080;" value="" disabled selected>- Select -</option>'+
                  '<option value="single">Single</option>'+
                  '<option value="married">Married</option>'+
                  '<option value="widowed">Widowed</option>'+
                  '<option value="separated-in-fact">Separated-in-Fact</option>'+
                  '<option value="legally-separated">Legally-Separated</option>'+
                  '<option value="single-HOF">Single-HOF</option>'+
                '</select>'+
              '</div>'+
              '<div class="form-group col-sm-3">'+
                '<label for="s_dplace">Age</label>'+
                '<input type="number" class="form-control" max="150" id="fam-age" name="age[]" placeholder="" required>'+
              '</div>'+
              '<div class="form-group col-sm-3">'+
                '<label for="s_dplace">Source of Income</label>'+
                '<input type="text" class="form-control" id="fam-age" name="source_income[]" placeholder="" required>'+
              '</div>'+
            '</div>'+
          '</div>'+
          '<hr style="border-bottom: 1px solid #67a2b2;">'+
        '</span>'
      );
      jQuery('#fam-age'+i+'').attr('maxvalue','3');
      $('#fam-lname'+i+'').focus();     
		});   
  $('body').on('click','.close',function() {
       $(this).closest('span').remove()
       
  });
</script>
<script language="javascript">
  var message="This function is not allowed here.";
  function clickIE4(){
        if (event.button==2){
            alert(message);
            return false;
        }
  }
  function clickNS4(e){
      if (document.layers||document.getElementById&&!document.all){
              if (e.which==2||e.which==3){
                        alert(message);
                        return false;
              }
      }
  }
  if (document.layers){
        document.captureEvents(Event.MOUSEDOWN);
        document.onmousedown=clickNS4;
  }
  else if (document.all&&!document.getElementById){
        document.onmousedown=clickIE4;
  }
  document.oncontextmenu=new Function("alert(message);return false;")
</script>