<style>
    .tooltiptext {
        font-size:small;
    }
    #tooltip {
        position: relative;
        display: inline-block;
    }
    #tooltip .tooltiptext {
        visibility: hidden;
        width: 120px;
        background-color: #555;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        position: absolute;
        z-index: 1;
        bottom: 125%;
        left: 50%;
        margin-left: -60px;
        opacity: 0;
        transition: opacity 0.3s;
    }

    #tooltip .tooltiptext::after {
        content: "";
        position: absolute;
        top: 100%;
        left: 50%;
        margin-left: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: #555 transparent transparent transparent;
    }

    #tooltip:hover .tooltiptext {
        visibility: visible;
        opacity: 1;
    }

    .color-green-dark {
    border: 1px solid #f6f6f6;
    background-color: #f6f6f6;
    width: 55px;
    border-radius: 5px;
    font-weight: bold;
    }
    .color-orange-dark {
    border: 1px solid #f6f6f6;
    background-color: #f6f6f6;
    width: 55px;
    border-radius: 5px;
    font-weight: bold;
    }
    .color-red-dark {
    border: 1px solid #f6f6f6;
    background-color: #f6f6f6;
    width: 55px;
    border-radius: 5px;
    font-weight: bold;
    }
    .color-blue-dark {
    border: 1px solid #f6f6f6;
    background-color: #f6f6f6;
    width: 55px;
    border-radius: 5px;
    font-weight: bold;
    }
    .color-gray-dark {
    border: 1px solid #f6f6f6;
    background-color: #f6f6f6;
    width: 55px;
    border-radius: 5px;
    font-weight: bold;
    }
    
    .color-cyan-dark {
    border: 1px solid #f6f6f6;
    background-color: #f6f6f6;
    width: 55px;
    border-radius: 5px;
    font-weight: bold;
    }

    .color-green-dark:hover {
    color: green;
    background-color: #ececec;
    }
    .color-orange-dark:hover {
    color: orange;
    background-color: #ececec;
    }
    .color-red-dark:hover {
    color: red;
    background-color: #ececec;
    }
    .color-blue-dark:hover {
    color: blue;
    background-color: #ececec;
    }
    .color-gray-dark:hover {
    color: black;
    background-color: #ececec;
    }
    .color-cyan-dark:hover {
    color: #67a2b2;
    background-color: #ececec;
    }

    
    .color-green {
    border: 1px solid white;
    background-color: white;
    width: 55px;
    font-weight: bold;
    }
    .color-orange {
    border: 1px solid white;
    background-color: white;
    width: 55px;
    font-weight: bold;
    }
    .color-red {
    border: 1px solid white;
    background-color: white;
    width: 55px;
    font-weight: bold;
    }
    .color-blue {
    border: 1px solid white;
    background-color: white;
    width: 55px;
    font-weight: bold;
    }
    .color-gray {
    border: 1px solid white;
    background-color: white;
    width: 55px;
    font-weight: bold;
    }
    
    .color-cyan {
    border: 1px solid white;
    background-color: white;
    width: 55px;
    font-weight: bold;
    }

    .color-green:hover {
    color: green;
    }
    .color-orange:hover {
    color: orange;
    }
    .color-red:hover {
    color: red;
    }
    .color-blue:hover {
    color: blue;
    }
    .color-gray:hover {
    color: black;
    }
    .color-cyan:hover {
    color: #67a2b2;
    }

    .color-green-dark-auto {
    border: 1px solid #f6f6f6;
    background-color: #f6f6f6;
    width: 55px;
    border-radius: 5px;
    font-weight: bold;
    width:auto;
    font-size:large;
    }
    .color-orange-dark-auto {
    border: 1px solid #f6f6f6;
    background-color: #f6f6f6;
    width: 55px;
    border-radius: 5px;
    font-weight: bold;
    width:auto;
    font-size:large;
    }
    .color-red-dark-auto {
    border: 1px solid #f6f6f6;
    background-color: #f6f6f6;
    width: 55px;
    border-radius: 5px;
    font-weight: bold;
    width:auto;
    font-size:large;
    }
    .color-blue-dark-auto {
    border: 1px solid #f6f6f6;
    background-color: #f6f6f6;
    width: 55px;
    border-radius: 5px;
    font-weight: bold;
    width:auto;
    font-size:large;
    }
    .color-gray-dark-auto {
    border: 1px solid #f6f6f6;
    background-color: #f6f6f6;
    width: 55px;
    border-radius: 5px;
    font-weight: bold;
    width:auto;
    font-size:large;
    }
    
    .color-cyan-dark-auto {
    border: 1px solid #f6f6f6;
    background-color: #f6f6f6;
    width: 55px;
    border-radius: 5px;
    font-weight: bold;
    width:auto;
    font-size:large;
    }

    .color-green-dark-auto:hover {
    color: green;
    background-color: #ececec;
    }
    .color-orange-dark-auto:hover {
    color: orange;
    background-color: #ececec;
    }
    .color-red-dark-auto:hover {
    color: red;
    background-color: #ececec;
    }
    .color-blue-dark-auto:hover {
    color: blue;
    background-color: #ececec;
    }
    .color-gray-dark-auto:hover {
    color: black;
    background-color: #ececec;
    }
    .color-cyan-dark-auto:hover {
    color: #67a2b2;
    background-color: #ececec;
    }

    
    .color-green-auto {
    border: 1px solid white;
    background-color: white;
    width: 55px;
    font-weight: bold;
    width:auto;
    font-size:large;
    }
    .color-orange-auto {
    border: 1px solid white;
    background-color: white;
    width: 55px;
    font-weight: bold;
    width:auto;
    font-size:large;
    }
    .color-red-auto {
    border: 1px solid white;
    background-color: white;
    width: 55px;
    font-weight: bold;
    width:auto;
    font-size:large;
    }
    .color-blue-auto {
    border: 1px solid white;
    background-color: white;
    width: 55px;
    font-weight: bold;
    width:auto;
    font-size:large;
    }
    .color-gray-auto {
    border: 1px solid white;
    background-color: white;
    width: 55px;
    font-weight: bold;
    width:auto;
    font-size:large;
    }
    
    .color-cyan-auto {
    border: 1px solid white;
    background-color: white;
    width: 55px;
    font-weight: bold;
    width:auto;
    font-size:large;
    }

    .color-green-auto:hover {
    color: green;
    }
    .color-orange-auto:hover {
    color: orange;
    }
    .color-red-auto:hover {
    color: red;
    }
    .color-blue-auto:hover {
    color: blue;
    }
    .color-gray-auto:hover {
    color: black;
    }
    .color-cyan-auto:hover {
    color: #67a2b2;
    }
</style>