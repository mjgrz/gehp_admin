<!DOCTYPE html>
<html lang="en">
<head>
<?php
include('login_session.php');
if($login_userlevel != "Administrator"){
  header("location:../../index.php");
}
error_reporting(E_ERROR | E_PARSE);
?>

<link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
<link rel="stylesheet" href="../../plugins/toastr/toastr.min.css">
<link rel="stylesheet" href="../../dist/css/adminlte.min.css">

<script src="../../plugins/jquery/jquery.min.js"></script>
<script src="../../plugins/toastr/toastr.min.js"></script>

<?php
if (isset($_SESSION["status"]) && $_SESSION["status"] !='') {
  $msg = $_SESSION["status"];
  echo "<span><script type='text/javascript'>toastr.success('$msg')</script></span>";
  
  unset($_SESSION["status"]);
}
if (isset($_SESSION["error"]) && $_SESSION["error"] !='') {
  $msg = $_SESSION["error"];
  echo "<span><script type='text/javascript'>toastr.error('$msg')</script></span>";
  
  unset($_SESSION["error"]);
}
if (isset($_SESSION["info"]) && $_SESSION["info"] !='') {
  $msg = $_SESSION["info"];
  echo "<span><script type='text/javascript'>toastr.info('$msg')</script></span>";
  
  unset($_SESSION["info"]);
}
else {

}
?>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>NHA | Government Employee's Housing Program</title>
  <link rel="shortcut icon" type="image/png" href="../../dist/img/nha_logo_circle.png">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="font-weight: bold;">
    <?php include("header.php"); ?>
  </nav>

  <aside class="main-sidebar sidebar-dark-primary elevation-4" style="background: #002d32; position: fixed;">
    <a href="../../index.php" class="brand-link">
      <img src="../../dist/img/nha_logo_circle.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"><b>NHA | GEHP</b></span>
    </a>

    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <?php
          $target_dir = "uploads/users/";
          $target_file = $target_dir . basename("$login_image");
          if(file_exists($target_file)){
            echo "<img src='uploads/users/$login_image' class='img-circle elevation-3' alt='User Image' style='height: 33px;'>";
          } 
          else {
            echo "<img src='uploads/users/user.png' width='150px'>";
          }
          ?>
        </div>
        <div class="info">
          <a href="users_profile.php" class="d-block"><?php echo $_SESSION['login_user'] ?></a>
        </div>
      </div>

      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
          <li class="nav-item">
            <a href="../../index.php" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-briefcase"></i>
              <p>
                Applicants
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <input type="text" class="form-control" id="sessionempty" name="sessionempty" value="" placeholder="Search" hidden>
              <li class="nav-item">
                <a href="../forms/registration.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Government Employees</p>
                </a>
              </li>
              <li class="nav-item" <?php echo $deploy;?>>
                <a href="../forms/registration_Uni.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Uniformed Personnel</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/registration_OFW.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>OFW</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item" <?php echo $project_hide ?>>
            <a href="../forms/project_view.php" onclick="sessionempty()" class="nav-link">
              <i class="nav-icon fas fa-building"></i>
              <p>
                Projects
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $user_hide ?>>
            <a href="../forms/users.php" onclick="sessionempty()" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Users
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $element_hide ?>>
            <a href="../forms/report_gen.php" class="nav-link active" style="background: #67a2b2;">
              <i class="nav-icon fas fa-scroll"></i>
              <p>
                Reports
              </p>
            </a>
          </li>
          
          <li class="nav-item" <?php echo $pages_hide ?>>
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-clone"></i>
              <p>
                Pages
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              
              <li class="nav-item">
                <a href="../forms/home.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Home</p>
                </a>
              </li>
              <li class="nav-item" hidden>
                <a href="../forms/howToApply.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>How To Apply</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/about.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>About</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/banner.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Banner</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="../forms/bulletin.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Bulletin</p>
                </a>
              </li>
            </ul>
          </li>
          
          <li class="nav-item" <?php echo $inv_hide;?>>
            <a href="../forms/inventory.php" class="nav-link">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                Inventory
              </p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>
  <script>
    function sessionempty() {
      $("#loaderIcon").show();
          jQuery.ajax({
          url: "session_clear.php",
          data: {'sessionempty': $("#sessionempty").val()},
          type: "POST",
          success:function(data){
              $("#").html(data);
              $("#loaderIcon").hide();
          },
          error:function (){
          }
      });
    }
  </script>

  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Reports</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="col-sm-12">
        </div>
        <div class="row">
          <?php include("report_modal/master_rep.php"); ?>
          <?php include("report_modal/master_rep_ofw.php"); ?>
          <?php include("report_modal/for_PQ.php"); ?>
          <?php include("report_modal/for_PQ_ofw.php"); ?>
          <?php include("report_modal/EMD_rep.php"); ?>
          <div class="col-sm-12">
            <div class="card card-primary card-tabs" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
              <div class="card-header p-0 pt-1" style="background: #67a2b2;">
                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#tab1" role="tab" aria-controls="tab1" aria-selected="false">
                      Government Employee - Applications (For PAG-IBIG)
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-one-home-tab" data-toggle="pill" href="#tab2" role="tab" aria-controls="tab2" aria-selected="false">
                      Overseas Filipino Workers - Applications (For PAG-IBIG)
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-one-home-tab" data-toggle="pill" href="#tab3" role="tab" aria-controls="tab3" aria-selected="false">
                      Government Employee - Applications (For Pre-Qualification)
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-one-home-tab" data-toggle="pill" href="#tab4" role="tab" aria-controls="tab4" aria-selected="false">
                      Overseas Filipino Workers - Applications (For Pre-Qualification)
                    </a>
                  </li>
                  <li class="nav-item" <?php echo $hideEMDreport; ?>>
                    <a class="nav-link" id="custom-tabs-one-home-tab" data-toggle="pill" href="#tab5" role="tab" aria-controls="tab5" aria-selected="false">
                      EMD Report
                    </a>
                  </li>
                </ul>
              </div>
              <div class="card-body">

                <div class="tab-content" id="custom-tabs-one-tabContent">
                  <!--Government Employee - Applications (For PAG-IBIG)-->
                  <div class="tab-pane fade show active" id="tab1" role="tabpanel" aria-labelledby="tab1" style="font-family: arial;">
                    <div style="text-align: center;"><img src="TCPDF-main/examples/images/header1.png" style="width: 500px; margin-right: 5px;">
                    </div>
                    <table cellspacing="0" cellpadding="1" border="1" style="width: 100%;">
                      <thead>
                        <tr>
                          <th style="text-align: left; font-weight: bold; padding-left: 10px; width: 15%;">Project Proponent:</th>
                          <td style="text-align: left; width: 60%; padding-left: 15px;">National Housing Authority</td>
                          <th rowspan="4" style="text-align: center; font-weight: bold;">Date Prepared:<br><span style="font-weight: normal;"><?php echo date('F d, Y') ?></span></th>
                        </tr>
                        <tr>
                          <th style="padding-left: 10px;">Region:</th>
                          <td style="padding-left: 15px;">-</td>
                        </tr>
                        <tr>
                          <th style="padding-left: 10px;">Project Name:</th>
                          <td style="padding-left: 15px;">-</td>
                        </tr>
                        <tr>
                          <th style="padding-left: 10px;">Location:</th>
                          <td style="padding-left: 15px;">-</td>
                        </tr>
                      </thead>
                    </table>
                    <br>
                    <table cellspacing="0" cellpadding="3" border="1" style="width: 100%;">
                      <thead>         
                        <tr>
                          <th rowspan="2" style="text-align: center; font-weight: bold; width: 9.09%;">No.</th>
                          <th rowspan="2" style="text-align: center; font-weight: bold; width: 9.09%;">Last Name</th>
                          <th rowspan="2" style="text-align: center; font-weight: bold; width: 9.09%;">First Name</th>
                          <th rowspan="2" style="text-align: center; font-weight: bold; width: 9.09%;">Middle Name</th>
                          <th rowspan="2" style="text-align: center; font-weight: bold; width: 9.09%;">Maiden Name</th>
                          <th rowspan="2" style="text-align: center; font-weight: bold; width: 9.09%;">Birthday</th>
                          <th rowspan="2" style="text-align: center; font-weight: bold; width: 9.09%;">Age</th>
                          <th rowspan="2" style="text-align: center; font-weight: bold; width: 9.09%;">Gross Monthly Income</th>
                          <th colspan="2" style="text-align: center; font-weight: bold;">Employment Status</th>
                        </tr>
                        <tr>
                          <th style="text-align: center; font-weight: bold; width: 9.09%;">Employer</th>
                          <th style="text-align: center; font-weight: bold; width: 9.09%;">Address</th>
                        </tr>
                      </thead>
                      <tbody> 
                        <?php
                          $z = 1;
                          for($z;$z<=5;$z++){
                            echo '     
                            <tr>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                            </tr> ';
                          }
                        ?>   
                      </tbody>
                    </table>
                    <br>
                    <div><p style="text-align: right; margin-top: 20px; padding-right: 30px; border-top: 1px solid gray; font-weight: bold;">1 / 1</p></div>
                    <div class="modal-footer justify-content-right" style="border: none; padding: 0;">
                      <button style="width:auto; font-size:large;" type="button" class="btn btn-default color-cyan" data-toggle="modal" data-dismiss="modal" data-target="#master_rep">
                        <i class="fas fa-print">
                        </i>&nbsp;Print
                      </button>
                    </div>
                  </div>
                  <!--End of Government Employee - Applications (For PAG-IBIG)-->

                  <!--Overseas Filipino Workers - Applications (For PAG-IBIG)-->
                  <div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="tab2" style="font-family: arial;">
                    <div style="text-align: center;"><img src="TCPDF-main/examples/images/header1.png" style="width: 500px; margin-right: 5px;">
                    </div>
                      <table cellspacing="0" cellpadding="1" border="1" style="width: 100%;">
                        <thead>
                          <tr>
                            <th style="text-align: left; font-weight: bold; padding-left: 10px; width: 15%;">Project Proponent:</th>
                            <td style="text-align: left; width: 60%; padding-left: 15px;">National Housing Authority</td>
                            <th rowspan="4" style="text-align: center; font-weight: bold;">Date Prepared:<br><span style="font-weight: normal;"><?php echo date('F d, Y') ?></span></th>
                          </tr>
                          <tr>
                            <th style="padding-left: 10px;">Region:</th>
                            <td style="padding-left: 15px;">-</td>
                          </tr>
                          <tr>
                            <th style="padding-left: 10px;">Project Name:</th>
                            <td style="padding-left: 15px;">-</td>
                          </tr>
                          <tr>
                            <th style="padding-left: 10px;">Location:</th>
                            <td style="padding-left: 15px;">-</td>
                          </tr>
                        </thead>
                      </table>
                      <br>
                      <table cellspacing="0" cellpadding="3" border="1" style="width: 100%;">
                        <thead>         
                          <tr>
                            <th rowspan="2" style="text-align: center; font-weight: bold; width: 9.09%;">No.</th>
                            <th rowspan="2" style="text-align: center; font-weight: bold; width: 9.09%;">Last Name</th>
                            <th rowspan="2" style="text-align: center; font-weight: bold; width: 9.09%;">First Name</th>
                            <th rowspan="2" style="text-align: center; font-weight: bold; width: 9.09%;">Middle Name</th>
                            <th rowspan="2" style="text-align: center; font-weight: bold; width: 9.09%;">Maiden Name</th>
                            <th rowspan="2" style="text-align: center; font-weight: bold; width: 9.09%;">Birthday</th>
                            <th rowspan="2" style="text-align: center; font-weight: bold; width: 9.09%;">Age</th>
                            <th rowspan="2" style="text-align: center; font-weight: bold; width: 9.09%;">Gross Monthly Income</th>
                            <th colspan="2" style="text-align: center; font-weight: bold;">Employment Status</th>
                          </tr>
                          <tr>
                            <th style="text-align: center; font-weight: bold; width: 9.09%;">Nature of Employment</th>
                            <th style="text-align: center; font-weight: bold; width: 9.09%;">Agency</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php
                          $z = 1;
                          for($z;$z<=5;$z++){
                            echo '         
                            <tr>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                            </tr>';
                          }
                        ?>
                        </tbody>
                      </table>
                      <br>
                    <div><p style="text-align: right; margin-top: 20px; padding-right: 30px; border-top: 1px solid gray; font-weight: bold;">1 / 1</p></div>
                  
                    <div class="modal-footer justify-content-right" style="border: none; padding: 0;">
                      <button style="width:auto; font-size:large;" type="button" class="btn btn-default color-cyan" data-toggle="modal" data-dismiss="modal" data-target="#master_rep_ofw">
                        <i class="fas fa-print">
                        </i>&nbsp;Print
                      </button>
                    </div>
                  </div>
                  <!--End of Overseas Filipino Workers - Applications (For PAG-IBIG)-->

                  <!--Government Employee - Applications (For Pre-Qualification)-->
                  <div class="tab-pane fade" id="tab3" role="tabpanel" aria-labelledby="tab3" style="font-family: arial;">
                    <div style="text-align: center;"><img src="TCPDF-main/examples/images/header1.png" style="width: 500px; margin-right: 5px;">
                    </div>
                      <table cellspacing="0" cellpadding="1" border="1" style="width: 100%;">
                        <thead>
                          <tr>
                            <th style="text-align: left; font-weight: bold; padding-left: 10px; width: 15%;">Project Proponent:</th>
                            <td style="text-align: left; width: 60%; padding-left: 15px;">National Housing Authority</td>
                            <th rowspan="4" style="text-align: center; font-weight: bold;">Date Prepared:<br><span style="font-weight: normal;"><?php echo date('F d, Y') ?></span></th>
                          </tr>
                          <tr>
                            <th style="padding-left: 10px;">Region:</th>
                            <td style="padding-left: 15px;">-</td>
                          </tr>
                          <tr>
                            <th style="padding-left: 10px;">Project Name:</th>
                            <td style="padding-left: 15px;">-</td>
                          </tr>
                          <tr>
                            <th style="padding-left: 10px;">Location:</th>
                            <td style="padding-left: 15px;">-</td>
                          </tr>
                        </thead>
                      </table>
                      <br>
                      <table cellspacing="0" cellpadding="1" border="1" style="width: 100%;">
                        <thead>         
                          <tr>
                            <th rowspan="2" style="text-align: center; font-weight: bold; width: 2%;">No.</th>
                            <th colspan="3" style="text-align: center; font-weight: bold; width: 18%;">Name of Household Head<br>(Maiden Name for married female applicant)</th>
                            <th rowspan="2" style="text-align: center; font-weight: bold; width: 6.75%;">Address</th>
                            <th rowspan="2" style="text-align: center; font-weight: bold;">Civil Status</th>
                            <th rowspan="2" style="text-align: center; font-weight: bold; width: 4%;">Sex</th>
                            <th rowspan="2" style="text-align: center; font-weight: bold;">Birthdate</th>
                            <th rowspan="2" style="text-align: center; font-weight: bold; width: 2.5%;">Age</th>
                            <th rowspan="2" style="text-align: center; font-weight: bold;">Gross Monthly Income</th>
                            <th rowspan="2" style="text-align: center; font-weight: bold; width: 6%;">Contact Number</th>
                            <th colspan="3" style="text-align: center; font-weight: bold; width: 18%;">Name of Spouse/Co-Owner<br>(Maiden Name for married female applicant)</th>
                            <th rowspan="2" style="text-align: center; font-weight: bold;">Birthdate</th>
                            <th rowspan="2" style="text-align: center; font-weight: bold; width: 2.5%;">Age</th>
                            <th colspan="3" style="text-align: center; font-weight: bold; width: 19.2%;">Employment Status</th>
                          </tr>
                          <tr>
                            <th style="text-align: center; font-weight: bold;">Last Name</th>
                            <th style="text-align: center; font-weight: bold;">First Name</th>
                            <th style="text-align: center; font-weight: bold;">Middle Name</th>
                            <th style="text-align: center; font-weight: bold;">Last Name</th>
                            <th style="text-align: center; font-weight: bold;">First Name</th>
                            <th style="text-align: center; font-weight: bold;">Middle Name</th>
                            <th style="text-align: center; font-weight: bold; width: 6.3%;">Employer / Agency / Branch of Service</th>
                            <th style="text-align: center; font-weight: bold; width: 6%;">Rank / Position</th>
                            <th style="text-align: center; font-weight: bold; width: 6.9%;">Serial / Badge / OEC No. / Employee No.</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php
                          $z = 1;
                          for($z;$z<=5;$z++){
                            echo '     
                            <tr>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                            </tr>';
                          }
                        ?>
                        </tbody>
                      </table>
                      <br>
                    <div><p style="text-align: right; margin-top: 20px; padding-right: 30px; border-top: 1px solid gray; font-weight: bold;">1 / 1</p></div>
                  
                    <div class="modal-footer justify-content-right" style="border: none; padding: 0;">
                      <button style="width:auto; font-size:large;" type="button" class="btn btn-default color-cyan" data-toggle="modal" data-dismiss="modal" data-target="#for_PQ">
                        <i class="fas fa-print">
                        </i>&nbsp;Print
                      </button>
                    </div>
                  </div>
                  <!--End of Government Employee - Applications (For Pre-Qualification)-->

                  <!--Overseas Filipino Workers - Applications (For Pre-Qualification)-->
                  <div class="tab-pane fade" id="tab4" role="tabpanel" aria-labelledby="tab4" style="font-family: arial;">
                    <div style="text-align: center;"><img src="TCPDF-main/examples/images/header1.png" style="width: 500px; margin-right: 5px;">
                    </div>
                      <table cellspacing="0" cellpadding="1" border="1" style="width: 100%;">
                        <thead>
                          <tr>
                            <th style="text-align: left; font-weight: bold; padding-left: 10px; width: 15%;">Project Proponent:</th>
                            <td style="text-align: left; width: 60%; padding-left: 15px;">National Housing Authority</td>
                            <th rowspan="4" style="text-align: center; font-weight: bold;">Date Prepared:<br><span style="font-weight: normal;"><?php echo date('F d, Y') ?></span></th>
                          </tr>
                          <tr>
                            <th style="padding-left: 10px;">Region:</th>
                            <td style="padding-left: 15px;">-</td>
                          </tr>
                          <tr>
                            <th style="padding-left: 10px;">Project Name:</th>
                            <td style="padding-left: 15px;">-</td>
                          </tr>
                          <tr>
                            <th style="padding-left: 10px;">Location:</th>
                            <td style="padding-left: 15px;">-</td>
                          </tr>
                        </thead>
                      </table>
                      <br>
                      <table cellspacing="0" cellpadding="1" border="1" style="width: 100%;">
                        <thead>         
                          <tr>
                            <th rowspan="2" style="text-align: center; font-weight: bold; width: 2%;">No.</th>
                            <th colspan="3" style="text-align: center; font-weight: bold; width: 18%;">Name of Household Head<br>(Maiden Name for married female applicant)</th>
                            <th rowspan="2" style="text-align: center; font-weight: bold; width: 6.75%;">Address</th>
                            <th rowspan="2" style="text-align: center; font-weight: bold;">Civil Status</th>
                            <th rowspan="2" style="text-align: center; font-weight: bold; width: 4%;">Sex</th>
                            <th rowspan="2" style="text-align: center; font-weight: bold;">Birthdate</th>
                            <th rowspan="2" style="text-align: center; font-weight: bold; width: 2.5%;">Age</th>
                            <th rowspan="2" style="text-align: center; font-weight: bold;">Gross Monthly Income</th>
                            <th rowspan="2" style="text-align: center; font-weight: bold; width: 6%;">Contact Number</th>
                            <th colspan="3" style="text-align: center; font-weight: bold; width: 18%;">Name of Spouse/Co-Owner<br>(Maiden Name for married female applicant)</th>
                            <th rowspan="2" style="text-align: center; font-weight: bold;">Birthdate</th>
                            <th rowspan="2" style="text-align: center; font-weight: bold; width: 2.5%;">Age</th>
                            <th colspan="3" style="text-align: center; font-weight: bold; width: 19.2%;">Employment Status</th>
                          </tr>
                          <tr>
                            <th style="text-align: center; font-weight: bold;">Last Name</th>
                            <th style="text-align: center; font-weight: bold;">First Name</th>
                            <th style="text-align: center; font-weight: bold;">Middle Name</th>
                            <th style="text-align: center; font-weight: bold;">Last Name</th>
                            <th style="text-align: center; font-weight: bold;">First Name</th>
                            <th style="text-align: center; font-weight: bold;">Middle Name</th>
                            <th style="text-align: center; font-weight: bold; width: 9.3%;">Employer / Agency</th>
                            <th style="text-align: center; font-weight: bold; width: 9.9%;">OEC Number</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php
                          $z = 1;
                          for($z;$z<=5;$z++){
                            echo '        
                            <tr>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                              <td style="text-align: center; font-weight: normal;">-</td>
                            </tr>';
                          }
                        ?>
                        </tbody>
                      </table>
                      <br>
                    <div><p style="text-align: right; margin-top: 20px; padding-right: 30px; border-top: 1px solid gray; font-weight: bold;">1 / 1</p></div>
                  
                    <div class="modal-footer justify-content-right" style="border: none; padding: 0;">
                      <button style="width:auto; font-size:large;" type="button" class="btn btn-default color-cyan" data-toggle="modal" data-dismiss="modal" data-target="#for_PQ_ofw">
                        <i class="fas fa-print">
                        </i>&nbsp;Print
                      </button>
                    </div>
                  </div>
                  <!--End of Overseas Filipino Workers - Applications (For Pre-Qualification)-->

                  <!--EMD Report-->
                  <div class="tab-pane fade" <?php echo $hideEMDreport; ?> id="tab5" role="tabpanel" aria-labelledby="tab5" style="font-family: arial;">
                    <div style="text-align: center; margin-bottom: 5px;">
                      <table cellpadding="1" cellspacing="0" style="text-align:center; width: 100%;">
                      <tr>
                          <th style="text-align: right; width: 40%; padding-right: 10px;"><img src="TCPDF-main/examples/images/nha-logo.png" width="50" border="0"/></th>
                          <th style="width: 18%; font-size: 12px;"><b>National Housing Authority<br>GOVERNMENT EMPLOYEE HOUSING PROGRAM</b></th>
                          <th style="text-align: left; width: 40%;"><img src="TCPDF-main/examples/images/balai_temp.png" width="90" border="0"/></th>
                      </tr>
                      </table>
                    </div>
                    <table cellspacing="0" cellpadding="3" border="1" style="width: 100%;">
                      <thead>         
                          <tr>
                              <th rowspan="3" style="text-align: center; font-weight: bold;"><div><br>Project Name</div></th>
                              <th rowspan="3" style="text-align: center; font-weight: bold;"><div><br>Remaining No. of Available Lots/Units</div></th>
                              <th rowspan="3" style="text-align: center; font-weight: bold;"><div><br>Total Qualified Applicants GEHP and OFW</div></th>
                              <th colspan="6" style="text-align: center; font-weight: bold;">Online Application</th>
                              <th colspan="6" style="text-align: center; font-weight: bold;">Manual Application</th>
                          </tr>
                          <tr>
                              <th colspan="3" style="text-align: center; font-weight: bold;">GEHP</th>
                              <th colspan="3" style="text-align: center; font-weight: bold;">OFW</th>
                              <th colspan="3" style="text-align: center; font-weight: bold;">GEHP</th>
                              <th colspan="3" style="text-align: center; font-weight: bold;">OFW</th>
                          </tr>
                          <tr>
                              <th style="text-align: center; font-weight: bold;">Qualified Applicants</th>
                              <th style="text-align: center; font-weight: bold;">Disqualified Applicants</th>
                              <th style="text-align: center; font-weight: bold;">New Applications for Evaluation</th>
                              <th style="text-align: center; font-weight: bold;">Qualified Applicants</th>
                              <th style="text-align: center; font-weight: bold;">Disqualified Applicants</th>
                              <th style="text-align: center; font-weight: bold;">New Applications for Evaluation</th>
                              <th style="text-align: center; font-weight: bold;">Qualified Applicants</th>
                              <th style="text-align: center; font-weight: bold;">Disqualified Applicants</th>
                              <th style="text-align: center; font-weight: bold;">New Applications for Evaluation</th>
                              <th style="text-align: center; font-weight: bold;">Qualified Applicants</th>
                              <th style="text-align: center; font-weight: bold;">Disqualified Applicants</th>
                              <th style="text-align: center; font-weight: bold;">New Applications for Evaluation</th>
                          </tr>
                      </thead>   
                      <tbody>
                        <?php
                          $z = 1;
                          for($z;$z<=5;$z++){
                            echo '
                            <tr>
                                <td style="text-align: center;">-</td>
                                <td style="text-align: center;">-</td>
                                <td style="text-align: center;">-</td>
                                <td style="text-align: center;">-</td>
                                <td style="text-align: center;">-</td>
                                <td style="text-align: center;">-</td>
                                <td style="text-align: center;">-</td>
                                <td style="text-align: center;">-</td>
                                <td style="text-align: center;">-</td>
                                <td style="text-align: center;">-</td>
                                <td style="text-align: center;">-</td>
                                <td style="text-align: center;">-</td>
                                <td style="text-align: center;">-</td>
                                <td style="text-align: center;">-</td>
                                <td style="text-align: center;">-</td>
                            </tr>';
                          }
                        ?>
                      </tbody>
                    </table>
                    <br>
                    <div><p style="text-align: right; margin-top: 20px; padding-right: 30px; border-top: 1px solid gray; font-weight: bold;">1 / 1</p></div>
                  
                    <div class="modal-footer justify-content-right" style="border: none; padding: 0;">
                      <button style="width:auto; font-size:large;" type="button" class="btn btn-default color-cyan" data-toggle="modal" data-dismiss="modal" data-target="#EMD_rep">
                        <i class="fas fa-print">
                        </i>&nbsp;Print
                      </button>
                    </div>
                  </div>
                  <!--End of EMD Report-->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <footer class="main-footer">
    <?php include('footer.php'); ?>
  </footer>

  <aside class="control-sidebar control-sidebar-dark">
  </aside>
</div>

<script src="../../plugins/jquery/jquery.min.js"></script>
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script src="../../dist/js/adminlte.min.js"></script>
<script src="../../dist/js/demo.js"></script>

</body>
</html>

<style>
  .content {
    -webkit-animation-duration: 0.7s;
    animation-duration: 0.7s;
  }
  
  @-webkit-keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  
  @keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  
  .content {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
  }

  td {
    background: #f5f5f5;
  }
  th {
    cursor: pointer;
  }
</style>
<script language="javascript">
           var message="This function is not allowed here.";
           function clickIE4(){
                 if (event.button==2){
                     alert(message);
                     return false;
                 }
           }
           function clickNS4(e){
                if (document.layers||document.getElementById&&!document.all){
                        if (e.which==2||e.which==3){
                                  alert(message);
                                  return false;
                        }
                }
           }
           if (document.layers){
                 document.captureEvents(Event.MOUSEDOWN);
                 document.onmousedown=clickNS4;
           }
           else if (document.all&&!document.getElementById){
                 document.onmousedown=clickIE4;
           }
           document.oncontextmenu=new Function("alert(message);return false;")
</script>
<?php include('custom_btn.php'); ?>