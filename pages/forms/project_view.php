<!DOCTYPE html>
<html lang="en">
<head>
<?php
include('login_session.php');
if($login_office == "Head Office"){
  if($login_officeID == "h60000000"){
  }
  else if($login_officeID == "h50000000"){
  }
  else{
    header("location:../../index.php");
  }
}
if($login_office == "Regional"){
    header("location:../../index.php");
}
error_reporting(E_ERROR | E_PARSE);
?>
<link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
<link rel="stylesheet" href="../../plugins/toastr/toastr.min.css">
<link rel="stylesheet" href="../../dist/css/adminlte.min.css">
<script src="../../plugins/jquery/jquery.min.js"></script>
<script src="../../plugins/toastr/toastr.min.js"></script>
<?php
if (isset($_SESSION["status"]) && $_SESSION["status"] !='') {
  $msg = $_SESSION["status"];
  echo "<span><script type='text/javascript'>toastr.success('$msg')</script></span>";
  unset($_SESSION["status"]);
}
if (isset($_SESSION["error"]) && $_SESSION["error"] !='') {
  $msg = $_SESSION["error"];
  echo "<span><script type='text/javascript'>toastr.error('$msg')</script></span>";
  unset($_SESSION["error"]);
}
if (isset($_SESSION["info"]) && $_SESSION["info"] !='') {
  $msg = $_SESSION["info"];
  echo "<span><script type='text/javascript'>toastr.info('$msg')</script></span>";
  unset($_SESSION["info"]);
}
else {
}
?>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>NHA | Government Employee's Housing Program</title>
<link rel="shortcut icon" type="image/png" href="../../dist/img/nha_logo_circle.png">

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
<link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
<link rel="stylesheet" href="../../dist/css/adminlte.min.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="font-weight: bold;">
    <?php include("header.php"); ?>
  </nav>

  <aside class="main-sidebar sidebar-dark-primary elevation-4" style="background: #002d32; position: fixed;">
    <a href="../../index.php" class="brand-link">
      <img src="../../dist/img/nha_logo_circle.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"><b>NHA | GEHP</b></span>
    </a>

    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <?php
          $target_dir = "uploads/users/";
          $target_file = $target_dir . basename("$login_image");
          if(file_exists($target_file)){
            echo "<img src='uploads/users/$login_image' class='img-circle elevation-3' alt='User Image' style='height: 33px;'>";
          } 
          else {
            echo "<img src='uploads/users/user.png' width='150px'>";
          }
          ?>
        </div>
        <div class="info">
          <a href="users_profile.php" class="d-block"><?php echo $_SESSION['login_user'] ?></a>
        </div>
      </div>

      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
          <li class="nav-item">
            <a href="../../index.php" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-briefcase"></i>
              <p>
                Applicants
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <input type="text" class="form-control" id="sessionempty" name="sessionempty" value="" placeholder="Search" hidden>
              <li class="nav-item">
                <a href="../forms/registration.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Government Employees</p>
                </a>
              </li>
              <li class="nav-item" <?php echo $deploy;?>>
                <a href="../forms/registration_Uni.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Uniformed Personnel</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/registration_OFW.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>OFW</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item" <?php echo $project_hide ?>>
            <a href="../forms/project_view.php" class="nav-link active" onclick="sessionempty()" style="background: #67a2b2;">
              <i class="nav-icon fas fa-building"></i>
              <p>
                Projects
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $user_hide ?>>
            <a href="../forms/users.php" onclick="sessionempty()" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Users
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $element_hide ?>>
            <a href="../forms/report_gen.php" class="nav-link">
              <i class="nav-icon fas fa-scroll"></i>
              <p>
                Reports
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $pages_hide ?>>
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-clone"></i>
              <p>
                Pages
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              
              <li class="nav-item">
                <a href="../forms/home.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Home</p>
                </a>
              </li>
              <li class="nav-item" hidden>
                <a href="../forms/howToApply.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>How To Apply</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/about.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>About</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/banner.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Banner</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="../forms/bulletin.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Bulletin</p>
                </a>
              </li>
            </ul>
          </li>
          
          <li class="nav-item" <?php echo $inv_hide;?>>
            <a href="../forms/inventory.php" class="nav-link">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                Inventory
              </p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>
  <script>
    function sessionempty() {
      $("#loaderIcon").show();
          jQuery.ajax({
          url: "session_clear.php",
          data: {'sessionempty': $("#sessionempty").val()},
          type: "POST",
          success:function(data){
              $("#").html(data);
              $("#loaderIcon").hide();
          },
          error:function (){
          }
      });
    }
  </script>
  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Projects</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div>
        </div>
      </div>
    </section>
    <form id="deleteProjForm" method="POST" action="projectsDelete_process.php" enctype="multipart/form-data"></form>
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div>
                <input type="text" name="username" value="<?php echo $_SESSION['login_user'] ?>" hidden>
                <a href="addProjects.php" class="btn btn-default color-green-dark" style="float:right; width:auto; font-size:large;">
                  <i class=" fas fa-plus"></i>&nbsp; Add Project
                </a>
                <button <?php if($login_officeID == "h60000000" && $login_userlevel == 'Administrator'){}else{echo $delete_disabled;} ?> type="button" data-toggle="modal"
                data-target="#delete" class="btn btn-default color-red-dark-auto">
                  <i class="fa fa-trash"></i>&nbsp; Delete
                </button>
                <?php include("delete_modalProj.php"); ?>
              </div><br>
              <div class="card card-primary" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                <div class="card-header" style="background: #67a2b2;">
                  <h3 class="card-title"></h3>
                </div>
                <div class="card-body">
                  <div class="col-sm-3">
                    <div class="input-group input-group-sm" style="padding: 7px 7px 0px;">
                      <input type="text" class="form-control" id="checker" name="checker" onkeyup="checksample()"
                       value="<?php echo $_SESSION["projsearch"]; ?>" placeholder="Search: Project Name / Island Group">
                    </div>
                    <script>
                      function checksample() {
                          $("#loaderIcon").show();
                              jQuery.ajax({
                              url: "projects_search.php",
                              data: {'checkers': $("#checker").val()},
                              type: "POST",
                              success:function(data){
                                  $("#checkxxx").html(data);
                                  $("#loaderIcon").hide();
                              },
                              error:function (){
                              }
                          });
                      }
                    </script>
                  </div>
                  <div class="col-sm-12" id="checkxxx">
                  <div style="overflow:auto; height: 650px; padding: 7px;">
                  <table id="projectTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th <?php echo $delete_hide; ?> style="white-space: nowrap; " class="project-actions text-center"><input id="chk_all" name="chk_all" type="checkbox"></th>
                      <th>Name</th>
                      <th>Overview</th>
                      <th>Location</th>
                      <th>Availability</th>
                      <th style="width:60px;">On Hold</th>
                      <th>Island</th>
                      <th>Region</th>
                      <th>Status</th>
                      <th>Image</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                      <?php
                      if(isset($_SESSION["projsearch"])){
                        try{
                          $selectdata=[
                            ':projsearch' => $_SESSION["projsearch"],
                            ':projsearch2' => $_SESSION["projsearch"],
                          ];
                          $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                          $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                          $projectselect = "SELECT * FROM project_info LEFT JOIN units_floor ON project_info.project_id = units_floor.project_id 
                                            LEFT JOIN nearby_estab ON project_info.project_id = nearby_estab.project_id
                                            LEFT JOIN other_files ON project_info.project_id = other_files.project_id
                                            WHERE PJ_NAME LIKE CONCAT('%', :projsearch, '%') OR island LIKE CONCAT('%', :projsearch2, '%')
                                            ORDER BY PJ_NAME";
                          
                          $sthselect = $dbh->prepare($projectselect);
                            $sthselect->execute($selectdata);
                            $count = $sthselect->rowCount();
                            $setcount = $count;
                            if (isset($_GET['pageno'])) {
                              $pageno = $_GET['pageno'];
                              //$i = $_GET['pageno'];
                              if($_GET['pageno'] == 1){
                                $i = 2;
                              }
                            } 
                            else {
                              $pageno = 1;
                              $i = 2;
                            }
                            $no_of_records_per_page = 5;
                            $offset = ($pageno-1) * $no_of_records_per_page;
                      
                            $total_pages = ceil($count / $no_of_records_per_page);
                      
                            $cnt=1;
                            
                            $sql2 = "$projectselect LIMIT $offset, $no_of_records_per_page";
                      
                            $sthsql2 = $dbh->prepare($sql2);
                            $sthsql2->execute($selectdata);
                            $sthsql2->setFetchMode(PDO::FETCH_ASSOC); 
                            $app_count = $sthsql2->rowCount();
                            $setcount = $app_count;
                          $i=0;
                          while ($projectrow = $sthsql2->fetch(PDO::FETCH_ASSOC)) {
                            $region = $projectrow['region'];
                            $regionselect = "SELECT Region FROM region WHERE psgc_reg = :region";
                            $sthregionselect = $dbh->prepare($regionselect);
                            $sthregionselect->bindParam(':region', $region);
                            $sthregionselect->execute();
                            $sthregionselect->setFetchMode(PDO::FETCH_ASSOC); 
                            while ($regionrow = $sthregionselect->fetch(PDO::FETCH_ASSOC)) {$regionshow = $regionrow["Region"];}
                            ?>
                            <tr>
                              <td <?php echo $delete_hide; ?> class="project-actions text-center" style="vertical-align: middle;"><input name="check[]" form="deleteProjForm"
                              type="checkbox" class='chkbox' value="<?php echo $projectrow['project_id']; ?>"/></td>
                              <td style="vertical-align: middle;"> <?php echo $projectrow['PJ_NAME'] ?> </td>
                              <td style="vertical-align: middle;"> <?php echo $projectrow['overview'] ?> </td>
                              <td style="vertical-align: middle;"> <?php echo $projectrow['location'] ?> </td>
                              <td style="vertical-align: middle; text-align:center"> <?php include('availability.php'); ?> </td>
                              <td style="vertical-align: middle; text-align:center"> <?php include('archived.php'); ?> </td>
                              <td style="vertical-align: middle;"> <?php echo $projectrow['island'] ?> </td>
                              <td style="vertical-align: middle;"> <?php echo $regionshow ?> </td>
                              <td style="vertical-align: middle;"> <?php echo $projectrow['project_status'] ?> </td>
                              <td class="project-actions text-center" style="vertical-align: middle;"> 
                              <?php 
                              $main_image = $projectrow['main_image'];
                              $target_dir = "uploads/projects/main_image/";
                              $target_file = $target_dir . basename("$main_image");
                              if(file_exists($target_file)){
                              ?>
                              <img src="uploads/projects/main_image/<?php echo $main_image ?>" style="border-radius: 5px;" width="150px" height="150px"> 
                              <?php
                              } 
                              else
                              {
                                echo "No image available.";
                              }
                              ?>
                              </td>
                              <td style="white-space: nowrap; vertical-align: middle;" class="project-actions text-center">
                                  
                                <a id="tooltip" type="button" href="EditProjects.php?id=<?php echo $projectrow['project_id'] ?>" name="submit" style="font-size:large;" class="btn btn-default btn-sm color-cyan-dark">
                                  <i class="fas fa-edit">
                                  </i>
                                  <span class="tooltiptext">Edit</span>
                                </a>
                              
                                <a id="tooltip" type="button" href="" data-toggle="modal" data-target="#modal-xl<?php echo $projectrow['project_id'] ?>" style="font-size:large;" class="btn btn-default btn-sm color-blue-dark">
                                  <i class="fa fa-eye">
                                  </i>
                                  <span class="tooltiptext">View</span>
                                </a>
                              </td>
                              <?php include("projects_modal.php");?>
                            </tr>
                            <?php    
                            $i++;
                          } 
                        }
                        catch(PDOException $e){
                            error_log('PDOException - ' . $e->getMessage(), 0);
                            http_response_code(500);
                            die('Error establishing connection with database');
                        }
                      }
                      ?>
                    </tbody>
                  </table>
                  </div>
                  <?php include("paging.php"); ?>
                  <?php include("delete_script.php");?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
  </div>
  <footer class="main-footer">
    <?php include('footer.php'); include('custom_btn.php'); ?>
  </footer>
  <aside class="control-sidebar control-sidebar-dark">
  </aside>
</div>
<script src="../../plugins/jquery/jquery.min.js"></script>
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script src="../../dist/js/adminlte.min.js"></script>
<script src="../../dist/js/demo.js"></script>
<script>
  $(function () {
    $("#projectTable").DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": false,
      "responsive": false,
      //"buttons": ["excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#projectTable_wrapper .col-md-6:eq(0)');
  });
</script>
</body>
</html>
<style>
  .content {
    -webkit-animation-duration: 0.7s;
    animation-duration: 0.7s;
  }
  @-webkit-keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  @keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  .content {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
  }
  td {
    background: #f5f5f5;
  }
</style>
<script language="javascript">
  var message="This function is not allowed here.";
  function clickIE4(){
        if (event.button==2){
            alert(message);
            return false;
        }
  }
  function clickNS4(e){
      if (document.layers||document.getElementById&&!document.all){
              if (e.which==2||e.which==3){
                        alert(message);
                        return false;
              }
      }
  }
  if (document.layers){
        document.captureEvents(Event.MOUSEDOWN);
        document.onmousedown=clickNS4;
  }
  else if (document.all&&!document.getElementById){
        document.onmousedown=clickIE4;
  }
  document.oncontextmenu=new Function("alert(message);return false;")
</script>