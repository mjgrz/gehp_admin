<?php
$ID = $_GET['ID'];
$BLK = $_GET['BLK'];
$LOT = $_GET['LOT'];
$PROP = $_GET['PROP'];
$HDRCLASS = $_GET['HDRCLASS'];
$PJID = $_GET['PJID'];
$PJNAME = $_GET['PJNAME'];
// $PJSTRING = $_GET['PJSTRING'];
$IDSELECT = $_GET['IDSELECT'];
$FLOOR = $_GET['FLOOR'];

$propArray = array("Available","Reserved","Awarded","Under Construction");
?>
<div class="modal-header" style="background:#67a2b2;">
  <h4 class="modal-title" style="color:white;">Lot Information - <?php echo $ID; ?></h4>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true" style="color: white;">&times;</span>
  </button>
</div>
<form method="POST" action="inv_update_stat.php">
  <div class="modal-body" style="background: #f6f6f6;">
    <div class="col-sm-12">

      <div class="col-lg-12" style="padding:5px 5px; border-bottom:1px solid lightgray;">
        <div class="row">
          <div class="col-6 pt-2">
            <b>Project Name: </b>
          </div>
          <div class="col-6 pt-2 text-right">
            <p><?php echo $PJNAME; ?></p>
          </div>
        </div>
      </div>

      <div class="col-lg-12" style="padding:5px 5px; border-bottom:1px solid lightgray;">
        <div class="row">
          <div class="col-6 pt-2">
            <b>Status: </b>
          </div>
          <?php if($PROP != "Awarded"){ ?>
          <div class="col-6 text-right">
            <select class="form-control" id="propStatus" onchange="run()">
              <?php
                foreach($propArray as $item){
                ?>
                  <option <?php if($PROP === $item){ echo "selected";} ?> 
                  value="<?php echo $item ?>">
                    <?php echo $item ?>
                  </option>
                <?php
                }
              ?>
            </select>
          </div>
          <?php } else { ?>
            <div class="col-6 pt-2 text-right">
              <p><?php echo $PROP; ?></p>
            </div>
          <?php } ?>
        </div>
      </div>

      <div class="col-lg-12" style="padding:5px 5px; border-bottom:1px solid lightgray;">
        <div class="row">
          <div class="col-6 pt-2">
            <b>Block/Building: </b>
          </div>
          <div class="col-6 pt-2 text-right">
            <p><?php echo $BLK; ?></p>
          </div>
        </div>
      </div>

      <div class="col-lg-12" style="padding:5px 5px; border-bottom:1px solid lightgray;">
        <div class="row">
          <div class="col-6 pt-2">
            <b>Lot/Unit: </b>
          </div>
          <div class="col-6 pt-2 text-right">
            <p><?php echo $LOT; ?></p>
          </div>
        </div>
      </div>

      <div class="col-lg-12" style="padding:5px 5px; border-bottom:1px solid lightgray;">
        <div class="row">
          <div class="col-6 pt-2">
            <b>Floor Area: </b>
          </div>
          <div class="col-6 pt-2 text-right">
            <p>60 SQM</p>
          </div>
        </div>
      </div>
      
      <div class="col-lg-12" style="padding:5px 5px;">
        <div class="row">
          <div class="col-6 pt-2">
            <b>Lot Area: </b>
          </div>
          <div class="col-6 pt-2 text-right">
            <p>80 SQM</p>
          </div>
        </div>
      </div>
      <div hidden>
        <textarea id="listTxtEditSlot" name="listTxtEditSlot" class="form-control" ></textarea>
        <input type="text" id="idTxt" name="idTxt" value="<?php echo $IDSELECT; ?>" class="form-control" ></input>
        <input type="text" id="pjidTxt" name="pjidTxt" value="<?php echo $PJID; ?>" class="form-control" ></input>
        <input type="text" id="floorTxt" name="floorTxt" value="<?php echo $FLOOR; ?>" class="form-control" ></input>
      </div>
    </div>
  </div>
  <div class="modal-footer justify-content-right" <?php if($PROP != "Awarded"){}else{echo "hidden";}?>>
    <button type="submit" class="btn btn-default color-cyan"
    style="font-size:large; width:auto;">
    <i class="fa fa-paper-plane"></i>&nbsp; Submit</button>
  </div>
</form>

<?php include('custom_btn.php'); ?>

<script>
function run() {
  var ARRAY = JSON.parse(document.getElementById('listTxtEditSlot').value);
  var ID = '<?php echo $ID ?>';
  var BLK = '<?php echo $BLK ?>';
  var LOT = '<?php echo $LOT ?>';

  var STAT = document.getElementById('propStatus').value

  var ITEM = {id: +LOT, blocknum: +BLK, lotnum: +LOT, prop: STAT};
  var UPDATED = ARRAY[BLK-1].items.splice(LOT-1, 1, ITEM);
  document.getElementById('listTxtEditSlot').value = JSON.stringify(ARRAY);
}
</script>