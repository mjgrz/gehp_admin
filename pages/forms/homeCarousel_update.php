<?php
session_start();
date_default_timezone_set("Asia/Hong_Kong");
$time = date("h:i a");
$date = date('F d, Y', strtotime(date("Y-m-d")));
$datetime = $date." ".$time;
$ext = $_POST["hc_id"];
$target_dir = "uploads/homeCarousel/";
$target_file1 = $target_dir . basename("banner-home-$ext.png");
$uploadOk1 = 1;
$filename1 = "banner-home-$ext.png";
$imageFileType1 = strtolower(pathinfo($target_file1,PATHINFO_EXTENSION));
include('../../../../dbcon.php'); 
  if(isset($_POST["submit"])) {
    $check1 = getimagesize($_FILES["slide_image2"]["tmp_name"]);
    if($check1 !== false) {
      $_SESSION["info"] = "File is an image - " . $check1["mime"] . ".";
      $uploadOk1 = 1;
    } else {
      $_SESSION["error"] = "File is not an image.";
      $uploadOk1 = 0;
    }
  }

  if ($_FILES["slide_image2"]["size"] <= 5000000) {
    if (file_exists($target_file1)) {
      unlink($target_file1);
      $uploadOk1 = 1;
    }
  }

  if ($_FILES["slide_image2"]["size"] > 5000000) {
    $_SESSION["error"] = "Sorry, your file is too large.";
    $uploadOk1 = 0;
  }

  if($imageFileType1 != "jpg" && $imageFileType1 != "png" && $imageFileType1 != "jpeg"
  && $imageFileType1 != "gif" && $imageFileType1 != "") {
    $_SESSION["error"] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk1 = 0;
  }

  if ($uploadOk1 == 0) {
    header('Location: ../forms/home.php');
  } else {
    $hc_id = $_POST["hc_id"];
    if ( is_numeric($hc_id) == true){
      try{
        $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sqlupdatedata = [
          ':filename1' => $filename1,
          ':hc_id' => $hc_id
        ];
        $sqlupdate = "UPDATE homecarousel SET hc_image=:filename1 WHERE hc_id=:hc_id";
        $sthsqlupdate = $dbh->prepare($sqlupdate);
        if (move_uploaded_file($_FILES["slide_image2"]["tmp_name"], $target_file1) && $sthsqlupdate->execute($sqlupdatedata)) {
            $auditdata = [
              ':activity' => "Edited a Slide on Carousel Banner",
              ':username' => $_SESSION['login_user'],
              ':datetime' => $datetime
            ];
            $audit = "INSERT INTO audit_trail (activity, username, date) VALUES (:activity, :username, :datetime)";
            $sthaudit = $dbh->prepare($audit);
            $sthaudit->execute($auditdata);
            $_SESSION["status"] = "Your data have been saved successfully.";
            header('Location: ../forms/home.php');
            $dbh = null;
        } 
        else 
        {
            $_SESSION["error"] = "Sorry, your data were not saved.";
            header('Location: ../forms/home.php');
            $dbh = null;
        }
      }
      catch(PDOException $e){
        error_log('PDOException - ' . $e->getMessage(), 0);
        http_response_code(500);
        die('Error establishing connection with database');
      }
    } 
    else{
    http_response_code(400);
    die('Error processing bad or malformed request');
    }
  }
?>

