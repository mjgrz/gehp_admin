<script>
    function checkuser_emailAvailabilityEdit() {
        $("#loaderIcon").show();
            jQuery.ajax({
            url: "user_emailSearchEdit.php",
            data: {'user_email': $("#email").val(), 'userid': $("#userid").val()},
            type: "POST",
            success:function(data){
                $("#user_email-availability-status").html(data);
                $("#loaderIcon").hide();
            },
            error:function (){
            }
        });
    }
</script>