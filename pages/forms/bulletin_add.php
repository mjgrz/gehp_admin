<?php
session_start();
date_default_timezone_set("Asia/Hong_Kong");
$title = $_POST["idrowcount"];
$date = date("Y-m-d");

$time = date("h:i a");
$dates = date('F d, Y', strtotime(date("Y-m-d")));
$datetime = $dates." ".$time;

$target_dir = "uploads/bulletin/";
$target_file = $target_dir . basename("bulletin"."-"."$title.png");
$uploadOk = 1;
$filename = "bulletin"."-"."$title.png";
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
include('../../../../dbcon.php');

if(isset($_POST["submit"])) {
  
  $check = getimagesize($_FILES["bulletin_image"]["tmp_name"]);
  if($check !== false) {
    $_SESSION["info"] = "File is an image - " . $check["mime"] . ".";
    $uploadOk = 1;

  } else {
    $_SESSION["error"] = "File is not an image.";
    $uploadOk = 0;
  }
}

if (file_exists($target_file)) {
  unlink($target_file);
  $uploadOk = 0;
}

if ($_FILES["bulletin_image"]["size"] > 5000000) {
  $_SESSION["error"] = "Sorry, your file is too large.";
  $uploadOk = 0;
}

if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
  $_SESSION["error"] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
  $uploadOk = 0;
}

if ($uploadOk == 0) {
  header('Location: ../forms/bulletin.php');

} else {

$category = $_POST["category"];
$bulletin_title = $_POST["bulletin_title"];
$bulletin_description = $_POST["bulletin_desc"];
$dates = date('Y-m-d', strtotime($_POST["date"]));
$uploader = $_POST["uploaderadd"];
  
  if (move_uploaded_file($_FILES["bulletin_image"]["tmp_name"], $target_file)) {
    try{
      $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $sqldata = [
        ':title' => $title,  
        ':category' => $category, 
        ':hc_title' => $bulletin_title, 
        ':hc_description' => $bulletin_description, 
        ':filename' => $filename, 
        ':uploader' => $uploader, 
        ':dates' => $dates
      ];
      $sql = "INSERT INTO bulletin (bulletin_id, bulletin_category, bulletin_title,	bulletin_desc, bulletin_image, uploader, date_upload)
      VALUES (:title, :category, :hc_title, :hc_description, :filename, :uploader, :dates)";
      $sthsql = $dbh->prepare($sql);
  
      if ($sthsql->execute($sqldata)) {
        $audit = "INSERT INTO audit_trail (activity, username, date) VALUES ('Added a Content on Bulletin (With Image)', '".$_SESSION['login_user']."', '$datetime')";
        mysqli_query($conn, $audit);

        $_SESSION["status"] = "Your data have been saved successfully.";
        header('Location: ../forms/bulletin.php');
        $dbh = null;
      }
      else {
        $_SESSION["error"] = "Sorry, your data were not saved.";
        header('Location: ../forms/bulletin.php');
        $dbh = null;
      }
    }
    catch(PDOException $e){
      error_log('PDOException - ' . $e->getMessage(), 0);
      http_response_code(500);
      die('Error establishing connection with database');
    }
  } 
  else {
    try{
      $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $sqldata = [
        ':title' => $title,  
        ':category' => $category, 
        ':hc_title' => $bulletin_title, 
        ':hc_description' => $bulletin_description, 
        ':filename' => $filename, 
        ':uploader' => $uploader, 
        ':dates' => $dates
      ];
      $sql = "INSERT INTO bulletin (bulletin_id, bulletin_category, bulletin_title,	bulletin_desc, bulletin_image, uploader, date_upload)
      VALUES (:title, :category, :hc_title, :hc_description, :filename, :uploader, :dates)";
      $sthsql = $dbh->prepare($sql);

      if ($sthsql->execute($sqldata)) {
        $auditdata = [
          ':activity' => "Added a Content on Bulletin (Without Image)",
          ':username' => $_SESSION['login_user'],
          ':datetime' => $datetime
        ];
        $audit = "INSERT INTO audit_trail (activity, username, date) VALUES (:activity, :username, :datetime)";
        $sthaudit = $dbh->prepare($audit);
        $sthaudit->execute($auditdata);

        $_SESSION["status"] = "Your data have been saved successfully.";
        header('Location: ../forms/bulletin.php');
        $dbh = null;
      }
      else {
        $_SESSION["error"] = "Sorry, your data were not saved.";
        header('Location: ../forms/bulletin.php');
        $dbh = null;
      }
    }
    catch(PDOException $e){
      error_log('PDOException - ' . $e->getMessage(), 0);
      http_response_code(500);
      die('Error establishing connection with database');
    }
  }
}
?>