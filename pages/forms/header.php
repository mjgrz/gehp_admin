<ul class="navbar-nav">
  <li class="nav-item">
    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
  </li>
  <li class="nav-item">
    <a href="../../index.php" class="nav-link">Home</a>
  </li>
</ul>

<ul class="navbar-nav ml-auto" style="color: gray;">
  <li class="nav-item">
    <a href="../../logout_session.php" class="nav-link">Logout</a>
  </li>
</ul>
