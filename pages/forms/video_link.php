<div class="modal fade" id="videoLink">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 5px;">
      <div class="modal-header" style="background: #67a2b2;">
        <h4 class="modal-title" style="color: white;">Attach Video Link</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: white;">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="col-sm-12">
            <input type="text"class="form-control"  id="uploadVid" value="<?php if(isset($videolink)){ echo $videolink; }else{}?>" name="uploadVid" required>
        </div>
      </div>
      <div class="modal-footer justify-content-right">
        <a class="btn btn-default color-cyan" style="width:auto;" data-dismiss="modal" onclick="videopreview()">Preview</a>
      </div>
    </div>
  </div>
</div>

<script>
  function videopreview() {
    var fileInput = document.getElementById('uploadVid').value;
    var stringGet = fileInput.substring(fileInput.indexOf('=') + 1);
    $("#video").attr("src", "https://www.youtube.com/embed/"+stringGet);
    document.getElementById("loader").style.display = "block";
  };
</script>