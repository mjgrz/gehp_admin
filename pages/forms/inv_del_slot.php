<?php
session_start();
$ID = $_POST['idTxt'];
$FLOOR = $_POST['floorTxt'];

include("../../../../dbcon.php"); 
$dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$querypjselect = "DELETE FROM inventory WHERE project_id = '$ID' AND floor_num = '$FLOOR'";
$sthpj = $dbh->prepare($querypjselect);
if($sthpj->execute()){
    $_SESSION["status"] = "Record has been deleted successfully.";
    if($FLOOR > 1){
        header('Location: ../forms/inventory_info.php?ID='.$ID.'&FLOOR=1');
    }
    else{
        header('Location: ../forms/inventory.php');
    }
}
else{
    $_SESSION["error"] = "Sorry, this record was not deleted.";
    header('Location: ../forms/inventory_info.php?ID='.$ID.'&FLOOR=1');
}
?>