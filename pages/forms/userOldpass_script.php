<script>
    function checkOldpass() {
        $("#loaderIcon").show();
        jQuery.ajax({
            url: "userOldpass_search.php",
            data:'old_password='+$("#old_password").val(),
            type: "POST",
            success:function(data){
                $("#old_pass-status").html(data);
                $("#loaderIcon").hide();
            },
            error:function (){
            }
        });
    }
</script>