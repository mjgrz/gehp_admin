<?php
  session_start();
  if(isset($_POST["toggleAvail2"])){
    $toggleAvail = $_POST["toggleAvail2"];
    $archTog_sql = "UPDATE project_info SET archived = :toggleAvail, availability = 'Unavailable' WHERE project_id = :toggleID";
  }
  if(!isset($_POST["toggleAvail2"])){
    $toggleAvail = '0';
    $archTog_sql = "UPDATE project_info SET archived = :toggleAvail WHERE project_id = :toggleID";
  }
  $toggleID = $_POST["toggleID2"];
  include("../../../../dbcon.php");
  try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $archTog_data = [
      ':toggleAvail' => $toggleAvail,
      ':toggleID' => $toggleID
    ];
    $stharchTog_sql = $dbh->prepare($archTog_sql);
    if($stharchTog_sql->execute($archTog_data)){
      $_SESSION["status"] = "Your data have been saved successfully.";
      header('Location: ../forms/project_view.php');
      $dbh = null;
    }
    else{
      $_SESSION["error"] = "Sorry, your data were not saved.";
      header('Location: ../forms/project_view.php');
      $dbh = null;
    }
  }
  catch(PDOException $e){
    error_log('PDOException - ' . $e->getMessage(), 0);
    http_response_code(500);
    die('Error establishing connection with database');
  }
?>