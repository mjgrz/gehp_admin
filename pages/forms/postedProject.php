<?php
include("../../../../dbcon.php");
try{
  $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
  $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $postselect = "SELECT * FROM posted_project";
  $sthpostselect = $dbh->prepare($postselect);
  $sthpostselect->execute();
  $sthpostselect->setFetchMode(PDO::FETCH_ASSOC); 
  if($sthpostselect->rowCount() > 0){
    while ($postrow = $sthpostselect->fetch(PDO::FETCH_ASSOC)) {
      $post_title = $postrow['post_title'];
      $project_name = $postrow['project_name'];
      $post_desc = $postrow['post_desc'];
      $post_image1 = $postrow['post_image1'];
      $post_image2 = $postrow['post_image2'];
      $post_image3 = $postrow['post_image3'];
      $post_image4 = $postrow['post_image4'];
      $post_image5 = $postrow['post_image5'];
    } 
    $dbh = null;
  }
  if($sthpostselect->rowCount() == 0)
  {
    $post_title = "";
    $project_name = "";
    $post_desc = "";
    $post_image1 = "";
    $post_image2 = "";
    $post_image3 = "";
    $post_image4 = "";
    $post_image5 = "";
    $dbh = null;
  }
}
catch(PDOException $e){
    error_log('PDOException - ' . $e->getMessage(), 0);
    http_response_code(500);
    die('Error establishing connection with database');
}
?>
<div class="card-body">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-primary" style="box-shadow: none; border: 1px solid #67a2b2;">
        <!--Submit Form for editing posted project on home page-->
        <form method="POST" action="postedProject_update.php" enctype="multipart/form-data">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <div class="form-group">
                  <label for="hc_title">Title of Post</label>
                  <input type="text" class="form-control" id="posted_title" name="posted_title"
                  oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $post_title ?>">
                </div>
                <div class="form-group">
                  <label for="hc_title">Name of Project</label>
                  <input type="text" class="form-control" id="project_name" name="project_name"
                  oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $project_name ?>">
                </div>
                <div class="form-group">
                  <label for="hc_description">Description</label>
                  <textarea class="form-control" id="posted_desc" name="posted_desc" style="resize: none; padding: 10px; height: 258px;"
                  oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $post_desc ?>"><?php echo $post_desc ?></textarea>
                </div>
              </div>

              <div class="col-sm-8">
                <div class="col-md-12" style="margin-top: 32px;">
                  <div class="col-md-12">
                    <div class="form-group" id="uploadDiv2">
                      <input type="text" class="form-control" id="postedImage1text" name="postedImage1text" value="<?php echo $post_image1 ?>" hidden>
                      <input type="file" id="postedImage1" name="postedImage1" title="Click in this area to upload an image." onchange="postedPreview1()">
                      <p id="posteduploadText1"></p>
                    </div>
                  </div>
                </div>
                
                <div class="col-12" style="margin-top: 30px;">
                  <div class="row">
                    <div class="col-sm-3">
                      <div class="col-12">
                        <div class="form-group" id="uploadDiv2">
                          <input type="text" class="form-control" id="postedImage2text" name="postedImage2text" value="<?php echo $post_image2 ?>" hidden>
                          <input type="file" id="postedImage2" name="postedImage2" title="Click in this area to upload an image." style="width: 93%;" onchange="postedPreview2()">
                          <p id="posteduploadText2"></p>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="col-md-12">
                        <div class="form-group" id="uploadDiv2">
                          <input type="text" class="form-control" id="postedImage3text" name="postedImage3text" value="<?php echo $post_image3 ?>" hidden>
                          <input type="file" id="postedImage3" name="postedImage3" title="Click in this area to upload an image." style="width: 93%;" onchange="postedPreview3()">
                          <p id="posteduploadText3"></p>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="col-md-12">
                        <div class="form-group" id="uploadDiv2">
                          <input type="text" class="form-control" id="postedImage4text" name="postedImage4text" value="<?php echo $post_image4 ?>" hidden>
                          <input type="file" id="postedImage4" name="postedImage4" title="Click in this area to upload an image." style="width: 93%;" onchange="postedPreview4()">
                          <p id="posteduploadText4"></p>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="col-md-12">
                        <div class="form-group" id="uploadDiv2">
                          <input type="text" class="form-control" id="postedImage5text" name="postedImage5text" value="<?php echo $post_image5 ?>" hidden>
                          <input type="file" id="postedImage5" name="postedImage5" title="Click in this area to upload an image." style="width: 93%;" onchange="postedPreview5()">
                          <p id="posteduploadText5"></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer">
            <div class="project-actions text-right">
              <button style="width:auto; font-size:large;" type="submit" class="btn btn-default color-cyan-dark"><i class="fa fa-paper-plane"></i>&nbsp; Submit</button>
            </div>
          </div>
        </form>
        <!--End of Submit Form for editing posted project on home page-->
      </div>
    </div>
  </div>
</div>             
<style>
  #uploadDiv2 {
    width: 100%;
    height: 200px;
    border: 2px dashed #67a2b2;
    border-radius: 5px;
  }
  #postedImage5, #postedImage4, #postedImage3, #postedImage2, #postedImage1 {
    position: absolute;
    margin: 0;
    padding: 0;
    width: 98%;
    height: 100%;
    outline: none;
    opacity: 0;
  }
  #postedImage5:hover, #postedImage4:hover, #postedImage3:hover, #postedImage2:hover, #postedImage1:hover {
    cursor: pointer;
  }
  #posteduploadText1, #posteduploadText2, #posteduploadText3, #posteduploadText4, #posteduploadText5 {
    text-align: center;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 5px;
  }
  #posteduploadText1 {
    background-image: url(uploads/postedProject/<?php echo $post_image1 ?>);
    background-position: center;
    background-repeat: no-repeat;
    background-size: auto 100%;
  }
  #posteduploadText2 {
    background-image: url(uploads/postedProject/<?php echo $post_image2 ?>);
    background-position: center;
    background-repeat: no-repeat;
    background-size: auto 100%;
  }
  #posteduploadText3 {
    background-image: url(uploads/postedProject/<?php echo $post_image3 ?>);
    background-position: center;
    background-repeat: no-repeat;
    background-size: auto 100%;
  }
  #posteduploadText4 {
    background-image: url(uploads/postedProject/<?php echo $post_image4 ?>);
    background-position: center;
    background-repeat: no-repeat;
    background-size: auto 100%;
  }
  #posteduploadText5 {
    background-image: url(uploads/postedProject/<?php echo $post_image5 ?>);
    background-position: center;
    background-repeat: no-repeat;
    background-size: auto 100%;
  }
  .content {
    -webkit-animation-duration: 0.7s;
    animation-duration: 0.7s;
  }
  @-webkit-keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  @keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  .content {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
  }
  input:hover, select:hover, textarea:hover {
    border: 1px solid #67a2b2;
  }
</style>
<script>
  $(function () {
    bsCustomFileInput.init();
  });
  function postedPreview1() {
    var urlimage = URL.createObjectURL(event.target.files[0]);
    document.getElementById("posteduploadText1").style.backgroundImage = "url("+urlimage+")";// specify the image path here
    document.getElementById("posteduploadText1").style.backgroundSize = "auto 100%";
    document.getElementById("posteduploadText1").style.backgroundRepeat = "no-repeat";
    document.getElementById("posteduploadText1").style.backgroundPosition = "center";
    document.getElementById("posteduploadText1").innerHTML = "";
  }
  function postedPreview2() {
    var urlimage = URL.createObjectURL(event.target.files[0]);
    document.getElementById("posteduploadText2").style.backgroundImage = "url("+urlimage+")";// specify the image path here
    document.getElementById("posteduploadText2").style.backgroundSize = "auto 100%";
    document.getElementById("posteduploadText2").style.backgroundRepeat = "no-repeat";
    document.getElementById("posteduploadText2").style.backgroundPosition = "center";
    document.getElementById("posteduploadText2").innerHTML = "";
  }
  function postedPreview3() {
    var urlimage = URL.createObjectURL(event.target.files[0]);
    document.getElementById("posteduploadText3").style.backgroundImage = "url("+urlimage+")";// specify the image path here
    document.getElementById("posteduploadText3").style.backgroundSize = "auto 100%";
    document.getElementById("posteduploadText3").style.backgroundRepeat = "no-repeat";
    document.getElementById("posteduploadText3").style.backgroundPosition = "center";
    document.getElementById("posteduploadText3").innerHTML = "";
  }
  function postedPreview4() {
    var urlimage = URL.createObjectURL(event.target.files[0]);
    document.getElementById("posteduploadText4").style.backgroundImage = "url("+urlimage+")";// specify the image path here
    document.getElementById("posteduploadText4").style.backgroundSize = "auto 100%";
    document.getElementById("posteduploadText4").style.backgroundRepeat = "no-repeat";
    document.getElementById("posteduploadText4").style.backgroundPosition = "center";
    document.getElementById("posteduploadText4").innerHTML = "";
  }
  function postedPreview5() {
    var urlimage = URL.createObjectURL(event.target.files[0]);
    document.getElementById("posteduploadText5").style.backgroundImage = "url("+urlimage+")";// specify the image path here
    document.getElementById("posteduploadText5").style.backgroundSize = "auto 100%";
    document.getElementById("posteduploadText5").style.backgroundRepeat = "no-repeat";
    document.getElementById("posteduploadText5").style.backgroundPosition = "center";
    document.getElementById("posteduploadText5").innerHTML = "";
  }
</script>