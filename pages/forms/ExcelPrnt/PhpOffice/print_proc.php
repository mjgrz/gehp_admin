<?php
session_start();
require "../../../../../../dbcon.php";
$fromdate = $_SESSION['fromdate'];
$todate = $_SESSION['todate'];
try{
    $proj_array = array();
    $counts_gov_array = array();
    $counts_ofw_array = array();
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    $sqlproj = "SELECT PJ_CODE, PJ_NAME, '$fromdate' as fromdate, '$todate' as todate FROM project_info ORDER BY PJ_NAME";
    $project = $dbh->query($sqlproj);
    foreach ($project as $rowproj) { 
        $appselect = "SELECT (SELECT COUNT(*) FROM applications LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
                    WHERE app_status = 'Qualified' AND source = 'Website/Frontend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate') as Qualified_front,
                    (SELECT COUNT(*) FROM applications LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
                    WHERE app_status = 'Disqualified' AND source = 'Website/Frontend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate') as Disqualified_front,
                    (SELECT COUNT(*) FROM applications LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
                    WHERE app_status = 'New Applicant' AND source = 'Website/Frontend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate'
                    OR app_status = 'Pending' AND source = 'Website/Frontend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate') as New_Applicant_front,
                    (SELECT COUNT(*) FROM applications LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
                    WHERE app_status = 'Qualified' AND source = 'Website/Backend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND 
                    app_date BETWEEN '$fromdate' AND '$todate') as Qualified_back,
                    (SELECT COUNT(*) FROM applications LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
                    WHERE app_status = 'Disqualified' AND source = 'Website/Backend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate') as Disqualified_back,
                    (SELECT COUNT(*) FROM applications LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
                    WHERE app_status = 'New Applicant' AND source = 'Website/Backend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate'
                    OR app_status = 'Pending' AND source = 'Website/Backend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate') as New_Applicant_back";
        $sthappselect = $dbh->prepare($appselect);
        $sthappselect->execute();
        $sthappselect->setFetchMode(PDO::FETCH_ASSOC); 
            while ($row = $sthappselect->fetch(PDO::FETCH_ASSOC)) {
                $counts_gov_array[] = array('Qualified_front' => $row['Qualified_front'], 
                            'Disqualified_front' => $row['Disqualified_front'],
                            'New_Applicant_front' => $row['New_Applicant_front'],
                            'Qualified_back' => $row['Qualified_back'],
                            'Disqualified_back' => $row['Disqualified_back'],
                            'New_Applicant_back' => $row['New_Applicant_back'],);
            }

        $appselectofw = "SELECT (SELECT COUNT(*) FROM ofw_applications LEFT JOIN project_info ON ofw_applications.PJ_CODE = project_info.PJ_CODE
                    WHERE app_status = 'Qualified' AND source = 'Website/Frontend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate') as Qualifiedofw_front,
                    (SELECT COUNT(*) FROM ofw_applications LEFT JOIN project_info ON ofw_applications.PJ_CODE = project_info.PJ_CODE
                    WHERE app_status = 'Disqualified' AND source = 'Website/Frontend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate') as Disqualifiedofw_front,
                    (SELECT COUNT(*) FROM ofw_applications LEFT JOIN project_info ON ofw_applications.PJ_CODE = project_info.PJ_CODE
                    WHERE app_status = 'New Applicant' AND source = 'Website/Frontend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate'
                    OR app_status = 'Pending' AND source = 'Website/Frontend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate') as New_Applicantofw_front,
                    (SELECT COUNT(*) FROM ofw_applications LEFT JOIN project_info ON ofw_applications.PJ_CODE = project_info.PJ_CODE
                    WHERE app_status = 'Qualified' AND source = 'Website/Backend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate') as Qualifiedofw_back,
                    (SELECT COUNT(*) FROM ofw_applications LEFT JOIN project_info ON ofw_applications.PJ_CODE = project_info.PJ_CODE
                    WHERE app_status = 'Disqualified' AND source = 'Website/Backend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate') as Disqualifiedofw_back,
                    (SELECT COUNT(*) FROM ofw_applications LEFT JOIN project_info ON ofw_applications.PJ_CODE = project_info.PJ_CODE
                    WHERE app_status = 'New Applicant' AND source = 'Website/Backend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate'
                    OR app_status = 'Pending' AND source = 'Website/Backend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate') as New_Applicantofw_back";
        $sthappselectofw = $dbh->prepare($appselectofw);
        $sthappselectofw->execute();
        $sthappselectofw->setFetchMode(PDO::FETCH_ASSOC); 
            while ($rowofw = $sthappselectofw->fetch(PDO::FETCH_ASSOC)) {
                $counts_ofw_array[] = array('Qualifiedofw_front' => $rowofw['Qualifiedofw_front'], 
                            'Disqualifiedofw_front' => $rowofw['Disqualifiedofw_front'],
                            'New_Applicantofw_front' => $rowofw['New_Applicantofw_front'],
                            'Qualifiedofw_back' => $rowofw['Qualifiedofw_back'],
                            'Disqualifiedofw_back' => $rowofw['Disqualifiedofw_back'],
                            'New_Applicantofw_back' => $rowofw['New_Applicantofw_back'],);
            }
            
            $proj_array[] = array('PJ_NAME' => $rowproj['PJ_NAME'],
                            'fromdate' => $rowproj['fromdate'],
                            'todate' => $rowproj['todate'],);
            
    }
    $_SESSION['proj_array'] = $proj_array;
    $_SESSION['counts_ofw_array'] = $counts_ofw_array;
    $_SESSION['counts_gov_array'] = $counts_gov_array;
    $dbh = null;
    echo "<script> location.href='download.php'; 
        setTimeout(function() {
            window.close()
        }, 500);
    </script>";
    exit;
}
catch(PDOException $e){
    error_log('PDOException - ' . $e->getMessage(), 0);
    http_response_code(500);
    die('Error establishing connection with database');
}
?>
