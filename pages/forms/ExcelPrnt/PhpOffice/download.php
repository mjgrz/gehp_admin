<?php
namespace PhpOffice;
include "autoload.php";
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use PhpOffice\PhpSpreadsheet\Worksheet\PageMargins;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

    $today = date("Y-m-d");
    $CntDisposition = "Content-Disposition: attachment;filename=\"";
    $CntDisposition = $CntDisposition . $today . "\"_sample.xlsx";
    header($CntDisposition);
    header('Cache-Control: max-age=0');

    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $sheet->getPageSetup()->setFitToPage(true);
    $sheet->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
    $sheet->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_LEGAL);

    $sheet->getPageMargins()->setTop(0.5);
    $sheet->getPageMargins()->setRight(0.28);
    $sheet->getPageMargins()->setLeft(0.28);
    $sheet->getPageMargins()->setBottom(0.5);
    
    $spreadsheet->getActiveSheet()->getColumnDimension("A")->setWidth(26);
    $spreadsheet->getActiveSheet()->getColumnDimension("B")->setWidth(13);
    $spreadsheet->getActiveSheet()->getColumnDimension("C")->setWidth(13);
    $spreadsheet->getActiveSheet()->getColumnDimension("D")->setWidth(13);
    $spreadsheet->getActiveSheet()->getColumnDimension("E")->setWidth(13);
    $spreadsheet->getActiveSheet()->getColumnDimension("F")->setWidth(13);
    $spreadsheet->getActiveSheet()->getColumnDimension("G")->setWidth(13);
    $spreadsheet->getActiveSheet()->getColumnDimension("H")->setWidth(13);
    $spreadsheet->getActiveSheet()->getColumnDimension("I")->setWidth(13);
    $spreadsheet->getActiveSheet()->getColumnDimension("J")->setWidth(13);
    $spreadsheet->getActiveSheet()->getColumnDimension("K")->setWidth(13);
    $spreadsheet->getActiveSheet()->getColumnDimension("L")->setWidth(13);
    $spreadsheet->getActiveSheet()->getColumnDimension("M")->setWidth(13);
    $spreadsheet->getActiveSheet()->getColumnDimension("N")->setWidth(13);
    $spreadsheet->getActiveSheet()->getColumnDimension("O")->setWidth(13);
    $spreadsheet->getActiveSheet()->getRowDimension(1)->setRowHeight(20);
    $spreadsheet->getActiveSheet()->getRowDimension(2)->setRowHeight(20);
    $spreadsheet->getActiveSheet()->getRowDimension(3)->setRowHeight(27);
    $spreadsheet->getActiveSheet()->getRowDimension(6)->setRowHeight(50);
    $spreadsheet->getActiveSheet()->mergeCells("F1:I1");
    $spreadsheet->getActiveSheet()->mergeCells("E1:E3");
    $spreadsheet->getActiveSheet()->mergeCells("J1:J3");
    $spreadsheet->getActiveSheet()->mergeCells("F2:I2");
    $spreadsheet->getActiveSheet()->mergeCells("F3:I3");
    $spreadsheet->getActiveSheet()->mergeCells("D4:I4");
    $spreadsheet->getActiveSheet()->mergeCells("J4:O4");
    $spreadsheet->getActiveSheet()->mergeCells("D5:F5");
    $spreadsheet->getActiveSheet()->mergeCells("G5:I5");
    $spreadsheet->getActiveSheet()->mergeCells("J5:L5");
    $spreadsheet->getActiveSheet()->mergeCells("M5:O5");
    $spreadsheet->getActiveSheet()->mergeCells("A4:A6");
    $spreadsheet->getActiveSheet()->mergeCells("B4:B6");
    $spreadsheet->getActiveSheet()->mergeCells("C4:C6");
    $spreadsheet->getActiveSheet()->getStyle('B1:D3')->getAlignment()->setWrapText(true); 
    $spreadsheet->getActiveSheet()->getStyle('B1:D3')->getAlignment()->setWrapText(true);
    $spreadsheet->getActiveSheet()->getStyle('A4:A6')->getAlignment()->setWrapText(true); 
    $spreadsheet->getActiveSheet()->getStyle('B4:B6')->getAlignment()->setWrapText(true); 
    $spreadsheet->getActiveSheet()->getStyle('C4:C6')->getAlignment()->setWrapText(true); 
    $spreadsheet->getActiveSheet()->getStyle('D6:O6')->getAlignment()->setWrapText(true); 
    $spreadsheet->getActiveSheet()->getStyle('A1:O6')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); 
    $spreadsheet->getActiveSheet()->getStyle('A1:O6')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER); 
    $spreadsheet->getActiveSheet()->getStyle('F1')->getAlignment()->setVertical(Alignment::VERTICAL_BOTTOM); 
    $spreadsheet->getActiveSheet()->getStyle('F3')->getAlignment()->setVertical(Alignment::VERTICAL_TOP); 
    
    $drawing = new Drawing();
    $drawing->setName('nha-logo');
    $drawing->setDescription('nha-logo');
    $drawing->setPath('../../TCPDF-main/examples/images/nha-logo.png');
    $drawing->setCoordinates('E1');
    $drawing->setOffsetX(50);
    $drawing->setOffsetY(15);
    $drawing->setRotation(0);
    $drawing->setHeight(50);
    $drawing->setWorksheet($spreadsheet->getActiveSheet());
    
    $drawing2 = new Drawing();
    $drawing2->setName('balai_temp');
    $drawing2->setDescription('balai_temp');
    $drawing2->setPath('../../TCPDF-main/examples/images/balai_temp.png');
    $drawing2->setCoordinates('J1');
    $drawing2->setOffsetX(0);
    $drawing2->setOffsetY(15);
    $drawing2->setRotation(0);
    $drawing2->setHeight(50);
    $drawing2->setWorksheet($spreadsheet->getActiveSheet());

    $sheet->setCellValue('A4', 'Project Name');
    $sheet->setCellValue('B4', 'Remaining No. of Available Lots/Units');
    $sheet->setCellValue('C4', 'Total Qualified Applicants GEHP and OFW');
    $sheet->setCellValue('D4', 'Online Application');
    $sheet->setCellValue('J4', 'Manual Application');
    $sheet->setCellValue('D5', 'GEHP');
    $sheet->setCellValue('G5', 'OFW');
    $sheet->setCellValue('J5', 'GEHP');
    $sheet->setCellValue('M5', 'OFW');
    $sheet->setCellValue('D6', 'Qualified Applicants');
    $sheet->setCellValue('E6', 'Disqualified Applicants');
    $sheet->setCellValue('F6', 'New Applications for Evaluation');
    $sheet->setCellValue('G6', 'Qualified Applicants');
    $sheet->setCellValue('H6', 'Disqualified Applicants');
    $sheet->setCellValue('I6', 'New Applications for Evaluation');
    $sheet->setCellValue('J6', 'Qualified Applicants');
    $sheet->setCellValue('K6', 'Disqualified Applicants');
    $sheet->setCellValue('L6', 'New Applications for Evaluation');
    $sheet->setCellValue('M6', 'Qualified Applicants');
    $sheet->setCellValue('N6', 'Disqualified Applicants');
    $sheet->setCellValue('O6', 'New Applications for Evaluation');

    $spreadsheet->getDefaultStyle()->getFont()->setSize(12);
    $spreadsheet
    ->getActiveSheet()
    ->getStyle("A4:O6")
    ->getBorders()
    ->getAllBorders()
    ->setBorderStyle(Border::BORDER_THIN);

    session_start();
    $i = 7;
    $i1 = 7;
    $i2 = 7;
    foreach($_SESSION['counts_gov_array'] as $rowgov => $setgov){
        $sheet->setCellValue('D'.$i1, $setgov['Qualified_front']);
        $sheet->setCellValue('E'.$i1, $setgov['Disqualified_front']);
        $sheet->setCellValue('F'.$i1, $setgov['New_Applicant_front']);
        $sheet->setCellValue('G'.$i1, $setgov['Qualified_back']);
        $sheet->setCellValue('H'.$i1, $setgov['Disqualified_back']);
        $sheet->setCellValue('I'.$i1, $setgov['New_Applicant_back']);
        $i1++;
    }
    foreach($_SESSION['counts_ofw_array'] as $rowofw => $setofw){
        $sheet->setCellValue('J'.$i2, $setofw['Qualifiedofw_front']);
        $sheet->setCellValue('K'.$i2, $setofw['Disqualifiedofw_front']);
        $sheet->setCellValue('L'.$i2, $setofw['New_Applicantofw_front']);
        $sheet->setCellValue('M'.$i2, $setofw['Qualifiedofw_back']);
        $sheet->setCellValue('N'.$i2, $setofw['Disqualifiedofw_back']);
        $sheet->setCellValue('O'.$i2, $setofw['New_Applicantofw_back']);
        $i2++;
    }
    foreach($_SESSION['proj_array'] as $rowproj => $setproj){
        $sheet->setCellValue('A'.$i, $setproj['PJ_NAME']);
        $sheet->setCellValue('C'.$i, '=SUM(D'.$i.',G'.$i.',J'.$i.',M'.$i.')');
        $sheet->getStyle('A'.$i.':O'.$i)
        ->getBorders()
        ->getAllBorders()
        ->setBorderStyle(Border::BORDER_THIN);
        $sheet->getStyle('A'.$i.':O'.$i)->getAlignment()->setWrapText(true); 
        $sheet->getStyle('B'.$i.':O'.$i)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); 
        $sheet->getStyle('B'.$i.':O'.$i)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER); 
        $fromdate = $setproj['fromdate'];
        $todate = $setproj['todate'];
        $i++;
    }
    $sheet->setCellValue('F1', 'National Housing Authority');
    $sheet->setCellValue('F2', 'GOVERNMENT EMPLOYEE HOUSING PROGRAM');
    $sheet->setCellValue('F3', date("F d, Y", strtotime($fromdate)).' - '.date("F d, Y", strtotime($todate)));
    $writer = new Xlsx($spreadsheet);
    #$writer->save('hello world.xlsx');
    $writer->save('php://output');
?>
