<?php
  include("../../../../dbcon.php");
  session_start();
  date_default_timezone_set("Asia/Hong_Kong");
  $time = date("h:i a");
  $date = date('F d, Y', strtotime(date("Y-m-d")));
  $datetime = $date." ".$time;
  $APP_ID = $_POST["APP_ID"];
  $usernam = $_POST["username"];
  $filename = $_POST["filename"];
  if(!isset($_POST['checkerOEC'])){
    $target_dir = "uploads/OEC_uploads/";
    $target_filevideo = $target_dir . basename("$filename");
    $uploadOkvideo = 1;
    $video = "$filename";
    $imageFileTypevideo = strtolower(pathinfo($target_filevideo,PATHINFO_EXTENSION));

    if (file_exists($target_filevideo)) {
      unlink($target_filevideo);
    }

    if ($_FILES["file"]["size"] > 20000000) {
      $_SESSION['error'] = "Sorry, your file is too large.";
      $uploadOkvideo = 0;
    }

    if($imageFileTypevideo != "jpeg" && $imageFileTypevideo != "pdf" && $imageFileTypevideo != "png") {
      $_SESSION['error'] = "Sorry, only docx and pdf files are allowed.";
      $uploadOkvideo = 0;
    }

    if ($uploadOkvideo == 0) {
      header('Location: registration_OFW.php');
    } 

    else {
      if ( is_numeric($APP_ID) == true){
        try{
          $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
          $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $sqldata = [
            ':username' => $usernam,
            ':filename' => $filename,
            ':APP_ID' => $APP_ID
          ];
          $sql = "UPDATE ofw_applications SET username = :username, fileUpload = :filename WHERE APP_ID = :APP_ID";
          $sthsql = $dbh->prepare($sql);
          if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_filevideo) && $sthsql->execute($sqldata)) {
            $auditdata = [
              ':activity' => "Updated the OEC File (With Scanned Copy)",
              ':username' => $_SESSION['login_user'],
              ':datetime' => $datetime
            ];
            $audit = "INSERT INTO audit_trail_applications (activity, username, date) VALUES (:activity, :username, :datetime)";
            $sthaudit = $dbh->prepare($audit);
            $sthaudit->execute($auditdata);
            $_SESSION["status"] = "Application's OEC File has been changed successfully.";
            header('Location: registration_OFW.php');
            $dbh = null;
          }
          else {  
            $_SESSION["error"] = "Sorry, unable to change OEC File.";
            header('Location: registration_OFW.php');
            $dbh = null;
          }
        }
        catch(PDOException $e){
          error_log('PDOException - ' . $e->getMessage(), 0);
          http_response_code(500);
          die('Error establishing connection with database');
        }
      } 
      else{
        http_response_code(400);
        die('Error processing bad or malformed request');
      }
    }
  }
  if(isset($_POST['checkerOEC'])){
    if ( is_numeric($APP_ID) == true){
      try{
        $checkerOEC = $_POST['checkerOEC'];
        $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sqldata = [
          ':username' => $usernam,
          ':checkerOEC' => $checkerOEC,
          ':APP_ID' => $APP_ID
        ];
        $sql = "UPDATE ofw_applications SET username = :username, fileUpload = :checkerOEC WHERE APP_ID = :APP_ID";
        $sthsql = $dbh->prepare($sql);
        if ($sthsql->execute($sqldata)) {
          $auditdata = [
            ':activity' => "Updated the OEC File (With Hard Copy)",
            ':username' => $_SESSION['login_user'],
            ':datetime' => $datetime
          ];
          $audit = "INSERT INTO audit_trail_applications (activity, username, date) VALUES (:activity, :username, :datetime)";
          $sthaudit = $dbh->prepare($audit);
          $sthaudit->execute($auditdata);
          $_SESSION["status"] = "Application's OEC File has been changed successfully.";
          header('Location: registration_OFW.php');
          $dbh = null;
        }
        else {  
          $_SESSION["error"] = "Sorry, unable to change OEC File.";
          header('Location: registration_OFW.php');
          $dbh = null;
        }
      }
      catch(PDOException $e){
        error_log('PDOException - ' . $e->getMessage(), 0);
        http_response_code(500);
        die('Error establishing connection with database');
      }
    } 
    else{
      http_response_code(400);
      die('Error processing bad or malformed request');
    }
  }
?>