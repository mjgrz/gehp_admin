<div class="card-body">
  <div class="row">
    <div class="col-md-6" style="margin-bottom: 20px; margin-top: 12px;">
      <table id="hc" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>Slide Number</th>
          <th>Image Slide</th>
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
          include("../../../../dbcon.php");
          try{
            $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $hcSelect = "SELECT hc_id, hc_image FROM homecarousel";
            $sthhcSelect = $dbh->prepare($hcSelect);
            $sthhcSelect->execute();
            $sthhcSelect->setFetchMode(PDO::FETCH_ASSOC); 
            if($sthhcSelect->rowCount() > 0){
              while ($hcRow = $sthhcSelect->fetch(PDO::FETCH_ASSOC)) { 
              ?>
              <tr>
              <td> <?php echo $hcRow['hc_id'] ?> </td>
              <td> <?php echo $hcRow['hc_image'] ?> </td>
              <td class="project-actions text-center">
                <a style="font-size:large;" id="tooltip" type="button" data-toggle="modal" data-target="#modal<?php echo $hcRow['hc_id'] ?>" class="btn btn-default btn-sm color-cyan-dark">  
                  <i class="fas fa-edit">
                  </i>
                  <span class="tooltiptext">View / Edit</span>
              </a>
              </td>
              <?php include("homeCarousel_edit.php"); ?>
              </tr>
              <?php
              } 
              $dbh = null;
            }
            else
            {}
          }
          catch(PDOException $e){
            error_log('PDOException - ' . $e->getMessage(), 0);
            http_response_code(500);
            die('Error establishing connection with database');
          }
        ?>
        </tbody>
      </table>
    </div>
    <div class="col-md-6">
      <div class="row">
        <div class="col-sm-12">
          <fieldset style="border: 1px solid #67a2b2; border-radius: 5px;">
            <legend style="width: min-content; white-space: nowrap; margin-left: 10px; padding: 0px 5px 0px 5px;">
              Additional Slide
            </legend>
            <div class="card card-primary" style="box-shadow: none;">
              <!--Submit Form for adding carousel slide-->
              <form method="POST" action="homeCarousel_slide.php" enctype="multipart/form-data">
                <div class="card-body">
                  <div class="col-sm-12">
                    <div class="form-group" id="uploadDiv1">
                      <?php
                      include("../../../../dbcon.php");
                      try{
                        $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        $countslide = "SELECT hc_id FROM homecarousel";
                        $sthcountslide = $dbh->prepare($countslide);
                        $sthcountslide->execute();
                        $CL = $sthcountslide->rowCount() + 1;
                        $dbh = null;
                      }
                      catch(PDOException $e){
                        error_log('PDOException - ' . $e->getMessage(), 0);
                        http_response_code(500);
                        die('Error establishing connection with database');
                      }
                      ?>
                      <input type="text" name="CL" value="<?php echo $CL ?>" hidden>
                      <input type="file" id="slide_image" name="slide_image" title="Click in this area to upload an image." onchange="preview()" required>
                      <p  id="textimage">Click in this area to upload an image.</p>
                    </div>
                  </div>
                </div>
                <div class="card-footer" style="margin-bottom: -20px;">
                  <div class="project-actions text-right">
                    <button style="width:auto; font-size:large;" type="submit" class="btn btn-default color-cyan-dark"><i class="fa fa-paper-plane"></i>&nbsp; Submit</button>
                  </div>
                </div>
              </form> 
              <!--End of Submit Form for adding carousel slide-->
            </div>
          </fieldset>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(function () {
  bsCustomFileInput.init();
});
function preview() {
  var urlimage = URL.createObjectURL(event.target.files[0]);
  document.getElementById("textimage").style.backgroundImage = "url("+urlimage+")"
  document.getElementById("textimage").style.backgroundSize = "100% auto";
  document.getElementById("textimage").style.backgroundRepeat = "no-repeat";
  document.getElementById("textimage").style.backgroundPosition = "center";
  document.getElementById("textimage").innerHTML = "";
}
$(function () {
  $("#hc").DataTable({
    "paging": false,
    "lengthChange": false,
    "searching": false,
    "ordering": true,
    "info": false,
    "autoWidth": false,
    "responsive": true,
    "buttons": [""]
  }).buttons().container().appendTo('#hc_wrapper .col-md-6:eq(0)');
});
</script>
<style>
  #uploadDiv1 {
    width: 100%;
    height: 228px;
    border: 2px dashed #67a2b2;
    border-radius: 5px;  
  }
  #slide_image {
    position: absolute;
    margin: 0;
    padding: 0;
    width: 98%;
    height: 100%;
    outline: none;
    opacity: 0;
  }
  #slide_image:hover {
    cursor: pointer;
  }
  #textimage, #textimageUpdate {
    text-align: center;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 5px;
  }
  #textimage {
    background-position: center;
    background-repeat: no-repeat;
    background-size: 100% auto;
  }
  .content {
    -webkit-animation-duration: 0.7s;
    animation-duration: 0.7s;
  }
  @-webkit-keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  @keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  .content {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
  }
  td {
    background: #f5f5f5;
  }
  input:hover, select:hover, textarea:hover {
    border: 1px solid #67a2b2;
  }
</style>
<?php include('custom_btn.php'); ?>