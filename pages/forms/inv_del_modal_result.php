<?php
  $EDITID = $SLCT_PJID;
  $EDITFLR = $SLCT_FLOOR;
  $EDITPJNAME = $name;
?>
<div class="modal-header" style="background:#67a2b2;">
  <h4 class="modal-title" style="color:white;">Delete - Floor <?php echo $EDITFLR; ?></h4>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true" style="color: white;">&times;</span>
  </button>
</div>
<form method="POST" action="inv_del_slot.php">
  <div class="modal-body" style="background: #f6f6f6;">
    <div class="col-sm-12">

      <div class="col-sm-12">
        <div class="col-lg-12">
          <div class="form-group">
            <p style="font-size: 20px;">Are you sure you want to delete this record?</p>
          </div>
        </div>
      </div>

      <div hidden>
        <input type="text" id="idTxt" name="idTxt" value="<?php echo $EDITID; ?>" class="form-control" ></input>
        <input type="text" id="floorTxt" name="floorTxt" value="<?php echo $EDITFLR; ?>" class="form-control" ></input>
        <input type="text" id="prjtTxt" name="prjtTxt" value="<?php echo $EDITPJNAME; ?>" class="form-control" ></input>
      </div>
    </div>
  </div>
  <div class="modal-footer justify-content-right">
    <button type="submit" class="btn btn-default color-red"
    style="font-size:large; width:auto;">
    <i class="fa fa-paper-plane"></i>&nbsp; Delete</button>
  </div>
</form>

<?php include('custom_btn.php'); ?>