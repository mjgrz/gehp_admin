<?php
include("../../../../dbcon.php"); 
session_start();
$dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$PJTXT = $_POST['editID'];
$FLRNUM = $_POST['editFLR'];
$LISTTXT = $_POST['listEditTxt'];

$queryslct =    "SELECT * FROM project_info WHERE project_id = '$PJTXT'";
$sthpj = $dbh->prepare($queryslct);
$sthpj->execute();
$sthpj->setFetchMode(PDO::FETCH_ASSOC); 
while ($rowpj = $sthpj->fetch(PDO::FETCH_ASSOC)) {
    $STR = $rowpj['PJ_NAME'];
    $PJ_NAME = str_replace(" ", "-", "$STR");
}

$target_dir = "uploads/inventory/";
$target_file = $target_dir . basename("$PJ_NAME"."-"."$FLRNUM".".jpg");
$uploadOk = 1;
$filename = "$PJ_NAME"."-"."$FLRNUM".".jpg";
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

if(!isset($_POST['checkMap'])) {
    if (file_exists($target_file)) {
        unlink($target_file);
    }
    
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "") {
        $_SESSION["error"] = "Sorry, only JPG, JPEG, and PNG files are allowed.";
        $uploadOk = 0;
    }
    
    if ($uploadOk == 0) {
        header('Location: ../forms/inventory_info.php?ID='.$PJTXT.'&FLOOR='.$FLRNUM);
    }
    
    else {
        try {
            $querysbmt = "UPDATE inventory SET slot_list = '$LISTTXT', slot_map = '$filename' 
                          WHERE project_id = '$PJTXT' AND floor_num = '$FLRNUM'";
            $sthinv = $dbh->prepare($querysbmt);
            if(move_uploaded_file($_FILES["builtMapFile"]["tmp_name"], $target_file) && $sthinv->execute()){
                $_SESSION["status"] = "Your map inventory has been changed successfully.";
                header('Location: ../forms/inventory_info.php?ID='.$PJTXT.'&FLOOR='.$FLRNUM);
                $dbh = null;
            }
            else{
                $_SESSION["error"] = "Sorry, unable to change map inventory.";
                header('Location: ../forms/inventory_info.php?ID='.$PJTXT.'&FLOOR='.$FLRNUM);
                $dbh = null;
            }
        }
        catch (PDOException $e) {
            error_log('PDOException - ' . $e->getMessage(), 0);
            http_response_code(500);
            die('Sorry, someting went wrong.');
        }
    }
}
else {
    try {
        $querysbmt = "UPDATE inventory SET slot_list = '$LISTTXT'  
                      WHERE project_id = '$PJTXT' AND floor_num = '$FLRNUM'";
        $sthinv = $dbh->prepare($querysbmt);
        if($sthinv->execute()){
            $_SESSION["status"] = "Your map inventory has been changed successfully.";
            header('Location: ../forms/inventory_info.php?ID='.$PJTXT.'&FLOOR='.$FLRNUM);
            $dbh = null;
        }
        else{
            $_SESSION["error"] = "Sorry, unable to change map inventory.";
            header('Location: ../forms/inventory_info.php?ID='.$PJTXT.'&FLOOR='.$FLRNUM);
            $dbh = null;
        }
    }
    catch (PDOException $e) {
        error_log('PDOException - ' . $e->getMessage(), 0);
        http_response_code(500);
        die('Sorry, someting went wrong.');
    }
}
?>