<?php
session_start();
date_default_timezone_set("Asia/Hong_Kong");
$date = date("Y-m-d");
$id = $_POST["id"];

$time = date("h:i a");
$dates = date('F d, Y', strtotime(date("Y-m-d")));
$datetime = $dates." ".$time;

$target_dir = "uploads/bulletin/";
$target_file1 = $target_dir . basename("bulletin"."-"."$id.png");
$uploadOk1 = 1;
$filename1 = "bulletin"."-"."$id.png";
$imageFileType1 = strtolower(pathinfo($target_file1,PATHINFO_EXTENSION));
include('../../../../dbcon.php'); 

  if(isset($_POST["submit"])) {
    
    $check1 = getimagesize($_FILES["bulletin_image_update"]["tmp_name"]);
    if($check1 !== false) {
      $_SESSION["info"] = "File is an image - " . $check1["mime"] . ".";
      $uploadOk1 = 1;
    } else {
      $_SESSION["error"] = "File is not an image.";
      $uploadOk1 = 0;
    }
  }

  if($_FILES["bulletin_image_update"]["size"] <= 5000000) {
    if (file_exists($target_file1)) {
      unlink($target_file1);
    }
  }

  if ($_FILES["bulletin_image_update"]["size"] > 5000000) {
    $_SESSION["error"] = "Sorry, your file is too large.";
    $uploadOk1 = 0;
  }

  if($imageFileType1 != "jpg" && $imageFileType1 != "png" && $imageFileType1 != "jpeg"
  && $imageFileType1 != "gif" && $imageFileType1 != "") {
    $_SESSION["error"] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk1 = 0;
  }

  if ($uploadOk1 == 0) {
    header('Location: ../forms/bulletin.php');

  } else {

    $bulletin_cat = $_POST["category"];
    $bulletin_title = $_POST["bulletin_title_update"];
    $bulletin_desc = $_POST["bulletin_desc_update"];
    $bulletin_id = $_POST["bulletin_id_update"];
    $bulletin_date = date('Y-m-d', strtotime($_POST["date"]));
    $uploader = $_POST["uploader"];
    

    if (move_uploaded_file($_FILES["bulletin_image_update"]["tmp_name"], $target_file1)) {
      if (is_numeric($bulletin_id) == true){
        try{
          $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
          $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $sqlupdatedata = [
            ':bulletin_cat' => $bulletin_cat,
            ':bulletin_title' => $bulletin_title,
            ':bulletin_desc' => $bulletin_desc,
            ':filename1' => $filename1,
            ':uploader' => $uploader,
            ':bulletin_date' => $bulletin_date,
            ':bulletin_id' => $bulletin_id
          ];
          $sqlupdate = "UPDATE bulletin SET bulletin_category=:bulletin_cat, bulletin_title=:bulletin_title, 
          bulletin_desc=:bulletin_desc, bulletin_image=:filename1, uploader=:uploader, date_upload=:bulletin_date WHERE bulletin_id=:bulletin_id";
          $sthsqlupdate = $dbh->prepare($sqlupdate);
          
          if ($sthsqlupdate->execute($sqlupdatedata)) {
            $auditdata = [
              ':activity' => "Edited a Content on Bulletin (With Image)",
              ':username' => $_SESSION['login_user'],
              ':datetime' => $datetime
            ];
            $audit = "INSERT INTO audit_trail (activity, username, date) VALUES (:activity, :username, :datetime)";
            $sthaudit = $dbh->prepare($audit);
            $sthaudit->execute($auditdata);

            $_SESSION["status"] = "Your data have been saved successfully.";
            header('Location: ../forms/bulletin.php');
            $dbh = null;
          }
          else {
            $_SESSION["error"] = "Sorry, your data were not saved.";
            header('Location: ../forms/bulletin.php');
            $dbh = null;
          }
        }
        catch(PDOException $e){
          error_log('PDOException - ' . $e->getMessage(), 0);
          http_response_code(500);
          die('Error establishing connection with database');
        }
      } 
      else{
      http_response_code(400);
      die('Error processing bad or malformed request');
      }
    } 
    else 
    {
      if (is_numeric($bulletin_id) == true){
        try{
          $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
          $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $sqlupdatedata = [
            ':bulletin_cat' => $bulletin_cat,
            ':bulletin_title' => $bulletin_title,
            ':bulletin_desc' => $bulletin_desc,
            ':filename1' => $filename1,
            ':uploader' => $uploader,
            ':bulletin_date' => $bulletin_date,
            ':bulletin_id' => $bulletin_id
          ];
      
          $sqlupdate = "UPDATE bulletin SET bulletin_category=:bulletin_cat, bulletin_title=:bulletin_title, 
          bulletin_desc=:bulletin_desc, bulletin_image=:filename1, uploader=:uploader, date_upload=:bulletin_date WHERE bulletin_id=:bulletin_id";
          $sthsqlupdate = $dbh->prepare($sqlupdate);

          if ($sthsqlupdate->execute($sqlupdatedata)) {
            $auditdata = [
              ':activity' => "Added a Content on Bulletin (Without Image)",
              ':username' => $_SESSION['login_user'],
              ':datetime' => $datetime
            ];
            $audit = "INSERT INTO audit_trail (activity, username, date) VALUES (:activity, :username, :datetime)";
            $sthaudit = $dbh->prepare($audit);
            $sthaudit->execute($auditdata);

            $_SESSION["status"] = "Your data have been saved successfully.";
            header('Location: ../forms/bulletin.php');
            $dbh = null;
          }
          else {
            $_SESSION["error"] = "Sorry, your data were not saved.";
            header('Location: ../forms/bulletin.php');
            $dbh = null;
          }
        }
        catch(PDOException $e){
          error_log('PDOException - ' . $e->getMessage(), 0);
          http_response_code(500);
          die('Error establishing connection with database');
        }
      } 
      else{
      http_response_code(400);
      die('Error processing bad or malformed request');
      }
    }
  }
?>



