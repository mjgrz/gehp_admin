<?php
$project_name_final = str_replace(" ", "-", "$project_name");
$target_dir = "uploads/projects/maps/";
$target_filemap1 = $target_dir . basename("$project_name_final-map1.png");
$target_filemap2 = $target_dir . basename("$project_name_final-map2.png");
$uploadOkmap1 = 1;
$uploadOkmap2 = 1;
$map1 = "$project_name_final.map1-png";
$map2 = "$project_name_final.map2-png";
$imageFileTypemap1 = strtolower(pathinfo($target_filemap1,PATHINFO_EXTENSION));
$imageFileTypemap2 = strtolower(pathinfo($target_filemap2,PATHINFO_EXTENSION));

  if(isset($_POST["submit"])) {
    
    $checkmap1 = getimagesize($_FILES["map_imgUp1"]["tmp_name"]);
    if($checkmap1 !== false) {
      echo "File is an image - " . $checkmap1["mime"] . ".";
      $uploadOkmap1 = 1;
    } else {
      echo "File is not an image.";
      $uploadOkmap1 = 0;
    }
  }

  if ($_FILES["map_imgUp1"]["size"] <= 50000000) {
    if (file_exists($target_filemap1)) {
      unlink($target_filemap1);
    }
  }

  if ($_FILES["map_imgUp1"]["size"] > 50000000) {
    echo "Sorry, your file is too large.";
    $uploadOkmap1 = 0;
  }

  if($imageFileTypemap1 != "jpg" && $imageFileTypemap1 != "png" && $imageFileTypemap1 != "jpeg"
  && $imageFileTypemap1 != "gif" && $imageFileTypemap1 != "") {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOkmap1 = 0;
  }

  if ($uploadOkmap1 == 0) {
    echo "Sorry, your file was not uploaded.";
  } else {

    if (move_uploaded_file($_FILES["map_imgUp1"]["tmp_name"], $target_filemap1)) {
    } 
    else 
    {
    }
  }
//<--------- END ---------->
  if(isset($_POST["submit"])) {
    
    $checkmap2 = getimagesize($_FILES["map_imgUp2"]["tmp_name"]);
    if($checkmap2 !== false) {
      echo "File is an image - " . $checkmap2["mime"] . ".";
      $uploadOkmap2 = 1;
    } else {
      echo "File is not an image.";
      $uploadOkmap2 = 0;
    }
  }

  if ($_FILES["map_imgUp2"]["size"] <= 50000000) {
    if (file_exists($target_filemap2)) {
      unlink($target_filemap2);
    }
  }

  if ($_FILES["map_imgUp2"]["size"] > 50000000) {
    echo "Sorry, your file is too large.";
    $uploadOkmap2 = 0;
  }

  if($imageFileTypemap2 != "jpg" && $imageFileTypemap2 != "png" && $imageFileTypemap2 != "jpeg"
  && $imageFileTypemap2 != "gif" && $imageFileTypemap2 != "") {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOkmap2 = 0;
  }

  if ($uploadOkmap2 == 0) {
    echo "Sorry, your file was not uploaded.";
  } else {

    if (move_uploaded_file($_FILES["map_imgUp2"]["tmp_name"], $target_filemap2)) {
    } 
    else 
    {
    }
  }
?>