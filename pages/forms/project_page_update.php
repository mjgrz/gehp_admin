<?php
session_start();
date_default_timezone_set("Asia/Hong_Kong");
$time = date("h:i a");
$date = date('F d, Y', strtotime(date("Y-m-d")));
$datetime = $date." ".$time;
  
$target_dir = "uploads/banner/";
$target_file = $target_dir . basename("banner-projectpage1.png");
$uploadOk = 1;
$filename = "banner-projectpage1.png";
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
include('../../../../dbcon.php');

if(isset($_POST["submit"])) {
  
  $check = getimagesize($_FILES["pro_uploadImage"]["tmp_name"]);
  if($check !== false) {
    $_SESSION["info"] = "File is an image - " . $check["mime"] . ".";
    $uploadOk = 1;
  } else {
    $_SESSION["error"] = "File is not an image.";
    $uploadOk = 0;
  }
}

if ($_FILES["pro_uploadImage"]["size"] <= 5000000) {
  if (file_exists($target_file)) {
    unlink($target_file);
  }
}

if ($_FILES["pro_uploadImage"]["size"] > 5000000) {
  $_SESSION["error"] = "Sorry, your file is too large.";
  $uploadOk = 0;
}

if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" && $imageFileType != "") {
  $_SESSION["error"] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
  $uploadOk = 0;
}

if ($uploadOk == 0) {
  header('Location: ../forms/banner.php');

} else {

$project_banner = $_POST["project_banner"];
$project_desc = $_POST["project_desc"];
$idproj = 1;
  
  if (move_uploaded_file($_FILES["pro_uploadImage"]["tmp_name"], $target_file)) {
    if ( is_numeric($idproj) == true){
      try{
        $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sqlprojectdata = [
          ':project_banner' => $project_banner,
          ':project_desc' => $project_desc,
          ':filename' => $filename,
          ':idproj' => $idproj
        ];
        $sqlproject = "UPDATE banner SET banner_title=:project_banner, banner_desc=:project_desc, banner_image=:filename WHERE id=:idproj";
        $sthsqlproject = $dbh->prepare($sqlproject);
        
        if ($sthsqlproject->execute($sqlprojectdata)) {
          $auditdata = [
            ':activity' => "Edited the Project_Page (With Image/Banner)",
            ':username' => $_SESSION['login_user'],
            ':datetime' => $datetime
          ];
          $audit = "INSERT INTO audit_trail (activity, username, date) VALUES (:activity, :username, :datetime)";
          $sthaudit = $dbh->prepare($audit);
          $sthaudit->execute($auditdata);

          $_SESSION["status"] = "Your data have been saved successfully.";
          header('Location: ../forms/banner.php');
          $dbh = null;
        }
        else {
          $_SESSION["error"] = "Sorry, your data were not saved.";
          header('Location: ../forms/banner.php');
          $dbh = null;
        }
      }
      catch(PDOException $e){
        error_log('PDOException - ' . $e->getMessage(), 0);
        http_response_code(500);
        die('Error establishing connection with database');
      }
    } 
    else{
      http_response_code(400);
      die('Error processing bad or malformed request');
    }
  } 
  else {
    if ( is_numeric($idproj) == true){
      try{
        $project_banner = $_POST["project_banner"];
        $project_desc = $_POST["project_desc"];
        $project_image = $_POST["pro_uploadImage_text"];
        $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sqlprojectdata = [
          ':project_banner' => $project_banner,
          ':project_desc' => $project_desc,
          ':filename' => $filename,
          ':idproj' => $idproj
        ];
        
        $sqlproject = "UPDATE banner SET banner_title='$project_banner', banner_desc='$project_desc', banner_image='$filename' WHERE id='1'";
        $sthsqlproject = $dbh->prepare($sqlproject);
        if ($sthsqlproject->execute($sqlprojectdata)) {
          $auditdata = [
            ':activity' => "Edited the Project_Page (Without Image/Banner)",
            ':username' => $_SESSION['login_user'],
            ':datetime' => $datetime
          ];
          $audit = "INSERT INTO audit_trail (activity, username, date) VALUES (:activity, :username, :datetime)";
          $sthaudit = $dbh->prepare($audit);
          $sthaudit->execute($auditdata);

          $_SESSION["status"] = "Your data have been saved successfully.";
          header('Location: ../forms/banner.php');
          $dbh = null;
        }
        else {
          $_SESSION["error"] = "Sorry, your data were not saved.";
          header('Location: ../forms/banner.php');
          $dbh = null;
        }
      }
      catch(PDOException $e){
        error_log('PDOException - ' . $e->getMessage(), 0);
        http_response_code(500);
        die('Error establishing connection with database');
      }
    } 
    else{
      http_response_code(400);
      die('Error processing bad or malformed request');
    }
  }
}
?>