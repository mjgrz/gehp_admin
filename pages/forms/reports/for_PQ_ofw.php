<?Php
require "../../../../../dbcon.php";
require_once('../TCPDF-main/tcpdf.php');

$pdf = new TCPDF('l', 'mm', 'USLEGAL', true, 'UTF-8', false); 

$pdf->SetTitle('Pre-Qualification List');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);

$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

$pdf->SetMargins(10, 10, 10);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

$pdf->AddPage();


$project = $_POST['project'];
$fromdate = $_POST['fromdate'];
$todate = $_POST['todate'];
$sort = $_POST['sort'];
try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $projectselect = "SELECT * FROM project_info 
                    LEFT JOIN units_floor ON project_info.project_id = units_floor.project_id
                    LEFT JOIN region ON project_info.region = region.psgc_reg
                    WHERE PJ_NAME = :project";
    $sthprojectselect = $dbh->prepare($projectselect);
    $sthprojectselect->bindParam(':project', $project);
    $sthprojectselect->execute();
    $sthprojectselect->setFetchMode(PDO::FETCH_ASSOC); 

    while ($rowproj = $sthprojectselect->fetch(PDO::FETCH_ASSOC)) {
        $Region = $rowproj["Region"];
        $PJ_NAME = $rowproj["PJ_NAME"];
        $location = $rowproj["location"];
    }
}
catch(PDOException $e){
error_log('PDOException - ' . $e->getMessage(), 0);
http_response_code(500);
die('Error establishing connection with database');
}

$pdf->SetFont ('helvetica', '', 8);
$header1 = '<div><img src="../TCPDF-main/examples/images/header1.png" style="width: 500px; text-align: center;">
            </div>';
$tbl1 .= '
        <table cellspacing="0" cellpadding="1" border="1" style="width: 100%;">
            <thead>
                <tr>
                    <th style="text-align: left; width: 15%; font-weight: bold;">  Project Proponent:</th>
                    <td style="text-align: left; width: 60%;">  National Housing Authority</td>
                    <th rowspan="4" style="text-align: center; width: 25%; font-weight: bold;"><div><br>Date Prepared:<br><span style="font-weight: normal;">'. date('F d, Y') .'</span></div></th>
                </tr>
                <tr>
                    <th style="font-weight: bold;">  Region:</th>
                    <td style="">  '. $Region .'</td>
                </tr>
                <tr>
                    <th style="font-weight: bold;">  Project Name:</th>
                    <td style="">  '. $PJ_NAME .'</td>
                </tr>
                <tr>
                    <th style="font-weight: bold;">  Location:</th>
                    <td style="">  '. $location .'</td>
                </tr>
            </thead>
        </table>
        ';
$break = '<div></div>';
$header = '
        <table cellspacing="0" cellpadding="2" border="1" style="width: 100%;">
            <thead>         
                <tr>
                    <th rowspan="2" style="text-align: center; font-weight: bold; width: 2%;"><div><br>No.</div></th>
                    <th colspan="3" style="text-align: center; font-weight: bold; width: 18%;">Name of Household Head<br>(Maiden Name for married female applicant)</th>
                    <th rowspan="2" style="text-align: center; font-weight: bold; width: 6.75%;"><div><br>Address</div></th>
                    <th rowspan="2" style="text-align: center; font-weight: bold;"><div><br>Civil Status</div></th>
                    <th rowspan="2" style="text-align: center; font-weight: bold; width: 4%;"><div><br>Sex</div></th>
                    <th rowspan="2" style="text-align: center; font-weight: bold;"><div><br>Birthdate</div></th>
                    <th rowspan="2" style="text-align: center; font-weight: bold; width: 2.5%;"><div><br>Age</div></th>
                    <th rowspan="2" style="text-align: center; font-weight: bold;"><div>Gross Monthly Income</div></th>
                    <th rowspan="2" style="text-align: center; font-weight: bold; width: 6%;"><div><br>Contact Number</div></th>
                    <th colspan="3" style="text-align: center; font-weight: bold; width: 18%;">Name of Spouse/Co-Owner<br>(Maiden Name for married female applicant)</th>
                    <th rowspan="2" style="text-align: center; font-weight: bold;"><div><br>Birthdate</div></th>
                    <th rowspan="2" style="text-align: center; font-weight: bold; width: 2.5%;"><div><br>Age</div></th>
                    <th colspan="3" style="text-align: center; font-weight: bold; width: 19.2%;">Employment Status</th>
                </tr>
                <tr>
                    <th style="text-align: center; font-weight: bold;"><div>Last Name</div></th>
                    <th style="text-align: center; font-weight: bold;"><div>First Name</div></th>
                    <th style="text-align: center; font-weight: bold;"><div>Middle Name</div></th>
                    <th style="text-align: center; font-weight: bold;"><div>Last Name</div></th>
                    <th style="text-align: center; font-weight: bold;"><div>First Name</div></th>
                    <th style="text-align: center; font-weight: bold;"><div>Middle Name</div></th>
                    <th style="text-align: center; font-weight: bold; width: 9.3%;">Employer / Agency</th>
                    <th style="text-align: center; font-weight: bold; width: 9.9%;">OEC Number</th>
                </tr>
            </thead>   ';
$footer = '</table>';

try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $officeselectdata = [
        ':fromdate' => $fromdate,
        ':todate' => $todate,
        ':project' => $project,
    ];
    $officeselect = "SELECT APP_ID, app_date, FNAME, LNAME, MNAME, BDATE, ADDR1, CSTATUS, sex, contact, SLNAME, SFNAME, SMNAME, SBDATE, EMPLOYER, emp_address, MINCOME
                    FROM ofw_applications
                    LEFT JOIN ofw_applicant_info ON ofw_applications.applicant_id = ofw_applicant_info.applicant_id
                    LEFT JOIN ofw_applicant_spouse ON ofw_applications.spouse_id = ofw_applicant_spouse.spouse_id
                    LEFT JOIN project_info ON ofw_applications.PJ_CODE = project_info.PJ_CODE
                    LEFT JOIN ofw_employment_status ON ofw_applications.employment_id = ofw_employment_status.employment_id
                    WHERE app_date BETWEEN :fromdate AND :todate AND PJ_NAME = :project
                    ORDER BY $sort";
    $sthofficeselect = $dbh->prepare($officeselect);
    $sthofficeselect->execute($officeselectdata);
    $sthofficeselect->setFetchMode(PDO::FETCH_ASSOC); 
    
    if($sthofficeselect->rowCount() > 0){
        $i = 0;
        while ($row = $sthofficeselect->fetch(PDO::FETCH_ASSOC)) {
            $i++;
            $dateOfBirth = $row["BDATE"];
            $diff = date_diff(date_create($dateOfBirth), date_create(date("Y-m-d")));
            if($row["SBDATE"] == ""){
                $SdateOfBirth = "";
                $Sdiff = "";
                $SdiffRes = "";
            }
            if($row["SBDATE"] != ""){
                $SdateOfBirth = date('m/d/Y', strtotime($row["SBDATE"]));
                $Sdiff = date_diff(date_create($SdateOfBirth), date_create(date("Y-m-d")));
                $SdiffRes = $Sdiff->format('%y');
            }
            $tbl .= '
                <tbody>
                    <tr>
                        <td style="text-align: center; width: 2%;"><p>'. $i .'</p></td>
                        <td style=" width: 6%;"><p>'. strtoupper($row["LNAME"]) .'</p></td>
                        <td style=" width: 6%;"><p>'. strtoupper($row["FNAME"]) .'</p></td>
                        <td style=" width: 6%;"><p>'. strtoupper($row["MNAME"]) .'</p></td>
                        <td style=" width: 6.75%;"><p>'. $row["ADDR1"] .'</p></td>
                        <td style="text-align: center;"><p>'. $row["CSTATUS"] .'</p></td>
                        <td style="text-align: center; width: 4%;"><p>'. $row["sex"] .'</p></td>
                        <td style="text-align: center;"><p>'. date('m/d/Y', strtotime($row["BDATE"])) .'</p></td>
                        <td style="text-align: center; width: 2.5%;"><p>'. $diff->format('%y') .'</p></td>
                        <td style="text-align: center;"><p>Php '. number_format(floatval($row["MINCOME"]),2) .'</p></td>
                        <td style="text-align: center; width: 6%;"><p>'. $row["contact"] .'</p></td>
                        <td style=" width: 6%;"><p>'. strtoupper($row["SLNAME"]) .'</p></td>
                        <td style=" width: 6%;"><p>'. strtoupper($row["SFNAME"]) .'</p></td>
                        <td style=" width: 6%;"><p>'. strtoupper($row["SMNAME"]) .'</p></td>
                        <td style="text-align: center;"><p>'. $SdateOfBirth .'</p></td>
                        <td style="text-align: center; width: 2.5%;"><p>'. $SdiffRes .'</p></td>
                        <td style="text-align: center; width: 9.3%;"><p>'. $row["EMPLOYER"] .'</p></td>
                        <td style="vertical-align: middle; text-align: center; width: 9.9%;"><p> - </p></td>
                    </tr>
                </tbody>
            ';
        }
        $dbh = null;
    }
    else {
        $tbl .= '
        <tbody>
            <tr>
                <td style="text-align: center; width: 100%;"><i>NO RECORDS</i></td>
            </tr>
        </tbody>
        '; 
        $dbh = null;
    }
}
catch(PDOException $e){
  error_log('PDOException - ' . $e->getMessage(), 0);
  http_response_code(500);
  die('Error establishing connection with database');
}
ob_end_clean();
$pdf->writeHTML($header1.$tbl1.$break.$header.$tbl.$footer, true, false, false, false);
$pdf->Output();
?>
