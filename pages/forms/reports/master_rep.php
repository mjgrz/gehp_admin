<?Php
require "../../../../../dbcon.php";
require_once('../TCPDF-main/tcpdf.php');

$pdf = new TCPDF('l', 'mm', 'USLEGAL', true, 'UTF-8', false); 

$pdf->SetTitle('List of Applications (PAG-IBIG)');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);

$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

$pdf->SetMargins(10, 10, 10);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

$pdf->AddPage();

$project = $_POST['project'];
$fromdate = $_POST['fromdate'];
$todate = $_POST['todate'];
$sort = $_POST['sort'];
try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    $projectselect = "SELECT * FROM project_info 
                    LEFT JOIN units_floor ON project_info.project_id = units_floor.project_id
                    LEFT JOIN region ON project_info.region = region.psgc_reg
                    WHERE PJ_NAME = :project";
    $sthprojectselect = $dbh->prepare($projectselect);
    $sthprojectselect->bindParam(':project', $project);
    $sthprojectselect->execute();
    $sthprojectselect->setFetchMode(PDO::FETCH_ASSOC); 

    while ($rowproj = $sthprojectselect->fetch(PDO::FETCH_ASSOC)) {
        $Region = $rowproj["Region"];
        $PJ_NAME = $rowproj["PJ_NAME"];
        $location = $rowproj["location"];
    }
}
catch(PDOException $e){
error_log('PDOException - ' . $e->getMessage(), 0);
http_response_code(500);
die('Error establishing connection with database');
}

$pdf->SetFont ('helvetica', '', 8);
$header1 = '<div><img src="../TCPDF-main/examples/images/header1.png" style="width: 500px; text-align: center;">
            </div>';
$tbl1 .= '
        <table cellspacing="0" cellpadding="1" border="1" style="width: 100%;">
            <thead>
                <tr>
                    <th style="text-align: left; width: 15%; font-weight: bold;">  Project Proponent:</th>
                    <td style="text-align: left; width: 60%;">  National Housing Authority</td>
                    <th rowspan="4" style="text-align: center; width: 25%; font-weight: bold;"><div><br>Date Prepared:<br><span style="font-weight: normal;">'. date('F d, Y') .'</span></div></th>
                </tr>
                <tr>
                    <th style="font-weight: bold;">  Region:</th>
                    <td style="">  '. $Region .'</td>
                </tr>
                <tr>
                    <th style="font-weight: bold;">  Project Name:</th>
                    <td style="">  '. $PJ_NAME .'</td>
                </tr>
                <tr>
                    <th style="font-weight: bold;">  Location:</th>
                    <td style="">  '. $location .'</td>
                </tr>
            </thead>
        </table>
    ';
$break = '<div></div>';
$header = '
        <table cellspacing="0" cellpadding="3" border="1" style="width: 100%;">
            <thead>         
                <tr>
                    <th rowspan="2" style="text-align: center; font-weight: bold; width: 3%;">No.</th>
                    <th rowspan="2" style="text-align: center; font-weight: bold;">Last Name</th>
                    <th rowspan="2" style="text-align: center; font-weight: bold;">First Name</th>
                    <th rowspan="2" style="text-align: center; font-weight: bold;">Middle Name</th>
                    <th rowspan="2" style="text-align: center; font-weight: bold;">Maiden Name</th>
                    <th rowspan="2" style="text-align: center; font-weight: bold;">Birthday</th>
                    <th rowspan="2" style="text-align: center; font-weight: bold; width: 4%;">Age</th>
                    <th rowspan="2" style="text-align: center; font-weight: bold;">Gross Monthly Income</th>
                    <th colspan="2" style="text-align: center; font-weight: bold; width: 33%;">Employment Status</th>
                </tr>
                <tr>
                    <th style="text-align: center; font-weight: bold; width: 16%;">Employer</th>
                    <th style="text-align: center; font-weight: bold; width: 17%;">Address</th>
                </tr>
            </thead>   ';
$footer = '</table>';

try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $officeselectdata = [
        ':fromdate' => $fromdate,
        ':todate' => $todate,
        ':project' => $project,
    ];
    $officeselect = "SELECT APP_ID, app_date, FNAME, LNAME, MNAME, BDATE, ADDR1, app_status, EMPLOYER,emp_address, motherSurname, MINCOME, TIMESTAMPDIFF(YEAR,BDATE,NOW()) AS AGE
                    FROM applications
                    LEFT JOIN applicant_info ON applications.applicant_id = applicant_info.applicant_id
                    LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
                    LEFT JOIN employment_status ON applications.employment_id = employment_status.employment_id
                    WHERE app_date BETWEEN :fromdate AND :todate AND PJ_NAME = :project
                    ORDER BY $sort";
    $sthofficeselect = $dbh->prepare($officeselect);
    $sthofficeselect->execute($officeselectdata);
    $sthofficeselect->setFetchMode(PDO::FETCH_ASSOC); 
    
    if($sthofficeselect->rowCount() > 0){
        $i = 0;
        while ($row = $sthofficeselect->fetch(PDO::FETCH_ASSOC)) {
            $i++;
            $dateOfBirth = $row["BDATE"];
            $diff = date_diff(date_create($dateOfBirth), date_create(date("Y-m-d")));
            $tbl .= '
                <tbody>
                    <tr>
                        <td style="text-align: center; width: 3%;">'. $i .'</td>
                        <td>'. strtoupper($row["LNAME"]) .'</td>
                        <td>'. strtoupper($row["FNAME"]) .'</td>
                        <td>'. strtoupper($row["MNAME"]) .'</td>
                        <td style="text-align: center;">'. $row["motherSurname"] .'</td>
                        <td style="text-align: center;">'. date('F d, Y', strtotime($row["BDATE"])) .'</td>
                        <td style="text-align: center; width: 4%;">'. $diff->format('%y') .'</td>
                        <td style="text-align: center;">Php '. number_format(floatval($row["MINCOME"]),2) .'</td>
                        <td style="text-align: center; width: 16%;">'. strtoupper($row["EMPLOYER"]) .'</td>
                        <td style="text-align: center; width: 17%;">'. strtoupper($row["emp_address"]) .'</td>
                    </tr>
                </tbody>
            ';
        }
        $dbh = null;
    }
    else {
        $tbl .= '
        <tbody>
            <tr>
                <td style="text-align: center; width: 100%;"><i>NO RECORDS</i></td>
            </tr>
        </tbody>
        '; 
        $dbh = null;
    }
}
catch(PDOException $e){
  error_log('PDOException - ' . $e->getMessage(), 0);
  http_response_code(500);
  die('Error establishing connection with database');
}
ob_end_clean();
$pdf->writeHTML($header1.$tbl1.$break.$header.$tbl.$footer, true, false, false, false);
$pdf->Output();
?>