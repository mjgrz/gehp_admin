
<?Php
require "../dbcon.php";//connection to database
//SQL to get 10 records
$sql="SELECT * FROM users";
// Include the main TCPDF library (search for installation path).
require_once('../TCPDF-main/tcpdf.php');
$pdf = new TCPDF('l', 'mm', 'LEGAL', true, 'UTF-8', false); 

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(10, 10, 10);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

$pdf->AddPage();

$pdf->SetFont ('times', '', 10);
$header = '
<div><img src="../TCPDF-main/examples/images/header.png" style="width: 500px; text-align: center;"></div>
        <table cellspacing="0" cellpadding="3" border="1" style="width: 100%;">
            <thead>         
                <tr>
                    <th style="text-align: center; font-weight: bold;">DATE OF APPLICATION</th>
                    <th style="text-align: center; font-weight: bold;">PROJECT NAME</th>
                    <th style="text-align: center; font-weight: bold;">NAME OF APPLICANT</th>
                    <th style="text-align: center; font-weight: bold;">DATE OF BIRTH</th>
                    <th style="text-align: center; font-weight: bold;">ADDRESS</th>
                </tr>
            </thead>   ';
$footer = '</table>'; 
$project = $_POST['project'];
$fromdate = $_POST['fromdate'];
$todate = $_POST['todate'];
$sort = $_POST['sort'];
$status = $_POST['status'];
$officeselect = "SELECT * FROM applications
                LEFT JOIN applicant_info ON applications.applicant_id = applicant_info.applicant_id
                LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
                WHERE app_date BETWEEN '$fromdate' AND '$todate' AND PJ_NAME = '$project' AND app_status = '$status'
                ORDER BY $sort";
$officeresult = mysqli_query($conn,$officeselect);
if(mysqli_num_rows($officeresult) > 0){
    while($row = mysqli_fetch_assoc($officeresult)){
        $tbl .= '
            <tbody>
                <tr>
                    <td style="text-align: center;">'. date('F d, Y', strtotime($row["app_date"])) .'</td>
                    <td>'. $row["PJ_NAME"] .'</td>
                    <td>'. ucwords(strtolower($row["LNAME"])) .','.' '.''. ucwords(strtolower($row["FNAME"])) .' '. ucwords(strtolower($row["MNAME"])) .'</td>
                    <td>'. date('F d, Y', strtotime($row["BDATE"])) .'</td>
                    <td>'. $row["ADDR1"] .'</td>
                </tr>
            </tbody>
        ';
    }
}
else {
    $tbl .= '
    <tbody>
        <tr>
            <td style="text-align: center; width: 100%;"><i>NO RECORDS</i></td>
        </tr>
    </tbody>
    '; 
}


// Clean any content of the output buffer
ob_end_clean();
$pdf->writeHTML($header.$tbl.$footer, true, false, false, false);
/// end of records /// 

$pdf->Output();
?>