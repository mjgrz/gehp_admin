<?Php
if(isset($_POST['emd_Exc'])){
    session_start();
    $_SESSION['fromdate'] = $_POST["fromdate"];
    $_SESSION['todate'] = $_POST["todate"];
    echo "<script> location.href='../ExcelPrnt/PhpOffice/print_proc.php'; </script>";
    exit;
}
if(isset($_POST['emd_PDF'])){
require "../../../../../dbcon.php";
require_once('../TCPDF-main/tcpdf.php');

$pdf = new TCPDF('l', 'mm', 'USLEGAL', true, 'UTF-8', false); 

$pdf->SetTitle('List of Applications (PAG-IBIG)');

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);

$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

$pdf->SetMargins(10, 10, 10);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

$pdf->AddPage();

$pdf->SetFont ('helvetica', '', 8);
$header1 = '<table cellpadding="1" cellspacing="1" border="0" style="text-align:center;">
            <tr>
                <td style="text-align: right; width: 37%; padding-right: 10px;"><img src="../TCPDF-main/examples/images/nha-logo.png" width="50" border="0"/></td>
                <td style="width: 26%; font-size: 12px;"><b>National Housing Authority<br>GOVERNMENT EMPLOYEE HOUSING PROGRAM<br>'
                .date("F d, Y", strtotime($_POST["fromdate"])).' - '.date("F d, Y", strtotime($_POST["todate"])).
                '</b></td>
                <td style="text-align: left; width: 37%;"><img src="../TCPDF-main/examples/images/balai_temp.png" width="90" border="0"/></td>
            </tr>
            </table>';

$break = '<div></div>';
$header = '
        <table cellspacing="0" cellpadding="3" border="1" style="width: 100%;">
            <thead>         
                <tr>
                    <th rowspan="3" style="text-align: center; font-weight: bold; width: 12.9%;">Project Name</th>
                    <th rowspan="3" style="text-align: center; font-weight: bold; width: 6.2%;">Remaining No. of Available Lots/Units</th>
                    <th rowspan="3" style="text-align: center; font-weight: bold; width: 6.2%;">Total Qualified Applicants GEHP and OFW</th>
                    <th colspan="6" style="text-align: center; font-weight: bold; width: 37.2%;">Online Application</th>
                    <th colspan="6" style="text-align: center; font-weight: bold; width: 37.2%;">Manual Application</th>
                </tr>
                <tr>
                    <th colspan="3" style="text-align: center; font-weight: bold;">GEHP</th>
                    <th colspan="3" style="text-align: center; font-weight: bold;">OFW</th>
                    <th colspan="3" style="text-align: center; font-weight: bold;">GEHP</th>
                    <th colspan="3" style="text-align: center; font-weight: bold;">OFW</th>
                </tr>
                <tr>
                    <th style="text-align: center; font-weight: bold; width: 6.2%;">Qualified Applicants</th>
                    <th style="text-align: center; font-weight: bold; width: 6.2%;">Disqualified Applicants</th>
                    <th style="text-align: center; font-weight: bold; width: 6.2%;">New Applications for Evaluation</th>
                    <th style="text-align: center; font-weight: bold; width: 6.2%;">Qualified Applicants</th>
                    <th style="text-align: center; font-weight: bold; width: 6.2%;">Disqualified Applicants</th>
                    <th style="text-align: center; font-weight: bold; width: 6.2%;">New Applications for Evaluation</th>
                    <th style="text-align: center; font-weight: bold; width: 6.2%;">Qualified Applicants</th>
                    <th style="text-align: center; font-weight: bold; width: 6.2%;">Disqualified Applicants</th>
                    <th style="text-align: center; font-weight: bold; width: 6.2%;">New Applications for Evaluation</th>
                    <th style="text-align: center; font-weight: bold; width: 6.2%;">Qualified Applicants</th>
                    <th style="text-align: center; font-weight: bold; width: 6.2%;">Disqualified Applicants</th>
                    <th style="text-align: center; font-weight: bold; width: 6.2%;">New Applications for Evaluation</th>
                </tr>
            </thead>';
$footer = '</table>';

$fromdate = $_POST["fromdate"];
$todate = $_POST["todate"];

try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    $sqlproj = "SELECT PJ_CODE, PJ_NAME FROM project_info ORDER BY PJ_NAME";
    $project = $dbh->query($sqlproj);
    foreach ($project as $rowproj) { 
        $appselect = "SELECT (SELECT COUNT(*) FROM applications LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
                    WHERE app_status = 'Qualified' AND source = 'Website/Frontend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate') as Qualified_front,
                    (SELECT COUNT(*) FROM applications LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
                    WHERE app_status = 'Disqualified' AND source = 'Website/Frontend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate') as Disqualified_front,
                    (SELECT COUNT(*) FROM applications LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
                    WHERE app_status = 'New Applicant' AND source = 'Website/Frontend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate'
                    OR app_status = 'Pending' AND source = 'Website/Frontend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate') as New_Applicant_front,
                    (SELECT COUNT(*) FROM applications LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
                    WHERE app_status = 'Qualified' AND source = 'Website/Backend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND 
                    app_date BETWEEN '$fromdate' AND '$todate') as Qualified_back,
                    (SELECT COUNT(*) FROM applications LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
                    WHERE app_status = 'Disqualified' AND source = 'Website/Backend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate') as Disqualified_back,
                    (SELECT COUNT(*) FROM applications LEFT JOIN project_info ON applications.PJ_CODE = project_info.PJ_CODE
                    WHERE app_status = 'New Applicant' AND source = 'Website/Backend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate'
                    OR app_status = 'Pending' AND source = 'Website/Backend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate') as New_Applicant_back";
        $sthappselect = $dbh->prepare($appselect);
        $sthappselect->execute();
        $sthappselect->setFetchMode(PDO::FETCH_ASSOC); 
            while ($row = $sthappselect->fetch(PDO::FETCH_ASSOC)) {
                $Qualified_front = $row["Qualified_front"];
                $Disqualified_front = $row["Disqualified_front"];
                $New_Applicant_front = $row["New_Applicant_front"];
                $Qualified_back = $row["Qualified_back"];
                $Disqualified_back = $row["Disqualified_back"];
                $New_Applicant_back = $row["New_Applicant_back"];
            }

        $appselectofw = "SELECT (SELECT COUNT(*) FROM ofw_applications LEFT JOIN project_info ON ofw_applications.PJ_CODE = project_info.PJ_CODE
                    WHERE app_status = 'Qualified' AND source = 'Website/Frontend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate') as Qualifiedofw_front,
                    (SELECT COUNT(*) FROM ofw_applications LEFT JOIN project_info ON ofw_applications.PJ_CODE = project_info.PJ_CODE
                    WHERE app_status = 'Disqualified' AND source = 'Website/Frontend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate') as Disqualifiedofw_front,
                    (SELECT COUNT(*) FROM ofw_applications LEFT JOIN project_info ON ofw_applications.PJ_CODE = project_info.PJ_CODE
                    WHERE app_status = 'New Applicant' AND source = 'Website/Frontend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate'
                    OR app_status = 'Pending' AND source = 'Website/Frontend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate') as New_Applicantofw_front,
                    (SELECT COUNT(*) FROM ofw_applications LEFT JOIN project_info ON ofw_applications.PJ_CODE = project_info.PJ_CODE
                    WHERE app_status = 'Qualified' AND source = 'Website/Backend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate') as Qualifiedofw_back,
                    (SELECT COUNT(*) FROM ofw_applications LEFT JOIN project_info ON ofw_applications.PJ_CODE = project_info.PJ_CODE
                    WHERE app_status = 'Disqualified' AND source = 'Website/Backend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate') as Disqualifiedofw_back,
                    (SELECT COUNT(*) FROM ofw_applications LEFT JOIN project_info ON ofw_applications.PJ_CODE = project_info.PJ_CODE
                    WHERE app_status = 'New Applicant' AND source = 'Website/Backend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate'
                    OR app_status = 'Pending' AND source = 'Website/Backend' AND project_info.PJ_CODE = ".$rowproj["PJ_CODE"]." AND
                    app_date BETWEEN '$fromdate' AND '$todate') as New_Applicantofw_back";
        $sthappselectofw = $dbh->prepare($appselectofw);
        $sthappselectofw->execute();
        $sthappselectofw->setFetchMode(PDO::FETCH_ASSOC); 
            while ($rowofw = $sthappselectofw->fetch(PDO::FETCH_ASSOC)) {
                $Qualifiedofw_front = $rowofw["Qualifiedofw_front"];
                $Disqualifiedofw_front = $rowofw["Disqualifiedofw_front"];
                $New_Applicantofw_front = $rowofw["New_Applicantofw_front"];
                $Qualifiedofw_back = $rowofw["Qualifiedofw_back"];
                $Disqualifiedofw_back = $rowofw["Disqualifiedofw_back"];
                $New_Applicantofw_back = $rowofw["Disqualifiedofw_back"];
            }
            $totalQualified = $Qualified_front + $Qualified_back + $Qualifiedofw_front + $Qualifiedofw_back;
            
        $tbl .= '
                <tr>
                    <td style="width: 12.9%;">'.$rowproj["PJ_NAME"].'</td>
                    <td style="width: 6.2%;"></td>
                    <td style="text-align: center; width: 6.2%;">'.$totalQualified.'</td>
                    <td style="text-align: center; width: 6.2%;">'.$Qualified_front.'</td>
                    <td style="text-align: center; width: 6.2%;">'.$Disqualified_front.'</td>
                    <td style="text-align: center; width: 6.2%;">'.$New_Applicant_front.'</td>
                    <td style="text-align: center; width: 6.2%;">'.$Qualifiedofw_front.'</td>
                    <td style="text-align: center; width: 6.2%;">'.$Disqualifiedofw_front.'</td>
                    <td style="text-align: center; width: 6.2%;">'.$New_Applicantofw_front.'</td>
                    <td style="text-align: center; width: 6.2%;">'.$Qualified_back.'</td>
                    <td style="text-align: center; width: 6.2%;">'.$Disqualified_back.'</td>
                    <td style="text-align: center; width: 6.2%;">'.$New_Applicant_back.'</td>
                    <td style="text-align: center; width: 6.2%;">'.$Qualifiedofw_back.'</td>
                    <td style="text-align: center; width: 6.2%;">'.$Disqualifiedofw_back.'</td>
                    <td style="text-align: center; width: 6.2%;">'.$New_Applicantofw_back.'</td>
                </tr>
        ';
    }
}
catch(PDOException $e){
  error_log('PDOException - ' . $e->getMessage(), 0);
  http_response_code(500);
  die('Error establishing connection with database');
}

ob_end_clean();
$pdf->writeHTML($header1.$break.$header.$tbl.$footer, true, false, false, false);
$pdf->Output();
}
?>