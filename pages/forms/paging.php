<?php
$selectedPage = "info";
$hangPage = "default";
?>

<div align="center" style="padding: 5px;">
  <div class="row" style="font-size: 16px;">
    <div class="col-sm-6 project-actions text-left" <?php if($total_pages == 0) {echo "hidden";} ?>>
      <ul class="pagination" <?php if($total_pages == 1) {echo "hidden";} ?>>
          <!--li <?php if($pageno == 1){echo 'hidden';} else{} ?> style="padding: 5px;"><a class="btn btn-default btn-sm" name = "numpage" href="?pageno=<?php if(isset($_GET['pageno'])){if($_GET['pageno'] == 1){echo '1';} else{echo $pageno - 1;}} else{echo '1';} ?>">Preview</a></li-->
          
          <li style="padding: 5px;"><a class="btn btn-<?php if($pageno == 1){ echo $selectedPage;} else{echo $hangPage; } ?> btn-sm" name = "numpage" href="?pageno=1">
            <?php if($pageno == 1){ echo 1;} else{echo 1; } ?><?php if($pageno >=5){echo '<a> . . . </a>';}?></a></li>

          <?php if($pageno <5){ 
          for($ipage=2;$ipage<$total_pages;$ipage++){ ?>
          <li style="padding: 5px;">
              <a class="btn btn-<?php if($ipage == $pageno){ echo $selectedPage;} else{echo $hangPage; } ?> btn-sm" name = "numpage" href="?pageno=<?php echo $ipage?>">
              <?php if($ipage == $pageno){ echo $ipage;} else{echo $ipage; } ?></a>
              <?php
              if($ipage>=5){
                echo '<a> . . . </a>';
                break;
              }
              ?>
          </li>
          <?php }} ?>
          
          <?php if($pageno >=5){
          for($ipage=$pageno-2;$ipage<$total_pages;$ipage++){ ?>
          <li style="padding: 5px;">
              <a class="btn btn-<?php if($ipage == $pageno) echo $selectedPage; else{echo $hangPage; } ?> btn-sm" name = "numpage" href="?pageno=<?php echo $ipage?>">
              <?php if($ipage == $pageno) echo $ipage; else{echo $ipage; } ?></a>
              <?php
              if($ipage>=$total_pages-1){
                break;
                  
                if($ipage>=$pageno+2){
                  echo '<a> . . . </a>';
                  break;
                }
              }
              else{
                if($ipage>=$pageno+2){
                  echo '<a> . . . </a>';
                  break;
                }
              }
              ?>
          </li>
          <?php }} ?>
          
          <li style="padding: 5px;"><a class="btn btn-<?php if($pageno == $total_pages){ echo $selectedPage;} else{echo $hangPage; } ?> btn-sm" name = "numpage" href="?pageno=<?php echo $total_pages?>">
          <?php if($pageno == $total_pages){ echo $total_pages;} else{echo $total_pages; } ?></a></li>

          <!--li <?php if($pageno == $total_pages){echo 'hidden';} else{} ?> style="padding: 5px;"><a class="btn btn-default btn-sm" name = "numpage"
          href="?pageno=<?php if(isset($_GET['pageno'])){if($_GET['pageno'] == $total_pages){echo $total_pages;} else{echo $pageno + 1;}} else{echo '2';} ?>">Next</a></li-->
      </ul>
    </div>
    <?php
      $differ = $setcount - 1;
      $one = $pageno * 10 - 9;
      $two = $one + $differ;
    ?>
    <!--showing number of results-->
    <div class="col-sm-6 project-actions text-right" style="padding: 5px 20px;" <?php if($total_pages == 0) {echo "hidden";} ?>>
      <i hidden><?php echo $pageno; ?>&nbsp;of&nbsp;<?php echo $total_pages; ?>&nbsp;page(s)</i>
      Showing <?php echo $one; ?>&nbsp;to&nbsp;<?php echo $two; ?>&nbsp;results
    </div>
  </div>
</div>
