<?php
  $office = $_POST["getOption"];
  include("login_session.php");
  include("../../../../dbcon.php");
  echo "<option value=''>- Select Office -</option>";
  try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    if($office == "Regional"){
      $regionsql = "SELECT ro_id, RO FROM table_ro_ ORDER BY RO";
      $sthregionsql = $dbh->prepare($regionsql);
      $sthregionsql->execute();
      $sthregionsql->setFetchMode(PDO::FETCH_ASSOC); 
      while ($regionrow = $sthregionsql->fetch(PDO::FETCH_ASSOC)) {
        if($regionrow['ro_id'] == $login_officeID) {
          $set = "selected";
        }
        else{
          $set = "";
        }
          echo "<option $set value='" . $regionrow['ro_id'] . "'>" . strtoupper($regionrow['RO']) . "</option>";
      }
      $dbh = null;
    }
    if($office == "Head Office"){
      $depsql = "SELECT office_id, description FROM department";
      $sthdepsql = $dbh->prepare($depsql);
      $sthdepsql->execute();
      $sthdepsql->setFetchMode(PDO::FETCH_ASSOC); 
      while ($deprow = $sthdepsql->fetch(PDO::FETCH_ASSOC)) {
        if($deprow['office_id'] == $login_officeID) {
          $set = "selected";
        }
        else{
          $set = "";
        }
          echo "<option $set value='" . $deprow['office_id'] . "'>" . $deprow['description'] . "</option>";
      }
      $dbh = null;
    }
  }
  catch(PDOException $e){
    error_log('PDOException - ' . $e->getMessage(), 0);
    http_response_code(500);
    die('Error establishing connection with database');
  }
?>












