<?php 
include('../../../../../dbcon.php'); 
 
if(!empty($_POST["region_id"])){ 
    $id = $_POST["region_id"];
    if (is_numeric($id) == true){
      try{
        $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $query = "SELECT ro_id FROM table_ro_ WHERE region_id = :id"; 
        $sthquery = $dbh->prepare($query);
        $sthquery->bindParam(':id', $id);
        $sthquery->execute();
        $sthquery->setFetchMode(PDO::FETCH_ASSOC); 
  
        if($sthquery->rowCount() > 0){
            echo '<option value="">- Select District Office -</option>'; 
            while ($row = $sthquery->fetch(PDO::FETCH_ASSOC)) {
                $dis_id = $row['ro_id'];
                if (is_numeric($dis_id) == true){
                  try{
                    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    $dis_query = "SELECT id, DO FROM table_do WHERE ro_id = :dis_id"; 
                    $sthdis_query = $dbh->prepare($dis_query);
                    $sthdis_query->bindParam(':dis_id', $dis_id);
                    $sthdis_query->execute();
                    $sthdis_query->setFetchMode(PDO::FETCH_ASSOC); 

                    while ($dis_row = $sthdis_query->fetch(PDO::FETCH_ASSOC)) {
                        echo '<option value="'.$dis_row['id'].'">'.strtoupper($dis_row['DO']).'</option>'; 
                    }
                    $dbh = null;
                }
                catch(PDOException $e){
                  error_log('PDOException - ' . $e->getMessage(), 0);
                  http_response_code(500);
                  die('Error establishing connection with database');
                }
              } 
              else{
              http_response_code(400);
              die('Error processing bad or malformed request');
              }
            } 
        }else{ 
            echo '<option value="">- District Office Not Available -</option>'; 
            $dbh = null;
        } 
    }
    catch(PDOException $e){
      error_log('PDOException - ' . $e->getMessage(), 0);
      http_response_code(500);
      die('Error establishing connection with database');
    }
  } 
  else{
  http_response_code(400);
  die('Error processing bad or malformed request');
  }
} 
?>