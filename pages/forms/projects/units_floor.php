<div class="card card-primary" style="box-shadow: none; border: 1px solid #67a2b2;">
  <div class="card-body">
    <div class="row">

      <div class="col-sm-4">
        <div class="form-group">
          <label for="location">Location</label>
          <input type="text" class="form-control" id="location"
          oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" name="location" >
        </div>

        <div class="form-group">
          <label for="bedroom">Bedroom</label>
          <input type="text" class="form-control" id="bedroom"
          oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" name="bedroom" >
        </div>
        <div class="form-group">
          <label for="bathroom">Bathroom</label>
          <input type="text" class="form-control" id="bathroom"
          oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" name="bathroom" >
        </div>
    
        <div class="form-group">
          <label for="lotarea">Lot Area</label>
          <input type="text" class="form-control" id="lotarea"
          oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" name="lotarea" >
        </div>
        <div class="form-group">
          <label for="floorarea">Floor Area</label>
          <input type="text" class="form-control" id="floorarea"
          oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" name="floorarea" >
        </div>

        <div class="form-group">
          <label for="specs">Specification</label>
          <input type="text" class="form-control" id="specs"
          oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\.d*)\./g, '$1');" name="specs" >
        </div>
      </div>
      
      <div class="col-sm-8">
        <div class="row">
          <div class="col-sm-6">
            <div class="col-md-12" style="margin-top: 32px;">
              <div class="form-group" id="floor_imgDiv">
                <input type="file" id="floor_imgUp1" name="floor_imgUp1" title="Click in this area to upload an image." onchange="floor_imgPreview1()" >
                <p id="floor_imgText1">Click in this area to upload an image.</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="col-md-12" style="margin-top: 32px;">
              <div class="form-group" id="floor_imgDiv">
                <input type="file" id="floor_imgUp2" name="floor_imgUp2" title="Click in this area to upload an image." onchange="floor_imgPreview2()" >
                <p id="floor_imgText2">Click in this area to upload an image.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(function () {
  bsCustomFileInput.init();
});
</script>

<style>
  #floor_imgDiv {
    width: 100%;
    height: 467px;
    border: 2px dashed #67a2b2;
    border-radius: 5px;
  }

  #floor_imgUp1, #floor_imgUp2 {
    position: absolute;
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    outline: none;
    opacity: 0;
  }

  #floor_imgUp1:hover, #floor_imgUp2:hover {
    cursor: pointer;
  }

  #floor_imgText1, #floor_imgText2 {
    text-align: center;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 5px;
  }

  .content {
    -webkit-animation-duration: 0.7s;
    animation-duration: 0.7s;
  }
  
  @-webkit-keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  
  @keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  
  .content {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
  }
</style>

<script>
  $(function () {
    $("#hc").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "buttons": [""]
    }).buttons().container().appendTo('#hc_wrapper .col-md-6:eq(0)');
  });
  
  function floor_imgPreview1() {
    var urlimage = URL.createObjectURL(event.target.files[0]);
    document.getElementById("floor_imgText1").style.backgroundImage = "url("+urlimage+")";// specify the image path here
    document.getElementById("floor_imgText1").style.backgroundSize = "auto 100%";
    document.getElementById("floor_imgText1").style.backgroundRepeat = "no-repeat";
    document.getElementById("floor_imgText1").style.backgroundPosition = "center";
    document.getElementById("floor_imgText1").innerHTML = "";
  }

  function floor_imgPreview2() {
    var urlimage = URL.createObjectURL(event.target.files[0]);
    document.getElementById("floor_imgText2").style.backgroundImage = "url("+urlimage+")";// specify the image path here
    document.getElementById("floor_imgText2").style.backgroundSize = "auto 100%";
    document.getElementById("floor_imgText2").style.backgroundRepeat = "no-repeat";
    document.getElementById("floor_imgText2").style.backgroundPosition = "center";
    document.getElementById("floor_imgText2").innerHTML = "";
  }
</script>