<div class="card card-primary" style="box-shadow: none; border: 1px solid #67a2b2;"><form method="POST" action="" enctype="multipart/form-data">
  <div class="card-body">
    <div class="row">

      <div class="col-sm-6">
        <div class="col-md-12" style="margin-top: 32px;">
          <div class="form-group" id="main_imageDiv">
            <input type="file" id="main_imageUpload" name="main_imageUpload" title="Click in this area to upload an image." onchange="previewMain_img()" required>
            <p id="main_imageText">Click in this area to upload an image.</p>
          </div>
        </div>
      </div>


      <div class="col-sm-6">
        <div class="form-group">
          <label>Status</label>
          <select class="form-control" name="status" id="status" >
            <option value="">- Select Status -</option>
            <option value="Completed">Completed Project</option>
            <option value="Ongoing">Ongoing Project</option>
            <option value="Upcoming">Upcoming Project</option>
          </select>
        </div>
        
        <div class="form-group">
          <label>Region</label>
          <select class="form-control" name="region" id="region" required>
            <option value="">- Select Region -</option>
            <?php
            include('../../../../../dbcon.php'); 
            try{
              $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
              $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
              $regionselect = "SELECT psgc_reg, Region FROM region ORDER by Region";
              $sthregionselect = $dbh->prepare($regionselect);
              $sthregionselect->execute();
              $sthregionselect->setFetchMode(PDO::FETCH_ASSOC); 

              if($sthregionselect->rowCount() > 0){
                while ($regionrow = $sthregionselect->fetch(PDO::FETCH_ASSOC)) {
              ?>
              <option value="<?php echo $regionrow['psgc_reg'] ?>"><?php echo strtoupper($regionrow['Region']) ?></option>
              <?php 
                } 
                $dbh = null;
              }
              else 
              { 
                $dbh = null;
              }
            }
            catch(PDOException $e){
              error_log('PDOException - ' . $e->getMessage(), 0);
              http_response_code(500);
              die('Error establishing connection with database');
            }
            ?>
          </select>
        </div>
        <div class="form-group">
          <label>District</label>
          <select class="form-control" name="district" id="district" style="padding-left:10px;">
          <option value=''>- Select District Office -</option>
          
          </select>
        </div>
        <div class="form-group">
          <label for="projectName">Project Code</label>&nbsp&nbsp&nbsp<span id="PJcode-availability-status"></span>
          <input type="text" class="form-control" id="projectCode" name="projectCode" pattern="[0-9]+" onkeyup="checkPJcodeAvailability()"
          oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');"/>
        </div>
        <?php include("projects/projectcode_script.php"); ?>
        <div class="form-group">
          <label for="projectName">Name of Project</label>
          <input type="text" class="form-control" id="projectName" name="projectName"
          oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" required>
        </div>
        <div class="form-group">
          <label for="overview">Overview</label>
          <textarea class="form-control" id="overview" name="overview" style="resize: none; padding: 6px;"
          oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'$./,-]/g, '').replace(/(\.d*)\./g, '$1');" rows="4"></textarea >
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(function () {
  bsCustomFileInput.init();
});
</script>

<style>

  #projectCode:invalid {
      border: 2px solid red;
  }


  #main_imageDiv {
    width: 100%;
    height: 540px;
    border: 2px dashed #67a2b2;
    border-radius: 5px;
  }

  #main_imageUpload {
    position: absolute;
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    outline: none;
    opacity: 0;
  }

  #main_imageUpload:hover {
    cursor: pointer;
  }

  #main_imageText {
    text-align: center;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 5px;
  }


  .content {
    -webkit-animation-duration: 0.7s;
    animation-duration: 0.7s;
  }
  
  @-webkit-keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  
  @keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  
  .content {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
  }

</style>

<script>
  $(function () {
    $("#hc").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "buttons": [""]
    }).buttons().container().appendTo('#hc_wrapper .col-md-6:eq(0)');
  });
  
function previewMain_img() {
  var urlimage = URL.createObjectURL(event.target.files[0]);
  document.getElementById("main_imageText").style.backgroundImage = "url("+urlimage+")";// specify the image path here
  document.getElementById("main_imageText").style.backgroundSize = "auto 100%";
  document.getElementById("main_imageText").style.backgroundRepeat = "no-repeat";
  document.getElementById("main_imageText").style.backgroundPosition = "center";
  document.getElementById("main_imageText").innerHTML = "";
}
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script>
$(document).ready(function(){
    $('#region').on('change', function(){
        var countryID = $(this).val();
        if(countryID){
            $.ajax({
                type:'POST',
                url:'projects/ajaxData_add.php',
                data:'region_id='+countryID,
                success:function(html){
                    $('#district').html(html);
                }
            }); 
        }else{
        }
    });
});
</script>