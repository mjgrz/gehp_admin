<div class="card card-primary" style="box-shadow: none; border: 1px solid #67a2b2;">
  <div class="card-body">
    <div class="col-sm-12">
      <div class="col-sm-12">
        <div class="row">
          <div class="col-sm-12">
            <div class="row">
              <div class="col-md-4" style="margin-top: 32px;">
                <div class="form-group" id="unit_imgDiv">
                  <input type="file" id="unit_imgUp1" name="unit_imgUp1" title="Click in this area to upload an image." onchange="unit_imagePreview1()" >
                  <p id="unit_imgUpText1">Click in this area to upload an image.</p>
                </div>
              </div>
              <div class="col-md-4" style="margin-top: 32px;">
                <div class="form-group" id="unit_imgDiv">
                  <input type="file" id="unit_imgUp2" name="unit_imgUp2" title="Click in this area to upload an image." onchange="unit_imagePreview2()" >
                  <p id="unit_imgUpText2">Click in this area to upload an image.</p>
                </div>
              </div>
              <div class="col-md-4" style="margin-top: 32px;">
                <div class="form-group" id="unit_imgDiv">
                  <input type="file" id="unit_imgUp3" name="unit_imgUp3" title="Click in this area to upload an image." onchange="unit_imagePreview3()" >
                  <p id="unit_imgUpText3">Click in this area to upload an image.</p>
                </div>
              </div>
            </div>
          </div>

          <div class="col-sm-12">
            <div class="row">
              <div class="col-md-4" style="margin-top: 32px;">
                <div class="form-group" id="unit_imgDiv">
                  <input type="file" id="unit_imgUp4" name="unit_imgUp4" title="Click in this area to upload an image." onchange="unit_imagePreview4()" >
                  <p id="unit_imgUpText4">Click in this area to upload an image.</p>
                </div>
              </div>
              <div class="col-md-4" style="margin-top: 32px;">
                <div class="form-group" id="unit_imgDiv">
                  <input type="file" id="unit_imgUp5" name="unit_imgUp5" title="Click in this area to upload an image." onchange="unit_imagePreview5()" >
                  <p id="unit_imgUpText5">Click in this area to upload an image.</p>
                </div>
              </div>
              <div class="col-md-4" style="margin-top: 32px;">
                <div class="form-group" id="unit_imgDiv">
                  <input type="file" id="unit_imgUp6" name="unit_imgUp6" title="Click in this area to upload an image." onchange="unit_imagePreview6()" >
                  <p id="unit_imgUpText6">Click in this area to upload an image.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(function () {
  bsCustomFileInput.init();
});
</script>

<style>
  #unit_imgDiv {
    width: 100%;
    height: 230px;
    border: 2px dashed #67a2b2;
    border-radius: 5px;
  }

  #unit_imgUp6, #unit_imgUp5, #unit_imgUp4, #unit_imgUp3, #unit_imgUp2, #unit_imgUp1 {
    position: absolute;
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    outline: none;
    opacity: 0;
  }

  #unit_imgUp6:hover, #unit_imgUp5:hover, #unit_imgUp4:hover, #unit_imgUp3:hover, #unit_imgUp2:hover, #unit_imgUp1:hover {
    cursor: pointer;
  }

  #unit_imgUpText6, #unit_imgUpText5, #unit_imgUpText4, #unit_imgUpText3, #unit_imgUpText2, #unit_imgUpText1 {
    text-align: center;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 5px;
  }

  .content {
    -webkit-animation-duration: 0.7s;
    animation-duration: 0.7s;
  }
  
  @-webkit-keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  
  @keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  
  .content {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
  }

</style>

<script>
  $(function () {
    $("#hc").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "buttons": [""]
    }).buttons().container().appendTo('#hc_wrapper .col-md-6:eq(0)');
  });

  function unit_imagePreview1() {
    var urlimage = URL.createObjectURL(event.target.files[0]);
    document.getElementById("unit_imgUpText1").style.backgroundImage = "url("+urlimage+")";// specify the image path here
    document.getElementById("unit_imgUpText1").style.backgroundSize = "auto 100%";
    document.getElementById("unit_imgUpText1").style.backgroundRepeat = "no-repeat";
    document.getElementById("unit_imgUpText1").style.backgroundPosition = "center";
    document.getElementById("unit_imgUpText1").innerHTML = "";
  }
  function unit_imagePreview2() {
    var urlimage = URL.createObjectURL(event.target.files[0]);
    document.getElementById("unit_imgUpText2").style.backgroundImage = "url("+urlimage+")";// specify the image path here
    document.getElementById("unit_imgUpText2").style.backgroundSize = "auto 100%";
    document.getElementById("unit_imgUpText2").style.backgroundRepeat = "no-repeat";
    document.getElementById("unit_imgUpText2").style.backgroundPosition = "center";
    document.getElementById("unit_imgUpText2").innerHTML = "";
  }
  function unit_imagePreview3() {
    var urlimage = URL.createObjectURL(event.target.files[0]);
    document.getElementById("unit_imgUpText3").style.backgroundImage = "url("+urlimage+")";// specify the image path here
    document.getElementById("unit_imgUpText3").style.backgroundSize = "auto 100%";
    document.getElementById("unit_imgUpText3").style.backgroundRepeat = "no-repeat";
    document.getElementById("unit_imgUpText3").style.backgroundPosition = "center";
    document.getElementById("unit_imgUpText3").innerHTML = "";
  }
  function unit_imagePreview4() {
    var urlimage = URL.createObjectURL(event.target.files[0]);
    document.getElementById("unit_imgUpText4").style.backgroundImage = "url("+urlimage+")";// specify the image path here
    document.getElementById("unit_imgUpText4").style.backgroundSize = "auto 100%";
    document.getElementById("unit_imgUpText4").style.backgroundRepeat = "no-repeat";
    document.getElementById("unit_imgUpText4").style.backgroundPosition = "center";
    document.getElementById("unit_imgUpText4").innerHTML = "";
  }
  function unit_imagePreview5() {
    var urlimage = URL.createObjectURL(event.target.files[0]);
    document.getElementById("unit_imgUpText5").style.backgroundImage = "url("+urlimage+")";// specify the image path here
    document.getElementById("unit_imgUpText5").style.backgroundSize = "auto 100%";
    document.getElementById("unit_imgUpText5").style.backgroundRepeat = "no-repeat";
    document.getElementById("unit_imgUpText5").style.backgroundPosition = "center";
    document.getElementById("unit_imgUpText5").innerHTML = "";
  }
  function unit_imagePreview6() {
    var urlimage = URL.createObjectURL(event.target.files[0]);
    document.getElementById("unit_imgUpText6").style.backgroundImage = "url("+urlimage+")";// specify the image path here
    document.getElementById("unit_imgUpText6").style.backgroundSize = "auto 100%";
    document.getElementById("unit_imgUpText6").style.backgroundRepeat = "no-repeat";
    document.getElementById("unit_imgUpText6").style.backgroundPosition = "center";
    document.getElementById("unit_imgUpText6").innerHTML = "";
  }
</script>