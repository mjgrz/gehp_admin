<div class="card card-primary" style="box-shadow: none; border: 1px solid #67a2b2;">
  <div class="card-body">
    <div class="row">

      <div class="col-sm-6">
        <div class="form-group">
          <label for="estab1">Establishment No. 1</label>
          <input type="text" class="form-control" id="estab1"
          oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\.d*)\./g, '$1');" name="estab1" >
        </div>

        <div class="form-group">
          <label for="estab2">Establishment No. 2</label>
          <input type="text" class="form-control" id="estab2"
          oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\.d*)\./g, '$1');" name="estab2" >
        </div>
        <div class="form-group">
          <label for="estab3">Establishment No. 3</label>
          <input type="text" class="form-control" id="estab3"
          oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\.d*)\./g, '$1');" name="estab3" >
        </div>
      </div>
      
      <div class="col-sm-6">
        <div class="form-group">
            <label for="estab4">Establishment No. 4</label>
            <input type="text" class="form-control" id="estab4"
          oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\.d*)\./g, '$1');" name="estab4" >
          </div>
          <div class="form-group">
            <label for="estab5">Establishment No. 5</label>
            <input type="text" class="form-control" id="estab5"
          oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\.d*)\./g, '$1');" name="estab5" >
          </div>

          <div class="form-group">
            <label for="estab6">Establishment No. 6</label>
            <input type="text" class="form-control" id="estab6"
          oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\.d*)\./g, '$1');" name="estab6" >
          </div>
      </div>
    </div>
  </div>
</div>
<script>
$(function () {
  bsCustomFileInput.init();
});
</script>

<style>
  .content {
    -webkit-animation-duration: 0.7s;
    animation-duration: 0.7s;
  }
  
  @-webkit-keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  
  @keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  
  .content {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
  }

</style>

<script>
  $(function () {
    $("#hc").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "buttons": [""]
    }).buttons().container().appendTo('#hc_wrapper .col-md-6:eq(0)');
  });
  
function previewupdate_apply() {
  var urlimage = URL.createObjectURL(event.target.files[0]);
  document.getElementById("app_uploadText").style.backgroundImage = "url("+urlimage+")";// specify the image path here
  document.getElementById("app_uploadText").style.backgroundSize = "auto 100%";
  document.getElementById("app_uploadText").style.backgroundRepeat = "no-repeat";
  document.getElementById("app_uploadText").style.backgroundPosition = "center";
  document.getElementById("app_uploadText").innerHTML = "";
}
</script>