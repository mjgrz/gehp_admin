<div class="card card-primary" style="box-shadow: none; border: 1px solid #67a2b2;">
  <div class="card-body">
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <div id="uploadDiv">
          <?php include("./loading.php"); ?>
            <input type="text" id="uploadVid2" name="uploadVid2" class="file_multi_video" hidden>
            <iframe id="video" width="100%" height="100%" src="" title="" frameborder="0" onload="onMyFrameLoad(this)"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
            </iframe>
            <script>
              function onMyFrameLoad() {
                document.getElementById("loader").style.display = "none";
              };
            </script>
          </div>
          <a style="font-size:large;" id="buttonid" type="button" data-toggle="modal" data-target="#videoLink" class="btn btn-info">  
            <i class="fa fa-upload"></i>&nbsp; Attach Video Link
          </a>
          <?php include("./video_link.php"); ?>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="../../plugins/jquery/jquery.min.js"></script>
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../dist/js/adminlte.min.js"></script>
<script src="../../dist/js/demo.js"></script>   
  
<script>
$(function () {
  bsCustomFileInput.init();
});
</script>

<style>
  #uploadDiv {
    width: 100%;
    height: 456px;
    border: 2px dashed #67a2b2;
    border-radius: 5px;
  }

  #uploadVid2 {
    position: absolute;
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    outline: none;
    opacity: 0;
  }

  #buttonid {
    margin-top: 10px;
    width: 100%;
    background: #67a2b2;
    border: 1px solid #67a2b2;
  }

  #buttonid:hover {
    background: #46949e;
    border: 1px solid #46949e;
  }

  #video_pro {
    border-radius: 5px;
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    outline: none;
  }

  #video_pro:hover {
    cursor: pointer;
  }

</style>