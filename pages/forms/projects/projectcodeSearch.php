<?php
include('../../../../../dbcon.php'); 
$PJ_CODEsearch = $_POST["projectCode"];

if(!empty($_POST["projectCode"])) {
  if (is_numeric($PJ_CODEsearch) == true){
    try{
      $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $projcodequery = "SELECT PJ_CODE FROM project_info WHERE PJ_CODE = :PJ_CODEsearch";
      $sthprojcodequery = $dbh->prepare($projcodequery);
      $sthprojcodequery->bindParam(':PJ_CODEsearch', $PJ_CODEsearch);
      
      if($sthprojcodequery->execute()) {
        if($sthprojcodequery->rowCount() > 0) {
          echo "<span style='color:red'> (Project Code already exists.)</span>";
          echo "<script>document.getElementById('projectCode').style.border = '2px solid red';</script>";
          echo "<script>document.getElementById('submitform').disabled = true;</script>";
        }
        elseif($_POST["projectCode"] == ".") {
          echo "<span style='color:red'> (Invalid input.)</span>";
        }
        else {
          echo "<span style='color:green'> (Project Code available.)</span>";
          echo "<script>document.getElementById('projectCode').style.border = '';</script>";
          echo "<script>document.getElementById('submitform').disabled = false;</script>";
        }
      }
      else {
        echo "<span style='color:red'> (Invalid input.)</span>";
      }
    }
    catch(PDOException $e){
      error_log('PDOException - ' . $e->getMessage(), 0);
      http_response_code(500);
      die('Error establishing connection with database');
    }
  } 
  else{
  http_response_code(400);
  die('Error processing bad or malformed request');
  }
}

else{
  echo "<script>document.getElementById('projectCode').style.border = '';</script>";
  echo "<script>document.getElementById('submitform').disabled = false;</script>";
}
?>