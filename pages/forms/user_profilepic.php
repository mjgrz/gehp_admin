<?php
session_start();
$username_session = $_POST["usernameprofile"];
$officeID_session = $_POST["officeidprofile"];
$userid_session = $_POST["useridprofile"];
$target_dir = "uploads/users/";
$target_file1 = $target_dir . basename("$username_session"."-"."$officeID_session".".png");
$uploadOk1 = 1;
$filename1 = "$username_session"."-"."$officeID_session".".png";
$imageFileType1 = strtolower(pathinfo($target_file1,PATHINFO_EXTENSION));
include("../../../../dbcon.php");

  if(isset($_POST["submit"])) {
    
    $check1 = getimagesize($_FILES["profilepic"]["tmp_name"]);
    if($check1 !== false) {
      $_SESSION["info"] = "File is an image - " . $check1["mime"] . ".";
      $uploadOk1 = 1;
    } else {
      $_SESSION["error"] = "File is not an image.";
      $uploadOk1 = 0;
    }
  }

  if ($_FILES["main_imageUpload"]["size"] <= 5000000) {
    if (file_exists($target_file1)) {
      unlink($target_file1);
    }
  }

  if ($_FILES["profilepic"]["size"] > 5000000) {
    $_SESSION["error"] = "Sorry, your file is too large.";
    $uploadOk1 = 0;
  }

  if($imageFileType1 != "jpg" && $imageFileType1 != "png" && $imageFileType1 != "jpeg"
  && $imageFileType1 != "gif" && $imageFileType1 != "") {
    $_SESSION["error"] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk1 = 0;
  }

  if ($uploadOk1 == 0) {
    header('Location: ../forms/users_profile.php');
  } else {
    if(is_numeric($userid_session)){
      try{
        $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sthsqlprofpicdata = [
          ':filename1' => $filename1,
          ':userid_session' => $userid_session
        ];
        $sqlprofpic = "UPDATE users SET user_image = :filename1 WHERE user_id = :userid_session";
        $sthsqlprofpic = $dbh->prepare($sqlprofpic);
        
        if (move_uploaded_file($_FILES["profilepic"]["tmp_name"], $target_file1) && $sthsqlprofpic->execute($sthsqlprofpicdata)) {
            $_SESSION["status"] = "Your new profile picture has been saved successfully.";
            header('Location: ../forms/users_profile.php');
            $dbh = null;
        } 
        else 
        {
            $_SESSION["error"] = "Sorry, your new profile picture has not been saved successfully.";
            header('Location: ../forms/users_profile.php');
            $dbh = null;
        }
      }
      catch(PDOException $e){
          error_log('PDOException - ' . $e->getMessage(), 0);
          http_response_code(500);
          die('Error establishing connection with database');
      }
    }
    else{
      http_response_code(400);
      die('Error processing bad or malformed request');
    }
  }
?>



