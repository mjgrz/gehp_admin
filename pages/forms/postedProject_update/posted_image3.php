<?php
$target_dir = "uploads/postedProject/";
$target_file3 = $target_dir . basename("postedImage3.png");
$uploadOk3 = 1;
$filename3 = "postedImage3.png";
$imageFileType3 = strtolower(pathinfo($target_file3,PATHINFO_EXTENSION));

  if(isset($_POST["submit"])) {
    
    $check3 = getimagesize($_FILES["postedImage3"]["tmp_name"]);
    if($check3 !== false) {
      echo "File is an image - " . $check3["mime"] . ".";
      $uploadOk3 = 1;
    } else {
      echo "File is not an image.";
      $uploadOk3 = 0;
    }
  }

  if ($_FILES["postedImage3"]["size"] > 50000000) {
    echo "Sorry, your file is too large.";
    $uploadOk3 = 0;
  }

  if($imageFileType3 != "jpg" && $imageFileType3 != "png" && $imageFileType3 != "jpeg"
  && $imageFileType3 != "gif" && $imageFileType3 != "") {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk3 = 0;
  }

  if ($uploadOk3 == 0) {
    echo "Sorry, your file was not uploaded.";
  } else {

    if (move_uploaded_file($_FILES["postedImage3"]["tmp_name"], $target_file3)) {
    } 
    else 
    {
    }
  }
?>