<?php
$target_dir = "uploads/postedProject/";
$target_file1 = $target_dir . basename("postedImage1.png");
$uploadOk1 = 1;
$filename1 = "postedImage1.png";
$imageFileType1 = strtolower(pathinfo($target_file1,PATHINFO_EXTENSION));

  if(isset($_POST["submit"])) {
    
    $check1 = getimagesize($_FILES["postedImage1"]["tmp_name"]);
    if($check1 !== false) {
      echo "File is an image - " . $check1["mime"] . ".";
      $uploadOk1 = 1;
    } else {
      echo "File is not an image.";
      $uploadOk1 = 0;
    }
  }

  if ($_FILES["postedImage1"]["size"] > 50000000) {
    echo "Sorry, your file is too large.";
    $uploadOk1 = 0;
  }

  if($imageFileType1 != "jpg" && $imageFileType1 != "png" && $imageFileType1 != "jpeg"
  && $imageFileType1 != "gif" && $imageFileType1 != "") {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk1 = 0;
  }

  if ($uploadOk1 == 0) {
    echo "Sorry, your file was not uploaded.";
  } else {

    if (move_uploaded_file($_FILES["postedImage1"]["tmp_name"], $target_file1)) {
    } 
    else 
    {
    }
  }
?>