<?php
include('login_session.php');
$oldpasscheck = $_POST["old_password"];
if(!empty($_POST["old_password"])) {
  try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $oldpasscheckquery = "SELECT username FROM users WHERE username=:login_session";
    $stholdpasscheckquery = $dbh->prepare($oldpasscheckquery);
    $stholdpasscheckquery->bindParam(':login_session', $login_session);
    $stholdpasscheckquery->execute();
    $user_count=$stholdpasscheckquery->rowCount();

    $oldpasscheckfind = "SELECT user_password FROM users WHERE username=:login_session";
    $stholdpasscheckfind = $dbh->prepare($oldpasscheckfind);
    $stholdpasscheckfind->bindParam(':login_session', $login_session);
    $stholdpasscheckfind->execute();
    $stholdpasscheckfind->setFetchMode(PDO::FETCH_ASSOC); 

    while ($usersrow = $stholdpasscheckfind->fetch(PDO::FETCH_ASSOC)) {
          $user_password = $usersrow["user_password"];
    }
    if(password_verify($oldpasscheck, $user_password)) {
      if($user_count>0) {
        echo "<script>document.getElementById('old_password').style.border = '';</script>";
        echo "<script>document.getElementById('new_password').disabled = false;</script>";
        echo "<script>document.getElementById('confirm_password').disabled = false;</script>";
        $dbh = null;
      }
      else {
        echo "<script>document.getElementById('old_password').style.border = '2px solid red';</script>";
        echo "<script>document.getElementById('submitform').disabled = true;</script>";
        echo "<script>document.getElementById('new_password').disabled = true;</script>";
        echo "<script>document.getElementById('confirm_password').disabled = true;</script>";
        $dbh = null;
      }
    }
    else {
      echo "<span style='color:red'> (Wrong Password.)</span>";
      echo "<script>document.getElementById('old_password').style.border = '2px solid red';</script>";
      echo "<script>document.getElementById('submitform').disabled = true;</script>";
      echo "<script>document.getElementById('new_password').disabled = true;</script>";
      echo "<script>document.getElementById('confirm_password').disabled = true;</script>";
      $dbh = null;
    }
  }
  catch(PDOException $e){
      error_log('PDOException - ' . $e->getMessage(), 0);
      http_response_code(500);
      die('Error establishing connection with database');
  }
}
else{
  echo "<script>document.getElementById('old_password').style.border = '';</script>";
  echo "<script>document.getElementById('submitform').disabled = true;</script>";
  echo "<script>document.getElementById('new_password').disabled = true;</script>";
  echo "<script>document.getElementById('confirm_password').disabled = true;</script>";
}
?>