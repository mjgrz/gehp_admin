<?php
$disqualified_id = $_GET['disqualified_id'];
include("../../../../dbcon.php");
session_start();
?>
<?php
if (is_numeric($disqualified_id) == true){
  try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $disqualified_select_modal = "SELECT remarks FROM applications WHERE APP_ID = :disqualified_id";
    $sthdisqualified_select_modal = $dbh->prepare($disqualified_select_modal);
    $sthdisqualified_select_modal->bindParam(':disqualified_id', $disqualified_id);
    $sthdisqualified_select_modal->execute();
    $sthdisqualified_select_modal->setFetchMode(PDO::FETCH_ASSOC); 
    while ($disqualified_modal = $sthdisqualified_select_modal->fetch(PDO::FETCH_ASSOC)) {
      $disqualified = $disqualified_modal["remarks"];
    }
    $dbh = null;
  }
  catch(PDOException $e){
    error_log('PDOException - ' . $e->getMessage(), 0);
    http_response_code(500);
    die('Error establishing connection with database');
  }
} 
else{
http_response_code(400);
die('Error processing bad or malformed request');
}
?>
<div class="modal-header" style="background: #67a2b2;">
  <h4 class="modal-title" style="color: white;">Disqualified</h4>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span style="color: white;" aria-hidden="true">&times;</span>
  </button>
</div>
<!--Submit Form for adding remarks-->
<form method="POST" action="registration_process.php">
  <div class="modal-body" style="background: #f6f6f6;">
    <div class="col-sm-12">
      <div class="col-lg-12">
        <div class="form-group">
          <p style="font-size: 20px;">Remarks</p>
          <input type="text" name="APP_ID" id="APP_ID" value="<?php echo $disqualified_id ?>" hidden>
          <input type="text" name="username" id="username" value="<?php echo $_SESSION['login_user']?>" hidden>
          <input type="text" name="status" id="status" value="Disqualified" hidden>
          <textarea class="form-control" style="resize: none;" name="remarks" value="<?php echo $disqualified ?>" rows="6"
          oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" required width="100%"><?php echo $disqualified ?></textarea>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer justify-content-right">
    <button type="submit" onclick="SubDisGov()" class="btn btn-default color-red" style="font-size:large; width:auto;"><i class="fa fa-paper-plane"></i>&nbsp; Submit</button>
  </div>
</form>

<script>
  function SubDisGov() {
var remarks = document.getElementById("remarks").value;
// Returns successful data submission message when the entered information is stored in database.
var dataString = 'remarks=' + remarks;
  if (remarks == '') {
  } 
  else {
    $.ajax({
    type: "POST",
    url: "registration_process.php",
    data: dataString,
    cache: false,
    success: function(html) {
    }
    });
  }
}
</script>