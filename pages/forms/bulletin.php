<!DOCTYPE html>
<html lang="en">
<head>
<?php
include('login_session.php');
if($login_officeID != "h60000000" || $login_officeID != "h50000000"){
  if($login_officeID == "h60000000"){
    
  }
  else if($login_officeID == "h50000000"){
    
  }
  else{
    header("location:../../index.php");
  }
}
error_reporting(E_ERROR | E_PARSE);
?>
<link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
<link rel="stylesheet" href="../../plugins/toastr/toastr.min.css">
<link rel="stylesheet" href="../../dist/css/adminlte.min.css">
<script src="../../plugins/jquery/jquery.min.js"></script>
<script src="../../plugins/toastr/toastr.min.js"></script>
<?php
if (isset($_SESSION["status"]) && $_SESSION["status"] !='') {
  $msg = $_SESSION["status"];
  echo "<span><script type='text/javascript'>toastr.success('$msg')</script></span>";
  unset($_SESSION["status"]);
}

if (isset($_SESSION["error"]) && $_SESSION["error"] !='') {
  $msg = $_SESSION["error"];
  echo "<span><script type='text/javascript'>toastr.error('$msg')</script></span>";
  unset($_SESSION["error"]);
}

if (isset($_SESSION["info"]) && $_SESSION["info"] !='') {
  $msg = $_SESSION["info"];
  echo "<span><script type='text/javascript'>toastr.info('$msg')</script></span>";
  unset($_SESSION["info"]);
}
else {}
?>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>NHA | Government Employee's Housing Program</title>
<link rel="shortcut icon" type="image/png" href="../../dist/img/nha_logo_circle.png">

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
<link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
<link rel="stylesheet" href="../../dist/css/adminlte.min.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="font-weight: bold;">
    <?php include("header.php"); ?>
  </nav>

  <aside class="main-sidebar sidebar-dark-primary elevation-4" style="background: #002d32; position: fixed;">
    <a href="../../index.php" class="brand-link">
      <img src="../../dist/img/nha_logo_circle.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"><b>NHA | GEHP</b></span>
    </a>

    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <?php
          $target_dir = "uploads/users/";
          $target_file = $target_dir . basename("$login_image");
          if(file_exists($target_file)){
            echo "<img src='uploads/users/$login_image' class='img-circle elevation-3' alt='User Image' style='height: 33px;'>";
          } 
          else {
            echo "<img src='uploads/users/user.png' width='150px'>";
          }
          ?>
        </div>
        <div class="info">
          <a href="users_profile.php" class="d-block"><?php echo $_SESSION['login_user'] ?></a>
        </div>
      </div>

      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
          <li class="nav-item">
            <a href="../../index.php" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-briefcase"></i>
              <p>
                Applicants
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <input type="text" class="form-control" id="sessionempty" name="sessionempty" value="" placeholder="Search" hidden>
              <li class="nav-item">
                <a href="../forms/registration.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Government Employees</p>
                </a>
              </li>
              <li class="nav-item" <?php echo $deploy;?>>
                <a href="../forms/registration_Uni.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Uniformed Personnel</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/registration_OFW.php" onclick="sessionempty()" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>OFW</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item" <?php echo $project_hide ?>>
            <a href="../forms/project_view.php" onclick="sessionempty()" class="nav-link">
              <i class="nav-icon fas fa-building"></i>
              <p>
                Projects
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $user_hide ?>>
            <a href="../forms/users.php" onclick="sessionempty()" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Users
              </p>
            </a>
          </li>

          <li class="nav-item" <?php echo $element_hide ?>>
            <a href="../forms/report_gen.php" class="nav-link">
              <i class="nav-icon fas fa-scroll"></i>
              <p>
                Reports
              </p>
            </a>
          </li>

          <li class="nav-item menu-open" <?php echo $pages_hide ?>>
            <a href="#" class="nav-link active" style="background: #67a2b2;">
              <i class="nav-icon fas fa-clone"></i>
              <p>
                Pages
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              
              <li class="nav-item">
                <a href="../forms/home.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Home</p>
                </a>
              </li>
              <li class="nav-item" hidden>
                <a href="../forms/howToApply.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>How To Apply</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/about.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>About</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../forms/banner.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Banner</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="../forms/bulletin.php" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Bulletin</p>
                </a>
              </li>
            </ul>
          </li>
          
          <li class="nav-item" <?php echo $inv_hide;?>>
            <a href="../forms/inventory.php" class="nav-link">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                Inventory
              </p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>
  <script>
    function sessionempty() {
      $("#loaderIcon").show();
          jQuery.ajax({
          url: "session_clear.php",
          data: {'sessionempty': $("#sessionempty").val()},
          type: "POST",
          success:function(data){
              $("#").html(data);
              $("#loaderIcon").hide();
          },
          error:function (){
          }
      });
    }
  </script>
  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Bulletin</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div>
        </div>
      </div>
    </section>

    <form method="POST" action="bulletin_delete.php" id="deleteForm"><input type="text" name="URL" value="<?php echo $_SERVER['REQUEST_URI']; ?>" hidden>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6">
            <div class="card card-primary" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
              <div class="card-header" style="background: #67a2b2;">
                <h3 class="card-title">Posts</h3>
              </div>
              <div class="card-body">
                <div>
                  <button type="button" data-toggle="modal" data-target="#delete" class="btn btn-default color-red-auto" $element_hide><i class="fa fa-trash"></i>&nbsp; Delete</button>
                  <?php include("delete_modal.php"); ?>
                </div>
                <table id="bulletin" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th style="white-space: nowrap; " class="project-actions text-center"><input id="chk_all" name="chk_all" type="checkbox"></th>
                    <th>Category</th>
                    <th>Title</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                    include("../../../../dbcon.php");
                    try{
                      $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                      $bulletinSelect = "SELECT * FROM `bulletin` ORDER BY bulletin_id ASC";
                      $sthbulletinSelect = $dbh->prepare($bulletinSelect);
                      $sthbulletinSelect->execute();
                      $sthbulletinSelect->setFetchMode(PDO::FETCH_ASSOC); 
                      $idrowcount = $sthbulletinSelect->rowCount() + 1;
                      if($sthbulletinSelect->rowCount() > 0){
                        while ($bulletinRow = $sthbulletinSelect->fetch(PDO::FETCH_ASSOC)) {
                          $modalID = $bulletinRow['bulletin_id'];
                          $modalCat = $bulletinRow['bulletin_category'];
                          $modalTitle = $bulletinRow['bulletin_title'];
                          $modalDesc = $bulletinRow['bulletin_desc'];
                          $modalImage = $bulletinRow['bulletin_image'];
                          $modalUploader = $bulletinRow['uploader'];
                          $modalUpload = $bulletinRow['date_upload'];
                      ?>
                        <tr>
                          <td class="project-actions text-center" style="vertical-align: middle;"><input name="check[]" form="deleteForm" type="checkbox" class='chkbox' value="<?php echo $bulletinRow['bulletin_id']; ?>"/></td>
                          </form>
                          <td> <?php echo $bulletinRow['bulletin_category'] ?> </td>
                          <td> <?php echo $bulletinRow['bulletin_title'] ?> </td>
                          <td class="project-actions text-center">
                            <a style="font-size:large;" id="tooltip" type="button" href="bulletin_edit.php?id=<?php echo $modalID ?>" class="btn btn-default btn-sm color-cyan-dark">
                              <i class="fas fa-edit">
                              </i>
                              <span class="tooltiptext">Edit</span>
                            </a>
                            <a style="font-size:large;" id="tooltip" type="button" href="" data-toggle="modal" data-target="#modal<?php echo $modalID ?>"  class="btn btn-default btn-sm color-blue-dark">
                              <i class="fa fa-eye">
                              </i>
                              <span class="tooltiptext">View</span>
                            </a>
                          </td>
                        </tr>
                        <?php    
                        include("bulletin_modal.php");
                        }
                        $dbh = null; 
                      }
                      else
                      {
                        $dbh = null;
                      }
                    }
                    catch(PDOException $e){
                      error_log('PDOException - ' . $e->getMessage(), 0);
                      http_response_code(500);
                      die('Error establishing connection with database');
                    }
                  ?>
                  </tbody>
                </table>
                <?php include("delete_script.php");?>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card card-primary" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
              <!--Submit Form for adding bulletin content-->
              <form method="POST" action="bulletin_add.php" enctype="multipart/form-data">
                <div class="card-header" style="background: #67a2b2;">
                  <h3 class="card-title" style="color: white;">Additional Post</h3>
                </div>
                <div class="card-body">
                  <div class="col-sm-12">
                    <div class="row">
                      <div class="form-group col-sm-6">
                        <label>Date</label>
                        <input type="date" class="form-control" name="date" id="date" required>
                      </div> 
                      <div class="form-group col-sm-6">
                        <label>Category</label>
                        <select class="form-control" name="category" id="category" required>
                          <option value="">- Select Category -</option>
                          <option value="Announcement">Announcement</option>
                          <option value="News">News</option>
                        </select>
                      </div>
                    </div>  
                    <div class="form-group">
                      <label for="bulletin_title">Title</label>
                      <input type="text" class="form-control" id="uploaderadd" name="uploaderadd" value="<?php echo $_SESSION['login_user'] ?>" hidden>
                      <input type="text" name="idrowcount" value="<?php echo $idrowcount ?>" hidden>
                      <input type="text" class="form-control" name="bulletin_title"
                      oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" id="bulletin_title" required>
                    </div>
                  </div>

                  <div class="col-sm-12">
                    <div class="row">
                      <div class="form-group col-sm-6">
                        <label for="bulletin_desc">Description</label>
                        <textarea class="form-control" name="bulletin_desc" id="bulletin_desc"
                      oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" style="resize: none;" rows="15" required></textarea>
                      </div>
                      <div class="col-sm-6">
                        <label for="uploadDiv">Image</label>
                        <div class="form-group col-sm-12" id="uploadDiv">
                          <input type="file" id="bulletin_image" name="bulletin_image" title="Click in this area to upload an image." required onchange="preview()">
                          <p id="bulletinimage">Click in this area to upload an image.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card-footer">
                  <div class="project-actions text-right">
                    <button style="width:auto; font-size:large;" type="submit" class="btn btn-default color-cyan-dark"><i class="fa fa-paper-plane"></i>&nbsp; Submit</button>
                  </div>
                </div>
              </form>
              <!--End of Submit Form for adding bulletin content-->
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <footer class="main-footer">
    <?php include('footer.php'); ?>
  </footer>
  <aside class="control-sidebar control-sidebar-dark">
  </aside>
</div>
<script src="../../plugins/jquery/jquery.min.js"></script>
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script src="../../dist/js/adminlte.min.js"></script>
<script src="../../dist/js/demo.js"></script>
<script>
  $(function () {
    $("#bulletin").DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": false,
      "info": false,
      "autoWidth": false,
      "responsive": true,
    }).buttons().container().appendTo('#bulletin_wrapper .col-md-6:eq(0)');
  });
</script>
<script>
function preview() {
  var urlimage = URL.createObjectURL(event.target.files[0]);
  document.getElementById("bulletinimage").style.backgroundImage = "url("+urlimage+")";// specify the image path here
  document.getElementById("bulletinimage").style.backgroundSize = "auto 100%";
  document.getElementById("bulletinimage").style.backgroundRepeat = "no-repeat";
  document.getElementById("bulletinimage").style.backgroundPosition = "center";
  document.getElementById("bulletinimage").innerHTML = "";
}
</script>
</body>
</html>
<style>
  #uploadDiv {
    width: 100%;
    height: 374px;
    border: 2px dashed #67a2b2;
    border-radius: 5px;
  }
  #bulletin_image{
    position: absolute;
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    outline: none;
    opacity: 0;
  }
  #bulletin_image:hover{
    cursor: pointer;
  }
  #bulletinimage{
    text-align: center;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .content {
    -webkit-animation-duration: 0.7s;
    animation-duration: 0.7s;
  }
  @-webkit-keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  @keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  .content {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
  }
  td {
    background: #f5f5f5;
  }
  input:hover, select:hover, textarea:hover {
    border: 1px solid #67a2b2;
  }
</style>
<script language="javascript">
  var message="This function is not allowed here.";
  function clickIE4(){
        if (event.button==2){
            alert(message);
            return false;
        }
  }
  function clickNS4(e){
      if (document.layers||document.getElementById&&!document.all){
              if (e.which==2||e.which==3){
                        alert(message);
                        return false;
              }
      }
  }
  if (document.layers){
        document.captureEvents(Event.MOUSEDOWN);
        document.onmousedown=clickNS4;
  }
  else if (document.all&&!document.getElementById){
        document.onmousedown=clickIE4;
  }
  document.oncontextmenu=new Function("alert(message);return false;")
</script>
<?php include('custom_btn.php'); ?>