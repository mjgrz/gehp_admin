<?php
include("../../../../dbcon.php"); 
session_start();
$dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$PJTXT = $_POST['prjTxt'];
$FLRNUM = substr($_POST['floorNum'],6);
$LISTTXT = $_POST['listTxt'];

$queryslct =    "SELECT * FROM project_info WHERE project_id = '$PJTXT'";
$sthpj = $dbh->prepare($queryslct);
$sthpj->execute();
$sthpj->setFetchMode(PDO::FETCH_ASSOC); 
while ($rowpj = $sthpj->fetch(PDO::FETCH_ASSOC)) {
    $STR = $rowpj['PJ_NAME'];
    $PJ_NAME = str_replace(" ", "-", "$STR");
}

$target_dir = "uploads/inventory/";
$target_file = $target_dir . basename("$PJ_NAME"."-"."$FLRNUM".".jpg");
$uploadOk = 1;
$filename = "$PJ_NAME"."-"."$FLRNUM".".jpg";
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

if (file_exists($target_file)) {
    unlink($target_file);
}

if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "") {
    $_SESSION["error"] = "Sorry, only JPG, JPEG, and PNG files are allowed.";
    $uploadOk = 0;
}

if ($uploadOk == 0) {
    header('Location: ../forms/inventory.php');
}

else {
    try {
        $querysbmt = "INSERT INTO inventory (project_id, floor_num, slot_list, slot_map) 
                    VALUES ('$PJTXT', '$FLRNUM', '$LISTTXT', '$filename')";
        $sthinv = $dbh->prepare($querysbmt);
        if(move_uploaded_file($_FILES["builtMapFile"]["tmp_name"], $target_file) && $sthinv->execute()){
            $_SESSION["status"] = "Your map inventory has been saved successfully.";
            header('Location: ../forms/inventory.php');
            $dbh = null;
        }
        else{
            header('Location: ../forms/inventory.php');
            $_SESSION["error"] = "Sorry, unable to add map inventory.";
            $dbh = null;
        }
    }
    catch (PDOException $e) {
        error_log('PDOException - ' . $e->getMessage(), 0);
        http_response_code(500);
        die('Sorry, someting went wrong.');
    }
}
?>