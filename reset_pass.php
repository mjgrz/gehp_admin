<!DOCTYPE html>
<html lang="en">
<head>
  
<link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
<link rel="stylesheet" href="plugins/toastr/toastr.min.css">
<link rel="stylesheet" href="dist/css/adminlte.min.css">

<script src="plugins/jquery/jquery.min.js"></script>
<script src="plugins/toastr/toastr.min.js"></script>

<?php
session_start();
if (isset($_SESSION["Invalid"]) && $_SESSION["Invalid"] !='') {
  $msg = $_SESSION['Invalid'];
  echo "<span><script type='text/javascript'>toastr.error('$msg')</script></span>";
  unset($_SESSION['Invalid']);
}
else {

}
?>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>NHA | Government Employee's Housing Program</title>
  <link rel="shortcut icon" type="image/png" href="dist/img/nha_logo_circle.png">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="plugins/toastr/toastr.min.css">
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
</head>
<body class="hold-transition login-page" id="body">
  <img src="dist/img/banner-003.jpg" id="BG">
<div class="login-box" style="float: left;" >
    <div class="card card-outline card-info" id="card">
      <div class="card-header text-center">
        <img src="dist/img/nha-logo.png" style="width: 16%" class="mb-3"> <span> <img src="dist/img/logo_balai.png" style="width: 20%;"></span>
        <br>
        <span><p class="h4"><b>NHA</b> | <a href="../index.php" id="homelink" >Government Employees' Housing Program</a></p></span>

        <style>
          #homelink:hover {
            color: #67a2b2;
            cursor: pointer;
          }
          #homelink {
            color: black;
          }
        </style>

      </div>
      <!--Reset Password Form-->
      <div class="card-body login-card-body">
        <p class="login-box-msg">You forgot your password? Here you can easily retrieve a new password.</p>
        <form action="reset_post.php" method="POST">
          <div class="input-group mb-3">
            <input type="text" class="form-control" id="username" name="username"
             oninput="this.value = this.value.replace(/[^0-9 a-z A-Z@_.]/g, '').replace(/(\*)\./g, '$1');" placeholder="Username" autofocus>
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fa fa-user"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="email" class="form-control" id="email" name="email" placeholder="Email">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-envelope"></span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <button type="submit" class="btn btn-info btn-block">Request new password</button>
            </div>
          </div>
        </form>
        
        <p class="mt-3 mb-1">
          <a href="login.php">Login</a>
        </p>
      </div>
      <!--End of Reset Password Form-->
    </div>
</div>

<script src="plugins/jquery/jquery.min.js"></script>
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="dist/js/adminlte.min.js"></script>
<script src="plugins/toastr/toastr.min.js"></script>
<script src="dist/js/demo.js"></script>

</body>
</html>

<style>
  #BG {
    opacity: 0.3;
    position: absolute;
    left: 0;
    top: 0;
    flex-shrink: 0;
    width: 100%;
    height: 100%;
    object-fit: cover;
    filter: blur(8px);
    -webkit-filter: blur(8px);
  }
  #body {
    overflow: hidden;
    position: relative;
  }
  .login-box {
    position: relative;
  }
  #card {
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    -webkit-animation-duration: 0.7s;
    animation-duration: 0.7s;
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
  }
  @-webkit-keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }
  @keyframes fadeIn {
    0% {opacity: 0;}
    100% {opacity: 1;}
  }

</style>
<script language="javascript">
           var message="This function is not allowed here.";
           function clickIE4(){
                 if (event.button==2){
                     alert(message);
                     return false;
                 }
           }
           function clickNS4(e){
                if (document.layers||document.getElementById&&!document.all){
                        if (e.which==2||e.which==3){
                                  alert(message);
                                  return false;
                        }
                }
           }
           if (document.layers){
                 document.captureEvents(Event.MOUSEDOWN);
                 document.onmousedown=clickNS4;
           }
           else if (document.all&&!document.getElementById){
                 document.onmousedown=clickIE4;
           }
           document.oncontextmenu=new Function("alert(message);return false;")
</script>