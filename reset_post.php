<?php  
   if(!empty($_POST['username']) && !empty($_POST['email'])) {  
      $user=$_POST['username'];  
      $email=$_POST['email']; 
      include("../../dbcon.php");

      //Reset Password(Random)
      $alphabet = 'abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ1234567890';
      $randompass = array();
      $alphaLength = strlen($alphabet) - 1;
      for ($i = 0; $i < 8; $i++) {
         $n = rand(0, $alphaLength);
         $randompass[] = $alphabet[$n];
      }
      $newpass = implode($randompass);
      $hashed_password = password_hash($newpass, PASSWORD_DEFAULT);
      //End of Reset Password(Random)

      try{
         $resetdata = [
            ':user' => $user,
            ':email' => $email
         ];

         $newpassdata = [
            ':hashed_password' => $hashed_password,
            ':user' => $user,
            ':email' => $email
         ];

         $dbhuser = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
         $dbhuser->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
         $query = "SELECT username, email FROM users WHERE username = :user and email = :email";
         $sthuser = $dbhuser->prepare($query);
         $sthuser->execute($resetdata);
         $sthuser->setFetchMode(PDO::FETCH_ASSOC);
         $numrows=$sthuser->rowCount();  

         $sqlreset = "UPDATE users SET user_password = :hashed_password WHERE username = :user and email = :email";
         $sthsqlreset = $dbhuser->prepare($sqlreset);

         if($numrows!=0) {  
            while ($row = $sthuser->fetch(PDO::FETCH_ASSOC))  {   
            $dbuser = $row['username'];  
            $dbemail = $row['email'];  
            }
            
            $emails = $dbemail;
            $app_pass = "pnlvwkmioeeexaqd";
            $email_pass = "nh@_g3hp&";
            $email_sender = "gehp.nha@gmail.com";

            require_once ("phpmailer/class.phpmailer.php");
            $Correo = new PHPMailer();
            $Correo->IsSMTP();
            $Correo->SMTPAuth = true;
            $Correo->SMTPSecure = "tls";
            $Correo->Host = "smtp.gmail.com";
            $Correo->Port = 587;
            $Correo->Username = "$email_sender";
            $Correo->Password = "$app_pass";
            $Correo->SetFrom("$email_sender","De Yo");
            $Correo->FromName = "GEHP System Support";
            $Correo->AddAddress("$emails");
            $Correo->Subject = "New Password Request";
            $Correo->Body = "<P>We would like to inform you that your password was successfully changed.</P>
                             <P><B>New password: ".$newpass."</B></P>";
            $Correo->IsHTML (true);
            if ($Correo->Send() && $sthsqlreset->execute($newpassdata)) {
               session_start(); 
               header("Location: login.php");  
               $_SESSION['Success'] = "Your new password has been sent to your email account! please check your inbox."; 
               $dbhuser = null; 
            }
            else {
               session_start(); 
               header("Location: login.php");  
               $_SESSION['Invalid'] = "Unable to send your request!"; 
               $dbhuser = null; 
            }
         }
         else {
            session_start(); 
            header("Location: login.php");  
            $_SESSION['Invalid'] = "Unable to send your request! username or email might be wrong."; 
            $dbhuser = null; 
         }  
      }
      catch(PDOException $e){
         error_log('PDOException - ' . $e->getMessage(), 0);
         http_response_code(500);
         die('Error establishing connection with database');
      }
   } 
   else {  
      session_start(); 
      header("Location: login.php");  
      $_SESSION['Invalid'] = "Invalid Entry!"; 
   }   
?>