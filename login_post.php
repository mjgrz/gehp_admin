<?php  
   if(!empty($_POST['login_username']) && !empty($_POST['login_password'])) {  
      $user=$_POST['login_username'];  
      $pass=$_POST['login_password']; 
      include("../../dbcon.php");
      try{
      $dbhuser = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
      $dbhuser->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $query = "SELECT username, office, user_password, user_id, email FROM users WHERE username = :user AND status = 'Active'";
      $sthuser = $dbhuser->prepare($query);
      $sthuser->bindParam(':user', $user);
      $sthuser->execute();
      $sthuser->setFetchMode(PDO::FETCH_ASSOC);
      $numrows=$sthuser->rowCount();  
         if($numrows!=0) {  
            while ($row = $sthuser->fetch(PDO::FETCH_ASSOC))  {   
            $dbusername = $row['username'];  
            $dboffice = $row['office'];  
            $dbpassword = $row['user_password'];  
            $dbuser_id = $row["user_id"];
            $dbemail = $row["email"];
            }

            if($user == $dbusername && password_verify($pass, $dbpassword)) {  
               session_start();  
               $_SESSION['office'] = $dboffice; 
               $_SESSION['login_user'] = $user; 
               $_SESSION['login_id'] = $dbuser_id; 
               $_SESSION['entry'] = "Hello $dbusername, Welcome to GEHP System!";
               date_default_timezone_set("Asia/Hong_Kong");
               $username = $_SESSION['login_user'];
               $time = date("h:i a");
               $date = date('F d, Y', strtotime(date("Y-m-d")));
               $datetime = $date." ".$time;

               $auditdata = [
                  ':activity' => "Logged In",
                  ':username' => $_SESSION['login_user'],
                  ':datetime' => $datetime
               ];
               $audit = "INSERT INTO audit_trail (activity, username, date) VALUES (:activity, :username, :datetime)";
               $sthaudit = $dbhuser->prepare($audit);
               $sthaudit->execute($auditdata);
               
               $countAT = "SELECT COUNT(*) as CountTodel FROM audit_trail";
               $sthcountAT = $dbhuser->prepare($countAT);
               $sthcountAT->execute();
               $sthcountAT->setFetchMode(PDO::FETCH_ASSOC);
               while ($rowCountAT = $sthcountAT->fetch(PDO::FETCH_ASSOC))  {   
                  $CountATres = $rowCountAT['CountTodel']; 
               }   
               if($CountATres > 700){
                  $delList = $CountATres - 700;
                  $reduceAT = "DELETE FROM audit_trail ORDER BY id ASC LIMIT ".$delList."";
                  $sthreduceAT = $dbhuser->prepare($reduceAT);
                  $sthreduceAT->execute();
               }
               else{ }

               header("Location: index.php");
               $dbhuser = null; 
            }
            else {
               session_start(); 
               header("Location: login.php");  
               $_SESSION['Invalid'] = "Wrong Username or Password!"; 
               $dbhuser = null; 
	         }  
         }
         else {
            session_start(); 
            header("Location: login.php");  
            $_SESSION['Invalid'] = "Wrong Username or Password!"; 
         }  
      }
      catch(PDOException $e){
         error_log('PDOException - ' . $e->getMessage(), 0);
         http_response_code(500);
         die('Error establishing connection with database');
      }
   } 
   else {  
      session_start(); 
      header("Location: login.php");  
      $_SESSION['Invalid'] = "Invalid Entry!"; 
   }   
?>